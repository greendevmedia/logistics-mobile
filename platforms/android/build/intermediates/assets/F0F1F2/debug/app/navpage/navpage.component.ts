import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { GlobalService } from "../services/global/global.service";
import { RouterExtensions } from "nativescript-angular/router";
import {
    getString,
	setString, 
	clear
} from "application-settings";
import { TabView, SelectedIndexChangedEventData, TabViewItem } from "ui/tab-view";
import { TNSFontIconService } from 'nativescript-ngx-fonticon';


@Component({
	moduleId: module.id,
	selector: 'app-navpage',
	templateUrl: './navpage.component.html',
	styleUrls: ['./navpage.component.scss']
})
export class NavpageComponent implements OnInit {

	private isSelected:boolean = true;


	constructor(private page: Page, private globalService: GlobalService, private routerExtensions: RouterExtensions, private fonticon: TNSFontIconService ) { }

	ngOnInit() { this.page.actionBarHidden = true; }

	public setTitleAndLnkForTC(){
		this.globalService.isTrailersListVisible = true;
		this.globalService.carAddTitle = "Karta techniczna - krok 1 z 3 ";
		this.globalService.carAddLink = (["/technical-card-1"]);
		this.routerExtensions.navigate(["/car-add"]);
	}

	public setTitleAndLnkForRefuellingAdd(){
		this.globalService.isTrailersListVisible = false;
		this.globalService.carAddTitle = "Dodaj tankowanie - krok 1 z 2";
		this.globalService.carAddLink = (["/refuelling-add"]);
		this.routerExtensions.navigate(["/car-add"]);
	}

	public setTitleAndLnkForAdditionalPayment(){
		this.globalService.isTrailersListVisible = false;
		this.globalService.carAddTitle = "Dodaj opłatę - krok 1 z 3";
		this.globalService.carAddLink = (["additional-payment1"]);
		this.routerExtensions.navigate(["/car-add"]);
	}

	public setTitleAndLnkForRepair(){
		this.globalService.isTrailersListVisible = true;
		this.globalService.carAddTitle = "Dodaj serwis - krok 1 z 3";
		this.globalService.carAddLink = (["repair-1"]);
		this.routerExtensions.navigate(["/car-add"]);
	}

	public logout(){
		this.globalService.loginFormVisibility = true;
		console.log(getString("username"));
		console.log(getString("pin"));
		setString("pin", "new");
		setString("username", "new");
		clear();
		this.routerExtensions.navigate(["/home"]);
	}

}
