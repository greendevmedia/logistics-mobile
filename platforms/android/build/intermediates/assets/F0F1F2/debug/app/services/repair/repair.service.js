"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var global_service_1 = require("../global/global.service");
var user_service_1 = require("../user/user.service");
var email = require("nativescript-email");
var json_service_1 = require("../json/json.service");
var date_service_1 = require("../date/date.service");
var router_1 = require("nativescript-angular/router");
var url_service_1 = require("../urlService/url.service");
var RepairService = (function () {
    function RepairService(globalService, userService, http, jsonService, dateService, routerExtensions, urlService) {
        this.globalService = globalService;
        this.userService = userService;
        this.http = http;
        this.jsonService = jsonService;
        this.dateService = dateService;
        this.routerExtensions = routerExtensions;
        this.urlService = urlService;
        // public repairsUrl: string = "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/driver/api/v1/repairs/";
        this.repairsUrl = this.urlService.apiUrl + "/driver/api/v1/repairs/";
        this.activityType = this.globalService.repairActivityList[1];
    }
    RepairService.prototype.addService = function (repairJSON) {
        this.headers = new http_1.Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
        this.options = new http_1.RequestOptions({ headers: this.headers });
        return this.http.post(this.repairsUrl, repairJSON, this.options);
    };
    RepairService.prototype.sendService = function () {
        var _this = this;
        if (this.jsonService.trailerName == null) {
            this.trailerName = "";
        }
        else {
            this.trailerName = "Przyczepa: " + this.jsonService.trailerName;
        }
        if (this.jsonService.carName == null) {
            this.carName = "";
        }
        else {
            this.carName = "Pojazd: " + this.jsonService.carName;
        }
        if (this.activityType == "DELIVERY") {
            this.activityTypeForEmail = "Oddanie na serwis";
        }
        else {
            this.activityTypeForEmail = "Odbiór po serwisie";
        }
        this.composeOptions = {
            to: ['pawel@plewinski-logistics.com', 'adam.b.ciechanski@wp.pl'],
            subject: this.activityTypeForEmail + " - " + this.carName + ' ' + this.trailerName,
            body: "Kierowca: " + this.userService.driverName + "\n" + this.carName + " " + this.trailerName + "\nData serwisu: " + this.dateService.dateFormatForPost() + "\nStan licznika: " + this.counter + "\nOpis serwisu: " + this.repairType + "\nAdres warsztatu: " + this.carShopAddress + "\nUwagi do pojazdu: " + this.vehicalCheck + "\nCena: " + this.price
        };
        email.available().then(function (available) {
            if (available) {
                email.compose(_this.composeOptions).then(function (result) {
                    _this.routerExtensions.navigate(["/navpage"]);
                });
            }
        }).catch(function (error) { return console.error(error); });
    };
    RepairService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [global_service_1.GlobalService, user_service_1.UserService, http_1.Http, json_service_1.JsonService, date_service_1.DateService, router_1.RouterExtensions, url_service_1.UrlService])
    ], RepairService);
    return RepairService;
}());
exports.RepairService = RepairService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVwYWlyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJyZXBhaXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyxzQ0FBd0U7QUFDeEUsMkRBQXlEO0FBQ3pELHFEQUFtRDtBQUNuRCwwQ0FBNEM7QUFDNUMscURBQW1EO0FBQ25ELHFEQUFtRDtBQUNuRCxzREFBK0Q7QUFDL0QseURBQXVEO0FBR3ZEO0lBbUJFLHVCQUFvQixhQUE0QixFQUFVLFdBQXdCLEVBQVUsSUFBVSxFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSxnQkFBa0MsRUFBVSxVQUFzQjtRQUFsTyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVUsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQWpCdFAsaUhBQWlIO1FBQzFHLGVBQVUsR0FBVyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyx5QkFBeUIsQ0FBQztRQUd4RSxpQkFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFhMkwsQ0FBQztJQUVwUCxrQ0FBVSxHQUFqQixVQUFrQixVQUFVO1FBQzFCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxjQUFPLENBQUMsRUFBRSxlQUFlLEVBQUUsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLGNBQWMsRUFBRSxpQ0FBaUMsRUFBRSxDQUFDLENBQUM7UUFDdkssSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLHFCQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDN0QsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBRU0sbUNBQVcsR0FBbEI7UUFBQSxpQkFvQ0M7UUFsQ0MsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUN4QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQztRQUNsRSxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsT0FBTyxHQUFHLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQztRQUN2RCxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxtQkFBbUIsQ0FBQTtRQUNqRCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsb0JBQW9CLEdBQUcsb0JBQW9CLENBQUE7UUFDbEQsQ0FBQztRQUlELElBQUksQ0FBQyxjQUFjLEdBQUc7WUFDcEIsRUFBRSxFQUFFLENBQUMsK0JBQStCLEVBQUUseUJBQXlCLENBQUM7WUFDaEUsT0FBTyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVc7WUFDbEYsSUFBSSxFQUFFLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixFQUFFLEdBQUcsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLHFCQUFxQixHQUFHLElBQUksQ0FBQyxjQUFjLEdBQUcsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFlBQVksR0FBRyxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUs7U0FDN1YsQ0FBQTtRQUdELEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxTQUFTO1lBQzlCLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTTtvQkFDNUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUE7Z0JBQzlDLENBQUMsQ0FBQyxDQUFBO1lBQ0osQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQXBCLENBQW9CLENBQUMsQ0FBQTtJQUN6QyxDQUFDO0lBL0RVLGFBQWE7UUFEekIsaUJBQVUsRUFBRTt5Q0FvQndCLDhCQUFhLEVBQXVCLDBCQUFXLEVBQWdCLFdBQUksRUFBdUIsMEJBQVcsRUFBdUIsMEJBQVcsRUFBNEIseUJBQWdCLEVBQXNCLHdCQUFVO09BbkIzTyxhQUFhLENBaUV6QjtJQUFELG9CQUFDO0NBQUEsQUFqRUQsSUFpRUM7QUFqRVksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwLCBIZWFkZXJzLCBSZXNwb25zZSwgUmVxdWVzdE9wdGlvbnMgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xuaW1wb3J0IHsgR2xvYmFsU2VydmljZSB9IGZyb20gJy4uL2dsb2JhbC9nbG9iYWwuc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCAqIGFzIGVtYWlsIGZyb20gXCJuYXRpdmVzY3JpcHQtZW1haWxcIjtcbmltcG9ydCB7IEpzb25TZXJ2aWNlIH0gZnJvbSAnLi4vanNvbi9qc29uLnNlcnZpY2UnO1xuaW1wb3J0IHsgRGF0ZVNlcnZpY2UgfSBmcm9tICcuLi9kYXRlL2RhdGUuc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgVXJsU2VydmljZSB9IGZyb20gJy4uL3VybFNlcnZpY2UvdXJsLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUmVwYWlyU2VydmljZSB7XG5cbiAgLy8gcHVibGljIHJlcGFpcnNVcmw6IHN0cmluZyA9IFwiaHR0cDovL2FwaWxvZ2lzdGljcy5ncmVlbmRldi5pbi9sb2dpc3RpY3MtMC4wLjEtU05BUFNIT1QvZHJpdmVyL2FwaS92MS9yZXBhaXJzL1wiO1xuICBwdWJsaWMgcmVwYWlyc1VybDogc3RyaW5nID0gdGhpcy51cmxTZXJ2aWNlLmFwaVVybCArIFwiL2RyaXZlci9hcGkvdjEvcmVwYWlycy9cIjtcbiAgcHVibGljIHJlcGFpckpTT05mb3JQT1NUO1xuICBwdWJsaWMgY291bnRlcjtcbiAgcHVibGljIGFjdGl2aXR5VHlwZSA9IHRoaXMuZ2xvYmFsU2VydmljZS5yZXBhaXJBY3Rpdml0eUxpc3RbMV07XG4gIHB1YmxpYyBhY3Rpdml0eVR5cGVGb3JFbWFpbDtcbiAgcHVibGljIHJlcGFpclR5cGU7XG4gIHB1YmxpYyBwcmljZTtcbiAgcHVibGljIGNhclNob3BBZGRyZXNzO1xuICBwdWJsaWMgaGVhZGVycztcbiAgcHVibGljIG9wdGlvbnM7XG4gIHB1YmxpYyBjb21wb3NlT3B0aW9uczogZW1haWwuQ29tcG9zZU9wdGlvbnM7XG4gIHB1YmxpYyBjYXJOYW1lO1xuICBwdWJsaWMgdHJhaWxlck5hbWU7XG4gIHB1YmxpYyB2ZWhpY2FsQ2hlY2s7XG5cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBwcml2YXRlIGh0dHA6IEh0dHAsIHByaXZhdGUganNvblNlcnZpY2U6IEpzb25TZXJ2aWNlLCBwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHVybFNlcnZpY2U6IFVybFNlcnZpY2UpIHsgfVxuXG4gIHB1YmxpYyBhZGRTZXJ2aWNlKHJlcGFpckpTT04pIHtcbiAgICB0aGlzLmhlYWRlcnMgPSBuZXcgSGVhZGVycyh7IFwiQXV0aG9yaXphdGlvblwiOiBcImJlYXJlciBcIiArIHRoaXMudXNlclNlcnZpY2UudXNlckFwaUFjY2Vzc0RhdGEuanNvbigpLmFjY2Vzc190b2tlbiwgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uOyBjaGFyc2V0PXV0Zi04XCIgfSk7XG4gICAgdGhpcy5vcHRpb25zID0gbmV3IFJlcXVlc3RPcHRpb25zKHsgaGVhZGVyczogdGhpcy5oZWFkZXJzIH0pO1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLnJlcGFpcnNVcmwsIHJlcGFpckpTT04sIHRoaXMub3B0aW9ucyk7XG4gIH1cblxuICBwdWJsaWMgc2VuZFNlcnZpY2UoKSB7XG5cbiAgICBpZiAodGhpcy5qc29uU2VydmljZS50cmFpbGVyTmFtZSA9PSBudWxsKSB7XG4gICAgICB0aGlzLnRyYWlsZXJOYW1lID0gXCJcIjtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy50cmFpbGVyTmFtZSA9IFwiUHJ6eWN6ZXBhOiBcIiArIHRoaXMuanNvblNlcnZpY2UudHJhaWxlck5hbWU7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuanNvblNlcnZpY2UuY2FyTmFtZSA9PSBudWxsKSB7XG4gICAgICB0aGlzLmNhck5hbWUgPSBcIlwiO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmNhck5hbWUgPSBcIlBvamF6ZDogXCIgKyB0aGlzLmpzb25TZXJ2aWNlLmNhck5hbWU7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuYWN0aXZpdHlUeXBlID09IFwiREVMSVZFUllcIikge1xuICAgICAgdGhpcy5hY3Rpdml0eVR5cGVGb3JFbWFpbCA9IFwiT2RkYW5pZSBuYSBzZXJ3aXNcIlxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmFjdGl2aXR5VHlwZUZvckVtYWlsID0gXCJPZGJpw7NyIHBvIHNlcndpc2llXCJcbiAgICB9XG5cblxuXG4gICAgdGhpcy5jb21wb3NlT3B0aW9ucyA9IHtcbiAgICAgIHRvOiBbJ3Bhd2VsQHBsZXdpbnNraS1sb2dpc3RpY3MuY29tJywgJ2FkYW0uYi5jaWVjaGFuc2tpQHdwLnBsJ10sXG4gICAgICBzdWJqZWN0OiB0aGlzLmFjdGl2aXR5VHlwZUZvckVtYWlsICsgXCIgLSBcIiArIHRoaXMuY2FyTmFtZSArICcgJyArIHRoaXMudHJhaWxlck5hbWUsXG4gICAgICBib2R5OiBcIktpZXJvd2NhOiBcIiArIHRoaXMudXNlclNlcnZpY2UuZHJpdmVyTmFtZSArIFwiXFxuXCIgKyB0aGlzLmNhck5hbWUgKyBcIiBcIiArIHRoaXMudHJhaWxlck5hbWUgKyBcIlxcbkRhdGEgc2Vyd2lzdTogXCIgKyB0aGlzLmRhdGVTZXJ2aWNlLmRhdGVGb3JtYXRGb3JQb3N0KCkgKyBcIlxcblN0YW4gbGljem5pa2E6IFwiICsgdGhpcy5jb3VudGVyICsgXCJcXG5PcGlzIHNlcndpc3U6IFwiICsgdGhpcy5yZXBhaXJUeXBlICsgXCJcXG5BZHJlcyB3YXJzenRhdHU6IFwiICsgdGhpcy5jYXJTaG9wQWRkcmVzcyArIFwiXFxuVXdhZ2kgZG8gcG9qYXpkdTogXCIgKyB0aGlzLnZlaGljYWxDaGVjayArIFwiXFxuQ2VuYTogXCIgKyB0aGlzLnByaWNlXG4gICAgfVxuXG5cbiAgICBlbWFpbC5hdmFpbGFibGUoKS50aGVuKGF2YWlsYWJsZSA9PiB7XG4gICAgICBpZiAoYXZhaWxhYmxlKSB7XG4gICAgICAgIGVtYWlsLmNvbXBvc2UodGhpcy5jb21wb3NlT3B0aW9ucykudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvbmF2cGFnZVwiXSlcbiAgICAgICAgfSlcbiAgICAgIH1cbiAgICB9KS5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSlcbiAgfVxuXG59XG4iXX0=