import { Component, OnInit } from '@angular/core';
import { DateService } from '../services/date/date.service';
import { Page } from 'tns-core-modules/ui/page/page';
import { RepairService } from '../services/repair/repair.service';
import { GlobalService } from '../services/global/global.service';
import { Switch } from "ui/switch";

@Component({
  moduleId: module.id,
  selector: 'app-repair-1',
  templateUrl: './repair-1.component.html',
  styleUrls: ['./repair-1.component.scss']
})
export class Repair1Component implements OnInit {

  public switchState = "Odbiór po serwisie";

  constructor(private dateService: DateService, private page: Page, private repairService: RepairService, private globalService: GlobalService) { }

  ngOnInit() { 
    this.repairService.activityType = this.globalService.repairActivityList[1];
    this.repairService.repairType = null;
    this.repairService.counter = null;
    this.repairService.price = null;
    this.repairService.repairType = null;
    this.repairService.carShopAddress = null;
    this.repairService.vehicalCheck = null;
    this.switchState = "Odbiór po serwisie";
  }

  public onChecked(args){
    let firstSwitch = <Switch>args.object;
    if(firstSwitch.checked) {
        this.switchState = "Oddanie na serwis";
        this.repairService.activityType = this.globalService.repairActivityList[0];
        console.log(this.repairService.activityType);
    } else {
        this.switchState = "Odbiór po serwisie";
        this.repairService.activityType = this.globalService.repairActivityList[1];
        console.log(this.repairService.activityType);
    }
  }

}

