import { Injectable } from '@angular/core';
import * as email from "nativescript-email";
import { JsonService } from "../json/json.service";
import { DateService } from "../date/date.service";
import { UserService } from '../user/user.service';
import { RouterExtensions } from "nativescript-angular/router";
import * as imagepicker from "nativescript-imagepicker";
import * as imagesourceModule from "image-source";
// var fs = require("file-system");


@Injectable()
export class TechnicalCardService {

	public counterStart;
	public counterStop;
	public oilLevel = "";
	public purity = "";
	public lighting = "";
	public tires = "";
	public suspension = "";
	public engineAndAccessories = "";
	public carBody = "";
	public composeOptions: email.ComposeOptions;
	public imagesPaths = [];
	public arrayOfAttachments = [];
	public trailerName;
	public carName;



	constructor(private jsonService: JsonService, private dateService: DateService, private userService: UserService, private routerExtensions: RouterExtensions) { }



	public getPicture() {
		var that = this;
		let context = imagepicker.create({
			mode: "multiple" // use "multiple" for multiple selection
		});
		context
			.authorize()
			.then(function () {
				return context.present();
			})
			.then(function (selection) {

				selection.forEach(function (selected) {
					that.imagesPaths.push(selected.fileUri);
					console.log(selected.fileUri);
					console.log(that.imagesPaths.length);

				});

			}).catch(function (e) {
				// process error
			});
	}

	public sendTechnicalCard() {
		if (this.imagesPaths.length > 0) {
			this.createMultipleAttachemnts(this.imagesPaths, this.arrayOfAttachments);
		}

		if (this.jsonService.trailerName == null) {
			this.trailerName = ""
		} else {
			this.trailerName = "Przyczepa: " + this.jsonService.trailerName;
		}

		if (this.jsonService.carName == null) {
			this.carName = ""
		} else {
			this.carName = "Pojazd: " + this.jsonService.carName;
		}

		this.composeOptions = {
			to: ['pawel@plewinski-logistics.com'],
			subject: 'Karta techniczna - ' + this.carName + " " + this.trailerName,
			body: "Kierowca: " + this.userService.driverName + "\n" + this.carName + " " + this.trailerName + "\nData: " + this.dateService.refuelingDate.day + "." + this.dateService.refuelingDate.month + "." + this.dateService.refuelingDate.year + "\nPoczątkowy stan licznika: " + this.counterStart + "\nKońcowy stan licznika: " + this.counterStop + "\nStan oleju: " + this.oilLevel + "\nCzystość: " + this.purity + "\nOświetlenie: " + this.lighting + "\nOpony: " + this.tires + "\nZawieszenie: " +  this.suspension + "\nSilnik oraz osprzęt: " + this.engineAndAccessories + "\nKaroseria: " + this.carBody
		}
		if (this.imagesPaths.length > 0) {
			this.composeOptions.attachments = this.arrayOfAttachments
		}


		email.available().then(available => {
			if (available) {
				email.compose(this.composeOptions).then(result => {
					this.routerExtensions.navigate(["/navpage"])
				})
			}
		}).catch(error => console.error(error))
	}

	public createMultipleAttachemnts(arrayOfLinks, arrayOfAttachments) {
		let num = 0;
		arrayOfLinks.forEach(element => {
			num = num + 1;
			arrayOfAttachments.push(
				{
					fileName: num + 'kt.png',
					path: element,
					mimeType: 'image/png'
				})

		});
	}



}
