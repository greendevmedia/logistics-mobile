import { Injectable } from '@angular/core';

@Injectable()
export class UrlService {

  // public apiUrl: string = "https://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT";
  public apiUrl: string = "https://api.plewinski-logistics.com/logistics-0.0.1-SNAPSHOT";
  // public apiUrl: string = "http://10.0.2.2:8080";

  constructor() { }

}
