"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var refueling_service_1 = require("../services/refueling/refueling.service");
var list_service_1 = require("../services/list/list.service");
var json_service_1 = require("../services/json/json.service");
var NavigationComponent = (function () {
    function NavigationComponent(page, refuelingService, listService, jsonService) {
        this.page = page;
        this.refuelingService = refuelingService;
        this.listService = listService;
        this.jsonService = jsonService;
    }
    NavigationComponent.prototype.ngOnInit = function () {
        this.data = this.refuelingService.refuelingJSONforPOST;
        this.page.actionBarHidden = true;
    };
    NavigationComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-navigation',
            templateUrl: './navigation.component.html',
            styleUrls: ['./navigation.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, refueling_service_1.RefuelingService, list_service_1.ListService, json_service_1.JsonService])
    ], NavigationComponent);
    return NavigationComponent;
}());
exports.NavigationComponent = NavigationComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuYXZpZ2F0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxnQ0FBK0I7QUFDL0IsNkVBQTJFO0FBQzNFLDhEQUE0RDtBQUM1RCw4REFBNEQ7QUFTNUQ7SUFHQyw2QkFBcUIsSUFBVSxFQUFVLGdCQUFrQyxFQUFVLFdBQXdCLEVBQVUsV0FBd0I7UUFBMUgsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBQUssQ0FBQztJQUVySixzQ0FBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUM7UUFDdkQsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBR2pDLENBQUM7SUFWVSxtQkFBbUI7UUFOL0IsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsZ0JBQWdCO1lBQzFCLFdBQVcsRUFBRSw2QkFBNkI7WUFDMUMsU0FBUyxFQUFFLENBQUMsNkJBQTZCLENBQUM7U0FDMUMsQ0FBQzt5Q0FJMEIsV0FBSSxFQUE0QixvQ0FBZ0IsRUFBdUIsMEJBQVcsRUFBdUIsMEJBQVc7T0FIbkksbUJBQW1CLENBYy9CO0lBQUQsMEJBQUM7Q0FBQSxBQWRELElBY0M7QUFkWSxrREFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XG5pbXBvcnQgeyBSZWZ1ZWxpbmdTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3JlZnVlbGluZy9yZWZ1ZWxpbmcuc2VydmljZVwiO1xuaW1wb3J0IHsgTGlzdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9saXN0L2xpc3Quc2VydmljZSc7XG5pbXBvcnQgeyBKc29uU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2pzb24vanNvbi5zZXJ2aWNlJztcblxuXG5AQ29tcG9uZW50KHtcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0c2VsZWN0b3I6ICdhcHAtbmF2aWdhdGlvbicsXG5cdHRlbXBsYXRlVXJsOiAnLi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vbmF2aWdhdGlvbi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIE5hdmlnYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG5cdHB1YmxpYyBkYXRhO1xuXHRjb25zdHJ1Y3RvciggcHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIHJlZnVlbGluZ1NlcnZpY2U6IFJlZnVlbGluZ1NlcnZpY2UsIHByaXZhdGUgbGlzdFNlcnZpY2U6IExpc3RTZXJ2aWNlLCBwcml2YXRlIGpzb25TZXJ2aWNlOiBKc29uU2VydmljZSApIHsgfVxuXG5cdG5nT25Jbml0KCkge1xuXHRcdHRoaXMuZGF0YSA9IHRoaXMucmVmdWVsaW5nU2VydmljZS5yZWZ1ZWxpbmdKU09OZm9yUE9TVDtcblx0XHR0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcblx0XG5cdFx0XG5cdCB9XG5cblxuXG59XG4iXX0=