"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var refueling_service_1 = require("../services/refueling/refueling.service");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../services/user/user.service");
var page_1 = require("ui/page");
var RefuelingListComponent = (function () {
    function RefuelingListComponent(refuelingService, routerExtensions, userService, page) {
        this.refuelingService = refuelingService;
        this.routerExtensions = routerExtensions;
        this.userService = userService;
        this.page = page;
    }
    RefuelingListComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.getRefuelings();
    };
    RefuelingListComponent.prototype.getRefuelings = function () {
        var _this = this;
        this.refuelingService.getRefuelings().subscribe(
        // (res) => { this.refuelingsJSONfromGET = res.json().content, this.refuelingsJSONfromGET = this.refuelingsJSONfromGET.reverse().filter(refueling => refueling.driverName === this.userService.driverName) },
        function (res) { _this.refuelingsJSONfromGET = res.json().content, _this.refuelingsJSONfromGET = _this.refuelingsJSONfromGET.filter(function (refueling) { return refueling.driverName === _this.userService.driverName; }); }, function (err) { alert("Błąd przy załadowaniu listy tankowań"); });
    };
    RefuelingListComponent.prototype.getRefuelingsById = function (id) {
        var _this = this;
        this.refuelingService.getRefuelingsById(id).subscribe(function (res) { _this.refuelingService.refuelingJSONfromGETbyID = res.json(), _this.routerExtensions.navigate(["/refueling-edit"]); }, function (err) { alert("Błąd przy załadowaniu tankowania do edycji"); });
    };
    RefuelingListComponent.prototype.showRefuelingDetails = function (refueling) {
        var note;
        if (refueling.warningNote == null) {
            note = "Brak";
        }
        else {
            note = refueling.warningNote;
        }
        alert({
            title: "Szczegóły tankowania",
            message: "Imię kierowcy: " + refueling.driverName + "\nStan licznika w km: " + refueling.counter + "\nIlość paliwa w litrach: " + refueling.quantity + "\nCena za litr paliwa: " + refueling.price + "\nTyp płatności: " + refueling.paymentType + "\nWaluta: " + refueling.currency.currencyCode + "\nAuto: " + refueling.car.name +
                "\nCena za litr adBlue: " + refueling.adBluePrice + "\nIlość adBlue w litrach: " + refueling.adBlueQuantity +
                "\nDodatkowe uwagi: " + note,
            okButtonText: "OK"
        });
    };
    RefuelingListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-refueling-list',
            templateUrl: './refueling-list.component.html',
            styleUrls: ['./refueling-list.component.scss']
        }),
        __metadata("design:paramtypes", [refueling_service_1.RefuelingService, router_1.RouterExtensions, user_service_1.UserService, page_1.Page])
    ], RefuelingListComponent);
    return RefuelingListComponent;
}());
exports.RefuelingListComponent = RefuelingListComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmdWVsaW5nLWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVmdWVsaW5nLWxpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELDZFQUEyRTtBQUMzRSxzREFBK0Q7QUFDL0QsOERBQTREO0FBQzVELGdDQUErQjtBQVEvQjtJQUlDLGdDQUFvQixnQkFBa0MsRUFBVSxnQkFBa0MsRUFBVSxXQUF3QixFQUFVLElBQVU7UUFBcEkscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07SUFBSSxDQUFDO0lBRTdKLHlDQUFRLEdBQVI7UUFDVyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRyw4Q0FBYSxHQUFwQjtRQUFBLGlCQU1JO1FBTEcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxDQUFDLFNBQVM7UUFDM0MsNk1BQTZNO1FBQzdNLFVBQUMsR0FBRyxJQUFPLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLFVBQUEsU0FBUyxJQUFJLE9BQUEsU0FBUyxDQUFDLFVBQVUsS0FBSyxLQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBcEQsQ0FBb0QsQ0FBQyxDQUFBLENBQUMsQ0FBQyxFQUMvTCxVQUFDLEdBQUcsSUFBTyxLQUFLLENBQUMsc0NBQXNDLENBQUMsQ0FBQSxDQUFDLENBQUMsQ0FDN0QsQ0FBQTtJQUNMLENBQUM7SUFHTSxrREFBaUIsR0FBeEIsVUFBeUIsRUFBRTtRQUEzQixpQkFLQztRQUpHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQ2pELFVBQUMsR0FBRyxJQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsR0FBRyxHQUFHLENBQUMsSUFBSSxFQUFFLEVBQUUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUcsQ0FBQSxDQUFDLENBQUMsRUFDL0gsVUFBQyxHQUFHLElBQU8sS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQ25FLENBQUE7SUFDTCxDQUFDO0lBRU0scURBQW9CLEdBQTNCLFVBQTRCLFNBQVM7UUFDakMsSUFBSSxJQUFJLENBQUM7UUFDVCxFQUFFLENBQUEsQ0FBQyxTQUFTLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDOUIsSUFBSSxHQUFHLE1BQU0sQ0FBQztRQUNsQixDQUFDO1FBQUEsSUFBSSxDQUFBLENBQUM7WUFDRixJQUFJLEdBQUcsU0FBUyxDQUFDLFdBQVcsQ0FBQTtRQUNoQyxDQUFDO1FBQ0QsS0FBSyxDQUFDO1lBQ0YsS0FBSyxFQUFFLHNCQUFzQjtZQUM3QixPQUFPLEVBQUUsaUJBQWlCLEdBQUcsU0FBUyxDQUFDLFVBQVUsR0FBRyx3QkFBd0IsR0FBRyxTQUFTLENBQUMsT0FBTyxHQUFHLDRCQUE0QixHQUFHLFNBQVMsQ0FBQyxRQUFRLEdBQUcseUJBQXlCLEdBQUcsU0FBUyxDQUFDLEtBQUssR0FBRyxtQkFBbUIsR0FBRyxTQUFTLENBQUMsV0FBVyxHQUFHLFlBQVksR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLFlBQVksR0FBRyxVQUFVLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJO2dCQUNuVSx5QkFBeUIsR0FBRyxTQUFTLENBQUMsV0FBVyxHQUFHLDRCQUE0QixHQUFHLFNBQVMsQ0FBQyxjQUFjO2dCQUMzRyxxQkFBcUIsR0FBRyxJQUFJO1lBQzVCLFlBQVksRUFBRSxJQUFJO1NBQ3JCLENBQUMsQ0FBQTtJQUNOLENBQUM7SUF6Q1Esc0JBQXNCO1FBTmxDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixXQUFXLEVBQUUsaUNBQWlDO1lBQzlDLFNBQVMsRUFBRSxDQUFDLGlDQUFpQyxDQUFDO1NBQzlDLENBQUM7eUNBS3FDLG9DQUFnQixFQUE0Qix5QkFBZ0IsRUFBdUIsMEJBQVcsRUFBZ0IsV0FBSTtPQUo1SSxzQkFBc0IsQ0EyQ2xDO0lBQUQsNkJBQUM7Q0FBQSxBQTNDRCxJQTJDQztBQTNDWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUmVmdWVsaW5nU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy9yZWZ1ZWxpbmcvcmVmdWVsaW5nLnNlcnZpY2VcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuXG5AQ29tcG9uZW50KHtcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0c2VsZWN0b3I6ICdhcHAtcmVmdWVsaW5nLWxpc3QnLFxuXHR0ZW1wbGF0ZVVybDogJy4vcmVmdWVsaW5nLWxpc3QuY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9yZWZ1ZWxpbmctbGlzdC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFJlZnVlbGluZ0xpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG5cdHByaXZhdGUgcmVmdWVsaW5nc0pTT05mcm9tR0VUO1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcmVmdWVsaW5nU2VydmljZTogUmVmdWVsaW5nU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJpdmF0ZSBwYWdlOiBQYWdlKSB7IH1cblxuXHRuZ09uSW5pdCgpIHsgXG4gICAgICAgICAgICB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuZ2V0UmVmdWVsaW5ncygpO1xuICAgIH1cblxuXHRwdWJsaWMgZ2V0UmVmdWVsaW5ncygpIHtcbiAgICAgICAgdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLmdldFJlZnVlbGluZ3MoKS5zdWJzY3JpYmUoXG4gICAgICAgICAgICAvLyAocmVzKSA9PiB7IHRoaXMucmVmdWVsaW5nc0pTT05mcm9tR0VUID0gcmVzLmpzb24oKS5jb250ZW50LCB0aGlzLnJlZnVlbGluZ3NKU09OZnJvbUdFVCA9IHRoaXMucmVmdWVsaW5nc0pTT05mcm9tR0VULnJldmVyc2UoKS5maWx0ZXIocmVmdWVsaW5nID0+IHJlZnVlbGluZy5kcml2ZXJOYW1lID09PSB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUpIH0sXG4gICAgICAgICAgICAocmVzKSA9PiB7IHRoaXMucmVmdWVsaW5nc0pTT05mcm9tR0VUID0gcmVzLmpzb24oKS5jb250ZW50LCB0aGlzLnJlZnVlbGluZ3NKU09OZnJvbUdFVCA9IHRoaXMucmVmdWVsaW5nc0pTT05mcm9tR0VULmZpbHRlcihyZWZ1ZWxpbmcgPT4gcmVmdWVsaW5nLmRyaXZlck5hbWUgPT09IHRoaXMudXNlclNlcnZpY2UuZHJpdmVyTmFtZSkgfSxcbiAgICAgICAgICAgIChlcnIpID0+IHsgYWxlcnQoXCJCxYLEhWQgcHJ6eSB6YcWCYWRvd2FuaXUgbGlzdHkgdGFua293YcWEXCIpIH1cbiAgICAgICAgKVxuICAgIH1cblxuXG4gICAgcHVibGljIGdldFJlZnVlbGluZ3NCeUlkKGlkKSB7XG4gICAgICAgIHRoaXMucmVmdWVsaW5nU2VydmljZS5nZXRSZWZ1ZWxpbmdzQnlJZChpZCkuc3Vic2NyaWJlKFxuICAgICAgICAgICAgKHJlcykgPT4geyB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZyb21HRVRieUlEID0gcmVzLmpzb24oKSwgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9yZWZ1ZWxpbmctZWRpdFwiXSwgKSB9LFxuICAgICAgICAgICAgKGVycikgPT4geyBhbGVydChcIkLFgsSFZCBwcnp5IHphxYJhZG93YW5pdSB0YW5rb3dhbmlhIGRvIGVkeWNqaVwiKSB9XG4gICAgICAgIClcbiAgICB9XG5cbiAgICBwdWJsaWMgc2hvd1JlZnVlbGluZ0RldGFpbHMocmVmdWVsaW5nKSB7XG4gICAgICAgIHZhciBub3RlO1xuICAgICAgICBpZihyZWZ1ZWxpbmcud2FybmluZ05vdGUgPT0gbnVsbCl7XG4gICAgICAgICAgICBub3RlID0gXCJCcmFrXCI7XG4gICAgICAgIH1lbHNle1xuICAgICAgICAgICAgbm90ZSA9IHJlZnVlbGluZy53YXJuaW5nTm90ZVxuICAgICAgICB9XG4gICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICAgIHRpdGxlOiBcIlN6Y3plZ8OzxYJ5IHRhbmtvd2FuaWFcIixcbiAgICAgICAgICAgIG1lc3NhZ2U6IFwiSW1pxJkga2llcm93Y3k6IFwiICsgcmVmdWVsaW5nLmRyaXZlck5hbWUgKyBcIlxcblN0YW4gbGljem5pa2EgdyBrbTogXCIgKyByZWZ1ZWxpbmcuY291bnRlciArIFwiXFxuSWxvxZvEhyBwYWxpd2EgdyBsaXRyYWNoOiBcIiArIHJlZnVlbGluZy5xdWFudGl0eSArIFwiXFxuQ2VuYSB6YSBsaXRyIHBhbGl3YTogXCIgKyByZWZ1ZWxpbmcucHJpY2UgKyBcIlxcblR5cCBwxYJhdG5vxZtjaTogXCIgKyByZWZ1ZWxpbmcucGF5bWVudFR5cGUgKyBcIlxcbldhbHV0YTogXCIgKyByZWZ1ZWxpbmcuY3VycmVuY3kuY3VycmVuY3lDb2RlICsgXCJcXG5BdXRvOiBcIiArIHJlZnVlbGluZy5jYXIubmFtZSArXG4gICAgICAgICAgICBcIlxcbkNlbmEgemEgbGl0ciBhZEJsdWU6IFwiICsgcmVmdWVsaW5nLmFkQmx1ZVByaWNlICsgXCJcXG5JbG/Fm8SHIGFkQmx1ZSB3IGxpdHJhY2g6IFwiICsgcmVmdWVsaW5nLmFkQmx1ZVF1YW50aXR5ICtcbiAgICAgICAgICAgIFwiXFxuRG9kYXRrb3dlIHV3YWdpOiBcIiArIG5vdGUsXG4gICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICB9KVxuICAgIH1cblxufVxuIl19