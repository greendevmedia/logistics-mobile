"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var user_service_1 = require("../user/user.service");
var url_service_1 = require("../urlService/url.service");
var RefuelingService = (function () {
    function RefuelingService(http, userService, urlService) {
        this.http = http;
        this.userService = userService;
        this.urlService = urlService;
        // public refuelingsUrl: string = "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/driver/api/v1/refuelings/";
        this.refuelingsUrl = this.urlService.apiUrl + "/driver/api/v1/refuelings/";
    }
    RefuelingService.prototype.addRefueling = function (refuelingJSON) {
        this.headers = new http_1.Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
        this.options = new http_1.RequestOptions({ headers: this.headers });
        return this.http.post(this.refuelingsUrl, refuelingJSON, this.options);
    };
    RefuelingService.prototype.getRefuelings = function () {
        this.headers = new http_1.Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
        this.options = new http_1.RequestOptions({ headers: this.headers });
        // return this.http.get(this.refuelingsUrl + "/refuelings-limit?page=2&size=100", this.options);
        return this.http.get(this.refuelingsUrl + "/refuelings-limit?page=0&size=100&sort=refuelingDate,desc", this.options);
    };
    RefuelingService.prototype.getRefuelingsById = function (id) {
        this.headers = new http_1.Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
        this.options = new http_1.RequestOptions({ headers: this.headers });
        return this.http.get(this.refuelingsUrl + id, this.options);
    };
    RefuelingService.prototype.updateRefueling = function (refuelingJSON) {
        this.headers = new http_1.Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
        this.options = new http_1.RequestOptions({ headers: this.headers });
        return this.http.put(this.refuelingsUrl, refuelingJSON, this.options);
    };
    RefuelingService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, user_service_1.UserService, url_service_1.UrlService])
    ], RefuelingService);
    return RefuelingService;
}());
exports.RefuelingService = RefuelingService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmdWVsaW5nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJyZWZ1ZWxpbmcuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyxzQ0FBd0U7QUFDeEUscURBQW1EO0FBQ25ELHlEQUF1RDtBQUd2RDtJQVNDLDBCQUFvQixJQUFVLEVBQVUsV0FBd0IsRUFBVSxVQUFzQjtRQUE1RSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBUGhHLHVIQUF1SDtRQUNoSCxrQkFBYSxHQUFXLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLDRCQUE0QixDQUFDO0lBTWUsQ0FBQztJQUU5Rix1Q0FBWSxHQUFuQixVQUFvQixhQUFhO1FBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxjQUFPLENBQUMsRUFBRSxlQUFlLEVBQUUsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLGNBQWMsRUFBRSxpQ0FBaUMsRUFBRSxDQUFDLENBQUM7UUFDdkssSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLHFCQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDN0QsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRU0sd0NBQWEsR0FBcEI7UUFDQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksY0FBTyxDQUFDLEVBQUUsZUFBZSxFQUFFLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7UUFDcEgsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLHFCQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDN0QsZ0dBQWdHO1FBQ2hHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLDJEQUEyRCxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN0SCxDQUFDO0lBRU0sNENBQWlCLEdBQXhCLFVBQXlCLEVBQUU7UUFDMUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGNBQU8sQ0FBQyxFQUFFLGVBQWUsRUFBRSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQ3BILElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxxQkFBYyxDQUFDLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBQzdELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVNLDBDQUFlLEdBQXRCLFVBQXVCLGFBQWE7UUFDbkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGNBQU8sQ0FBQyxFQUFFLGVBQWUsRUFBRSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxZQUFZLEVBQUUsY0FBYyxFQUFFLGlDQUFpQyxFQUFFLENBQUMsQ0FBQztRQUN2SyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUkscUJBQWMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUM3RCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFsQ1csZ0JBQWdCO1FBRDVCLGlCQUFVLEVBQUU7eUNBVWMsV0FBSSxFQUF1QiwwQkFBVyxFQUFzQix3QkFBVTtPQVRwRixnQkFBZ0IsQ0FvQzVCO0lBQUQsdUJBQUM7Q0FBQSxBQXBDRCxJQW9DQztBQXBDWSw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwLCBIZWFkZXJzLCBSZXNwb25zZSwgUmVxdWVzdE9wdGlvbnMgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vdXNlci91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IFVybFNlcnZpY2UgfSBmcm9tICcuLi91cmxTZXJ2aWNlL3VybC5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFJlZnVlbGluZ1NlcnZpY2Uge1xuXG5cdC8vIHB1YmxpYyByZWZ1ZWxpbmdzVXJsOiBzdHJpbmcgPSBcImh0dHA6Ly9hcGlsb2dpc3RpY3MuZ3JlZW5kZXYuaW4vbG9naXN0aWNzLTAuMC4xLVNOQVBTSE9UL2RyaXZlci9hcGkvdjEvcmVmdWVsaW5ncy9cIjtcblx0cHVibGljIHJlZnVlbGluZ3NVcmw6IHN0cmluZyA9IHRoaXMudXJsU2VydmljZS5hcGlVcmwgKyBcIi9kcml2ZXIvYXBpL3YxL3JlZnVlbGluZ3MvXCI7XG5cdHB1YmxpYyBoZWFkZXJzO1xuXHRwdWJsaWMgb3B0aW9ucztcblx0cHVibGljIHJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRDtcblx0cHVibGljIHJlZnVlbGluZ0pTT05mb3JQT1NUO1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cCwgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsIHByaXZhdGUgdXJsU2VydmljZTogVXJsU2VydmljZSkgeyB9XG5cblx0cHVibGljIGFkZFJlZnVlbGluZyhyZWZ1ZWxpbmdKU09OKSB7XG5cdFx0dGhpcy5oZWFkZXJzID0gbmV3IEhlYWRlcnMoeyBcIkF1dGhvcml6YXRpb25cIjogXCJiZWFyZXIgXCIgKyB0aGlzLnVzZXJTZXJ2aWNlLnVzZXJBcGlBY2Nlc3NEYXRhLmpzb24oKS5hY2Nlc3NfdG9rZW4sIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOFwiIH0pO1xuXHRcdHRoaXMub3B0aW9ucyA9IG5ldyBSZXF1ZXN0T3B0aW9ucyh7IGhlYWRlcnM6IHRoaXMuaGVhZGVycyB9KTtcblx0XHRyZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5yZWZ1ZWxpbmdzVXJsLCByZWZ1ZWxpbmdKU09OLCB0aGlzLm9wdGlvbnMpO1xuXHR9XG5cblx0cHVibGljIGdldFJlZnVlbGluZ3MoKSB7XG5cdFx0dGhpcy5oZWFkZXJzID0gbmV3IEhlYWRlcnMoeyBcIkF1dGhvcml6YXRpb25cIjogXCJiZWFyZXIgXCIgKyB0aGlzLnVzZXJTZXJ2aWNlLnVzZXJBcGlBY2Nlc3NEYXRhLmpzb24oKS5hY2Nlc3NfdG9rZW4gfSk7XG5cdFx0dGhpcy5vcHRpb25zID0gbmV3IFJlcXVlc3RPcHRpb25zKHsgaGVhZGVyczogdGhpcy5oZWFkZXJzIH0pO1xuXHRcdC8vIHJldHVybiB0aGlzLmh0dHAuZ2V0KHRoaXMucmVmdWVsaW5nc1VybCArIFwiL3JlZnVlbGluZ3MtbGltaXQ/cGFnZT0yJnNpemU9MTAwXCIsIHRoaXMub3B0aW9ucyk7XG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5nZXQodGhpcy5yZWZ1ZWxpbmdzVXJsICsgXCIvcmVmdWVsaW5ncy1saW1pdD9wYWdlPTAmc2l6ZT0xMDAmc29ydD1yZWZ1ZWxpbmdEYXRlLGRlc2NcIiwgdGhpcy5vcHRpb25zKTtcblx0fVxuXG5cdHB1YmxpYyBnZXRSZWZ1ZWxpbmdzQnlJZChpZCkge1xuXHRcdHRoaXMuaGVhZGVycyA9IG5ldyBIZWFkZXJzKHsgXCJBdXRob3JpemF0aW9uXCI6IFwiYmVhcmVyIFwiICsgdGhpcy51c2VyU2VydmljZS51c2VyQXBpQWNjZXNzRGF0YS5qc29uKCkuYWNjZXNzX3Rva2VuIH0pO1xuXHRcdHRoaXMub3B0aW9ucyA9IG5ldyBSZXF1ZXN0T3B0aW9ucyh7IGhlYWRlcnM6IHRoaXMuaGVhZGVycyB9KTtcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldCh0aGlzLnJlZnVlbGluZ3NVcmwgKyBpZCwgdGhpcy5vcHRpb25zKTtcblx0fVxuXG5cdHB1YmxpYyB1cGRhdGVSZWZ1ZWxpbmcocmVmdWVsaW5nSlNPTikge1xuXHRcdHRoaXMuaGVhZGVycyA9IG5ldyBIZWFkZXJzKHsgXCJBdXRob3JpemF0aW9uXCI6IFwiYmVhcmVyIFwiICsgdGhpcy51c2VyU2VydmljZS51c2VyQXBpQWNjZXNzRGF0YS5qc29uKCkuYWNjZXNzX3Rva2VuLCBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb247IGNoYXJzZXQ9dXRmLThcIiB9KTtcblx0XHR0aGlzLm9wdGlvbnMgPSBuZXcgUmVxdWVzdE9wdGlvbnMoeyBoZWFkZXJzOiB0aGlzLmhlYWRlcnMgfSk7XG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wdXQodGhpcy5yZWZ1ZWxpbmdzVXJsLCByZWZ1ZWxpbmdKU09OLCB0aGlzLm9wdGlvbnMpO1xuXHR9XG5cbn1cbiJdfQ==