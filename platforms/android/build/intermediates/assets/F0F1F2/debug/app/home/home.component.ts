import { Component, OnInit } from '@angular/core';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ListPicker } from "ui/list-picker";
import { LoginService } from "../services/login/login.service";
import { GlobalService } from "../services/global/global.service";
import { UserService } from "../services/user/user.service";
import { RouterExtensions } from "nativescript-angular/router";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { Page } from "ui/page";
import {
	getString,
	setString,
	setNumber,
	getNumber
} from "application-settings";
import { TNSFontIconService } from 'nativescript-ngx-fonticon';


@Component({
	moduleId: module.id,
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public pin;
	public body;
	public isDriverPicked: boolean = false;

	constructor(private loginService: LoginService, private globalService: GlobalService, private userService: UserService,
		private routerExtensions: RouterExtensions, private page: Page, private fonticon: TNSFontIconService) {
	}

	ngOnInit(): void {
		this.userService.driverName = null;
		this.globalService.activityIndicator = false;
		this.page.actionBarHidden = true;
		this.globalService.loginFormVisibility = true;
		this.automaticLogin();

	}

	public selectedIndexChanged(args) {
		let picker = <ListPicker>args.object;
		this.userService.driverName = this.globalService.driversList[picker.selectedIndex];
		this.isDriverPicked = true;
		this.userService.driverId = picker.selectedIndex + 1;
		setNumber("driverId", this.userService.driverId);
	}

	public login() {
		
		this.globalService.loginFormVisibility = false;
		this.globalService.activityIndicator = true;
		let driverFirstSecondName = this.userService.driverName.split(" ", 2);
		let firstName = driverFirstSecondName[0].charAt(0).toUpperCase() + driverFirstSecondName[0].slice(1).toLowerCase();
		let secondName = driverFirstSecondName[1].charAt(0).toUpperCase() + driverFirstSecondName[1].slice(1).toLowerCase();
		this.userService.driverName = firstName +" "+ secondName;
		let index = 0;
		for(let driver of this.globalService.driversList){
			index+=1;
			if(driver === this.userService.driverName){
				break;
			}
		}
		setString("username", this.userService.driverName);
		setString("pin", this.pin);
		setNumber("driverId", index);
		console.log(getString("username"));
		console.log(getNumber("driverId"));
		console.log(getString("pin"));
		

		this.body = this.body = `username=` + this.userService.driverName + '&password=' + this.pin + '&client_id='
			+ this.globalService.client_id + '&client_secret=' + this.globalService.client_secret + '&grant_type=' + this.globalService.grant_type;

		this.loginService.login(this.body).subscribe(
			(res) => { this.userService.userApiAccessData = res, this.userService.isUserLogged = true, this.routerExtensions.navigate(["/navpage"], { clearHistory: true }), setString("username", this.userService.driverName), setString("pin", this.pin), this.pin = "" },
			(err) => {
				alert({
					title: "Błąd",
					message: "Wprowadź poprawny numer PIN do wybranego kierowcy",
					okButtonText: "OK"
				}),

					this.userService.userApiAccessData = null;
				this.userService.isUserLogged = false;
				this.globalService.activityIndicator = false;
				this.globalService.loginFormVisibility = true;
			}
		)
	}

	public automaticLogin() {
		if (getString("username") !== undefined && getString("pin") !== undefined) {
			this.userService.driverName = getString("username");
			this.pin = getString("pin");
			this.login();
		}
	}



}
