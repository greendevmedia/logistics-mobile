"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var global_service_1 = require("../global/global.service");
var list_service_1 = require("../list/list.service");
var JsonService = (function () {
    function JsonService(globalService, listService) {
        this.globalService = globalService;
        this.listService = listService;
        this.isWarningNoteCollapsed = false;
        this.switchState = "Dodatkowe uwagi - NIE";
        this.isAdBlueCollapsed = false;
        this.switchStateAdBlue = "AdBlue - NIE";
    }
    JsonService.prototype.selectedIndexChangedOnPaymentTypes = function (args) {
        var picker = args.object;
        this.pickedPaymentType = this.globalService.paymentTypesList[picker.selectedIndex];
        this.isListItemsPicked();
    };
    JsonService.prototype.selectedIndexChangedOnCurrencies = function (args) {
        var picker = args.object;
        this.pickedCurrencyId = picker.selectedIndex + 1;
        this.isListItemsPicked();
    };
    JsonService.prototype.selectedIndexChangedOnCars = function (args) {
        var picker = args.object;
        this.pickedCarId = picker.selectedIndex + 1;
        this.correctPickedCarId = this.listService.carsListIndexes[this.pickedCarId - 1];
        console.log(this.correctPickedCarId);
        this.carName = this.listService.carsList[picker.selectedIndex];
    };
    JsonService.prototype.selectedIndexChangedOnTrailers = function (args) {
        var picker = args.object;
        this.pickedTrailerId = picker.selectedIndex + 1;
        this.trailerName = this.listService.trailersList[picker.selectedIndex];
    };
    JsonService.prototype.isListItemsPicked = function () {
        if (this.pickedCurrencyId > 0 && this.pickedPaymentType !== null) {
            return true;
        }
    };
    JsonService.prototype.onFirstChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.switchState = "Dodatkowe uwagi - TAK";
            this.isWarningNoteCollapsed = true;
        }
        else {
            this.switchState = "Dodatkowe uwagi - NIE";
            this.isWarningNoteCollapsed = false;
        }
    };
    JsonService.prototype.onSecondChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.switchStateAdBlue = "AdBlue - TAK";
            this.isAdBlueCollapsed = true;
        }
        else {
            this.switchStateAdBlue = "AdBlue - NIE";
            this.isAdBlueCollapsed = false;
        }
    };
    JsonService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [global_service_1.GlobalService, list_service_1.ListService])
    ], JsonService);
    return JsonService;
}());
exports.JsonService = JsonService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsianNvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBRTNDLDJEQUF5RDtBQUV6RCxxREFBbUQ7QUFHbkQ7SUFzQkMscUJBQW9CLGFBQTRCLEVBQVUsV0FBd0I7UUFBOUQsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVp4RSwyQkFBc0IsR0FBWSxLQUFLLENBQUM7UUFDeEMsZ0JBQVcsR0FBVyx1QkFBdUIsQ0FBQztRQU9qRCxzQkFBaUIsR0FBWSxLQUFLLENBQUM7UUFDbkMsc0JBQWlCLEdBQVcsY0FBYyxDQUFDO0lBR29DLENBQUM7SUFFaEYsd0RBQWtDLEdBQXpDLFVBQTBDLElBQUk7UUFDdkMsSUFBSSxNQUFNLEdBQWUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbkYsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVTLHNEQUFnQyxHQUF2QyxVQUF3QyxJQUFJO1FBQ3hDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFUyxnREFBMEIsR0FBakMsVUFBa0MsSUFBSTtRQUNsQyxJQUFJLE1BQU0sR0FBZSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUMsQ0FBQyxDQUFDLENBQUM7UUFDL0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRU0sb0RBQThCLEdBQXJDLFVBQXNDLElBQUk7UUFDdEMsSUFBSSxNQUFNLEdBQWUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMzQyxJQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFHTSx1Q0FBaUIsR0FBeEI7UUFDTyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsS0FBSyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2hFLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztJQUNSLENBQUM7SUFFTSxvQ0FBYyxHQUFyQixVQUFzQixJQUFJO1FBQ25CLElBQUksV0FBVyxHQUFXLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdEMsRUFBRSxDQUFBLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyx1QkFBdUIsQ0FBQztZQUMzQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO1FBRXZDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxXQUFXLEdBQUcsdUJBQXVCLENBQUM7WUFDM0MsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztRQUV4QyxDQUFDO0lBQ0wsQ0FBQztJQUVNLHFDQUFlLEdBQXRCLFVBQXVCLElBQUk7UUFDN0IsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsY0FBYyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDL0IsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ1AsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGNBQWMsQ0FBQztZQUN4QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLENBQUM7SUFDRixDQUFDO0lBL0VXLFdBQVc7UUFEdkIsaUJBQVUsRUFBRTt5Q0F1QnVCLDhCQUFhLEVBQXVCLDBCQUFXO09BdEJ0RSxXQUFXLENBaUZ2QjtJQUFELGtCQUFDO0NBQUEsQUFqRkQsSUFpRkM7QUFqRlksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBMaXN0UGlja2VyIH0gZnJvbSBcInVpL2xpc3QtcGlja2VyXCI7XG5pbXBvcnQgeyBHbG9iYWxTZXJ2aWNlIH0gZnJvbSBcIi4uL2dsb2JhbC9nbG9iYWwuc2VydmljZVwiO1xuaW1wb3J0IHsgU3dpdGNoIH0gZnJvbSBcInVpL3N3aXRjaFwiO1xuaW1wb3J0IHsgTGlzdFNlcnZpY2UgfSBmcm9tICcuLi9saXN0L2xpc3Quc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBKc29uU2VydmljZSB7XG5cblx0cHVibGljIGNhck5hbWU7XG5cdHB1YmxpYyBjb3VudGVyOiBudW1iZXI7XG4gICAgcHVibGljIHF1YW50aXR5OiBudW1iZXI7XG5cdHB1YmxpYyBwcmljZTogbnVtYmVyO1xuXHRwdWJsaWMgcGlja2VkUGF5bWVudFR5cGU6IHN0cmluZztcblx0cHVibGljIHBpY2tlZEN1cnJlbmN5SWQ6IG51bWJlcjtcbiAgICBwdWJsaWMgcGlja2VkQ2FySWQ6IG51bWJlcjtcbiAgICBwdWJsaWMgY29ycmVjdFBpY2tlZENhcklkOiBudW1iZXI7XG4gICAgcHVibGljIGlzV2FybmluZ05vdGVDb2xsYXBzZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBwdWJsaWMgc3dpdGNoU3RhdGU6IHN0cmluZyA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gTklFXCI7XG4gICAgcHVibGljIHdhcm5pbmdOb3RlO1xuICAgIHB1YmxpYyBhZEJsdWVRdWFudGl0eTtcbiAgICBwdWJsaWMgYWRCbHVlUHJpY2U7XG4gICAgcHVibGljIHRyYWlsZXJOYW1lO1xuICAgIHB1YmxpYyBwaWNrZWRUcmFpbGVySWQ7XG5cblx0cHVibGljIGlzQWRCbHVlQ29sbGFwc2VkOiBib29sZWFuID0gZmFsc2U7XG5cdHB1YmxpYyBzd2l0Y2hTdGF0ZUFkQmx1ZTogc3RyaW5nID0gXCJBZEJsdWUgLSBOSUVcIjtcbiAgICBcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgbGlzdFNlcnZpY2U6IExpc3RTZXJ2aWNlKSB7IH1cblxuXHRwdWJsaWMgc2VsZWN0ZWRJbmRleENoYW5nZWRPblBheW1lbnRUeXBlcyhhcmdzKSB7XG4gICAgICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcbiAgICAgICAgdGhpcy5waWNrZWRQYXltZW50VHlwZSA9IHRoaXMuZ2xvYmFsU2VydmljZS5wYXltZW50VHlwZXNMaXN0W3BpY2tlci5zZWxlY3RlZEluZGV4XTtcbiAgICAgICAgdGhpcy5pc0xpc3RJdGVtc1BpY2tlZCgpO1xuXHR9XG5cbiAgICBwdWJsaWMgc2VsZWN0ZWRJbmRleENoYW5nZWRPbkN1cnJlbmNpZXMoYXJncykge1xuICAgICAgICBsZXQgcGlja2VyID0gPExpc3RQaWNrZXI+YXJncy5vYmplY3Q7XG4gICAgICAgIHRoaXMucGlja2VkQ3VycmVuY3lJZCA9IHBpY2tlci5zZWxlY3RlZEluZGV4ICsgMTsgXG4gICAgICAgIHRoaXMuaXNMaXN0SXRlbXNQaWNrZWQoKTtcblx0fVxuICAgIFxuICAgIHB1YmxpYyBzZWxlY3RlZEluZGV4Q2hhbmdlZE9uQ2FycyhhcmdzKSB7XG4gICAgICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcbiAgICAgICAgdGhpcy5waWNrZWRDYXJJZCA9IHBpY2tlci5zZWxlY3RlZEluZGV4ICsgMTtcbiAgICAgICAgdGhpcy5jb3JyZWN0UGlja2VkQ2FySWQgPSB0aGlzLmxpc3RTZXJ2aWNlLmNhcnNMaXN0SW5kZXhlc1t0aGlzLnBpY2tlZENhcklkLTFdO1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmNvcnJlY3RQaWNrZWRDYXJJZCk7XG5cdFx0dGhpcy5jYXJOYW1lID0gdGhpcy5saXN0U2VydmljZS5jYXJzTGlzdFtwaWNrZXIuc2VsZWN0ZWRJbmRleF07XG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBzZWxlY3RlZEluZGV4Q2hhbmdlZE9uVHJhaWxlcnMoYXJncykge1xuICAgICAgICBsZXQgcGlja2VyID0gPExpc3RQaWNrZXI+YXJncy5vYmplY3Q7XG5cdFx0dGhpcy5waWNrZWRUcmFpbGVySWQgPSBwaWNrZXIuc2VsZWN0ZWRJbmRleCArIDE7XG5cdFx0dGhpcy50cmFpbGVyTmFtZSA9IHRoaXMubGlzdFNlcnZpY2UudHJhaWxlcnNMaXN0W3BpY2tlci5zZWxlY3RlZEluZGV4XTtcblx0fVxuXG5cdFxuXHRwdWJsaWMgaXNMaXN0SXRlbXNQaWNrZWQoKSB7XG4gICAgICAgIGlmICh0aGlzLnBpY2tlZEN1cnJlbmN5SWQgPiAwICYmIHRoaXMucGlja2VkUGF5bWVudFR5cGUgIT09IG51bGwgKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuXHR9XG5cdFxuXHRwdWJsaWMgb25GaXJzdENoZWNrZWQoYXJncykge1xuICAgICAgICBsZXQgZmlyc3RTd2l0Y2ggPSA8U3dpdGNoPmFyZ3Mub2JqZWN0O1xuICAgICAgICBpZihmaXJzdFN3aXRjaC5jaGVja2VkKSB7XG4gICAgICAgICAgICB0aGlzLnN3aXRjaFN0YXRlID0gXCJEb2RhdGtvd2UgdXdhZ2kgLSBUQUtcIjtcbiAgICAgICAgICAgIHRoaXMuaXNXYXJuaW5nTm90ZUNvbGxhcHNlZCA9IHRydWU7XG5cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc3dpdGNoU3RhdGUgPSBcIkRvZGF0a293ZSB1d2FnaSAtIE5JRVwiO1xuICAgICAgICAgICAgdGhpcy5pc1dhcm5pbmdOb3RlQ29sbGFwc2VkID0gZmFsc2U7XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBvblNlY29uZENoZWNrZWQoYXJncykge1xuXHRcdGxldCBmaXJzdFN3aXRjaCA9IDxTd2l0Y2g+YXJncy5vYmplY3Q7XG5cdFx0aWYgKGZpcnN0U3dpdGNoLmNoZWNrZWQpIHtcblx0XHRcdHRoaXMuc3dpdGNoU3RhdGVBZEJsdWUgPSBcIkFkQmx1ZSAtIFRBS1wiO1xuXHRcdFx0dGhpcy5pc0FkQmx1ZUNvbGxhcHNlZCA9IHRydWU7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHRoaXMuc3dpdGNoU3RhdGVBZEJsdWUgPSBcIkFkQmx1ZSAtIE5JRVwiO1xuXHRcdFx0dGhpcy5pc0FkQmx1ZUNvbGxhcHNlZCA9IGZhbHNlO1xuXHRcdH1cblx0fVxuXG59XG4iXX0=