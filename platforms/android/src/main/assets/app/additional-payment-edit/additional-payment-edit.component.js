"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var date_service_1 = require("../services/date/date.service");
var additional_payment_service_1 = require("../services/additional-payment/additional-payment.service");
var global_service_1 = require("../services/global/global.service");
var list_service_1 = require("../services/list/list.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var AdditionalPaymentEditComponent = (function () {
    function AdditionalPaymentEditComponent(page, listService, dateService, additionalPaymentService, globalService, userService, routerExtensions) {
        this.page = page;
        this.listService = listService;
        this.dateService = dateService;
        this.additionalPaymentService = additionalPaymentService;
        this.globalService = globalService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
    }
    AdditionalPaymentEditComponent.prototype.ngOnInit = function () {
        this.isBusy = false;
        this.page.actionBarHidden = true;
        this.listService.getCurrencies();
        this.listService.getCars();
        this.listService.getAdditionalPaymentsNames();
        this.currency = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.currency.currencyCode;
        this.paymentType = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.paymentType;
        this.additionaPaymentId = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.id;
        this.totalPrice = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.totalPrice;
        console.log(this.totalPrice);
        this.paymentName = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.paymentName.paymentName;
        this.carName = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.car.name;
    };
    AdditionalPaymentEditComponent.prototype.selectedIndexChangedOnPaymentTypes = function (args) {
        var picker = args.object;
        this.paymentType = this.globalService.paymentTypesList[picker.selectedIndex];
    };
    AdditionalPaymentEditComponent.prototype.selectedIndexChangedOnCurrencies = function (args) {
        var picker = args.object;
        this.currencyId = picker.selectedIndex + 1;
    };
    AdditionalPaymentEditComponent.prototype.selectedIndexChangedOnCars = function (args) {
        var picker = args.object;
        this.carId = picker.selectedIndex + 1;
        this.correctPickedCarId = this.listService.carsListIndexes[this.carId - 1];
    };
    AdditionalPaymentEditComponent.prototype.updateAdditionalPayment = function () {
        var _this = this;
        this.isBusy = true;
        this.additionalPaymentService.additionalPaymentJSONforPOST = {
            id: this.additionaPaymentId,
            paymentDay: this.dateService.dateFormatForPost(),
            paymentName: {
                id: this.additionalPaymentService.pickedAdditionalpaymentNameId
            },
            totalPrice: this.totalPrice,
            paymentType: this.paymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.correctPickedCarId
            },
            currency: {
                id: this.currencyId
            }
        };
        this.additionalPaymentService.updateAdditionalPayment(this.additionalPaymentService.additionalPaymentJSONforPOST).subscribe(function (res) {
            _this.isBusy = false;
            alert({
                title: "Potwierdzenie",
                message: "Opłata zmieniona poprawnie",
                okButtonText: "OK"
            }), _this.routerExtensions.navigate(["/additional-payment-details"]);
        }, function (err) {
            _this.isBusy = false;
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    AdditionalPaymentEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-additional-payment-edit',
            templateUrl: './additional-payment-edit.component.html',
            styleUrls: ['./additional-payment-edit.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, list_service_1.ListService, date_service_1.DateService, additional_payment_service_1.AdditionalPaymentService, global_service_1.GlobalService, user_service_1.UserService, router_1.RouterExtensions])
    ], AdditionalPaymentEditComponent);
    return AdditionalPaymentEditComponent;
}());
exports.AdditionalPaymentEditComponent = AdditionalPaymentEditComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkaXRpb25hbC1wYXltZW50LWVkaXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYWRkaXRpb25hbC1wYXltZW50LWVkaXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELGdDQUErQjtBQUMvQiw4REFBNEQ7QUFDNUQsd0dBQXFHO0FBQ3JHLG9FQUFrRTtBQUNsRSw4REFBNEQ7QUFFNUQsOERBQTREO0FBQzVELHNEQUErRDtBQVEvRDtJQWdCQyx3Q0FBb0IsSUFBVSxFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSx3QkFBa0QsRUFBVSxhQUE0QixFQUFVLFdBQXdCLEVBQVUsZ0JBQWtDO1FBQTlQLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtRQUFVLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO0lBQU0sQ0FBQztJQUV6UixpREFBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLDBCQUEwQixFQUFFLENBQUM7UUFDOUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0NBQWdDLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQztRQUNyRyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxXQUFXLENBQUM7UUFDOUYsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxFQUFFLENBQUM7UUFDNUYsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0NBQWdDLENBQUMsVUFBVSxDQUFDO1FBQzVGLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGdDQUFnQyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUM7UUFDMUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0NBQWdDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztJQUN4RixDQUFDO0lBRU0sMkVBQWtDLEdBQXpDLFVBQTBDLElBQUk7UUFDN0MsSUFBSSxNQUFNLEdBQWUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFTSx5RUFBZ0MsR0FBdkMsVUFBd0MsSUFBSTtRQUMzQyxJQUFJLE1BQU0sR0FBZSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVNLG1FQUEwQixHQUFqQyxVQUFrQyxJQUFJO1FBQ3JDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBRU0sZ0VBQXVCLEdBQTlCO1FBQUEsaUJBc0NDO1FBckNBLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyw0QkFBNEIsR0FBRztZQUM1RCxFQUFFLEVBQUUsSUFBSSxDQUFDLGtCQUFrQjtZQUMzQixVQUFVLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRTtZQUNoRCxXQUFXLEVBQUU7Z0JBQ1osRUFBRSxFQUFFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyw2QkFBNkI7YUFDL0Q7WUFDRCxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7WUFDM0IsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO1lBQzdCLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVU7WUFDdkMsR0FBRyxFQUFFO2dCQUNKLEVBQUUsRUFBRSxJQUFJLENBQUMsa0JBQWtCO2FBQzNCO1lBQ0QsUUFBUSxFQUFFO2dCQUNULEVBQUUsRUFBRSxJQUFJLENBQUMsVUFBVTthQUNuQjtTQUVELENBQUE7UUFFRCxJQUFJLENBQUMsd0JBQXdCLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLDRCQUE0QixDQUFDLENBQUMsU0FBUyxDQUMxSCxVQUFDLEdBQUc7WUFDSixLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNuQixLQUFLLENBQUM7Z0JBQ0wsS0FBSyxFQUFFLGVBQWU7Z0JBQ3RCLE9BQU8sRUFBRSw0QkFBNEI7Z0JBQ3JDLFlBQVksRUFBRSxJQUFJO2FBQ2xCLENBQUMsRUFBRSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFBO1FBQ3BFLENBQUMsRUFDRCxVQUFDLEdBQUc7WUFDSixLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNuQixLQUFLLENBQUM7Z0JBQ0wsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLGdDQUFnQztnQkFDekMsWUFBWSxFQUFFLElBQUk7YUFDbEIsQ0FBQyxDQUFBO1FBQ0gsQ0FBQyxDQUNELENBQUM7SUFDSCxDQUFDO0lBdkZXLDhCQUE4QjtRQU4xQyxnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSw2QkFBNkI7WUFDdkMsV0FBVyxFQUFFLDBDQUEwQztZQUN2RCxTQUFTLEVBQUUsQ0FBQywwQ0FBMEMsQ0FBQztTQUN2RCxDQUFDO3lDQWlCeUIsV0FBSSxFQUF1QiwwQkFBVyxFQUF1QiwwQkFBVyxFQUFvQyxxREFBd0IsRUFBeUIsOEJBQWEsRUFBdUIsMEJBQVcsRUFBNEIseUJBQWdCO09BaEJ0USw4QkFBOEIsQ0F5RjFDO0lBQUQscUNBQUM7Q0FBQSxBQXpGRCxJQXlGQztBQXpGWSx3RUFBOEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XG5pbXBvcnQgeyBEYXRlU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2RhdGUvZGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IEFkZGl0aW9uYWxQYXltZW50U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FkZGl0aW9uYWwtcGF5bWVudC9hZGRpdGlvbmFsLXBheW1lbnQuc2VydmljZSc7XG5pbXBvcnQgeyBHbG9iYWxTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZ2xvYmFsL2dsb2JhbC5zZXJ2aWNlJztcbmltcG9ydCB7IExpc3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbGlzdC9saXN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgTGlzdFBpY2tlciB9IGZyb20gXCJ1aS9saXN0LXBpY2tlclwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy91c2VyL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuXG5AQ29tcG9uZW50KHtcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0c2VsZWN0b3I6ICdhcHAtYWRkaXRpb25hbC1wYXltZW50LWVkaXQnLFxuXHR0ZW1wbGF0ZVVybDogJy4vYWRkaXRpb25hbC1wYXltZW50LWVkaXQuY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9hZGRpdGlvbmFsLXBheW1lbnQtZWRpdC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEFkZGl0aW9uYWxQYXltZW50RWRpdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cblx0cHVibGljIGFkZGl0aW9uYVBheW1lbnRJZDtcblx0cHVibGljIHRvdGFsUHJpY2U7XG5cdHB1YmxpYyBwYXltZW50TmFtZTtcblx0cHVibGljIHBheW1lbnROYW1lSWQ7XG5cdHB1YmxpYyBwYXltZW50VHlwZTtcblx0cHVibGljIGNhck5hbWU7XG5cdHB1YmxpYyBjYXJJZDtcblx0cHVibGljIGNvcnJlY3RQaWNrZWRDYXJJZDtcblx0cHVibGljIGN1cnJlbmN5O1xuXHRwdWJsaWMgY3VycmVuY3lJZDtcblx0cHJpdmF0ZSBpc0J1c3k7XG5cblxuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSBsaXN0U2VydmljZTogTGlzdFNlcnZpY2UsIHByaXZhdGUgZGF0ZVNlcnZpY2U6IERhdGVTZXJ2aWNlLCBwcml2YXRlIGFkZGl0aW9uYWxQYXltZW50U2VydmljZTogQWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLCBwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsICkgeyB9XG5cblx0bmdPbkluaXQoKSB7XG5cdFx0dGhpcy5pc0J1c3kgPSBmYWxzZTtcblx0XHR0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcblx0XHR0aGlzLmxpc3RTZXJ2aWNlLmdldEN1cnJlbmNpZXMoKTtcblx0XHR0aGlzLmxpc3RTZXJ2aWNlLmdldENhcnMoKTtcblx0XHR0aGlzLmxpc3RTZXJ2aWNlLmdldEFkZGl0aW9uYWxQYXltZW50c05hbWVzKCk7XG5cdFx0dGhpcy5jdXJyZW5jeSA9IHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLmFkZGl0aW9uYWxQYXltZW50SlNPTmZyb21HRVRieUlkLmN1cnJlbmN5LmN1cnJlbmN5Q29kZTtcblx0XHR0aGlzLnBheW1lbnRUeXBlID0gdGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UuYWRkaXRpb25hbFBheW1lbnRKU09OZnJvbUdFVGJ5SWQucGF5bWVudFR5cGU7XG5cdFx0dGhpcy5hZGRpdGlvbmFQYXltZW50SWQgPSB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5hZGRpdGlvbmFsUGF5bWVudEpTT05mcm9tR0VUYnlJZC5pZDtcblx0XHR0aGlzLnRvdGFsUHJpY2UgPSB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5hZGRpdGlvbmFsUGF5bWVudEpTT05mcm9tR0VUYnlJZC50b3RhbFByaWNlO1xuXHRcdGNvbnNvbGUubG9nKHRoaXMudG90YWxQcmljZSk7XG5cdFx0dGhpcy5wYXltZW50TmFtZSA9IHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLmFkZGl0aW9uYWxQYXltZW50SlNPTmZyb21HRVRieUlkLnBheW1lbnROYW1lLnBheW1lbnROYW1lO1xuXHRcdHRoaXMuY2FyTmFtZSA9IHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLmFkZGl0aW9uYWxQYXltZW50SlNPTmZyb21HRVRieUlkLmNhci5uYW1lO1xuXHR9XG5cblx0cHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkT25QYXltZW50VHlwZXMoYXJncykge1xuXHRcdGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcblx0XHR0aGlzLnBheW1lbnRUeXBlID0gdGhpcy5nbG9iYWxTZXJ2aWNlLnBheW1lbnRUeXBlc0xpc3RbcGlja2VyLnNlbGVjdGVkSW5kZXhdO1xuXHR9XG5cblx0cHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkT25DdXJyZW5jaWVzKGFyZ3MpIHtcblx0XHRsZXQgcGlja2VyID0gPExpc3RQaWNrZXI+YXJncy5vYmplY3Q7XG5cdFx0dGhpcy5jdXJyZW5jeUlkID0gcGlja2VyLnNlbGVjdGVkSW5kZXggKyAxO1xuXHR9XG5cblx0cHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkT25DYXJzKGFyZ3MpIHtcblx0XHRsZXQgcGlja2VyID0gPExpc3RQaWNrZXI+YXJncy5vYmplY3Q7XG5cdFx0dGhpcy5jYXJJZCA9IHBpY2tlci5zZWxlY3RlZEluZGV4ICsgMTtcblx0XHR0aGlzLmNvcnJlY3RQaWNrZWRDYXJJZCA9IHRoaXMubGlzdFNlcnZpY2UuY2Fyc0xpc3RJbmRleGVzW3RoaXMuY2FySWQgLSAxXTtcblx0fVxuXG5cdHB1YmxpYyB1cGRhdGVBZGRpdGlvbmFsUGF5bWVudCgpIHtcblx0XHR0aGlzLmlzQnVzeSA9IHRydWU7XG5cdFx0dGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UuYWRkaXRpb25hbFBheW1lbnRKU09OZm9yUE9TVCA9IHtcblx0XHRcdGlkOiB0aGlzLmFkZGl0aW9uYVBheW1lbnRJZCxcblx0XHRcdHBheW1lbnREYXk6IHRoaXMuZGF0ZVNlcnZpY2UuZGF0ZUZvcm1hdEZvclBvc3QoKSxcblx0XHRcdHBheW1lbnROYW1lOiB7XG5cdFx0XHRcdGlkOiB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5waWNrZWRBZGRpdGlvbmFscGF5bWVudE5hbWVJZFxuXHRcdFx0fSxcblx0XHRcdHRvdGFsUHJpY2U6IHRoaXMudG90YWxQcmljZSxcblx0XHRcdHBheW1lbnRUeXBlOiB0aGlzLnBheW1lbnRUeXBlLFxuXHRcdFx0ZHJpdmVyTmFtZTogdGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lLFxuXHRcdFx0Y2FyOiB7XG5cdFx0XHRcdGlkOiB0aGlzLmNvcnJlY3RQaWNrZWRDYXJJZFxuXHRcdFx0fSxcblx0XHRcdGN1cnJlbmN5OiB7XG5cdFx0XHRcdGlkOiB0aGlzLmN1cnJlbmN5SWRcblx0XHRcdH1cblxuXHRcdH1cblxuXHRcdHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLnVwZGF0ZUFkZGl0aW9uYWxQYXltZW50KHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLmFkZGl0aW9uYWxQYXltZW50SlNPTmZvclBPU1QpLnN1YnNjcmliZShcblx0XHRcdChyZXMpID0+IHtcblx0XHRcdHRoaXMuaXNCdXN5ID0gZmFsc2U7XG5cdFx0XHRcdGFsZXJ0KHtcblx0XHRcdFx0XHR0aXRsZTogXCJQb3R3aWVyZHplbmllXCIsXG5cdFx0XHRcdFx0bWVzc2FnZTogXCJPcMWCYXRhIHptaWVuaW9uYSBwb3ByYXduaWVcIixcblx0XHRcdFx0XHRva0J1dHRvblRleHQ6IFwiT0tcIlxuXHRcdFx0XHR9KSwgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9hZGRpdGlvbmFsLXBheW1lbnQtZGV0YWlsc1wiXSlcblx0XHRcdH0sXG5cdFx0XHQoZXJyKSA9PiB7XG5cdFx0XHR0aGlzLmlzQnVzeSA9IGZhbHNlO1xuXHRcdFx0XHRhbGVydCh7XG5cdFx0XHRcdFx0dGl0bGU6IFwiQsWCxIVkXCIsXG5cdFx0XHRcdFx0bWVzc2FnZTogXCJQb3ByYXcgZGFuZSBpIHNwcsOzYnVqIHBvbm93bmllXCIsXG5cdFx0XHRcdFx0b2tCdXR0b25UZXh0OiBcIk9LXCJcblx0XHRcdFx0fSlcblx0XHRcdH0sXG5cdFx0KTtcblx0fVxuXG59XG4iXX0=