"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var date_service_1 = require("../services/date/date.service");
var page_1 = require("tns-core-modules/ui/page/page");
var repair_service_1 = require("../services/repair/repair.service");
var global_service_1 = require("../services/global/global.service");
var Repair1Component = (function () {
    function Repair1Component(dateService, page, repairService, globalService) {
        this.dateService = dateService;
        this.page = page;
        this.repairService = repairService;
        this.globalService = globalService;
        this.switchState = "Odbiór po serwisie";
    }
    Repair1Component.prototype.ngOnInit = function () {
        this.repairService.activityType = this.globalService.repairActivityList[1];
        this.repairService.repairType = null;
        this.repairService.counter = null;
        this.repairService.price = null;
        this.repairService.repairType = null;
        this.repairService.carShopAddress = null;
        this.repairService.vehicalCheck = null;
        this.switchState = "Odbiór po serwisie";
    };
    Repair1Component.prototype.onChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.switchState = "Oddanie na serwis";
            this.repairService.activityType = this.globalService.repairActivityList[0];
            console.log(this.repairService.activityType);
        }
        else {
            this.switchState = "Odbiór po serwisie";
            this.repairService.activityType = this.globalService.repairActivityList[1];
            console.log(this.repairService.activityType);
        }
    };
    Repair1Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-repair-1',
            templateUrl: './repair-1.component.html',
            styleUrls: ['./repair-1.component.scss']
        }),
        __metadata("design:paramtypes", [date_service_1.DateService, page_1.Page, repair_service_1.RepairService, global_service_1.GlobalService])
    ], Repair1Component);
    return Repair1Component;
}());
exports.Repair1Component = Repair1Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVwYWlyLTEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVwYWlyLTEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELDhEQUE0RDtBQUM1RCxzREFBcUQ7QUFDckQsb0VBQWtFO0FBQ2xFLG9FQUFrRTtBQVNsRTtJQUlFLDBCQUFvQixXQUF3QixFQUFVLElBQVUsRUFBVSxhQUE0QixFQUFVLGFBQTRCO1FBQXhILGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQVUsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFGckksZ0JBQVcsR0FBRyxvQkFBb0IsQ0FBQztJQUVzRyxDQUFDO0lBRWpKLG1DQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDbEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDekMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxXQUFXLEdBQUcsb0JBQW9CLENBQUM7SUFDMUMsQ0FBQztJQUVNLG9DQUFTLEdBQWhCLFVBQWlCLElBQUk7UUFDbkIsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUEsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNyQixJQUFJLENBQUMsV0FBVyxHQUFHLG1CQUFtQixDQUFDO1lBQ3ZDLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxXQUFXLEdBQUcsb0JBQW9CLENBQUM7WUFDeEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDakQsQ0FBQztJQUNILENBQUM7SUE1QlUsZ0JBQWdCO1FBTjVCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGNBQWM7WUFDeEIsV0FBVyxFQUFFLDJCQUEyQjtZQUN4QyxTQUFTLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztTQUN6QyxDQUFDO3lDQUtpQywwQkFBVyxFQUFnQixXQUFJLEVBQXlCLDhCQUFhLEVBQXlCLDhCQUFhO09BSmpJLGdCQUFnQixDQThCNUI7SUFBRCx1QkFBQztDQUFBLEFBOUJELElBOEJDO0FBOUJZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEYXRlU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2RhdGUvZGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IFBhZ2UgfSBmcm9tICd0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZSc7XG5pbXBvcnQgeyBSZXBhaXJTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvcmVwYWlyL3JlcGFpci5zZXJ2aWNlJztcbmltcG9ydCB7IEdsb2JhbFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9nbG9iYWwvZ2xvYmFsLnNlcnZpY2UnO1xuaW1wb3J0IHsgU3dpdGNoIH0gZnJvbSBcInVpL3N3aXRjaFwiO1xuXG5AQ29tcG9uZW50KHtcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgc2VsZWN0b3I6ICdhcHAtcmVwYWlyLTEnLFxuICB0ZW1wbGF0ZVVybDogJy4vcmVwYWlyLTEuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9yZXBhaXItMS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFJlcGFpcjFDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHB1YmxpYyBzd2l0Y2hTdGF0ZSA9IFwiT2RiacOzciBwbyBzZXJ3aXNpZVwiO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZGF0ZVNlcnZpY2U6IERhdGVTZXJ2aWNlLCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgcmVwYWlyU2VydmljZTogUmVwYWlyU2VydmljZSwgcHJpdmF0ZSBnbG9iYWxTZXJ2aWNlOiBHbG9iYWxTZXJ2aWNlKSB7IH1cblxuICBuZ09uSW5pdCgpIHsgXG4gICAgdGhpcy5yZXBhaXJTZXJ2aWNlLmFjdGl2aXR5VHlwZSA9IHRoaXMuZ2xvYmFsU2VydmljZS5yZXBhaXJBY3Rpdml0eUxpc3RbMV07XG4gICAgdGhpcy5yZXBhaXJTZXJ2aWNlLnJlcGFpclR5cGUgPSBudWxsO1xuICAgIHRoaXMucmVwYWlyU2VydmljZS5jb3VudGVyID0gbnVsbDtcbiAgICB0aGlzLnJlcGFpclNlcnZpY2UucHJpY2UgPSBudWxsO1xuICAgIHRoaXMucmVwYWlyU2VydmljZS5yZXBhaXJUeXBlID0gbnVsbDtcbiAgICB0aGlzLnJlcGFpclNlcnZpY2UuY2FyU2hvcEFkZHJlc3MgPSBudWxsO1xuICAgIHRoaXMucmVwYWlyU2VydmljZS52ZWhpY2FsQ2hlY2sgPSBudWxsO1xuICAgIHRoaXMuc3dpdGNoU3RhdGUgPSBcIk9kYmnDs3IgcG8gc2Vyd2lzaWVcIjtcbiAgfVxuXG4gIHB1YmxpYyBvbkNoZWNrZWQoYXJncyl7XG4gICAgbGV0IGZpcnN0U3dpdGNoID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcbiAgICBpZihmaXJzdFN3aXRjaC5jaGVja2VkKSB7XG4gICAgICAgIHRoaXMuc3dpdGNoU3RhdGUgPSBcIk9kZGFuaWUgbmEgc2Vyd2lzXCI7XG4gICAgICAgIHRoaXMucmVwYWlyU2VydmljZS5hY3Rpdml0eVR5cGUgPSB0aGlzLmdsb2JhbFNlcnZpY2UucmVwYWlyQWN0aXZpdHlMaXN0WzBdO1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnJlcGFpclNlcnZpY2UuYWN0aXZpdHlUeXBlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnN3aXRjaFN0YXRlID0gXCJPZGJpw7NyIHBvIHNlcndpc2llXCI7XG4gICAgICAgIHRoaXMucmVwYWlyU2VydmljZS5hY3Rpdml0eVR5cGUgPSB0aGlzLmdsb2JhbFNlcnZpY2UucmVwYWlyQWN0aXZpdHlMaXN0WzFdO1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnJlcGFpclNlcnZpY2UuYWN0aXZpdHlUeXBlKTtcbiAgICB9XG4gIH1cblxufVxuXG4iXX0=