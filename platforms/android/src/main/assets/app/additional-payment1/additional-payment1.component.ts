import { Component, OnInit } from '@angular/core';
import { ListService } from '../services/list/list.service';
import { AdditionalPaymentService } from '../services/additional-payment/additional-payment.service';
import { Page } from "ui/page";

@Component({
	moduleId: module.id,
	selector: 'app-additional-payment1',
	templateUrl: './additional-payment1.component.html',
	styleUrls: ['./additional-payment1.component.scss']
})
export class AdditionalPayment1Component implements OnInit {

	constructor(private page: Page, private listService: ListService, private additionalPaymentService: AdditionalPaymentService) { }

	ngOnInit() { 
	
		this.listService.getAdditionalPaymentsNames();
		this.additionalPaymentService.pickedAdditionalpaymentNameId = null;
	}

	public isAdditionalPaymentNamePicked(){
		if (this.additionalPaymentService.pickedAdditionalpaymentNameId > 0 ) {
            return true;
        }
	}



}
