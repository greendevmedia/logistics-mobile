"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var list_service_1 = require("../services/list/list.service");
var additional_payment_service_1 = require("../services/additional-payment/additional-payment.service");
var page_1 = require("ui/page");
var AdditionalPayment1Component = (function () {
    function AdditionalPayment1Component(page, listService, additionalPaymentService) {
        this.page = page;
        this.listService = listService;
        this.additionalPaymentService = additionalPaymentService;
    }
    AdditionalPayment1Component.prototype.ngOnInit = function () {
        this.listService.getAdditionalPaymentsNames();
        this.additionalPaymentService.pickedAdditionalpaymentNameId = null;
    };
    AdditionalPayment1Component.prototype.isAdditionalPaymentNamePicked = function () {
        if (this.additionalPaymentService.pickedAdditionalpaymentNameId > 0) {
            return true;
        }
    };
    AdditionalPayment1Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-additional-payment1',
            templateUrl: './additional-payment1.component.html',
            styleUrls: ['./additional-payment1.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, list_service_1.ListService, additional_payment_service_1.AdditionalPaymentService])
    ], AdditionalPayment1Component);
    return AdditionalPayment1Component;
}());
exports.AdditionalPayment1Component = AdditionalPayment1Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkaXRpb25hbC1wYXltZW50MS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhZGRpdGlvbmFsLXBheW1lbnQxLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCw4REFBNEQ7QUFDNUQsd0dBQXFHO0FBQ3JHLGdDQUErQjtBQVEvQjtJQUVDLHFDQUFvQixJQUFVLEVBQVUsV0FBd0IsRUFBVSx3QkFBa0Q7UUFBeEcsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtJQUFJLENBQUM7SUFFakksOENBQVEsR0FBUjtRQUVDLElBQUksQ0FBQyxXQUFXLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztRQUM5QyxJQUFJLENBQUMsd0JBQXdCLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxDQUFDO0lBQ3BFLENBQUM7SUFFTSxtRUFBNkIsR0FBcEM7UUFDQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsNkJBQTZCLEdBQUcsQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUM3RCxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7SUFDUixDQUFDO0lBZFcsMkJBQTJCO1FBTnZDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxXQUFXLEVBQUUsc0NBQXNDO1lBQ25ELFNBQVMsRUFBRSxDQUFDLHNDQUFzQyxDQUFDO1NBQ25ELENBQUM7eUNBR3lCLFdBQUksRUFBdUIsMEJBQVcsRUFBb0MscURBQXdCO09BRmhILDJCQUEyQixDQWtCdkM7SUFBRCxrQ0FBQztDQUFBLEFBbEJELElBa0JDO0FBbEJZLGtFQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBMaXN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2xpc3QvbGlzdC5zZXJ2aWNlJztcbmltcG9ydCB7IEFkZGl0aW9uYWxQYXltZW50U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FkZGl0aW9uYWwtcGF5bWVudC9hZGRpdGlvbmFsLXBheW1lbnQuc2VydmljZSc7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcblxuQENvbXBvbmVudCh7XG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXG5cdHNlbGVjdG9yOiAnYXBwLWFkZGl0aW9uYWwtcGF5bWVudDEnLFxuXHR0ZW1wbGF0ZVVybDogJy4vYWRkaXRpb25hbC1wYXltZW50MS5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL2FkZGl0aW9uYWwtcGF5bWVudDEuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBBZGRpdGlvbmFsUGF5bWVudDFDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSBsaXN0U2VydmljZTogTGlzdFNlcnZpY2UsIHByaXZhdGUgYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlOiBBZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UpIHsgfVxuXG5cdG5nT25Jbml0KCkgeyBcblx0XG5cdFx0dGhpcy5saXN0U2VydmljZS5nZXRBZGRpdGlvbmFsUGF5bWVudHNOYW1lcygpO1xuXHRcdHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLnBpY2tlZEFkZGl0aW9uYWxwYXltZW50TmFtZUlkID0gbnVsbDtcblx0fVxuXG5cdHB1YmxpYyBpc0FkZGl0aW9uYWxQYXltZW50TmFtZVBpY2tlZCgpe1xuXHRcdGlmICh0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5waWNrZWRBZGRpdGlvbmFscGF5bWVudE5hbWVJZCA+IDAgKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuXHR9XG5cblxuXG59XG4iXX0=