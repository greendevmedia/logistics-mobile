"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var email = require("nativescript-email");
var json_service_1 = require("../json/json.service");
var date_service_1 = require("../date/date.service");
var user_service_1 = require("../user/user.service");
var router_1 = require("nativescript-angular/router");
var imagepicker = require("nativescript-imagepicker");
// var fs = require("file-system");
var TechnicalCardService = (function () {
    function TechnicalCardService(jsonService, dateService, userService, routerExtensions) {
        this.jsonService = jsonService;
        this.dateService = dateService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.oilLevel = "";
        this.purity = "";
        this.lighting = "";
        this.tires = "";
        this.suspension = "";
        this.engineAndAccessories = "";
        this.carBody = "";
        this.imagesPaths = [];
        this.arrayOfAttachments = [];
    }
    TechnicalCardService.prototype.getPicture = function () {
        var that = this;
        var context = imagepicker.create({
            mode: "multiple" // use "multiple" for multiple selection
        });
        context
            .authorize()
            .then(function () {
            return context.present();
        })
            .then(function (selection) {
            selection.forEach(function (selected) {
                that.imagesPaths.push(selected.fileUri);
                console.log(selected.fileUri);
                console.log(that.imagesPaths.length);
            });
        }).catch(function (e) {
            // process error
        });
    };
    TechnicalCardService.prototype.sendTechnicalCard = function () {
        var _this = this;
        if (this.imagesPaths.length > 0) {
            this.createMultipleAttachemnts(this.imagesPaths, this.arrayOfAttachments);
        }
        if (this.jsonService.trailerName == null) {
            this.trailerName = "";
        }
        else {
            this.trailerName = "Przyczepa: " + this.jsonService.trailerName;
        }
        if (this.jsonService.carName == null) {
            this.carName = "";
        }
        else {
            this.carName = "Pojazd: " + this.jsonService.carName;
        }
        this.composeOptions = {
            to: ['pawel@plewinski-logistics.com'],
            subject: 'Karta techniczna - ' + this.carName + " " + this.trailerName,
            body: "Kierowca: " + this.userService.driverName + "\n" + this.carName + " " + this.trailerName + "\nData: " + this.dateService.refuelingDate.day + "." + this.dateService.refuelingDate.month + "." + this.dateService.refuelingDate.year + "\nPoczątkowy stan licznika: " + this.counterStart + "\nKońcowy stan licznika: " + this.counterStop + "\nStan oleju: " + this.oilLevel + "\nCzystość: " + this.purity + "\nOświetlenie: " + this.lighting + "\nOpony: " + this.tires + "\nZawieszenie: " + this.suspension + "\nSilnik oraz osprzęt: " + this.engineAndAccessories + "\nKaroseria: " + this.carBody
        };
        if (this.imagesPaths.length > 0) {
            this.composeOptions.attachments = this.arrayOfAttachments;
        }
        email.available().then(function (available) {
            if (available) {
                email.compose(_this.composeOptions).then(function (result) {
                    _this.routerExtensions.navigate(["/navpage"]);
                });
            }
        }).catch(function (error) { return console.error(error); });
    };
    TechnicalCardService.prototype.createMultipleAttachemnts = function (arrayOfLinks, arrayOfAttachments) {
        var num = 0;
        arrayOfLinks.forEach(function (element) {
            num = num + 1;
            arrayOfAttachments.push({
                fileName: num + 'kt.png',
                path: element,
                mimeType: 'image/png'
            });
        });
    };
    TechnicalCardService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [json_service_1.JsonService, date_service_1.DateService, user_service_1.UserService, router_1.RouterExtensions])
    ], TechnicalCardService);
    return TechnicalCardService;
}());
exports.TechnicalCardService = TechnicalCardService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVjaG5pY2FsLWNhcmQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInRlY2huaWNhbC1jYXJkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsMENBQTRDO0FBQzVDLHFEQUFtRDtBQUNuRCxxREFBbUQ7QUFDbkQscURBQW1EO0FBQ25ELHNEQUErRDtBQUMvRCxzREFBd0Q7QUFFeEQsbUNBQW1DO0FBSW5DO0lBbUJDLDhCQUFvQixXQUF3QixFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSxnQkFBa0M7UUFBeEksZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQWZySixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2QsV0FBTSxHQUFHLEVBQUUsQ0FBQztRQUNaLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFDZCxVQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ1gsZUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNoQix5QkFBb0IsR0FBRyxFQUFFLENBQUM7UUFDMUIsWUFBTyxHQUFHLEVBQUUsQ0FBQztRQUViLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLHVCQUFrQixHQUFHLEVBQUUsQ0FBQztJQU1pSSxDQUFDO0lBSTFKLHlDQUFVLEdBQWpCO1FBQ0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksT0FBTyxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUM7WUFDaEMsSUFBSSxFQUFFLFVBQVUsQ0FBQyx3Q0FBd0M7U0FDekQsQ0FBQyxDQUFDO1FBQ0gsT0FBTzthQUNMLFNBQVMsRUFBRTthQUNYLElBQUksQ0FBQztZQUNMLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDMUIsQ0FBQyxDQUFDO2FBQ0QsSUFBSSxDQUFDLFVBQVUsU0FBUztZQUV4QixTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsUUFBUTtnQkFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRXRDLENBQUMsQ0FBQyxDQUFDO1FBRUosQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztZQUNuQixnQkFBZ0I7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sZ0RBQWlCLEdBQXhCO1FBQUEsaUJBa0NDO1FBakNBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDM0UsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUE7UUFDdEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ1AsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUM7UUFDakUsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUE7UUFDbEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ1AsSUFBSSxDQUFDLE9BQU8sR0FBRyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUM7UUFDdEQsQ0FBQztRQUVELElBQUksQ0FBQyxjQUFjLEdBQUc7WUFDckIsRUFBRSxFQUFFLENBQUMsK0JBQStCLENBQUM7WUFDckMsT0FBTyxFQUFFLHFCQUFxQixHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXO1lBQ3RFLElBQUksRUFBRSxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxJQUFJLEdBQUcsOEJBQThCLEdBQUcsSUFBSSxDQUFDLFlBQVksR0FBRywyQkFBMkIsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxpQkFBaUIsR0FBSSxJQUFJLENBQUMsVUFBVSxHQUFHLHlCQUF5QixHQUFHLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxlQUFlLEdBQUcsSUFBSSxDQUFDLE9BQU87U0FDamxCLENBQUE7UUFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQTtRQUMxRCxDQUFDO1FBR0QsS0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLFNBQVM7WUFDL0IsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDZixLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNO29CQUM3QyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQTtnQkFDN0MsQ0FBQyxDQUFDLENBQUE7WUFDSCxDQUFDO1FBQ0YsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFBO0lBQ3hDLENBQUM7SUFFTSx3REFBeUIsR0FBaEMsVUFBaUMsWUFBWSxFQUFFLGtCQUFrQjtRQUNoRSxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDWixZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUMzQixHQUFHLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNkLGtCQUFrQixDQUFDLElBQUksQ0FDdEI7Z0JBQ0MsUUFBUSxFQUFFLEdBQUcsR0FBRyxRQUFRO2dCQUN4QixJQUFJLEVBQUUsT0FBTztnQkFDYixRQUFRLEVBQUUsV0FBVzthQUNyQixDQUFDLENBQUE7UUFFSixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUEvRlcsb0JBQW9CO1FBRGhDLGlCQUFVLEVBQUU7eUNBb0JxQiwwQkFBVyxFQUF1QiwwQkFBVyxFQUF1QiwwQkFBVyxFQUE0Qix5QkFBZ0I7T0FuQmhKLG9CQUFvQixDQW1HaEM7SUFBRCwyQkFBQztDQUFBLEFBbkdELElBbUdDO0FBbkdZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIGVtYWlsIGZyb20gXCJuYXRpdmVzY3JpcHQtZW1haWxcIjtcbmltcG9ydCB7IEpzb25TZXJ2aWNlIH0gZnJvbSBcIi4uL2pzb24vanNvbi5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBEYXRlU2VydmljZSB9IGZyb20gXCIuLi9kYXRlL2RhdGUuc2VydmljZVwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuLi91c2VyL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0ICogYXMgaW1hZ2VwaWNrZXIgZnJvbSBcIm5hdGl2ZXNjcmlwdC1pbWFnZXBpY2tlclwiO1xuaW1wb3J0ICogYXMgaW1hZ2Vzb3VyY2VNb2R1bGUgZnJvbSBcImltYWdlLXNvdXJjZVwiO1xuLy8gdmFyIGZzID0gcmVxdWlyZShcImZpbGUtc3lzdGVtXCIpO1xuXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBUZWNobmljYWxDYXJkU2VydmljZSB7XG5cblx0cHVibGljIGNvdW50ZXJTdGFydDtcblx0cHVibGljIGNvdW50ZXJTdG9wO1xuXHRwdWJsaWMgb2lsTGV2ZWwgPSBcIlwiO1xuXHRwdWJsaWMgcHVyaXR5ID0gXCJcIjtcblx0cHVibGljIGxpZ2h0aW5nID0gXCJcIjtcblx0cHVibGljIHRpcmVzID0gXCJcIjtcblx0cHVibGljIHN1c3BlbnNpb24gPSBcIlwiO1xuXHRwdWJsaWMgZW5naW5lQW5kQWNjZXNzb3JpZXMgPSBcIlwiO1xuXHRwdWJsaWMgY2FyQm9keSA9IFwiXCI7XG5cdHB1YmxpYyBjb21wb3NlT3B0aW9uczogZW1haWwuQ29tcG9zZU9wdGlvbnM7XG5cdHB1YmxpYyBpbWFnZXNQYXRocyA9IFtdO1xuXHRwdWJsaWMgYXJyYXlPZkF0dGFjaG1lbnRzID0gW107XG5cdHB1YmxpYyB0cmFpbGVyTmFtZTtcblx0cHVibGljIGNhck5hbWU7XG5cblxuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUganNvblNlcnZpY2U6IEpzb25TZXJ2aWNlLCBwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSwgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykgeyB9XG5cblxuXG5cdHB1YmxpYyBnZXRQaWN0dXJlKCkge1xuXHRcdHZhciB0aGF0ID0gdGhpcztcblx0XHRsZXQgY29udGV4dCA9IGltYWdlcGlja2VyLmNyZWF0ZSh7XG5cdFx0XHRtb2RlOiBcIm11bHRpcGxlXCIgLy8gdXNlIFwibXVsdGlwbGVcIiBmb3IgbXVsdGlwbGUgc2VsZWN0aW9uXG5cdFx0fSk7XG5cdFx0Y29udGV4dFxuXHRcdFx0LmF1dGhvcml6ZSgpXG5cdFx0XHQudGhlbihmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHJldHVybiBjb250ZXh0LnByZXNlbnQoKTtcblx0XHRcdH0pXG5cdFx0XHQudGhlbihmdW5jdGlvbiAoc2VsZWN0aW9uKSB7XG5cblx0XHRcdFx0c2VsZWN0aW9uLmZvckVhY2goZnVuY3Rpb24gKHNlbGVjdGVkKSB7XG5cdFx0XHRcdFx0dGhhdC5pbWFnZXNQYXRocy5wdXNoKHNlbGVjdGVkLmZpbGVVcmkpO1xuXHRcdFx0XHRcdGNvbnNvbGUubG9nKHNlbGVjdGVkLmZpbGVVcmkpO1xuXHRcdFx0XHRcdGNvbnNvbGUubG9nKHRoYXQuaW1hZ2VzUGF0aHMubGVuZ3RoKTtcblxuXHRcdFx0XHR9KTtcblxuXHRcdFx0fSkuY2F0Y2goZnVuY3Rpb24gKGUpIHtcblx0XHRcdFx0Ly8gcHJvY2VzcyBlcnJvclxuXHRcdFx0fSk7XG5cdH1cblxuXHRwdWJsaWMgc2VuZFRlY2huaWNhbENhcmQoKSB7XG5cdFx0aWYgKHRoaXMuaW1hZ2VzUGF0aHMubGVuZ3RoID4gMCkge1xuXHRcdFx0dGhpcy5jcmVhdGVNdWx0aXBsZUF0dGFjaGVtbnRzKHRoaXMuaW1hZ2VzUGF0aHMsIHRoaXMuYXJyYXlPZkF0dGFjaG1lbnRzKTtcblx0XHR9XG5cblx0XHRpZiAodGhpcy5qc29uU2VydmljZS50cmFpbGVyTmFtZSA9PSBudWxsKSB7XG5cdFx0XHR0aGlzLnRyYWlsZXJOYW1lID0gXCJcIlxuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLnRyYWlsZXJOYW1lID0gXCJQcnp5Y3plcGE6IFwiICsgdGhpcy5qc29uU2VydmljZS50cmFpbGVyTmFtZTtcblx0XHR9XG5cblx0XHRpZiAodGhpcy5qc29uU2VydmljZS5jYXJOYW1lID09IG51bGwpIHtcblx0XHRcdHRoaXMuY2FyTmFtZSA9IFwiXCJcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy5jYXJOYW1lID0gXCJQb2phemQ6IFwiICsgdGhpcy5qc29uU2VydmljZS5jYXJOYW1lO1xuXHRcdH1cblxuXHRcdHRoaXMuY29tcG9zZU9wdGlvbnMgPSB7XG5cdFx0XHR0bzogWydwYXdlbEBwbGV3aW5za2ktbG9naXN0aWNzLmNvbSddLFxuXHRcdFx0c3ViamVjdDogJ0thcnRhIHRlY2huaWN6bmEgLSAnICsgdGhpcy5jYXJOYW1lICsgXCIgXCIgKyB0aGlzLnRyYWlsZXJOYW1lLFxuXHRcdFx0Ym9keTogXCJLaWVyb3djYTogXCIgKyB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUgKyBcIlxcblwiICsgdGhpcy5jYXJOYW1lICsgXCIgXCIgKyB0aGlzLnRyYWlsZXJOYW1lICsgXCJcXG5EYXRhOiBcIiArIHRoaXMuZGF0ZVNlcnZpY2UucmVmdWVsaW5nRGF0ZS5kYXkgKyBcIi5cIiArIHRoaXMuZGF0ZVNlcnZpY2UucmVmdWVsaW5nRGF0ZS5tb250aCArIFwiLlwiICsgdGhpcy5kYXRlU2VydmljZS5yZWZ1ZWxpbmdEYXRlLnllYXIgKyBcIlxcblBvY3rEhXRrb3d5IHN0YW4gbGljem5pa2E6IFwiICsgdGhpcy5jb3VudGVyU3RhcnQgKyBcIlxcbktvxYRjb3d5IHN0YW4gbGljem5pa2E6IFwiICsgdGhpcy5jb3VudGVyU3RvcCArIFwiXFxuU3RhbiBvbGVqdTogXCIgKyB0aGlzLm9pbExldmVsICsgXCJcXG5DenlzdG/Fm8SHOiBcIiArIHRoaXMucHVyaXR5ICsgXCJcXG5PxZt3aWV0bGVuaWU6IFwiICsgdGhpcy5saWdodGluZyArIFwiXFxuT3Bvbnk6IFwiICsgdGhpcy50aXJlcyArIFwiXFxuWmF3aWVzemVuaWU6IFwiICsgIHRoaXMuc3VzcGVuc2lvbiArIFwiXFxuU2lsbmlrIG9yYXogb3NwcnrEmXQ6IFwiICsgdGhpcy5lbmdpbmVBbmRBY2Nlc3NvcmllcyArIFwiXFxuS2Fyb3NlcmlhOiBcIiArIHRoaXMuY2FyQm9keVxuXHRcdH1cblx0XHRpZiAodGhpcy5pbWFnZXNQYXRocy5sZW5ndGggPiAwKSB7XG5cdFx0XHR0aGlzLmNvbXBvc2VPcHRpb25zLmF0dGFjaG1lbnRzID0gdGhpcy5hcnJheU9mQXR0YWNobWVudHNcblx0XHR9XG5cblxuXHRcdGVtYWlsLmF2YWlsYWJsZSgpLnRoZW4oYXZhaWxhYmxlID0+IHtcblx0XHRcdGlmIChhdmFpbGFibGUpIHtcblx0XHRcdFx0ZW1haWwuY29tcG9zZSh0aGlzLmNvbXBvc2VPcHRpb25zKS50aGVuKHJlc3VsdCA9PiB7XG5cdFx0XHRcdFx0dGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9uYXZwYWdlXCJdKVxuXHRcdFx0XHR9KVxuXHRcdFx0fVxuXHRcdH0pLmNhdGNoKGVycm9yID0+IGNvbnNvbGUuZXJyb3IoZXJyb3IpKVxuXHR9XG5cblx0cHVibGljIGNyZWF0ZU11bHRpcGxlQXR0YWNoZW1udHMoYXJyYXlPZkxpbmtzLCBhcnJheU9mQXR0YWNobWVudHMpIHtcblx0XHRsZXQgbnVtID0gMDtcblx0XHRhcnJheU9mTGlua3MuZm9yRWFjaChlbGVtZW50ID0+IHtcblx0XHRcdG51bSA9IG51bSArIDE7XG5cdFx0XHRhcnJheU9mQXR0YWNobWVudHMucHVzaChcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGZpbGVOYW1lOiBudW0gKyAna3QucG5nJyxcblx0XHRcdFx0XHRwYXRoOiBlbGVtZW50LFxuXHRcdFx0XHRcdG1pbWVUeXBlOiAnaW1hZ2UvcG5nJ1xuXHRcdFx0XHR9KVxuXG5cdFx0fSk7XG5cdH1cblxuXG5cbn1cbiJdfQ==