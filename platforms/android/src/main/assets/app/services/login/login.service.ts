import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { GlobalService } from "../global/global.service";
import * as  base64 from "base-64";
import * as utf8 from "utf8";

@Injectable()
export class LoginService {

	public loginUrl: string = this.globalService.keycloakUrl + "/auth/realms/Logistics/protocol/openid-connect/token";
	public logoutUrl = this.globalService.keycloakUrl + "/auth/realms/Logistics/protocol/openid-connect/logout";
	public headers = new Headers({ "Content-Type": "application/x-www-form-urlencoded" });
	public options = new RequestOptions({ headers: this.headers });

	constructor(private http: Http, private globalService: GlobalService) { }

	public login(body) {
		return this.http.post(this.loginUrl, encodeURI(body), this.options);
	}

}
