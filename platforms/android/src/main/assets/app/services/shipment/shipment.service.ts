import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { UserService } from '../user/user.service';
import { UrlService } from '../urlService/url.service';

@Injectable()
export class ShipmentService {

  constructor(private http: Http, private userService: UserService, private urlService: UrlService) { }

  public getShipmentForDriverByDrivernameAndNumber(number) {
    let headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
    let options = new RequestOptions({ headers: headers });
    let url = this.urlService.apiUrl + "/driver/api/v1/shipments/ship/" + encodeURI(this.userService.driverName.toLowerCase()) + "/" + number;
    return this.http.get(url, options);
  }

  public getShipmentForDriverByNumber(number) {
    let headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
    let options = new RequestOptions({ headers: headers });
    let url = this.urlService.apiUrl + "/driver/api/v1/shipments/ship/" + number;
    console.log(url);
    return this.http.get(url, options);
  }

  public updateDate(shipmentWithNewDate){
    let headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token,  "Content-Type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    let url = this.urlService.apiUrl + "/driver/api/v1/shipments/";
    return this.http.put(url, shipmentWithNewDate, options);
  }

}
