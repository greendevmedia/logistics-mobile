import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {


	public grant_type: string= "password";
	public client_id: string= "Logistics";
	public keycloakUrl: string= "https://www.keycloak.plewinski-logistics.com:5513";
	public client_secret: string = "92a5e14c-0588-43ca-96ef-921003eb2bdd";
	public driversList = ["Włodzimierz Fertyk", "Mirosław Tecław" ,"Leszek Tecław" ,"Krzysiek Chojnacki" ,"Adam Ciechański"
	,"Paweł Plewiński" ,"Adam Wilczyński" ,"Tomasz Czekała", "Vadzim Neplashau"];
	public paymentTypesList = ["CASH", "DKV"];
	public repairActivityList = ["DELIVERY", "RECEPTION"]
	public activityIndicator: boolean;
	public loginFormVisibility: boolean = true;
	public carAddLink;
	public carAddTitle;
	public isTrailersListVisible = false;

	constructor() { }

}
