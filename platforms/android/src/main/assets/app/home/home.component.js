"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var login_service_1 = require("../services/login/login.service");
var global_service_1 = require("../services/global/global.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var application_settings_1 = require("application-settings");
var nativescript_ngx_fonticon_1 = require("nativescript-ngx-fonticon");
var HomeComponent = (function () {
    function HomeComponent(loginService, globalService, userService, routerExtensions, page, fonticon) {
        this.loginService = loginService;
        this.globalService = globalService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.fonticon = fonticon;
        this.isDriverPicked = false;
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.userService.driverName = null;
        this.globalService.activityIndicator = false;
        this.page.actionBarHidden = true;
        this.globalService.loginFormVisibility = true;
        this.automaticLogin();
    };
    HomeComponent.prototype.selectedIndexChanged = function (args) {
        var picker = args.object;
        this.userService.driverName = this.globalService.driversList[picker.selectedIndex];
        this.isDriverPicked = true;
        this.userService.driverId = picker.selectedIndex + 1;
        application_settings_1.setNumber("driverId", this.userService.driverId);
    };
    HomeComponent.prototype.login = function () {
        var _this = this;
        this.globalService.loginFormVisibility = false;
        this.globalService.activityIndicator = true;
        var driverFirstSecondName = this.userService.driverName.split(" ", 2);
        var firstName = driverFirstSecondName[0].charAt(0).toUpperCase() + driverFirstSecondName[0].slice(1).toLowerCase();
        var secondName = driverFirstSecondName[1].charAt(0).toUpperCase() + driverFirstSecondName[1].slice(1).toLowerCase();
        this.userService.driverName = firstName + " " + secondName;
        var index = 0;
        for (var _i = 0, _a = this.globalService.driversList; _i < _a.length; _i++) {
            var driver = _a[_i];
            index += 1;
            if (driver === this.userService.driverName) {
                break;
            }
        }
        application_settings_1.setString("username", this.userService.driverName);
        application_settings_1.setString("pin", this.pin);
        application_settings_1.setNumber("driverId", index);
        console.log(application_settings_1.getString("username"));
        console.log(application_settings_1.getNumber("driverId"));
        console.log(application_settings_1.getString("pin"));
        this.body = this.body = "username=" + this.userService.driverName + '&password=' + this.pin + '&client_id='
            + this.globalService.client_id + '&client_secret=' + this.globalService.client_secret + '&grant_type=' + this.globalService.grant_type;
        this.loginService.login(this.body).subscribe(function (res) { _this.userService.userApiAccessData = res, _this.userService.isUserLogged = true, _this.routerExtensions.navigate(["/navpage"], { clearHistory: true }), application_settings_1.setString("username", _this.userService.driverName), application_settings_1.setString("pin", _this.pin), _this.pin = ""; }, function (err) {
            alert({
                title: "Błąd",
                message: "Wprowadź poprawny numer PIN do wybranego kierowcy",
                okButtonText: "OK"
            }),
                _this.userService.userApiAccessData = null;
            _this.userService.isUserLogged = false;
            _this.globalService.activityIndicator = false;
            _this.globalService.loginFormVisibility = true;
        });
    };
    HomeComponent.prototype.automaticLogin = function () {
        if (application_settings_1.getString("username") !== undefined && application_settings_1.getString("pin") !== undefined) {
            this.userService.driverName = application_settings_1.getString("username");
            this.pin = application_settings_1.getString("pin");
            this.login();
        }
    };
    HomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-home',
            templateUrl: './home.component.html',
            styleUrls: ['./home.component.scss']
        }),
        __metadata("design:paramtypes", [login_service_1.LoginService, global_service_1.GlobalService, user_service_1.UserService,
            router_1.RouterExtensions, page_1.Page, nativescript_ngx_fonticon_1.TNSFontIconService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUdsRCxpRUFBK0Q7QUFDL0Qsb0VBQWtFO0FBQ2xFLDhEQUE0RDtBQUM1RCxzREFBK0Q7QUFFL0QsZ0NBQStCO0FBQy9CLDZEQUs4QjtBQUM5Qix1RUFBK0Q7QUFTL0Q7SUFNQyx1QkFBb0IsWUFBMEIsRUFBVSxhQUE0QixFQUFVLFdBQXdCLEVBQzdHLGdCQUFrQyxFQUFVLElBQVUsRUFBVSxRQUE0QjtRQURqRixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDN0cscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFvQjtRQUg5RixtQkFBYyxHQUFZLEtBQUssQ0FBQztJQUl2QyxDQUFDO0lBRUQsZ0NBQVEsR0FBUjtRQUNDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztRQUM3QyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDOUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBRXZCLENBQUM7SUFFTSw0Q0FBb0IsR0FBM0IsVUFBNEIsSUFBSTtRQUMvQixJQUFJLE1BQU0sR0FBZSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUNyRCxnQ0FBUyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFTSw2QkFBSyxHQUFaO1FBQUEsaUJBeUNDO1FBdkNBLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1FBQy9DLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQzVDLElBQUkscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN0RSxJQUFJLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25ILElBQUksVUFBVSxHQUFHLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsR0FBRyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDcEgsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsU0FBUyxHQUFFLEdBQUcsR0FBRSxVQUFVLENBQUM7UUFDekQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsR0FBRyxDQUFBLENBQWUsVUFBOEIsRUFBOUIsS0FBQSxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBOUIsY0FBOEIsRUFBOUIsSUFBOEI7WUFBNUMsSUFBSSxNQUFNLFNBQUE7WUFDYixLQUFLLElBQUUsQ0FBQyxDQUFDO1lBQ1QsRUFBRSxDQUFBLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUEsQ0FBQztnQkFDMUMsS0FBSyxDQUFDO1lBQ1AsQ0FBQztTQUNEO1FBQ0QsZ0NBQVMsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNuRCxnQ0FBUyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsZ0NBQVMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFHOUIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxZQUFZLEdBQUcsSUFBSSxDQUFDLEdBQUcsR0FBRyxhQUFhO2NBQ3hHLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLGlCQUFpQixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxHQUFHLGNBQWMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQztRQUV4SSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUMzQyxVQUFDLEdBQUcsSUFBTyxLQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixHQUFHLEdBQUcsRUFBRSxLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksR0FBRyxJQUFJLEVBQUUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsZ0NBQVMsQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsRUFBRSxnQ0FBUyxDQUFDLEtBQUssRUFBRSxLQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUEsQ0FBQyxDQUFDLEVBQ2hRLFVBQUMsR0FBRztZQUNILEtBQUssQ0FBQztnQkFDTCxLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsbURBQW1EO2dCQUM1RCxZQUFZLEVBQUUsSUFBSTthQUNsQixDQUFDO2dCQUVELEtBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzNDLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUN0QyxLQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztZQUM3QyxLQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUMvQyxDQUFDLENBQ0QsQ0FBQTtJQUNGLENBQUM7SUFFTSxzQ0FBYyxHQUFyQjtRQUNDLEVBQUUsQ0FBQyxDQUFDLGdDQUFTLENBQUMsVUFBVSxDQUFDLEtBQUssU0FBUyxJQUFJLGdDQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUMzRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxnQ0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BELElBQUksQ0FBQyxHQUFHLEdBQUcsZ0NBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDZCxDQUFDO0lBQ0YsQ0FBQztJQTVFVyxhQUFhO1FBTnpCLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsV0FBVyxFQUFFLHVCQUF1QjtZQUNwQyxTQUFTLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztTQUNwQyxDQUFDO3lDQU9pQyw0QkFBWSxFQUF5Qiw4QkFBYSxFQUF1QiwwQkFBVztZQUMzRix5QkFBZ0IsRUFBZ0IsV0FBSSxFQUFvQiw4Q0FBa0I7T0FQekYsYUFBYSxDQWdGekI7SUFBRCxvQkFBQztDQUFBLEFBaEZELElBZ0ZDO0FBaEZZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2Zvcm1zXCI7XG5pbXBvcnQgeyBMaXN0UGlja2VyIH0gZnJvbSBcInVpL2xpc3QtcGlja2VyXCI7XG5pbXBvcnQgeyBMb2dpblNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvbG9naW4vbG9naW4uc2VydmljZVwiO1xuaW1wb3J0IHsgR2xvYmFsU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy9nbG9iYWwvZ2xvYmFsLnNlcnZpY2VcIjtcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgU2VsZWN0ZWRJbmRleENoYW5nZWRFdmVudERhdGEgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWRyb3AtZG93blwiO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XG5pbXBvcnQge1xuXHRnZXRTdHJpbmcsXG5cdHNldFN0cmluZyxcblx0c2V0TnVtYmVyLFxuXHRnZXROdW1iZXJcbn0gZnJvbSBcImFwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5pbXBvcnQgeyBUTlNGb250SWNvblNlcnZpY2UgfSBmcm9tICduYXRpdmVzY3JpcHQtbmd4LWZvbnRpY29uJztcblxuXG5AQ29tcG9uZW50KHtcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0c2VsZWN0b3I6ICdhcHAtaG9tZScsXG5cdHRlbXBsYXRlVXJsOiAnLi9ob21lLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vaG9tZS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG5cdHB1YmxpYyBwaW47XG5cdHB1YmxpYyBib2R5O1xuXHRwdWJsaWMgaXNEcml2ZXJQaWNrZWQ6IGJvb2xlYW4gPSBmYWxzZTtcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGxvZ2luU2VydmljZTogTG9naW5TZXJ2aWNlLCBwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLFxuXHRcdHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucywgcHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIGZvbnRpY29uOiBUTlNGb250SWNvblNlcnZpY2UpIHtcblx0fVxuXG5cdG5nT25Jbml0KCk6IHZvaWQge1xuXHRcdHRoaXMudXNlclNlcnZpY2UuZHJpdmVyTmFtZSA9IG51bGw7XG5cdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmFjdGl2aXR5SW5kaWNhdG9yID0gZmFsc2U7XG5cdFx0dGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG5cdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmxvZ2luRm9ybVZpc2liaWxpdHkgPSB0cnVlO1xuXHRcdHRoaXMuYXV0b21hdGljTG9naW4oKTtcblxuXHR9XG5cblx0cHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkKGFyZ3MpIHtcblx0XHRsZXQgcGlja2VyID0gPExpc3RQaWNrZXI+YXJncy5vYmplY3Q7XG5cdFx0dGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lID0gdGhpcy5nbG9iYWxTZXJ2aWNlLmRyaXZlcnNMaXN0W3BpY2tlci5zZWxlY3RlZEluZGV4XTtcblx0XHR0aGlzLmlzRHJpdmVyUGlja2VkID0gdHJ1ZTtcblx0XHR0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlcklkID0gcGlja2VyLnNlbGVjdGVkSW5kZXggKyAxO1xuXHRcdHNldE51bWJlcihcImRyaXZlcklkXCIsIHRoaXMudXNlclNlcnZpY2UuZHJpdmVySWQpO1xuXHR9XG5cblx0cHVibGljIGxvZ2luKCkge1xuXHRcdFxuXHRcdHRoaXMuZ2xvYmFsU2VydmljZS5sb2dpbkZvcm1WaXNpYmlsaXR5ID0gZmFsc2U7XG5cdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmFjdGl2aXR5SW5kaWNhdG9yID0gdHJ1ZTtcblx0XHRsZXQgZHJpdmVyRmlyc3RTZWNvbmROYW1lID0gdGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lLnNwbGl0KFwiIFwiLCAyKTtcblx0XHRsZXQgZmlyc3ROYW1lID0gZHJpdmVyRmlyc3RTZWNvbmROYW1lWzBdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgZHJpdmVyRmlyc3RTZWNvbmROYW1lWzBdLnNsaWNlKDEpLnRvTG93ZXJDYXNlKCk7XG5cdFx0bGV0IHNlY29uZE5hbWUgPSBkcml2ZXJGaXJzdFNlY29uZE5hbWVbMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBkcml2ZXJGaXJzdFNlY29uZE5hbWVbMV0uc2xpY2UoMSkudG9Mb3dlckNhc2UoKTtcblx0XHR0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUgPSBmaXJzdE5hbWUgK1wiIFwiKyBzZWNvbmROYW1lO1xuXHRcdGxldCBpbmRleCA9IDA7XG5cdFx0Zm9yKGxldCBkcml2ZXIgb2YgdGhpcy5nbG9iYWxTZXJ2aWNlLmRyaXZlcnNMaXN0KXtcblx0XHRcdGluZGV4Kz0xO1xuXHRcdFx0aWYoZHJpdmVyID09PSB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUpe1xuXHRcdFx0XHRicmVhaztcblx0XHRcdH1cblx0XHR9XG5cdFx0c2V0U3RyaW5nKFwidXNlcm5hbWVcIiwgdGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lKTtcblx0XHRzZXRTdHJpbmcoXCJwaW5cIiwgdGhpcy5waW4pO1xuXHRcdHNldE51bWJlcihcImRyaXZlcklkXCIsIGluZGV4KTtcblx0XHRjb25zb2xlLmxvZyhnZXRTdHJpbmcoXCJ1c2VybmFtZVwiKSk7XG5cdFx0Y29uc29sZS5sb2coZ2V0TnVtYmVyKFwiZHJpdmVySWRcIikpO1xuXHRcdGNvbnNvbGUubG9nKGdldFN0cmluZyhcInBpblwiKSk7XG5cdFx0XG5cblx0XHR0aGlzLmJvZHkgPSB0aGlzLmJvZHkgPSBgdXNlcm5hbWU9YCArIHRoaXMudXNlclNlcnZpY2UuZHJpdmVyTmFtZSArICcmcGFzc3dvcmQ9JyArIHRoaXMucGluICsgJyZjbGllbnRfaWQ9J1xuXHRcdFx0KyB0aGlzLmdsb2JhbFNlcnZpY2UuY2xpZW50X2lkICsgJyZjbGllbnRfc2VjcmV0PScgKyB0aGlzLmdsb2JhbFNlcnZpY2UuY2xpZW50X3NlY3JldCArICcmZ3JhbnRfdHlwZT0nICsgdGhpcy5nbG9iYWxTZXJ2aWNlLmdyYW50X3R5cGU7XG5cblx0XHR0aGlzLmxvZ2luU2VydmljZS5sb2dpbih0aGlzLmJvZHkpLnN1YnNjcmliZShcblx0XHRcdChyZXMpID0+IHsgdGhpcy51c2VyU2VydmljZS51c2VyQXBpQWNjZXNzRGF0YSA9IHJlcywgdGhpcy51c2VyU2VydmljZS5pc1VzZXJMb2dnZWQgPSB0cnVlLCB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL25hdnBhZ2VcIl0sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pLCBzZXRTdHJpbmcoXCJ1c2VybmFtZVwiLCB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUpLCBzZXRTdHJpbmcoXCJwaW5cIiwgdGhpcy5waW4pLCB0aGlzLnBpbiA9IFwiXCIgfSxcblx0XHRcdChlcnIpID0+IHtcblx0XHRcdFx0YWxlcnQoe1xuXHRcdFx0XHRcdHRpdGxlOiBcIkLFgsSFZFwiLFxuXHRcdFx0XHRcdG1lc3NhZ2U6IFwiV3Byb3dhZMW6IHBvcHJhd255IG51bWVyIFBJTiBkbyB3eWJyYW5lZ28ga2llcm93Y3lcIixcblx0XHRcdFx0XHRva0J1dHRvblRleHQ6IFwiT0tcIlxuXHRcdFx0XHR9KSxcblxuXHRcdFx0XHRcdHRoaXMudXNlclNlcnZpY2UudXNlckFwaUFjY2Vzc0RhdGEgPSBudWxsO1xuXHRcdFx0XHR0aGlzLnVzZXJTZXJ2aWNlLmlzVXNlckxvZ2dlZCA9IGZhbHNlO1xuXHRcdFx0XHR0aGlzLmdsb2JhbFNlcnZpY2UuYWN0aXZpdHlJbmRpY2F0b3IgPSBmYWxzZTtcblx0XHRcdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmxvZ2luRm9ybVZpc2liaWxpdHkgPSB0cnVlO1xuXHRcdFx0fVxuXHRcdClcblx0fVxuXG5cdHB1YmxpYyBhdXRvbWF0aWNMb2dpbigpIHtcblx0XHRpZiAoZ2V0U3RyaW5nKFwidXNlcm5hbWVcIikgIT09IHVuZGVmaW5lZCAmJiBnZXRTdHJpbmcoXCJwaW5cIikgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0dGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lID0gZ2V0U3RyaW5nKFwidXNlcm5hbWVcIik7XG5cdFx0XHR0aGlzLnBpbiA9IGdldFN0cmluZyhcInBpblwiKTtcblx0XHRcdHRoaXMubG9naW4oKTtcblx0XHR9XG5cdH1cblxuXG5cbn1cbiJdfQ==