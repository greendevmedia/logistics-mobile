"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var repair_service_1 = require("../services/repair/repair.service");
var date_service_1 = require("../services/date/date.service");
var user_service_1 = require("../services/user/user.service");
var json_service_1 = require("../services/json/json.service");
var router_1 = require("nativescript-angular/router");
var Repair2Component = (function () {
    function Repair2Component(page, repairService, dateService, userService, jsonService, routerExtensions) {
        this.page = page;
        this.repairService = repairService;
        this.dateService = dateService;
        this.userService = userService;
        this.jsonService = jsonService;
        this.routerExtensions = routerExtensions;
    }
    Repair2Component.prototype.ngOnInit = function () {
        this.isBusy = false;
    };
    Repair2Component.prototype.addService = function () {
        var _this = this;
        this.isBusy = true;
        this.repairService.repairJSONforPOST = {
            repairDate: this.dateService.dateFormatForPost(),
            counter: this.repairService.counter,
            price: this.repairService.price,
            repairType: this.repairService.repairType,
            driverName: this.userService.driverName,
            carShopAddress: this.repairService.carShopAddress,
            activityType: this.repairService.activityType,
            vehicalCheck: this.repairService.vehicalCheck
        };
        if (this.jsonService.pickedCarId > 0) {
            this.repairService.repairJSONforPOST.car = { id: this.jsonService.correctPickedCarId };
        }
        if (this.jsonService.pickedTrailerId > 0) {
            this.repairService.repairJSONforPOST.trailer = { id: this.jsonService.pickedTrailerId };
        }
        this.repairService.addService(this.repairService.repairJSONforPOST).subscribe(function (res) {
            _this.isBusy = false;
            alert({
                title: "Potwierdzenie",
                message: "Serwis dodany poprawnie",
                okButtonText: "OK"
            }), _this.repairService.sendService();
        }, function (err) {
            _this.isBusy = false;
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    Repair2Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-repair-2',
            templateUrl: './repair-2.component.html',
            styleUrls: ['./repair-2.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, repair_service_1.RepairService, date_service_1.DateService, user_service_1.UserService, json_service_1.JsonService, router_1.RouterExtensions])
    ], Repair2Component);
    return Repair2Component;
}());
exports.Repair2Component = Repair2Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVwYWlyLTIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVwYWlyLTIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHNEQUFxRDtBQUNyRCxvRUFBa0U7QUFDbEUsOERBQTREO0FBQzVELDhEQUE0RDtBQUM1RCw4REFBNEQ7QUFDNUQsc0RBQStEO0FBUS9EO0lBSUUsMEJBQW9CLElBQVUsRUFBVSxhQUE0QixFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSxXQUF3QixFQUFVLGdCQUFrQztRQUFsTSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO0lBQUksQ0FBQztJQUUzTixtQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVNLHFDQUFVLEdBQWpCO1FBQUEsaUJBdUNDO1FBdENDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLEdBQUc7WUFDckMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEVBQUU7WUFDaEQsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTztZQUNuQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLO1lBQy9CLFVBQVUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVU7WUFDekMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtZQUN2QyxjQUFjLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjO1lBQ2pELFlBQVksRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVk7WUFDN0MsWUFBWSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWTtTQUM5QyxDQUFBO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsR0FBRyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixFQUFFLENBQUE7UUFDeEYsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsQ0FBQTtRQUN6RixDQUFDO1FBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLFNBQVMsQ0FDM0UsVUFBQyxHQUFHO1lBQ0osS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDbEIsS0FBSyxDQUFDO2dCQUNKLEtBQUssRUFBRSxlQUFlO2dCQUN0QixPQUFPLEVBQUUseUJBQXlCO2dCQUNsQyxZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDLEVBQUUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtRQUN0QyxDQUFDLEVBQ0QsVUFBQyxHQUFHO1lBQ0osS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDbEIsS0FBSyxDQUFDO2dCQUNKLEtBQUssRUFBRSxNQUFNO2dCQUNiLE9BQU8sRUFBRSxnQ0FBZ0M7Z0JBQ3pDLFlBQVksRUFBRSxJQUFJO2FBQ25CLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FDRixDQUFDO0lBQ0osQ0FBQztJQWpEVSxnQkFBZ0I7UUFONUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsY0FBYztZQUN4QixXQUFXLEVBQUUsMkJBQTJCO1lBQ3hDLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO1NBQ3pDLENBQUM7eUNBSzBCLFdBQUksRUFBeUIsOEJBQWEsRUFBdUIsMEJBQVcsRUFBdUIsMEJBQVcsRUFBdUIsMEJBQVcsRUFBNEIseUJBQWdCO09BSjNNLGdCQUFnQixDQW1ENUI7SUFBRCx1QkFBQztDQUFBLEFBbkRELElBbURDO0FBbkRZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2UnO1xuaW1wb3J0IHsgUmVwYWlyU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3JlcGFpci9yZXBhaXIuc2VydmljZSc7XG5pbXBvcnQgeyBEYXRlU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2RhdGUvZGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdXNlci91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgSnNvblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9qc29uL2pzb24uc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuXG5AQ29tcG9uZW50KHtcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgc2VsZWN0b3I6ICdhcHAtcmVwYWlyLTInLFxuICB0ZW1wbGF0ZVVybDogJy4vcmVwYWlyLTIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9yZXBhaXItMi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFJlcGFpcjJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHByaXZhdGUgaXNCdXN5O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSByZXBhaXJTZXJ2aWNlOiBSZXBhaXJTZXJ2aWNlLCBwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSwgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsIHByaXZhdGUganNvblNlcnZpY2U6IEpzb25TZXJ2aWNlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuaXNCdXN5ID0gZmFsc2U7XG4gIH1cblxuICBwdWJsaWMgYWRkU2VydmljZSgpIHtcbiAgICB0aGlzLmlzQnVzeSA9IHRydWU7XG4gICAgdGhpcy5yZXBhaXJTZXJ2aWNlLnJlcGFpckpTT05mb3JQT1NUID0ge1xuICAgICAgcmVwYWlyRGF0ZTogdGhpcy5kYXRlU2VydmljZS5kYXRlRm9ybWF0Rm9yUG9zdCgpLFxuICAgICAgY291bnRlcjogdGhpcy5yZXBhaXJTZXJ2aWNlLmNvdW50ZXIsXG4gICAgICBwcmljZTogdGhpcy5yZXBhaXJTZXJ2aWNlLnByaWNlLFxuICAgICAgcmVwYWlyVHlwZTogdGhpcy5yZXBhaXJTZXJ2aWNlLnJlcGFpclR5cGUsXG4gICAgICBkcml2ZXJOYW1lOiB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUsXG4gICAgICBjYXJTaG9wQWRkcmVzczogdGhpcy5yZXBhaXJTZXJ2aWNlLmNhclNob3BBZGRyZXNzLFxuICAgICAgYWN0aXZpdHlUeXBlOiB0aGlzLnJlcGFpclNlcnZpY2UuYWN0aXZpdHlUeXBlLFxuICAgICAgdmVoaWNhbENoZWNrOiB0aGlzLnJlcGFpclNlcnZpY2UudmVoaWNhbENoZWNrXG4gICAgfVxuXG4gICAgaWYgKHRoaXMuanNvblNlcnZpY2UucGlja2VkQ2FySWQgPiAwKSB7XG4gICAgICB0aGlzLnJlcGFpclNlcnZpY2UucmVwYWlySlNPTmZvclBPU1QuY2FyID0geyBpZDogdGhpcy5qc29uU2VydmljZS5jb3JyZWN0UGlja2VkQ2FySWQgfVxuICAgIH1cblxuICAgIGlmICh0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZFRyYWlsZXJJZCA+IDApIHtcbiAgICAgIHRoaXMucmVwYWlyU2VydmljZS5yZXBhaXJKU09OZm9yUE9TVC50cmFpbGVyID0geyBpZDogdGhpcy5qc29uU2VydmljZS5waWNrZWRUcmFpbGVySWQgfVxuICAgIH1cblxuICAgIHRoaXMucmVwYWlyU2VydmljZS5hZGRTZXJ2aWNlKHRoaXMucmVwYWlyU2VydmljZS5yZXBhaXJKU09OZm9yUE9TVCkuc3Vic2NyaWJlKFxuICAgICAgKHJlcykgPT4ge1xuICAgICAgdGhpcy5pc0J1c3kgPSBmYWxzZTtcbiAgICAgICAgYWxlcnQoe1xuICAgICAgICAgIHRpdGxlOiBcIlBvdHdpZXJkemVuaWVcIixcbiAgICAgICAgICBtZXNzYWdlOiBcIlNlcndpcyBkb2RhbnkgcG9wcmF3bmllXCIsXG4gICAgICAgICAgb2tCdXR0b25UZXh0OiBcIk9LXCJcbiAgICAgICAgfSksIHRoaXMucmVwYWlyU2VydmljZS5zZW5kU2VydmljZSgpXG4gICAgICB9LFxuICAgICAgKGVycikgPT4ge1xuICAgICAgdGhpcy5pc0J1c3kgPSBmYWxzZTtcbiAgICAgICAgYWxlcnQoe1xuICAgICAgICAgIHRpdGxlOiBcIkLFgsSFZFwiLFxuICAgICAgICAgIG1lc3NhZ2U6IFwiUG9wcmF3IGRhbmUgaSBzcHLDs2J1aiBwb25vd25pZVwiLFxuICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJPS1wiXG4gICAgICAgIH0pXG4gICAgICB9LFxuICAgICk7XG4gIH1cblxufSJdfQ==