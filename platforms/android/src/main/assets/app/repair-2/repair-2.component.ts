import { Component, OnInit } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page/page';
import { RepairService } from '../services/repair/repair.service';
import { DateService } from '../services/date/date.service';
import { UserService } from '../services/user/user.service';
import { JsonService } from '../services/json/json.service';
import { RouterExtensions } from "nativescript-angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-repair-2',
  templateUrl: './repair-2.component.html',
  styleUrls: ['./repair-2.component.scss']
})
export class Repair2Component implements OnInit {

  private isBusy;

  constructor(private page: Page, private repairService: RepairService, private dateService: DateService, private userService: UserService, private jsonService: JsonService, private routerExtensions: RouterExtensions) { }

  ngOnInit() {
    this.isBusy = false;
  }

  public addService() {
    this.isBusy = true;
    this.repairService.repairJSONforPOST = {
      repairDate: this.dateService.dateFormatForPost(),
      counter: this.repairService.counter,
      price: this.repairService.price,
      repairType: this.repairService.repairType,
      driverName: this.userService.driverName,
      carShopAddress: this.repairService.carShopAddress,
      activityType: this.repairService.activityType,
      vehicalCheck: this.repairService.vehicalCheck
    }

    if (this.jsonService.pickedCarId > 0) {
      this.repairService.repairJSONforPOST.car = { id: this.jsonService.correctPickedCarId }
    }

    if (this.jsonService.pickedTrailerId > 0) {
      this.repairService.repairJSONforPOST.trailer = { id: this.jsonService.pickedTrailerId }
    }

    this.repairService.addService(this.repairService.repairJSONforPOST).subscribe(
      (res) => {
      this.isBusy = false;
        alert({
          title: "Potwierdzenie",
          message: "Serwis dodany poprawnie",
          okButtonText: "OK"
        }), this.repairService.sendService()
      },
      (err) => {
      this.isBusy = false;
        alert({
          title: "Błąd",
          message: "Popraw dane i spróbuj ponownie",
          okButtonText: "OK"
        })
      },
    );
  }

}