"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var date_service_1 = require("../services/date/date.service");
var additional_payment_service_1 = require("../services/additional-payment/additional-payment.service");
var json_service_1 = require("../services/json/json.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var list_service_1 = require("../services/list/list.service");
var AddiionalPaymentDetailsComponent = (function () {
    function AddiionalPaymentDetailsComponent(page, dateService, additionalPaymentService, userService, routerExtensions, listService, jsonService) {
        this.page = page;
        this.dateService = dateService;
        this.additionalPaymentService = additionalPaymentService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.listService = listService;
        this.jsonService = jsonService;
    }
    AddiionalPaymentDetailsComponent.prototype.ngOnInit = function () {
        this.data = this.additionalPaymentService.additionalPaymentJSONforPOST;
        this.page.actionBarHidden = true;
    };
    AddiionalPaymentDetailsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-addiional-payment-details',
            templateUrl: './addiional-payment-details.component.html',
            styleUrls: ['./addiional-payment-details.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, date_service_1.DateService, additional_payment_service_1.AdditionalPaymentService, user_service_1.UserService, router_1.RouterExtensions, list_service_1.ListService, json_service_1.JsonService])
    ], AddiionalPaymentDetailsComponent);
    return AddiionalPaymentDetailsComponent;
}());
exports.AddiionalPaymentDetailsComponent = AddiionalPaymentDetailsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkaWlvbmFsLXBheW1lbnQtZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhZGRpaW9uYWwtcGF5bWVudC1kZXRhaWxzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxnQ0FBK0I7QUFDL0IsOERBQTREO0FBQzVELHdHQUFxRztBQUNyRyw4REFBNEQ7QUFDNUQsOERBQTREO0FBQzVELHNEQUErRDtBQUMvRCw4REFBNEQ7QUFRNUQ7SUFJQywwQ0FBb0IsSUFBVSxFQUFVLFdBQXdCLEVBQVUsd0JBQWtELEVBQVMsV0FBd0IsRUFBVSxnQkFBa0MsRUFBVSxXQUF3QixFQUFVLFdBQXdCO1FBQXpQLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFBUyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBQ3pRLENBQUM7SUFFTCxtREFBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsNEJBQTRCLENBQUM7UUFDdkUsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBQ2xDLENBQUM7SUFWVyxnQ0FBZ0M7UUFONUMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsK0JBQStCO1lBQ3pDLFdBQVcsRUFBRSw0Q0FBNEM7WUFDekQsU0FBUyxFQUFFLENBQUMsNENBQTRDLENBQUM7U0FDekQsQ0FBQzt5Q0FLeUIsV0FBSSxFQUF1QiwwQkFBVyxFQUFvQyxxREFBd0IsRUFBc0IsMEJBQVcsRUFBNEIseUJBQWdCLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXO09BSmpRLGdDQUFnQyxDQVk1QztJQUFELHVDQUFDO0NBQUEsQUFaRCxJQVlDO0FBWlksNEVBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0IHsgRGF0ZVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9kYXRlL2RhdGUuc2VydmljZSc7XG5pbXBvcnQgeyBBZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hZGRpdGlvbmFsLXBheW1lbnQvYWRkaXRpb25hbC1wYXltZW50LnNlcnZpY2UnO1xuaW1wb3J0IHsgSnNvblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9qc29uL2pzb24uc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBMaXN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2xpc3QvbGlzdC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXG5cdHNlbGVjdG9yOiAnYXBwLWFkZGlpb25hbC1wYXltZW50LWRldGFpbHMnLFxuXHR0ZW1wbGF0ZVVybDogJy4vYWRkaWlvbmFsLXBheW1lbnQtZGV0YWlscy5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL2FkZGlpb25hbC1wYXltZW50LWRldGFpbHMuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBBZGRpaW9uYWxQYXltZW50RGV0YWlsc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cdFxuXHRwdWJsaWMgZGF0YTtcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgZGF0ZVNlcnZpY2U6IERhdGVTZXJ2aWNlLCBwcml2YXRlIGFkZGl0aW9uYWxQYXltZW50U2VydmljZTogQWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsIHByaXZhdGUgbGlzdFNlcnZpY2U6IExpc3RTZXJ2aWNlLCBwcml2YXRlIGpzb25TZXJ2aWNlOiBKc29uU2VydmljZVxuXHQpIHsgfVxuXG5cdG5nT25Jbml0KCkgeyBcblx0XHR0aGlzLmRhdGEgPSB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5hZGRpdGlvbmFsUGF5bWVudEpTT05mb3JQT1NUO1xuXHRcdHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xuXHR9XG5cbn1cbiJdfQ==