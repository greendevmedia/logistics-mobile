import { Component, OnInit } from '@angular/core';
import { ListPicker } from "ui/list-picker";
import { ListService } from '../services/list/list.service';
import { Page } from "ui/page";
import { JsonService } from "../services/json/json.service";
import { RouterExtensions } from "nativescript-angular/router";
import { GlobalService } from "../services/global/global.service";

@Component({
	moduleId: module.id,
	selector: 'app-car-add',
	templateUrl: './car-add.component.html',
	styleUrls: ['./car-add.component.scss']
})
export class CarAddComponent implements OnInit {

	

	constructor(private listService: ListService,  private page: Page, private jsonService: JsonService, private globalService: GlobalService, private routerExtensions: RouterExtensions ) { }

	ngOnInit() { 
		this.jsonService.pickedCarId = null;
		this.jsonService.carName = null;
		this.jsonService.trailerName = null;
		this.jsonService.pickedTrailerId = null;
		this.listService.getCars();
		// this.page.actionBarHidden = true;
		this.listService.getCurrencies();
		this.listService.getTrailers();
	}
	
	public isCarIdPicked(){
		if (this.jsonService.pickedCarId > 0 || this.jsonService.pickedTrailerId > 0) {
			return true;
        }
	}

	public setLinkForNextView(){
		this.routerExtensions.navigate(this.globalService.carAddLink);
	}



}
