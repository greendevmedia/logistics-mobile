"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var date_service_1 = require("../services/date/date.service");
var additional_payment_service_1 = require("../services/additional-payment/additional-payment.service");
var global_service_1 = require("../services/global/global.service");
var list_service_1 = require("../services/list/list.service");
var json_service_1 = require("../services/json/json.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var AdditionalPayment2Component = (function () {
    function AdditionalPayment2Component(page, dateService, additionalPaymentService, globalService, listService, jsonService, userService, routerExtensions) {
        this.page = page;
        this.dateService = dateService;
        this.additionalPaymentService = additionalPaymentService;
        this.globalService = globalService;
        this.listService = listService;
        this.jsonService = jsonService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
    }
    AdditionalPayment2Component.prototype.ngOnInit = function () {
        this.additionalPaymentService.totalPrice = null;
        this.isBusy = false;
    };
    AdditionalPayment2Component.prototype.addAdditionalPayment = function () {
        var _this = this;
        this.isBusy = true;
        this.additionalPaymentService.additionalPaymentJSONforPOST = {
            paymentDay: this.dateService.dateFormatForPost(),
            paymentName: {
                id: this.additionalPaymentService.pickedAdditionalpaymentNameId
            },
            totalPrice: this.additionalPaymentService.totalPrice,
            paymentType: this.jsonService.pickedPaymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.jsonService.correctPickedCarId
            },
            currency: {
                id: this.jsonService.pickedCurrencyId
            }
        };
        this.additionalPaymentService.addAdditionalPayment(this.additionalPaymentService.additionalPaymentJSONforPOST).subscribe(function (res) {
            _this.isBusy = false;
            alert({
                title: "Potwierdzenie",
                message: "Opłata dodana poprawnie",
                okButtonText: "OK"
            }), _this.routerExtensions.navigate(["/additional-payment-details"]);
        }, function (err) {
            _this.isBusy = false;
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    AdditionalPayment2Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-additional-payment2',
            templateUrl: './additional-payment2.component.html',
            styleUrls: ['./additional-payment2.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, date_service_1.DateService, additional_payment_service_1.AdditionalPaymentService, global_service_1.GlobalService, list_service_1.ListService, json_service_1.JsonService, user_service_1.UserService, router_1.RouterExtensions])
    ], AdditionalPayment2Component);
    return AdditionalPayment2Component;
}());
exports.AdditionalPayment2Component = AdditionalPayment2Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkaXRpb25hbC1wYXltZW50Mi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhZGRpdGlvbmFsLXBheW1lbnQyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxnQ0FBK0I7QUFDL0IsOERBQTREO0FBQzVELHdHQUFxRztBQUNyRyxvRUFBa0U7QUFDbEUsOERBQTREO0FBQzVELDhEQUE0RDtBQUM1RCw4REFBNEQ7QUFDNUQsc0RBQStEO0FBUS9EO0lBSUMscUNBQW9CLElBQVUsRUFBVSxXQUF3QixFQUFVLHdCQUFrRCxFQUFVLGFBQTRCLEVBQVUsV0FBd0IsRUFBVSxXQUF3QixFQUFVLFdBQXdCLEVBQVUsZ0JBQWtDO1FBQWhTLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFBVSxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFDaFQsQ0FBQztJQUVMLDhDQUFRLEdBQVI7UUFDQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUNoRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUNyQixDQUFDO0lBR00sMERBQW9CLEdBQTNCO1FBQUEsaUJBbUNDO1FBbENBLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyw0QkFBNEIsR0FBRztZQUM1RCxVQUFVLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRTtZQUNoRCxXQUFXLEVBQUU7Z0JBQ1osRUFBRSxFQUFFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyw2QkFBNkI7YUFDL0Q7WUFDRCxVQUFVLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFVBQVU7WUFDcEQsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCO1lBQy9DLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVU7WUFDdkMsR0FBRyxFQUFFO2dCQUNKLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQjthQUN2QztZQUNELFFBQVEsRUFBRTtnQkFDVCxFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0I7YUFDckM7U0FFRCxDQUFBO1FBRUQsSUFBSSxDQUFDLHdCQUF3QixDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLFNBQVMsQ0FDdkgsVUFBQyxHQUFHO1lBQU0sS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDN0IsS0FBSyxDQUFDO2dCQUNMLEtBQUssRUFBRSxlQUFlO2dCQUN0QixPQUFPLEVBQUUseUJBQXlCO2dCQUNsQyxZQUFZLEVBQUUsSUFBSTthQUNsQixDQUFDLEVBQUUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLDZCQUE2QixDQUFDLENBQUMsQ0FBQTtRQUNwRSxDQUFDLEVBQ0QsVUFBQyxHQUFHO1lBQU0sS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDN0IsS0FBSyxDQUFDO2dCQUNMLEtBQUssRUFBRSxNQUFNO2dCQUNiLE9BQU8sRUFBRSxnQ0FBZ0M7Z0JBQ3pDLFlBQVksRUFBRSxJQUFJO2FBQ2xCLENBQUMsQ0FBQTtRQUNILENBQUMsQ0FDRCxDQUFDO0lBQ0gsQ0FBQztJQWhEVywyQkFBMkI7UUFOdkMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUseUJBQXlCO1lBQ25DLFdBQVcsRUFBRSxzQ0FBc0M7WUFDbkQsU0FBUyxFQUFFLENBQUMsc0NBQXNDLENBQUM7U0FDbkQsQ0FBQzt5Q0FLeUIsV0FBSSxFQUF1QiwwQkFBVyxFQUFvQyxxREFBd0IsRUFBeUIsOEJBQWEsRUFBdUIsMEJBQVcsRUFBdUIsMEJBQVcsRUFBdUIsMEJBQVcsRUFBNEIseUJBQWdCO09BSnhTLDJCQUEyQixDQWtEdkM7SUFBRCxrQ0FBQztDQUFBLEFBbERELElBa0RDO0FBbERZLGtFQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCB7IERhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZGF0ZS9kYXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWRkaXRpb25hbC1wYXltZW50L2FkZGl0aW9uYWwtcGF5bWVudC5zZXJ2aWNlJztcbmltcG9ydCB7IEdsb2JhbFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9nbG9iYWwvZ2xvYmFsLnNlcnZpY2UnO1xuaW1wb3J0IHsgTGlzdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9saXN0L2xpc3Quc2VydmljZSc7XG5pbXBvcnQgeyBKc29uU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2pzb24vanNvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdXNlci91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuQENvbXBvbmVudCh7XG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXG5cdHNlbGVjdG9yOiAnYXBwLWFkZGl0aW9uYWwtcGF5bWVudDInLFxuXHR0ZW1wbGF0ZVVybDogJy4vYWRkaXRpb25hbC1wYXltZW50Mi5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL2FkZGl0aW9uYWwtcGF5bWVudDIuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBBZGRpdGlvbmFsUGF5bWVudDJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG5cdHByaXZhdGUgaXNCdXN5O1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSBkYXRlU2VydmljZTogRGF0ZVNlcnZpY2UsIHByaXZhdGUgYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlOiBBZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UsIHByaXZhdGUgZ2xvYmFsU2VydmljZTogR2xvYmFsU2VydmljZSwgcHJpdmF0ZSBsaXN0U2VydmljZTogTGlzdFNlcnZpY2UsIHByaXZhdGUganNvblNlcnZpY2U6IEpzb25TZXJ2aWNlLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zXG5cdCkgeyB9XG5cblx0bmdPbkluaXQoKSB7XG5cdFx0dGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UudG90YWxQcmljZSA9IG51bGw7XG5cdFx0dGhpcy5pc0J1c3kgPSBmYWxzZTtcblx0fVxuXG5cblx0cHVibGljIGFkZEFkZGl0aW9uYWxQYXltZW50KCkge1xuXHRcdHRoaXMuaXNCdXN5ID0gdHJ1ZTtcblx0XHR0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5hZGRpdGlvbmFsUGF5bWVudEpTT05mb3JQT1NUID0ge1xuXHRcdFx0cGF5bWVudERheTogdGhpcy5kYXRlU2VydmljZS5kYXRlRm9ybWF0Rm9yUG9zdCgpLFxuXHRcdFx0cGF5bWVudE5hbWU6IHsgXG5cdFx0XHRcdGlkOiB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5waWNrZWRBZGRpdGlvbmFscGF5bWVudE5hbWVJZCBcblx0XHRcdH0sXG5cdFx0XHR0b3RhbFByaWNlOiB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS50b3RhbFByaWNlLFxuXHRcdFx0cGF5bWVudFR5cGU6IHRoaXMuanNvblNlcnZpY2UucGlja2VkUGF5bWVudFR5cGUsXG5cdFx0XHRkcml2ZXJOYW1lOiB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUsXG5cdFx0XHRjYXI6IHtcblx0XHRcdFx0aWQ6IHRoaXMuanNvblNlcnZpY2UuY29ycmVjdFBpY2tlZENhcklkXG5cdFx0XHR9LFxuXHRcdFx0Y3VycmVuY3k6IHtcblx0XHRcdFx0aWQ6IHRoaXMuanNvblNlcnZpY2UucGlja2VkQ3VycmVuY3lJZFxuXHRcdFx0fVxuXG5cdFx0fVxuXG5cdFx0dGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UuYWRkQWRkaXRpb25hbFBheW1lbnQodGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UuYWRkaXRpb25hbFBheW1lbnRKU09OZm9yUE9TVCkuc3Vic2NyaWJlKFxuXHRcdFx0KHJlcykgPT4ge3RoaXMuaXNCdXN5ID0gZmFsc2U7XG5cdFx0XHRcdGFsZXJ0KHtcblx0XHRcdFx0XHR0aXRsZTogXCJQb3R3aWVyZHplbmllXCIsXG5cdFx0XHRcdFx0bWVzc2FnZTogXCJPcMWCYXRhIGRvZGFuYSBwb3ByYXduaWVcIixcblx0XHRcdFx0XHRva0J1dHRvblRleHQ6IFwiT0tcIlxuXHRcdFx0XHR9KSwgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9hZGRpdGlvbmFsLXBheW1lbnQtZGV0YWlsc1wiXSlcblx0XHRcdH0sXG5cdFx0XHQoZXJyKSA9PiB7dGhpcy5pc0J1c3kgPSBmYWxzZTtcblx0XHRcdFx0YWxlcnQoe1xuXHRcdFx0XHRcdHRpdGxlOiBcIkLFgsSFZFwiLFxuXHRcdFx0XHRcdG1lc3NhZ2U6IFwiUG9wcmF3IGRhbmUgaSBzcHLDs2J1aiBwb25vd25pZVwiLFxuXHRcdFx0XHRcdG9rQnV0dG9uVGV4dDogXCJPS1wiXG5cdFx0XHRcdH0pXG5cdFx0XHR9LFxuXHRcdCk7XG5cdH1cblxufVxuIl19