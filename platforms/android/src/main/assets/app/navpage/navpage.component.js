"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var global_service_1 = require("../services/global/global.service");
var router_1 = require("nativescript-angular/router");
var application_settings_1 = require("application-settings");
var nativescript_ngx_fonticon_1 = require("nativescript-ngx-fonticon");
var NavpageComponent = (function () {
    function NavpageComponent(page, globalService, routerExtensions, fonticon) {
        this.page = page;
        this.globalService = globalService;
        this.routerExtensions = routerExtensions;
        this.fonticon = fonticon;
        this.isSelected = true;
    }
    NavpageComponent.prototype.ngOnInit = function () { this.page.actionBarHidden = true; };
    NavpageComponent.prototype.setTitleAndLnkForTC = function () {
        this.globalService.isTrailersListVisible = true;
        this.globalService.carAddTitle = "Karta techniczna - krok 1 z 3 ";
        this.globalService.carAddLink = (["/technical-card-1"]);
        this.routerExtensions.navigate(["/car-add"]);
    };
    NavpageComponent.prototype.setTitleAndLnkForRefuellingAdd = function () {
        this.globalService.isTrailersListVisible = false;
        this.globalService.carAddTitle = "Dodaj tankowanie - krok 1 z 2";
        this.globalService.carAddLink = (["/refuelling-add"]);
        this.routerExtensions.navigate(["/car-add"]);
    };
    NavpageComponent.prototype.setTitleAndLnkForAdditionalPayment = function () {
        this.globalService.isTrailersListVisible = false;
        this.globalService.carAddTitle = "Dodaj opłatę - krok 1 z 3";
        this.globalService.carAddLink = (["additional-payment1"]);
        this.routerExtensions.navigate(["/car-add"]);
    };
    NavpageComponent.prototype.setTitleAndLnkForRepair = function () {
        this.globalService.isTrailersListVisible = true;
        this.globalService.carAddTitle = "Dodaj serwis - krok 1 z 3";
        this.globalService.carAddLink = (["repair-1"]);
        this.routerExtensions.navigate(["/car-add"]);
    };
    NavpageComponent.prototype.logout = function () {
        this.globalService.loginFormVisibility = true;
        console.log(application_settings_1.getString("username"));
        console.log(application_settings_1.getString("pin"));
        application_settings_1.setString("pin", "new");
        application_settings_1.setString("username", "new");
        application_settings_1.clear();
        this.routerExtensions.navigate(["/home"]);
    };
    NavpageComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-navpage',
            templateUrl: './navpage.component.html',
            styleUrls: ['./navpage.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, global_service_1.GlobalService, router_1.RouterExtensions, nativescript_ngx_fonticon_1.TNSFontIconService])
    ], NavpageComponent);
    return NavpageComponent;
}());
exports.NavpageComponent = NavpageComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2cGFnZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuYXZwYWdlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxnQ0FBK0I7QUFDL0Isb0VBQWtFO0FBQ2xFLHNEQUErRDtBQUMvRCw2REFJOEI7QUFFOUIsdUVBQStEO0FBUy9EO0lBS0MsMEJBQW9CLElBQVUsRUFBVSxhQUE0QixFQUFVLGdCQUFrQyxFQUFVLFFBQTRCO1FBQWxJLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFvQjtRQUg5SSxlQUFVLEdBQVcsSUFBSSxDQUFDO0lBR3lILENBQUM7SUFFNUosbUNBQVEsR0FBUixjQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7SUFFekMsOENBQW1CLEdBQTFCO1FBQ0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDaEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsZ0NBQWdDLENBQUM7UUFDbEUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRU0seURBQThCLEdBQXJDO1FBQ0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDakQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsK0JBQStCLENBQUM7UUFDakUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRU0sNkRBQWtDLEdBQXpDO1FBQ0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDakQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsMkJBQTJCLENBQUM7UUFDN0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRU0sa0RBQXVCLEdBQTlCO1FBQ0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDaEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsMkJBQTJCLENBQUM7UUFDN0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVNLGlDQUFNLEdBQWI7UUFDQyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUM5QixnQ0FBUyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN4QixnQ0FBUyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM3Qiw0QkFBSyxFQUFFLENBQUM7UUFDUixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBN0NXLGdCQUFnQjtRQU41QixnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFdBQVcsRUFBRSwwQkFBMEI7WUFDdkMsU0FBUyxFQUFFLENBQUMsMEJBQTBCLENBQUM7U0FDdkMsQ0FBQzt5Q0FNeUIsV0FBSSxFQUF5Qiw4QkFBYSxFQUE0Qix5QkFBZ0IsRUFBb0IsOENBQWtCO09BTDFJLGdCQUFnQixDQStDNUI7SUFBRCx1QkFBQztDQUFBLEFBL0NELElBK0NDO0FBL0NZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCB7IEdsb2JhbFNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvZ2xvYmFsL2dsb2JhbC5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHtcbiAgICBnZXRTdHJpbmcsXG5cdHNldFN0cmluZywgXG5cdGNsZWFyXG59IGZyb20gXCJhcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xuaW1wb3J0IHsgVGFiVmlldywgU2VsZWN0ZWRJbmRleENoYW5nZWRFdmVudERhdGEsIFRhYlZpZXdJdGVtIH0gZnJvbSBcInVpL3RhYi12aWV3XCI7XG5pbXBvcnQgeyBUTlNGb250SWNvblNlcnZpY2UgfSBmcm9tICduYXRpdmVzY3JpcHQtbmd4LWZvbnRpY29uJztcblxuXG5AQ29tcG9uZW50KHtcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0c2VsZWN0b3I6ICdhcHAtbmF2cGFnZScsXG5cdHRlbXBsYXRlVXJsOiAnLi9uYXZwYWdlLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vbmF2cGFnZS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIE5hdnBhZ2VDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG5cdHByaXZhdGUgaXNTZWxlY3RlZDpib29sZWFuID0gdHJ1ZTtcblxuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSBnbG9iYWxTZXJ2aWNlOiBHbG9iYWxTZXJ2aWNlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsIHByaXZhdGUgZm9udGljb246IFROU0ZvbnRJY29uU2VydmljZSApIHsgfVxuXG5cdG5nT25Jbml0KCkgeyB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTsgfVxuXG5cdHB1YmxpYyBzZXRUaXRsZUFuZExua0ZvclRDKCl7XG5cdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmlzVHJhaWxlcnNMaXN0VmlzaWJsZSA9IHRydWU7XG5cdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmNhckFkZFRpdGxlID0gXCJLYXJ0YSB0ZWNobmljem5hIC0ga3JvayAxIHogMyBcIjtcblx0XHR0aGlzLmdsb2JhbFNlcnZpY2UuY2FyQWRkTGluayA9IChbXCIvdGVjaG5pY2FsLWNhcmQtMVwiXSk7XG5cdFx0dGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9jYXItYWRkXCJdKTtcblx0fVxuXG5cdHB1YmxpYyBzZXRUaXRsZUFuZExua0ZvclJlZnVlbGxpbmdBZGQoKXtcblx0XHR0aGlzLmdsb2JhbFNlcnZpY2UuaXNUcmFpbGVyc0xpc3RWaXNpYmxlID0gZmFsc2U7XG5cdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmNhckFkZFRpdGxlID0gXCJEb2RhaiB0YW5rb3dhbmllIC0ga3JvayAxIHogMlwiO1xuXHRcdHRoaXMuZ2xvYmFsU2VydmljZS5jYXJBZGRMaW5rID0gKFtcIi9yZWZ1ZWxsaW5nLWFkZFwiXSk7XG5cdFx0dGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9jYXItYWRkXCJdKTtcblx0fVxuXG5cdHB1YmxpYyBzZXRUaXRsZUFuZExua0ZvckFkZGl0aW9uYWxQYXltZW50KCl7XG5cdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmlzVHJhaWxlcnNMaXN0VmlzaWJsZSA9IGZhbHNlO1xuXHRcdHRoaXMuZ2xvYmFsU2VydmljZS5jYXJBZGRUaXRsZSA9IFwiRG9kYWogb3DFgmF0xJkgLSBrcm9rIDEgeiAzXCI7XG5cdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmNhckFkZExpbmsgPSAoW1wiYWRkaXRpb25hbC1wYXltZW50MVwiXSk7XG5cdFx0dGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9jYXItYWRkXCJdKTtcblx0fVxuXG5cdHB1YmxpYyBzZXRUaXRsZUFuZExua0ZvclJlcGFpcigpe1xuXHRcdHRoaXMuZ2xvYmFsU2VydmljZS5pc1RyYWlsZXJzTGlzdFZpc2libGUgPSB0cnVlO1xuXHRcdHRoaXMuZ2xvYmFsU2VydmljZS5jYXJBZGRUaXRsZSA9IFwiRG9kYWogc2Vyd2lzIC0ga3JvayAxIHogM1wiO1xuXHRcdHRoaXMuZ2xvYmFsU2VydmljZS5jYXJBZGRMaW5rID0gKFtcInJlcGFpci0xXCJdKTtcblx0XHR0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2Nhci1hZGRcIl0pO1xuXHR9XG5cblx0cHVibGljIGxvZ291dCgpe1xuXHRcdHRoaXMuZ2xvYmFsU2VydmljZS5sb2dpbkZvcm1WaXNpYmlsaXR5ID0gdHJ1ZTtcblx0XHRjb25zb2xlLmxvZyhnZXRTdHJpbmcoXCJ1c2VybmFtZVwiKSk7XG5cdFx0Y29uc29sZS5sb2coZ2V0U3RyaW5nKFwicGluXCIpKTtcblx0XHRzZXRTdHJpbmcoXCJwaW5cIiwgXCJuZXdcIik7XG5cdFx0c2V0U3RyaW5nKFwidXNlcm5hbWVcIiwgXCJuZXdcIik7XG5cdFx0Y2xlYXIoKTtcblx0XHR0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2hvbWVcIl0pO1xuXHR9XG5cbn1cbiJdfQ==