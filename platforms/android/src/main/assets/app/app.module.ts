import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { LoginService } from "./services/login/login.service";
import { GlobalService } from "./services/global/global.service"
import { UserService } from "./services/user/user.service";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { RefuelingService } from "./services/refueling/refueling.service";
import { DropDownModule } from "nativescript-drop-down/angular";
import { RefuelingEditComponent } from "./refueling-edit/refueling-edit.component"
import { DateService } from "./services/date/date.service";
import { ListService } from "./services/list/list.service";
import { NavigationComponent } from "./navigation/navigation.component"
import { NavpageComponent } from "./navpage/navpage.component"
import { CarAddComponent } from "./car-add/car-add.component";
import { JsonService } from "./services/json/json.service";
import { TechnicalCardService } from "./services/technical-card/technical-card.service";
import { TechnicalCard1Component } from "./technical-card-1/technical-card-1.component";
import { TechnicalCard2Component } from "./technical-card-2/technical-card-2.component";
import { RefuellingAddComponent } from "./refuelling-add/refuelling-add.component";
import { RefuelingListComponent } from "./refueling-list/refueling-list.component";
import { AdditionalPayment1Component } from "./additional-payment1/additional-payment1.component";
import { AdditionalPayment2Component } from "./additional-payment2/additional-payment2.component";
import { AdditionalPaymentService } from "./services/additional-payment/additional-payment.service";
import { AddiionalPaymentDetailsComponent } from "./addiional-payment-details/addiional-payment-details.component";
import { AdditionlPaymentsListComponent } from "./additionl-payments-list/additionl-payments-list.component";
import { AdditionalPaymentEditComponent } from "./additional-payment-edit/additional-payment-edit.component";
import { Repair1Component } from "./repair-1/repair-1.component";
import { RepairService } from "./services/repair/repair.service";
import { Repair2Component } from "./repair-2/repair-2.component";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { UrlService } from "./services/urlService/url.service";
import { ShipmentService } from "./services/shipment/shipment.service";
import { ShipmentComponent } from "./shipment/shipment.component";



@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptFormsModule,
        NativeScriptHttpModule,
        DropDownModule,
        TNSFontIconModule.forRoot({
			'fa': './assets/font-awesome.css',
            'ion': './assets/ionicons.css',
            'mdi': 'material-design-icons.css'
		})
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        RefuelingEditComponent,
        NavigationComponent,
        NavpageComponent,
        CarAddComponent,
        TechnicalCard1Component,
        TechnicalCard2Component,
        RefuellingAddComponent,
        RefuelingListComponent,
        AdditionalPayment1Component,
        AdditionalPayment2Component,
        AddiionalPaymentDetailsComponent,
        AdditionlPaymentsListComponent,
        AdditionalPaymentEditComponent,
        Repair1Component,
        Repair2Component,
        ShipmentComponent
    ],
    providers: [
        GlobalService,
        LoginService,
        UserService,
        RefuelingService,
        DateService,
        ListService,
        JsonService,
        TechnicalCardService,
        AdditionalPaymentService,
        RepairService,
        UrlService,
        ShipmentService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
