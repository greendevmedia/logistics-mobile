import { Component, OnInit } from '@angular/core';
import { RefuelingService } from "../services/refueling/refueling.service";
import { DatePicker } from "ui/date-picker";
import { DateService } from "../services/date/date.service";
import { GlobalService } from "../services/global/global.service";
import { ListService } from '../services/list/list.service';
import { Switch } from "ui/switch";
import { ListPicker } from "ui/list-picker";
import { UserService } from '../services/user/user.service';
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";


@Component({
    moduleId: module.id,
    selector: 'app-refueling-edit',
    templateUrl: './refueling-edit.component.html',
    styleUrls: ['./refueling-edit.component.scss']
})
export class RefuelingEditComponent implements OnInit {

    public refuelingJSONforUpdate;
    public refuelingID;
    public counter: number;
    public quantity: number;
    public price: number;
    public paymentType: string;
    public pickedPaymentId;
    public car: string;
    public pickedCarId;
    public currency: string;
    public pickedCurrencyId;
    public refuelingJSONforUPDATE;
    public warningNote: string;
    public isWarningNoteCollapsed: boolean = false;
    public firstChecked: boolean = false;
    public switchState = "Dodatkowe uwagi - NIE";
    public isAllPicked: boolean = true;
    public adBlueQuantity;
    public adBluePrice;
    public adBlueFirstCheck;
    public adBlueSwitchState = "AdBlue - NIE";
    public isAdBlueCollapsed;


    constructor(private refuelingService: RefuelingService, private dateService: DateService, private globalService: GlobalService, private listService: ListService, private userService: UserService, private routerExtensions: RouterExtensions, private page: Page) { }

    ngOnInit() {
        this.page.actionBarHidden = true;
        this.listService.getCars();
        this.listService.getCurrencies();
        this.refuelingID = this.refuelingService.refuelingJSONfromGETbyID.id;
        this.paymentType = this.refuelingService.refuelingJSONfromGETbyID.paymentType;
        this.counter = this.refuelingService.refuelingJSONfromGETbyID.counter;
        this.quantity = this.refuelingService.refuelingJSONfromGETbyID.quantity;
        this.price = this.refuelingService.refuelingJSONfromGETbyID.price;
        this.warningNote = this.refuelingService.refuelingJSONfromGETbyID.warningNote;
        this.currency = this.refuelingService.refuelingJSONfromGETbyID.currency.currencyCode;
        this.car = this.refuelingService.refuelingJSONfromGETbyID.car.name;
        this.firstChecked = this.firstSwitchCheck(this.switchState, this.warningNote);
        this.adBluePrice = this.refuelingService.refuelingJSONfromGETbyID.adBluePrice;
        this.adBlueQuantity = this.refuelingService.refuelingJSONfromGETbyID.adBlueQuantity;
        this.adBlueFirstCheck = this.firstSwitchCheckAdBlue(this.adBlueSwitchState, this.adBluePrice, this.adBlueQuantity)
    }


    public firstSwitchCheck(switchState, warningNote) {
        if (warningNote !== null) {
            switchState = "Dodatkowe uwagi - TAK";
            console.log(true);
            return true
        } else {
            switchState = "Dodatkowe Uwagi - NIE";
            console.log(false);
            return false;
        }
    }

    public firstSwitchCheckAdBlue(switchState, adBluePrice, adBlueQuantity) {
        if (adBluePrice !=0 && adBlueQuantity !=0) {
            switchState = "AdBlue - TAK";
            console.log(true);
            return true
        } else {
            switchState = "AdBlue - NIE";
            console.log(false);
            return false;
        }
    }

    public onFirstChecked(args) {
        let firstSwitch = <Switch>args.object;
        if (firstSwitch.checked) {
            this.switchState = "Dodatkowe uwagi - TAK";
            this.isWarningNoteCollapsed = true;
        } else {
            this.switchState = "Dodatkowe uwagi - NIE";
            this.isWarningNoteCollapsed = false;
        }
    }

    public onFirstCheckedAdBlue(args) {
        let firstSwitch = <Switch>args.object;
        if (firstSwitch.checked) {
            this.adBlueSwitchState = "AdBlue - TAK";
            this.isAdBlueCollapsed = true;
        } else {
            this.adBlueSwitchState = "AdBlue - NIE";
            this.isAdBlueCollapsed = false;
        }
    }

    public selectedIndexChangedOnPaymentTypes(args) {
        let picker = <ListPicker>args.object;
        this.paymentType = this.globalService.paymentTypesList[picker.selectedIndex];
        this.pickedPaymentId = picker.selectedIndex + 1;
        console.log(this.paymentType + " " + this.pickedPaymentId)
    }

    public selectedIndexChangedOnCurrencies(args) {
        let picker = <ListPicker>args.object;
        this.currency = this.listService.currenciesList[picker.selectedIndex];
        this.pickedCurrencyId = picker.selectedIndex + 1;
        console.log(this.currency + " " + this.pickedCurrencyId);
    }

    public selectedIndexChangedOnCars(args) {
        let picker = <ListPicker>args.object;
        this.car = this.listService.carsList[picker.selectedIndex];
        this.pickedCarId = picker.selectedIndex + 1;
        console.log(this.car + " " + this.pickedCarId)
    }

    public updateRefueling() {
        this.refuelingService.refuelingJSONforPOST = {
            id: this.refuelingID,
            refuelingDate: this.dateService.dateFormatForPost(),
            counter: this.counter,
            quantity: this.quantity,
            price: this.price,
            paymentType: this.paymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.pickedCarId
            },
            currency: {
                id: this.pickedCurrencyId
            },
            warningNote: this.warningNote,
            adBlueQuantity: this.adBlueQuantity,
            adBluePrice: this.adBluePrice
        }

        this.refuelingService.updateRefueling(this.refuelingService.refuelingJSONforPOST).subscribe(
            (res) => {
                alert({
                    title: "Potwierdzenie",
                    message: "Tankowanie zmienione poprawnie",
                    okButtonText: "OK"
                }), this.routerExtensions.navigate(["/navigation"], { clearHistory: true })
            },
            (err) => {
                alert({
                    title: "Błąd",
                    message: "Popraw dane i spróbuj ponownie",
                    okButtonText: "OK"
                })
            },
        );
    }

}
