"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var refueling_service_1 = require("../services/refueling/refueling.service");
var date_service_1 = require("../services/date/date.service");
var global_service_1 = require("../services/global/global.service");
var list_service_1 = require("../services/list/list.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var RefuelingEditComponent = (function () {
    function RefuelingEditComponent(refuelingService, dateService, globalService, listService, userService, routerExtensions, page) {
        this.refuelingService = refuelingService;
        this.dateService = dateService;
        this.globalService = globalService;
        this.listService = listService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.isWarningNoteCollapsed = false;
        this.firstChecked = false;
        this.switchState = "Dodatkowe uwagi - NIE";
        this.isAllPicked = true;
        this.adBlueSwitchState = "AdBlue - NIE";
    }
    RefuelingEditComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.listService.getCars();
        this.listService.getCurrencies();
        this.refuelingID = this.refuelingService.refuelingJSONfromGETbyID.id;
        this.paymentType = this.refuelingService.refuelingJSONfromGETbyID.paymentType;
        this.counter = this.refuelingService.refuelingJSONfromGETbyID.counter;
        this.quantity = this.refuelingService.refuelingJSONfromGETbyID.quantity;
        this.price = this.refuelingService.refuelingJSONfromGETbyID.price;
        this.warningNote = this.refuelingService.refuelingJSONfromGETbyID.warningNote;
        this.currency = this.refuelingService.refuelingJSONfromGETbyID.currency.currencyCode;
        this.car = this.refuelingService.refuelingJSONfromGETbyID.car.name;
        this.firstChecked = this.firstSwitchCheck(this.switchState, this.warningNote);
        this.adBluePrice = this.refuelingService.refuelingJSONfromGETbyID.adBluePrice;
        this.adBlueQuantity = this.refuelingService.refuelingJSONfromGETbyID.adBlueQuantity;
        this.adBlueFirstCheck = this.firstSwitchCheckAdBlue(this.adBlueSwitchState, this.adBluePrice, this.adBlueQuantity);
    };
    RefuelingEditComponent.prototype.firstSwitchCheck = function (switchState, warningNote) {
        if (warningNote !== null) {
            switchState = "Dodatkowe uwagi - TAK";
            console.log(true);
            return true;
        }
        else {
            switchState = "Dodatkowe Uwagi - NIE";
            console.log(false);
            return false;
        }
    };
    RefuelingEditComponent.prototype.firstSwitchCheckAdBlue = function (switchState, adBluePrice, adBlueQuantity) {
        if (adBluePrice != 0 && adBlueQuantity != 0) {
            switchState = "AdBlue - TAK";
            console.log(true);
            return true;
        }
        else {
            switchState = "AdBlue - NIE";
            console.log(false);
            return false;
        }
    };
    RefuelingEditComponent.prototype.onFirstChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.switchState = "Dodatkowe uwagi - TAK";
            this.isWarningNoteCollapsed = true;
        }
        else {
            this.switchState = "Dodatkowe uwagi - NIE";
            this.isWarningNoteCollapsed = false;
        }
    };
    RefuelingEditComponent.prototype.onFirstCheckedAdBlue = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.adBlueSwitchState = "AdBlue - TAK";
            this.isAdBlueCollapsed = true;
        }
        else {
            this.adBlueSwitchState = "AdBlue - NIE";
            this.isAdBlueCollapsed = false;
        }
    };
    RefuelingEditComponent.prototype.selectedIndexChangedOnPaymentTypes = function (args) {
        var picker = args.object;
        this.paymentType = this.globalService.paymentTypesList[picker.selectedIndex];
        this.pickedPaymentId = picker.selectedIndex + 1;
        console.log(this.paymentType + " " + this.pickedPaymentId);
    };
    RefuelingEditComponent.prototype.selectedIndexChangedOnCurrencies = function (args) {
        var picker = args.object;
        this.currency = this.listService.currenciesList[picker.selectedIndex];
        this.pickedCurrencyId = picker.selectedIndex + 1;
        console.log(this.currency + " " + this.pickedCurrencyId);
    };
    RefuelingEditComponent.prototype.selectedIndexChangedOnCars = function (args) {
        var picker = args.object;
        this.car = this.listService.carsList[picker.selectedIndex];
        this.pickedCarId = picker.selectedIndex + 1;
        console.log(this.car + " " + this.pickedCarId);
    };
    RefuelingEditComponent.prototype.updateRefueling = function () {
        var _this = this;
        this.refuelingService.refuelingJSONforPOST = {
            id: this.refuelingID,
            refuelingDate: this.dateService.dateFormatForPost(),
            counter: this.counter,
            quantity: this.quantity,
            price: this.price,
            paymentType: this.paymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.pickedCarId
            },
            currency: {
                id: this.pickedCurrencyId
            },
            warningNote: this.warningNote,
            adBlueQuantity: this.adBlueQuantity,
            adBluePrice: this.adBluePrice
        };
        this.refuelingService.updateRefueling(this.refuelingService.refuelingJSONforPOST).subscribe(function (res) {
            alert({
                title: "Potwierdzenie",
                message: "Tankowanie zmienione poprawnie",
                okButtonText: "OK"
            }), _this.routerExtensions.navigate(["/navigation"], { clearHistory: true });
        }, function (err) {
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    RefuelingEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-refueling-edit',
            templateUrl: './refueling-edit.component.html',
            styleUrls: ['./refueling-edit.component.scss']
        }),
        __metadata("design:paramtypes", [refueling_service_1.RefuelingService, date_service_1.DateService, global_service_1.GlobalService, list_service_1.ListService, user_service_1.UserService, router_1.RouterExtensions, page_1.Page])
    ], RefuelingEditComponent);
    return RefuelingEditComponent;
}());
exports.RefuelingEditComponent = RefuelingEditComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmdWVsaW5nLWVkaXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVmdWVsaW5nLWVkaXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELDZFQUEyRTtBQUUzRSw4REFBNEQ7QUFDNUQsb0VBQWtFO0FBQ2xFLDhEQUE0RDtBQUc1RCw4REFBNEQ7QUFDNUQsc0RBQStEO0FBQy9ELGdDQUErQjtBQVMvQjtJQTBCSSxnQ0FBb0IsZ0JBQWtDLEVBQVUsV0FBd0IsRUFBVSxhQUE0QixFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSxnQkFBa0MsRUFBVSxJQUFVO1FBQTlPLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBWDNQLDJCQUFzQixHQUFZLEtBQUssQ0FBQztRQUN4QyxpQkFBWSxHQUFZLEtBQUssQ0FBQztRQUM5QixnQkFBVyxHQUFHLHVCQUF1QixDQUFDO1FBQ3RDLGdCQUFXLEdBQVksSUFBSSxDQUFDO1FBSTVCLHNCQUFpQixHQUFHLGNBQWMsQ0FBQztJQUk0TixDQUFDO0lBRXZRLHlDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHdCQUF3QixDQUFDLEVBQUUsQ0FBQztRQUNyRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLENBQUM7UUFDOUUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHdCQUF3QixDQUFDLFFBQVEsQ0FBQztRQUN4RSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUM7UUFDbEUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDO1FBQzlFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7UUFDckYsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztRQUNuRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5RSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLENBQUM7UUFDOUUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUMsY0FBYyxDQUFDO1FBQ3BGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO0lBQ3RILENBQUM7SUFHTSxpREFBZ0IsR0FBdkIsVUFBd0IsV0FBVyxFQUFFLFdBQVc7UUFDNUMsRUFBRSxDQUFDLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdkIsV0FBVyxHQUFHLHVCQUF1QixDQUFDO1lBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNmLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLFdBQVcsR0FBRyx1QkFBdUIsQ0FBQztZQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25CLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakIsQ0FBQztJQUNMLENBQUM7SUFFTSx1REFBc0IsR0FBN0IsVUFBOEIsV0FBVyxFQUFFLFdBQVcsRUFBRSxjQUFjO1FBQ2xFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsSUFBRyxDQUFDLElBQUksY0FBYyxJQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEMsV0FBVyxHQUFHLGNBQWMsQ0FBQztZQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xCLE1BQU0sQ0FBQyxJQUFJLENBQUE7UUFDZixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixXQUFXLEdBQUcsY0FBYyxDQUFDO1lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkIsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO0lBQ0wsQ0FBQztJQUVNLCtDQUFjLEdBQXJCLFVBQXNCLElBQUk7UUFDdEIsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLHVCQUF1QixDQUFDO1lBQzNDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7UUFDdkMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLFdBQVcsR0FBRyx1QkFBdUIsQ0FBQztZQUMzQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1FBQ3hDLENBQUM7SUFDTCxDQUFDO0lBRU0scURBQW9CLEdBQTNCLFVBQTRCLElBQUk7UUFDNUIsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsY0FBYyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDbEMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLGlCQUFpQixHQUFHLGNBQWMsQ0FBQztZQUN4QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQ25DLENBQUM7SUFDTCxDQUFDO0lBRU0sbUVBQWtDLEdBQXpDLFVBQTBDLElBQUk7UUFDMUMsSUFBSSxNQUFNLEdBQWUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzdFLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7SUFDOUQsQ0FBQztJQUVNLGlFQUFnQyxHQUF2QyxVQUF3QyxJQUFJO1FBQ3hDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVNLDJEQUEwQixHQUFqQyxVQUFrQyxJQUFJO1FBQ2xDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUM1QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtJQUNsRCxDQUFDO0lBRU0sZ0RBQWUsR0FBdEI7UUFBQSxpQkFvQ0M7UUFuQ0csSUFBSSxDQUFDLGdCQUFnQixDQUFDLG9CQUFvQixHQUFHO1lBQ3pDLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVztZQUNwQixhQUFhLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRTtZQUNuRCxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7WUFDN0IsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtZQUN2QyxHQUFHLEVBQUU7Z0JBQ0QsRUFBRSxFQUFFLElBQUksQ0FBQyxXQUFXO2FBQ3ZCO1lBQ0QsUUFBUSxFQUFFO2dCQUNOLEVBQUUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCO2FBQzVCO1lBQ0QsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO1lBQzdCLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYztZQUNuQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7U0FDaEMsQ0FBQTtRQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLG9CQUFvQixDQUFDLENBQUMsU0FBUyxDQUN2RixVQUFDLEdBQUc7WUFDQSxLQUFLLENBQUM7Z0JBQ0YsS0FBSyxFQUFFLGVBQWU7Z0JBQ3RCLE9BQU8sRUFBRSxnQ0FBZ0M7Z0JBQ3pDLFlBQVksRUFBRSxJQUFJO2FBQ3JCLENBQUMsRUFBRSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQTtRQUMvRSxDQUFDLEVBQ0QsVUFBQyxHQUFHO1lBQ0EsS0FBSyxDQUFDO2dCQUNGLEtBQUssRUFBRSxNQUFNO2dCQUNiLE9BQU8sRUFBRSxnQ0FBZ0M7Z0JBQ3pDLFlBQVksRUFBRSxJQUFJO2FBQ3JCLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FDSixDQUFDO0lBQ04sQ0FBQztJQXRKUSxzQkFBc0I7UUFObEMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFdBQVcsRUFBRSxpQ0FBaUM7WUFDOUMsU0FBUyxFQUFFLENBQUMsaUNBQWlDLENBQUM7U0FDakQsQ0FBQzt5Q0EyQndDLG9DQUFnQixFQUF1QiwwQkFBVyxFQUF5Qiw4QkFBYSxFQUF1QiwwQkFBVyxFQUF1QiwwQkFBVyxFQUE0Qix5QkFBZ0IsRUFBZ0IsV0FBSTtPQTFCelAsc0JBQXNCLENBd0psQztJQUFELDZCQUFDO0NBQUEsQUF4SkQsSUF3SkM7QUF4Slksd0RBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJlZnVlbGluZ1NlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvcmVmdWVsaW5nL3JlZnVlbGluZy5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBEYXRlUGlja2VyIH0gZnJvbSBcInVpL2RhdGUtcGlja2VyXCI7XG5pbXBvcnQgeyBEYXRlU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy9kYXRlL2RhdGUuc2VydmljZVwiO1xuaW1wb3J0IHsgR2xvYmFsU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy9nbG9iYWwvZ2xvYmFsLnNlcnZpY2VcIjtcbmltcG9ydCB7IExpc3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbGlzdC9saXN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgU3dpdGNoIH0gZnJvbSBcInVpL3N3aXRjaFwiO1xuaW1wb3J0IHsgTGlzdFBpY2tlciB9IGZyb20gXCJ1aS9saXN0LXBpY2tlclwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy91c2VyL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XG5cblxuQENvbXBvbmVudCh7XG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICBzZWxlY3RvcjogJ2FwcC1yZWZ1ZWxpbmctZWRpdCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3JlZnVlbGluZy1lZGl0LmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9yZWZ1ZWxpbmctZWRpdC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFJlZnVlbGluZ0VkaXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgcHVibGljIHJlZnVlbGluZ0pTT05mb3JVcGRhdGU7XG4gICAgcHVibGljIHJlZnVlbGluZ0lEO1xuICAgIHB1YmxpYyBjb3VudGVyOiBudW1iZXI7XG4gICAgcHVibGljIHF1YW50aXR5OiBudW1iZXI7XG4gICAgcHVibGljIHByaWNlOiBudW1iZXI7XG4gICAgcHVibGljIHBheW1lbnRUeXBlOiBzdHJpbmc7XG4gICAgcHVibGljIHBpY2tlZFBheW1lbnRJZDtcbiAgICBwdWJsaWMgY2FyOiBzdHJpbmc7XG4gICAgcHVibGljIHBpY2tlZENhcklkO1xuICAgIHB1YmxpYyBjdXJyZW5jeTogc3RyaW5nO1xuICAgIHB1YmxpYyBwaWNrZWRDdXJyZW5jeUlkO1xuICAgIHB1YmxpYyByZWZ1ZWxpbmdKU09OZm9yVVBEQVRFO1xuICAgIHB1YmxpYyB3YXJuaW5nTm90ZTogc3RyaW5nO1xuICAgIHB1YmxpYyBpc1dhcm5pbmdOb3RlQ29sbGFwc2VkOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHVibGljIGZpcnN0Q2hlY2tlZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHB1YmxpYyBzd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gTklFXCI7XG4gICAgcHVibGljIGlzQWxsUGlja2VkOiBib29sZWFuID0gdHJ1ZTtcbiAgICBwdWJsaWMgYWRCbHVlUXVhbnRpdHk7XG4gICAgcHVibGljIGFkQmx1ZVByaWNlO1xuICAgIHB1YmxpYyBhZEJsdWVGaXJzdENoZWNrO1xuICAgIHB1YmxpYyBhZEJsdWVTd2l0Y2hTdGF0ZSA9IFwiQWRCbHVlIC0gTklFXCI7XG4gICAgcHVibGljIGlzQWRCbHVlQ29sbGFwc2VkO1xuXG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJlZnVlbGluZ1NlcnZpY2U6IFJlZnVlbGluZ1NlcnZpY2UsIHByaXZhdGUgZGF0ZVNlcnZpY2U6IERhdGVTZXJ2aWNlLCBwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgbGlzdFNlcnZpY2U6IExpc3RTZXJ2aWNlLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHBhZ2U6IFBhZ2UpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xuICAgICAgICB0aGlzLmxpc3RTZXJ2aWNlLmdldENhcnMoKTtcbiAgICAgICAgdGhpcy5saXN0U2VydmljZS5nZXRDdXJyZW5jaWVzKCk7XG4gICAgICAgIHRoaXMucmVmdWVsaW5nSUQgPSB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZyb21HRVRieUlELmlkO1xuICAgICAgICB0aGlzLnBheW1lbnRUeXBlID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5wYXltZW50VHlwZTtcbiAgICAgICAgdGhpcy5jb3VudGVyID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5jb3VudGVyO1xuICAgICAgICB0aGlzLnF1YW50aXR5ID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5xdWFudGl0eTtcbiAgICAgICAgdGhpcy5wcmljZSA9IHRoaXMucmVmdWVsaW5nU2VydmljZS5yZWZ1ZWxpbmdKU09OZnJvbUdFVGJ5SUQucHJpY2U7XG4gICAgICAgIHRoaXMud2FybmluZ05vdGUgPSB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZyb21HRVRieUlELndhcm5pbmdOb3RlO1xuICAgICAgICB0aGlzLmN1cnJlbmN5ID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5jdXJyZW5jeS5jdXJyZW5jeUNvZGU7XG4gICAgICAgIHRoaXMuY2FyID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5jYXIubmFtZTtcbiAgICAgICAgdGhpcy5maXJzdENoZWNrZWQgPSB0aGlzLmZpcnN0U3dpdGNoQ2hlY2sodGhpcy5zd2l0Y2hTdGF0ZSwgdGhpcy53YXJuaW5nTm90ZSk7XG4gICAgICAgIHRoaXMuYWRCbHVlUHJpY2UgPSB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZyb21HRVRieUlELmFkQmx1ZVByaWNlO1xuICAgICAgICB0aGlzLmFkQmx1ZVF1YW50aXR5ID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5hZEJsdWVRdWFudGl0eTtcbiAgICAgICAgdGhpcy5hZEJsdWVGaXJzdENoZWNrID0gdGhpcy5maXJzdFN3aXRjaENoZWNrQWRCbHVlKHRoaXMuYWRCbHVlU3dpdGNoU3RhdGUsIHRoaXMuYWRCbHVlUHJpY2UsIHRoaXMuYWRCbHVlUXVhbnRpdHkpXG4gICAgfVxuXG5cbiAgICBwdWJsaWMgZmlyc3RTd2l0Y2hDaGVjayhzd2l0Y2hTdGF0ZSwgd2FybmluZ05vdGUpIHtcbiAgICAgICAgaWYgKHdhcm5pbmdOb3RlICE9PSBudWxsKSB7XG4gICAgICAgICAgICBzd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gVEFLXCI7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0cnVlKTtcbiAgICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBzd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIFV3YWdpIC0gTklFXCI7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhmYWxzZSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgZmlyc3RTd2l0Y2hDaGVja0FkQmx1ZShzd2l0Y2hTdGF0ZSwgYWRCbHVlUHJpY2UsIGFkQmx1ZVF1YW50aXR5KSB7XG4gICAgICAgIGlmIChhZEJsdWVQcmljZSAhPTAgJiYgYWRCbHVlUXVhbnRpdHkgIT0wKSB7XG4gICAgICAgICAgICBzd2l0Y2hTdGF0ZSA9IFwiQWRCbHVlIC0gVEFLXCI7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0cnVlKTtcbiAgICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBzd2l0Y2hTdGF0ZSA9IFwiQWRCbHVlIC0gTklFXCI7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhmYWxzZSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgb25GaXJzdENoZWNrZWQoYXJncykge1xuICAgICAgICBsZXQgZmlyc3RTd2l0Y2ggPSA8U3dpdGNoPmFyZ3Mub2JqZWN0O1xuICAgICAgICBpZiAoZmlyc3RTd2l0Y2guY2hlY2tlZCkge1xuICAgICAgICAgICAgdGhpcy5zd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gVEFLXCI7XG4gICAgICAgICAgICB0aGlzLmlzV2FybmluZ05vdGVDb2xsYXBzZWQgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gTklFXCI7XG4gICAgICAgICAgICB0aGlzLmlzV2FybmluZ05vdGVDb2xsYXBzZWQgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBvbkZpcnN0Q2hlY2tlZEFkQmx1ZShhcmdzKSB7XG4gICAgICAgIGxldCBmaXJzdFN3aXRjaCA9IDxTd2l0Y2g+YXJncy5vYmplY3Q7XG4gICAgICAgIGlmIChmaXJzdFN3aXRjaC5jaGVja2VkKSB7XG4gICAgICAgICAgICB0aGlzLmFkQmx1ZVN3aXRjaFN0YXRlID0gXCJBZEJsdWUgLSBUQUtcIjtcbiAgICAgICAgICAgIHRoaXMuaXNBZEJsdWVDb2xsYXBzZWQgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5hZEJsdWVTd2l0Y2hTdGF0ZSA9IFwiQWRCbHVlIC0gTklFXCI7XG4gICAgICAgICAgICB0aGlzLmlzQWRCbHVlQ29sbGFwc2VkID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc2VsZWN0ZWRJbmRleENoYW5nZWRPblBheW1lbnRUeXBlcyhhcmdzKSB7XG4gICAgICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcbiAgICAgICAgdGhpcy5wYXltZW50VHlwZSA9IHRoaXMuZ2xvYmFsU2VydmljZS5wYXltZW50VHlwZXNMaXN0W3BpY2tlci5zZWxlY3RlZEluZGV4XTtcbiAgICAgICAgdGhpcy5waWNrZWRQYXltZW50SWQgPSBwaWNrZXIuc2VsZWN0ZWRJbmRleCArIDE7XG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMucGF5bWVudFR5cGUgKyBcIiBcIiArIHRoaXMucGlja2VkUGF5bWVudElkKVxuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RlZEluZGV4Q2hhbmdlZE9uQ3VycmVuY2llcyhhcmdzKSB7XG4gICAgICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcbiAgICAgICAgdGhpcy5jdXJyZW5jeSA9IHRoaXMubGlzdFNlcnZpY2UuY3VycmVuY2llc0xpc3RbcGlja2VyLnNlbGVjdGVkSW5kZXhdO1xuICAgICAgICB0aGlzLnBpY2tlZEN1cnJlbmN5SWQgPSBwaWNrZXIuc2VsZWN0ZWRJbmRleCArIDE7XG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuY3VycmVuY3kgKyBcIiBcIiArIHRoaXMucGlja2VkQ3VycmVuY3lJZCk7XG4gICAgfVxuXG4gICAgcHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkT25DYXJzKGFyZ3MpIHtcbiAgICAgICAgbGV0IHBpY2tlciA9IDxMaXN0UGlja2VyPmFyZ3Mub2JqZWN0O1xuICAgICAgICB0aGlzLmNhciA9IHRoaXMubGlzdFNlcnZpY2UuY2Fyc0xpc3RbcGlja2VyLnNlbGVjdGVkSW5kZXhdO1xuICAgICAgICB0aGlzLnBpY2tlZENhcklkID0gcGlja2VyLnNlbGVjdGVkSW5kZXggKyAxO1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmNhciArIFwiIFwiICsgdGhpcy5waWNrZWRDYXJJZClcbiAgICB9XG5cbiAgICBwdWJsaWMgdXBkYXRlUmVmdWVsaW5nKCkge1xuICAgICAgICB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZvclBPU1QgPSB7XG4gICAgICAgICAgICBpZDogdGhpcy5yZWZ1ZWxpbmdJRCxcbiAgICAgICAgICAgIHJlZnVlbGluZ0RhdGU6IHRoaXMuZGF0ZVNlcnZpY2UuZGF0ZUZvcm1hdEZvclBvc3QoKSxcbiAgICAgICAgICAgIGNvdW50ZXI6IHRoaXMuY291bnRlcixcbiAgICAgICAgICAgIHF1YW50aXR5OiB0aGlzLnF1YW50aXR5LFxuICAgICAgICAgICAgcHJpY2U6IHRoaXMucHJpY2UsXG4gICAgICAgICAgICBwYXltZW50VHlwZTogdGhpcy5wYXltZW50VHlwZSxcbiAgICAgICAgICAgIGRyaXZlck5hbWU6IHRoaXMudXNlclNlcnZpY2UuZHJpdmVyTmFtZSxcbiAgICAgICAgICAgIGNhcjoge1xuICAgICAgICAgICAgICAgIGlkOiB0aGlzLnBpY2tlZENhcklkXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY3VycmVuY3k6IHtcbiAgICAgICAgICAgICAgICBpZDogdGhpcy5waWNrZWRDdXJyZW5jeUlkXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgd2FybmluZ05vdGU6IHRoaXMud2FybmluZ05vdGUsXG4gICAgICAgICAgICBhZEJsdWVRdWFudGl0eTogdGhpcy5hZEJsdWVRdWFudGl0eSxcbiAgICAgICAgICAgIGFkQmx1ZVByaWNlOiB0aGlzLmFkQmx1ZVByaWNlXG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnJlZnVlbGluZ1NlcnZpY2UudXBkYXRlUmVmdWVsaW5nKHRoaXMucmVmdWVsaW5nU2VydmljZS5yZWZ1ZWxpbmdKU09OZm9yUE9TVCkuc3Vic2NyaWJlKFxuICAgICAgICAgICAgKHJlcykgPT4ge1xuICAgICAgICAgICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwiUG90d2llcmR6ZW5pZVwiLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBcIlRhbmtvd2FuaWUgem1pZW5pb25lIHBvcHJhd25pZVwiLFxuICAgICAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICAgICAgICAgIH0pLCB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL25hdmlnYXRpb25cIl0sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgKGVycikgPT4ge1xuICAgICAgICAgICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwiQsWCxIVkXCIsXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IFwiUG9wcmF3IGRhbmUgaSBzcHLDs2J1aiBwb25vd25pZVwiLFxuICAgICAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9LFxuICAgICAgICApO1xuICAgIH1cblxufVxuIl19