"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GlobalService = (function () {
    function GlobalService() {
        this.grant_type = "password";
        this.client_id = "Logistics";
        this.keycloakUrl = "https://42.ip-145-239-85.eu";
        this.client_secret = "92a5e14c-0588-43ca-96ef-921003eb2bdd";
        this.driversList = ["Tomasz Czekała", "Włodzimierz Fertyk", "Krzysiek Chojnacki", "Adam Ciechański",
            "Adam Wilczyński", "Leszek Tecław", "Paweł Plewiński", "Mirosław Tecław"];
        this.paymentTypesList = ["CASH", "DKV"];
        this.repairActivityList = ["DELIVERY", "RECEPTION"];
        this.loginFormVisibility = true;
        this.isTrailersListVisible = false;
    }
    GlobalService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], GlobalService);
    return GlobalService;
}());
exports.GlobalService = GlobalService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2xvYmFsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJnbG9iYWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUczQztJQWlCQztRQWRPLGVBQVUsR0FBVSxVQUFVLENBQUM7UUFDL0IsY0FBUyxHQUFVLFdBQVcsQ0FBQztRQUMvQixnQkFBVyxHQUFVLDZCQUE2QixDQUFDO1FBQ25ELGtCQUFhLEdBQVcsc0NBQXNDLENBQUM7UUFDL0QsZ0JBQVcsR0FBRyxDQUFDLGdCQUFnQixFQUFFLG9CQUFvQixFQUFFLG9CQUFvQixFQUFFLGlCQUFpQjtZQUNyRyxpQkFBaUIsRUFBRSxlQUFlLEVBQUUsaUJBQWlCLEVBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNsRSxxQkFBZ0IsR0FBRyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNuQyx1QkFBa0IsR0FBRyxDQUFDLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQTtRQUU5Qyx3QkFBbUIsR0FBWSxJQUFJLENBQUM7UUFHcEMsMEJBQXFCLEdBQUcsS0FBSyxDQUFDO0lBRXJCLENBQUM7SUFqQkwsYUFBYTtRQUR6QixpQkFBVSxFQUFFOztPQUNBLGFBQWEsQ0FtQnpCO0lBQUQsb0JBQUM7Q0FBQSxBQW5CRCxJQW1CQztBQW5CWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEdsb2JhbFNlcnZpY2Uge1xuXG5cblx0cHVibGljIGdyYW50X3R5cGU6IHN0cmluZz0gXCJwYXNzd29yZFwiO1xuXHRwdWJsaWMgY2xpZW50X2lkOiBzdHJpbmc9IFwiTG9naXN0aWNzXCI7XG5cdHB1YmxpYyBrZXljbG9ha1VybDogc3RyaW5nPSBcImh0dHBzOi8vNDIuaXAtMTQ1LTIzOS04NS5ldVwiO1xuXHRwdWJsaWMgY2xpZW50X3NlY3JldDogc3RyaW5nID0gXCI5MmE1ZTE0Yy0wNTg4LTQzY2EtOTZlZi05MjEwMDNlYjJiZGRcIjtcblx0cHVibGljIGRyaXZlcnNMaXN0ID0gW1wiVG9tYXN6IEN6ZWthxYJhXCIsIFwiV8WCb2R6aW1pZXJ6IEZlcnR5a1wiLCBcIktyenlzaWVrIENob2puYWNraVwiLCBcIkFkYW0gQ2llY2hhxYRza2lcIixcblx0XCJBZGFtIFdpbGN6ecWEc2tpXCIsIFwiTGVzemVrIFRlY8WCYXdcIiwgXCJQYXdlxYIgUGxld2nFhHNraVwiLFwiTWlyb3PFgmF3IFRlY8WCYXdcIl07XG5cdHB1YmxpYyBwYXltZW50VHlwZXNMaXN0ID0gW1wiQ0FTSFwiLCBcIkRLVlwiXTtcblx0cHVibGljIHJlcGFpckFjdGl2aXR5TGlzdCA9IFtcIkRFTElWRVJZXCIsIFwiUkVDRVBUSU9OXCJdXG5cdHB1YmxpYyBhY3Rpdml0eUluZGljYXRvcjogYm9vbGVhbjtcblx0cHVibGljIGxvZ2luRm9ybVZpc2liaWxpdHk6IGJvb2xlYW4gPSB0cnVlO1xuXHRwdWJsaWMgY2FyQWRkTGluaztcblx0cHVibGljIGNhckFkZFRpdGxlO1xuXHRwdWJsaWMgaXNUcmFpbGVyc0xpc3RWaXNpYmxlID0gZmFsc2U7XG5cblx0Y29uc3RydWN0b3IoKSB7IH1cblxufVxuIl19