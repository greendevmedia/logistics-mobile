import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { UserService } from "../user/user.service";

@Injectable()
export class RefuelingService {

	public refuelingsUrl: string = "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/driver/api/v1/refuelings/";
	public headers;
	public options;
	public refuelingJSONfromGETbyID;
	public refuelingJSONforPOST;

	constructor(private http: Http, private userService: UserService) { }

	public addRefueling(refuelingJSON) {
		this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
		this.options = new RequestOptions({ headers: this.headers });
		return this.http.post(this.refuelingsUrl,refuelingJSON, this.options);
	}

	public getRefuelings(){
		this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token});
		this.options = new RequestOptions({ headers: this.headers });
		return this.http.get(this.refuelingsUrl + "/refuelings-limit?page=1&size=100", this.options);
	}

	public getRefuelingsById(id){
		this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token});
		this.options = new RequestOptions({ headers: this.headers });
		return this.http.get(this.refuelingsUrl+id, this.options);
	}

	public updateRefueling(refuelingJSON){
		this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
		this.options = new RequestOptions({ headers: this.headers });
		return this.http.put(this.refuelingsUrl, refuelingJSON, this.options);
	}

}
