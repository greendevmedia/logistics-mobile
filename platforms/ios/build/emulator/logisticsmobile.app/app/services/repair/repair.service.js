"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var global_service_1 = require("../global/global.service");
var user_service_1 = require("../user/user.service");
var email = require("nativescript-email");
var json_service_1 = require("../json/json.service");
var date_service_1 = require("../date/date.service");
var router_1 = require("nativescript-angular/router");
var RepairService = (function () {
    function RepairService(globalService, userService, http, jsonService, dateService, routerExtensions) {
        this.globalService = globalService;
        this.userService = userService;
        this.http = http;
        this.jsonService = jsonService;
        this.dateService = dateService;
        this.routerExtensions = routerExtensions;
        this.repairsUrl = "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/driver/api/v1/repairs/";
        this.activityType = this.globalService.repairActivityList[1];
    }
    RepairService.prototype.addService = function (repairJSON) {
        this.headers = new http_1.Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
        this.options = new http_1.RequestOptions({ headers: this.headers });
        return this.http.post(this.repairsUrl, repairJSON, this.options);
    };
    RepairService.prototype.sendService = function () {
        var _this = this;
        if (this.jsonService.trailerName == null) {
            this.trailerName = "";
        }
        else {
            this.trailerName = "Przyczepa: " + this.jsonService.trailerName;
        }
        if (this.jsonService.carName == null) {
            this.carName = "";
        }
        else {
            this.carName = "Pojazd: " + this.jsonService.carName;
        }
        this.composeOptions = {
            to: ['pawel@plewinski-logistics.com', 'adam.b.ciechanski@wp.pl', 'tomasz.czekala1@gmail.com'],
            subject: 'Serwis - ' + this.carName + ' ' + this.trailerName,
            body: "Kierowca: " + this.userService.driverName + "\n" + this.carName + " " + this.trailerName + "\nData serwisu: " + this.dateService.dateFormatForPost() + "\nStan licznika: " + this.counter + "\nOpis serwisu: " + this.repairType + "\nAdres warsztatu: " + this.carShopAddress + "\nUwagi do pojazdu: " + this.vehicalCheck + "\nCena: " + this.price
        };
        email.available().then(function (available) {
            if (available) {
                email.compose(_this.composeOptions).then(function (result) {
                    _this.routerExtensions.navigate(["/navpage"]);
                });
            }
        }).catch(function (error) { return console.error(error); });
    };
    RepairService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [global_service_1.GlobalService, user_service_1.UserService, http_1.Http, json_service_1.JsonService, date_service_1.DateService, router_1.RouterExtensions])
    ], RepairService);
    return RepairService;
}());
exports.RepairService = RepairService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVwYWlyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJyZXBhaXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyxzQ0FBd0U7QUFDeEUsMkRBQXlEO0FBQ3pELHFEQUFtRDtBQUNuRCwwQ0FBNEM7QUFDNUMscURBQW1EO0FBQ25ELHFEQUFtRDtBQUNuRCxzREFBK0Q7QUFHL0Q7SUFpQkUsdUJBQW9CLGFBQTRCLEVBQVUsV0FBd0IsRUFBVSxJQUFVLEVBQVUsV0FBd0IsRUFBVSxXQUF3QixFQUFVLGdCQUFrQztRQUFsTSxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBZi9NLGVBQVUsR0FBVyxpRkFBaUYsQ0FBQztRQUd2RyxpQkFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFZMkosQ0FBQztJQUVwTixrQ0FBVSxHQUFqQixVQUFrQixVQUFVO1FBQzVCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxjQUFPLENBQUMsRUFBRSxlQUFlLEVBQUUsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLGNBQWMsRUFBRSxpQ0FBaUMsRUFBRSxDQUFDLENBQUM7UUFDdkssSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLHFCQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDN0QsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRU0sbUNBQVcsR0FBbEI7UUFBQSxpQkE4QkE7UUE1QkUsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztZQUN2QyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUN4QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQztRQUNsRSxDQUFDO1FBRUQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztZQUNuQyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsT0FBTyxHQUFHLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQztRQUN2RCxDQUFDO1FBSUgsSUFBSSxDQUFDLGNBQWMsR0FBRztZQUNsQixFQUFFLEVBQUUsQ0FBQywrQkFBK0IsRUFBRSx5QkFBeUIsRUFBRSwyQkFBMkIsQ0FBQztZQUNoRyxPQUFPLEVBQUUsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXO1lBQ3pELElBQUksRUFBRSxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLEdBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLG1CQUFtQixHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFVBQVUsR0FBRyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsY0FBYyxHQUFHLHNCQUFzQixHQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLO1NBQ2hXLENBQUE7UUFHRCxLQUFLLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsU0FBUztZQUMvQixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNmLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU07b0JBQzdDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFBO2dCQUM3QyxDQUFDLENBQUMsQ0FBQTtZQUNILENBQUM7UUFDRixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFwQixDQUFvQixDQUFDLENBQUE7SUFDeEMsQ0FBQztJQXZEVyxhQUFhO1FBRHpCLGlCQUFVLEVBQUU7eUNBa0J3Qiw4QkFBYSxFQUF1QiwwQkFBVyxFQUFnQixXQUFJLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQTRCLHlCQUFnQjtPQWpCM00sYUFBYSxDQXlEekI7SUFBRCxvQkFBQztDQUFBLEFBekRELElBeURDO0FBekRZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cCwgSGVhZGVycywgUmVzcG9uc2UsIFJlcXVlc3RPcHRpb25zIH0gZnJvbSBcIkBhbmd1bGFyL2h0dHBcIjtcbmltcG9ydCB7IEdsb2JhbFNlcnZpY2UgfSBmcm9tICcuLi9nbG9iYWwvZ2xvYmFsLnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuLi91c2VyL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgKiBhcyBlbWFpbCBmcm9tIFwibmF0aXZlc2NyaXB0LWVtYWlsXCI7XG5pbXBvcnQgeyBKc29uU2VydmljZSB9IGZyb20gJy4uL2pzb24vanNvbi5zZXJ2aWNlJztcbmltcG9ydCB7IERhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vZGF0ZS9kYXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFJlcGFpclNlcnZpY2Uge1xuXG4gIHB1YmxpYyByZXBhaXJzVXJsOiBzdHJpbmcgPSBcImh0dHA6Ly9hcGlsb2dpc3RpY3MuZ3JlZW5kZXYuaW4vbG9naXN0aWNzLTAuMC4xLVNOQVBTSE9UL2RyaXZlci9hcGkvdjEvcmVwYWlycy9cIjtcbiAgcHVibGljIHJlcGFpckpTT05mb3JQT1NUO1xuICBwdWJsaWMgY291bnRlcjtcbiAgcHVibGljIGFjdGl2aXR5VHlwZSA9IHRoaXMuZ2xvYmFsU2VydmljZS5yZXBhaXJBY3Rpdml0eUxpc3RbMV07XG4gIHB1YmxpYyByZXBhaXJUeXBlO1xuICBwdWJsaWMgcHJpY2U7XG4gIHB1YmxpYyBjYXJTaG9wQWRkcmVzcztcbiAgcHVibGljIGhlYWRlcnM7XG4gIHB1YmxpYyBvcHRpb25zO1xuICBwdWJsaWMgY29tcG9zZU9wdGlvbnM6IGVtYWlsLkNvbXBvc2VPcHRpb25zO1xuICBwdWJsaWMgY2FyTmFtZTtcbiAgcHVibGljIHRyYWlsZXJOYW1lO1xuICBwdWJsaWMgdmVoaWNhbENoZWNrO1xuICBcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBwcml2YXRlIGh0dHA6IEh0dHAsIHByaXZhdGUganNvblNlcnZpY2U6IEpzb25TZXJ2aWNlLCBwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zKSB7IH1cblxuICBwdWJsaWMgYWRkU2VydmljZShyZXBhaXJKU09OKSB7XG5cdFx0dGhpcy5oZWFkZXJzID0gbmV3IEhlYWRlcnMoeyBcIkF1dGhvcml6YXRpb25cIjogXCJiZWFyZXIgXCIgKyB0aGlzLnVzZXJTZXJ2aWNlLnVzZXJBcGlBY2Nlc3NEYXRhLmpzb24oKS5hY2Nlc3NfdG9rZW4sIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOFwiIH0pO1xuXHRcdHRoaXMub3B0aW9ucyA9IG5ldyBSZXF1ZXN0T3B0aW9ucyh7IGhlYWRlcnM6IHRoaXMuaGVhZGVycyB9KTtcblx0XHRyZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5yZXBhaXJzVXJsLCByZXBhaXJKU09OLCB0aGlzLm9wdGlvbnMpO1xuICB9XG4gIFxuICBwdWJsaWMgc2VuZFNlcnZpY2UoKSB7XG4gICAgXG4gICAgaWYodGhpcy5qc29uU2VydmljZS50cmFpbGVyTmFtZSA9PSBudWxsKXtcbiAgICAgIHRoaXMudHJhaWxlck5hbWUgPSBcIlwiO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnRyYWlsZXJOYW1lID0gXCJQcnp5Y3plcGE6IFwiICsgdGhpcy5qc29uU2VydmljZS50cmFpbGVyTmFtZTtcbiAgICB9XG5cbiAgICBpZih0aGlzLmpzb25TZXJ2aWNlLmNhck5hbWUgPT0gbnVsbCl7XG4gICAgICB0aGlzLmNhck5hbWUgPSBcIlwiO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmNhck5hbWUgPSBcIlBvamF6ZDogXCIgKyB0aGlzLmpzb25TZXJ2aWNlLmNhck5hbWU7XG4gICAgfVxuXG4gICAgXG5cblx0XHR0aGlzLmNvbXBvc2VPcHRpb25zID0ge1xuICAgICAgdG86IFsncGF3ZWxAcGxld2luc2tpLWxvZ2lzdGljcy5jb20nLCAnYWRhbS5iLmNpZWNoYW5za2lAd3AucGwnLCAndG9tYXN6LmN6ZWthbGExQGdtYWlsLmNvbSddLFxuXHRcdFx0c3ViamVjdDogJ1NlcndpcyAtICcgKyB0aGlzLmNhck5hbWUgKyAnICcgKyB0aGlzLnRyYWlsZXJOYW1lLFxuICAgICAgYm9keTogXCJLaWVyb3djYTogXCIgKyB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUgKyBcIlxcblwiICsgdGhpcy5jYXJOYW1lICsgXCIgXCIgKyB0aGlzLnRyYWlsZXJOYW1lICsgIFwiXFxuRGF0YSBzZXJ3aXN1OiBcIiArIHRoaXMuZGF0ZVNlcnZpY2UuZGF0ZUZvcm1hdEZvclBvc3QoKSArIFwiXFxuU3RhbiBsaWN6bmlrYTogXCIgKyB0aGlzLmNvdW50ZXIgKyBcIlxcbk9waXMgc2Vyd2lzdTogXCIgKyB0aGlzLnJlcGFpclR5cGUgKyBcIlxcbkFkcmVzIHdhcnN6dGF0dTogXCIgKyB0aGlzLmNhclNob3BBZGRyZXNzICsgXCJcXG5Vd2FnaSBkbyBwb2phemR1OiBcIiArIHRoaXMudmVoaWNhbENoZWNrICsgXCJcXG5DZW5hOiBcIiArIHRoaXMucHJpY2UgXG5cdFx0fVxuXG5cblx0XHRlbWFpbC5hdmFpbGFibGUoKS50aGVuKGF2YWlsYWJsZSA9PiB7XG5cdFx0XHRpZiAoYXZhaWxhYmxlKSB7XG5cdFx0XHRcdGVtYWlsLmNvbXBvc2UodGhpcy5jb21wb3NlT3B0aW9ucykudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0XHRcdHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvbmF2cGFnZVwiXSlcblx0XHRcdFx0fSlcblx0XHRcdH1cblx0XHR9KS5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSlcblx0fVxuXG59XG4iXX0=