import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { GlobalService } from '../global/global.service';
import { UserService } from '../user/user.service';
import * as email from "nativescript-email";
import { JsonService } from '../json/json.service';
import { DateService } from '../date/date.service';
import { RouterExtensions } from "nativescript-angular/router";

@Injectable()
export class RepairService {

  public repairsUrl: string = "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/driver/api/v1/repairs/";
  public repairJSONforPOST;
  public counter;
  public activityType = this.globalService.repairActivityList[1];
  public repairType;
  public price;
  public carShopAddress;
  public headers;
  public options;
  public composeOptions: email.ComposeOptions;
  public carName;
  public trailerName;
  public vehicalCheck;
  

  constructor(private globalService: GlobalService, private userService: UserService, private http: Http, private jsonService: JsonService, private dateService: DateService, private routerExtensions: RouterExtensions) { }

  public addService(repairJSON) {
		this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
		this.options = new RequestOptions({ headers: this.headers });
		return this.http.post(this.repairsUrl, repairJSON, this.options);
  }
  
  public sendService() {
    
    if(this.jsonService.trailerName == null){
      this.trailerName = "";
    } else {
      this.trailerName = "Przyczepa: " + this.jsonService.trailerName;
    }

    if(this.jsonService.carName == null){
      this.carName = "";
    } else {
      this.carName = "Pojazd: " + this.jsonService.carName;
    }

    

		this.composeOptions = {
      to: ['pawel@plewinski-logistics.com', 'adam.b.ciechanski@wp.pl', 'tomasz.czekala1@gmail.com'],
			subject: 'Serwis - ' + this.carName + ' ' + this.trailerName,
      body: "Kierowca: " + this.userService.driverName + "\n" + this.carName + " " + this.trailerName +  "\nData serwisu: " + this.dateService.dateFormatForPost() + "\nStan licznika: " + this.counter + "\nOpis serwisu: " + this.repairType + "\nAdres warsztatu: " + this.carShopAddress + "\nUwagi do pojazdu: " + this.vehicalCheck + "\nCena: " + this.price 
		}


		email.available().then(available => {
			if (available) {
				email.compose(this.composeOptions).then(result => {
					this.routerExtensions.navigate(["/navpage"])
				})
			}
		}).catch(error => console.error(error))
	}

}
