import { Injectable } from '@angular/core';
import { ListPicker } from "ui/list-picker";
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { UserService } from '../user/user.service';

@Injectable()
export class AdditionalPaymentService {

	public pickedAdditionalpaymentNameId;
	public additionalPaymentUrl: string = "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/driver/api/v1/additional-payments/";
	public headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
	public options = new RequestOptions({ headers: this.headers });
	public additionalPaymentJSONforPOST;
	public additionalPaymentJSONfromGET;
	public additionalPaymentJSONfromGETbyId;
	public totalPrice;
	public paymentType;
	public currency;


	constructor(private http: Http, private userService: UserService) { }

	public selectedIndexChangedOnAdditionalPaymentsNames(args) {
		let picker = <ListPicker>args.object;
		this.pickedAdditionalpaymentNameId = picker.selectedIndex + 1;
		console.log(this.pickedAdditionalpaymentNameId);
	}

	public addAdditionalPayment(additionalPaymentJSON) {
		this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
		this.options = new RequestOptions({ headers: this.headers });
		return this.http.post(this.additionalPaymentUrl, additionalPaymentJSON, this.options);
	}

	public getAdditionalPayment() {
		this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
		this.options = new RequestOptions({ headers: this.headers });
		return this.http.get(this.additionalPaymentUrl + "/additional-payment-limit?page=0&size=100", this.options);
	}

	public getAdditionalPaymentById(id) {
		this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
		this.options = new RequestOptions({ headers: this.headers });
		return this.http.get(this.additionalPaymentUrl+id, this.options);
	}

	public updateAdditionalPayment(additionalPaymentJSON) {
		this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json; charset=utf-8" });
		this.options = new RequestOptions({ headers: this.headers });
		return this.http.put(this.additionalPaymentUrl, additionalPaymentJSON, this.options);
	}





}
