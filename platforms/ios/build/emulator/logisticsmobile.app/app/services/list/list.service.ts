import { Injectable } from '@angular/core';
import { ListPicker } from "ui/list-picker";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { UserService } from "../user/user.service";
@Injectable()
export class ListService {

    public carsUrl: string = "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/manager/api/v1/cars/";
    public currenciesUrl: string = "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/manager/api/v1/currencies/";
    public additionalPaymentNamesUrl: string = "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/driver/api/v1/payments-name/";
    public trailersURL: string =  "http://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT/manager/api/v1/trailers";
    public headers;
    public options;
    public carsFromGetJSON;
    public carsList = [];
    public currenciesFromGetJSON;
    public currenciesList = [];
    public additionalPaymentsNamesFromGetJSON;
    public additionalPaymentsNamesList =[];
    public trailersFromGetJSON;
    public trailersList = [];


    constructor(private http: Http, private userService: UserService) { }

    public getCars() {
        this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
        this.options = new RequestOptions({ headers: this.headers });
        this.http.get(this.carsUrl, this.options).subscribe(
            (res) => { this.carsFromGetJSON = res.json(), this.carsList =  this.carsJSONtoList(this.carsFromGetJSON)},
            (err) => { alert("Błąd przy załadowaniu listy samochodów") })
    }

    public carsJSONtoList(carsJSON){
        let list = [];
        for(let i in carsJSON){
            list.push(carsJSON[i].name);
        }
        return list;
    }

    public getCurrencies() {
        this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
        this.options = new RequestOptions({ headers: this.headers });
        this.http.get(this.currenciesUrl, this.options).subscribe(
            (res) => { this.currenciesFromGetJSON = res.json(), this.currenciesList =  this.currenciesJSONtoList(this.currenciesFromGetJSON)},
            (err) => { alert("Błąd przy załadowaniu listy walut") })
    }

    public currenciesJSONtoList(currenciesJSON){
        let list = [];
        for(let i in currenciesJSON){
            list.push(currenciesJSON[i].currencyCode);
        }
        return list;
    }

    public getAdditionalPaymentsNames() {
        this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
        this.options = new RequestOptions({ headers: this.headers });
        this.http.get(this.additionalPaymentNamesUrl, this.options).subscribe(
            (res) => { this.additionalPaymentsNamesFromGetJSON = res.json(), this.additionalPaymentsNamesList =  this.additionalPaymentsJSONtoList(this.additionalPaymentsNamesFromGetJSON)},
            (err) => { alert("Błąd przy załadowaniu listy dodatkowych opłat") })
    }

    public additionalPaymentsJSONtoList(additiotnalPaymentsNamesJSON){
        let list = [];
        for(let i in additiotnalPaymentsNamesJSON){
            list.push(additiotnalPaymentsNamesJSON[i].paymentName);
        }
        return list;
    }

    public getTrailers() {
        this.headers = new Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
        this.options = new RequestOptions({ headers: this.headers });
        this.http.get(this.trailersURL, this.options).subscribe(
            (res) => { this.trailersFromGetJSON = res.json(), this.trailersList =  this.trailersJSONtoList(this.trailersFromGetJSON)},
            (err) => { alert("Błąd przy załadowaniu listy samochodów") })
    }

    public trailersJSONtoList(trailersJSON){
        let list = [];
        for(let i in trailersJSON){
            list.push(trailersJSON[i].name);
        }
        return list;
    }




}

