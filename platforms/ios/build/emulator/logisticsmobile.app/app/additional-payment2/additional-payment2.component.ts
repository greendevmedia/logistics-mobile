import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { DateService } from '../services/date/date.service';
import { AdditionalPaymentService } from '../services/additional-payment/additional-payment.service';
import { GlobalService } from '../services/global/global.service';
import { ListService } from '../services/list/list.service';
import { JsonService } from '../services/json/json.service';
import { UserService } from '../services/user/user.service';
import { RouterExtensions } from "nativescript-angular/router";

@Component({
	moduleId: module.id,
	selector: 'app-additional-payment2',
	templateUrl: './additional-payment2.component.html',
	styleUrls: ['./additional-payment2.component.scss']
})
export class AdditionalPayment2Component implements OnInit {

	constructor(private page: Page, private dateService: DateService, private additionalPaymentService: AdditionalPaymentService, private globalService: GlobalService, private listService: ListService, private jsonService: JsonService, private userService: UserService, private routerExtensions: RouterExtensions
	) { }

	ngOnInit() {
		this.additionalPaymentService.totalPrice = null;
	}


	public addAdditionalPayment() {
		this.additionalPaymentService.additionalPaymentJSONforPOST = {
			paymentDay: this.dateService.dateFormatForPost(),
			paymentName: { 
				id: this.additionalPaymentService.pickedAdditionalpaymentNameId 
			},
			totalPrice: this.additionalPaymentService.totalPrice,
			paymentType: this.jsonService.pickedPaymentType,
			driverName: this.userService.driverName,
			car: {
				id: this.jsonService.pickedCarId
			},
			currency: {
				id: this.jsonService.pickedCurrencyId
			}

		}

		this.additionalPaymentService.addAdditionalPayment(this.additionalPaymentService.additionalPaymentJSONforPOST).subscribe(
			(res) => {console.log(this.additionalPaymentService.additionalPaymentJSONforPOST.totalPrice)
				alert({
					title: "Potwierdzenie",
					message: "Opłata dodana poprawnie",
					okButtonText: "OK"
				}), this.routerExtensions.navigate(["/additional-payment-details"])
			},
			(err) => {
				alert({
					title: "Błąd",
					message: "Popraw dane i spróbuj ponownie",
					okButtonText: "OK"
				})
			},
		);
	}

}
