"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_routing_1 = require("./app.routing");
var app_component_1 = require("./app.component");
var home_component_1 = require("./home/home.component");
var forms_1 = require("nativescript-angular/forms");
var http_1 = require("nativescript-angular/http");
var login_service_1 = require("./services/login/login.service");
var global_service_1 = require("./services/global/global.service");
var user_service_1 = require("./services/user/user.service");
var refueling_service_1 = require("./services/refueling/refueling.service");
var angular_1 = require("nativescript-drop-down/angular");
var refueling_edit_component_1 = require("./refueling-edit/refueling-edit.component");
var date_service_1 = require("./services/date/date.service");
var list_service_1 = require("./services/list/list.service");
var navigation_component_1 = require("./navigation/navigation.component");
var navpage_component_1 = require("./navpage/navpage.component");
var car_add_component_1 = require("./car-add/car-add.component");
var json_service_1 = require("./services/json/json.service");
var technical_card_service_1 = require("./services/technical-card/technical-card.service");
var technical_card_1_component_1 = require("./technical-card-1/technical-card-1.component");
var technical_card_2_component_1 = require("./technical-card-2/technical-card-2.component");
var refuelling_add_component_1 = require("./refuelling-add/refuelling-add.component");
var refueling_list_component_1 = require("./refueling-list/refueling-list.component");
var additional_payment1_component_1 = require("./additional-payment1/additional-payment1.component");
var additional_payment2_component_1 = require("./additional-payment2/additional-payment2.component");
var additional_payment_service_1 = require("./services/additional-payment/additional-payment.service");
var addiional_payment_details_component_1 = require("./addiional-payment-details/addiional-payment-details.component");
var additionl_payments_list_component_1 = require("./additionl-payments-list/additionl-payments-list.component");
var additional_payment_edit_component_1 = require("./additional-payment-edit/additional-payment-edit.component");
var repair_1_component_1 = require("./repair-1/repair-1.component");
var repair_service_1 = require("./services/repair/repair.service");
var repair_2_component_1 = require("./repair-2/repair-2.component");
var nativescript_ngx_fonticon_1 = require("nativescript-ngx-fonticon");
var AppModule = (function () {
    /*
    Pass your application module to the bootstrapModule function located in main.ts to start your app
    */
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                app_component_1.AppComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_1.AppRoutingModule,
                forms_1.NativeScriptFormsModule,
                http_1.NativeScriptHttpModule,
                angular_1.DropDownModule,
                nativescript_ngx_fonticon_1.TNSFontIconModule.forRoot({
                    'fa': './assets/font-awesome.css',
                    'ion': './assets/ionicons.css',
                    'mdi': 'material-design-icons.css'
                })
            ],
            declarations: [
                app_component_1.AppComponent,
                home_component_1.HomeComponent,
                refueling_edit_component_1.RefuelingEditComponent,
                navigation_component_1.NavigationComponent,
                navpage_component_1.NavpageComponent,
                car_add_component_1.CarAddComponent,
                technical_card_1_component_1.TechnicalCard1Component,
                technical_card_2_component_1.TechnicalCard2Component,
                refuelling_add_component_1.RefuellingAddComponent,
                refueling_list_component_1.RefuelingListComponent,
                additional_payment1_component_1.AdditionalPayment1Component,
                additional_payment2_component_1.AdditionalPayment2Component,
                addiional_payment_details_component_1.AddiionalPaymentDetailsComponent,
                additionl_payments_list_component_1.AdditionlPaymentsListComponent,
                additional_payment_edit_component_1.AdditionalPaymentEditComponent,
                repair_1_component_1.Repair1Component,
                repair_2_component_1.Repair2Component
            ],
            providers: [
                global_service_1.GlobalService,
                login_service_1.LoginService,
                user_service_1.UserService,
                refueling_service_1.RefuelingService,
                date_service_1.DateService,
                list_service_1.ListService,
                json_service_1.JsonService,
                technical_card_service_1.TechnicalCardService,
                additional_payment_service_1.AdditionalPaymentService,
                repair_service_1.RepairService
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
        /*
        Pass your application module to the bootstrapModule function located in main.ts to start your app
        */
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBQzlFLDZDQUFpRDtBQUNqRCxpREFBK0M7QUFDL0Msd0RBQXNEO0FBQ3RELG9EQUFxRTtBQUNyRSxrREFBbUU7QUFDbkUsZ0VBQThEO0FBQzlELG1FQUFnRTtBQUNoRSw2REFBMkQ7QUFFM0QsNEVBQTBFO0FBQzFFLDBEQUFnRTtBQUNoRSxzRkFBa0Y7QUFDbEYsNkRBQTJEO0FBQzNELDZEQUEyRDtBQUMzRCwwRUFBdUU7QUFDdkUsaUVBQThEO0FBQzlELGlFQUE4RDtBQUM5RCw2REFBMkQ7QUFDM0QsMkZBQXdGO0FBQ3hGLDRGQUF3RjtBQUN4Riw0RkFBd0Y7QUFDeEYsc0ZBQW1GO0FBQ25GLHNGQUFtRjtBQUNuRixxR0FBa0c7QUFDbEcscUdBQWtHO0FBQ2xHLHVHQUFvRztBQUNwRyx1SEFBbUg7QUFDbkgsaUhBQTZHO0FBQzdHLGlIQUE2RztBQUM3RyxvRUFBaUU7QUFDakUsbUVBQWlFO0FBQ2pFLG9FQUFpRTtBQUNqRSx1RUFBOEQ7QUEwRDlEO0lBSEE7O01BRUU7SUFDRjtJQUF5QixDQUFDO0lBQWIsU0FBUztRQXREckIsZUFBUSxDQUFDO1lBQ04sU0FBUyxFQUFFO2dCQUNQLDRCQUFZO2FBQ2Y7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsd0NBQWtCO2dCQUNsQiw4QkFBZ0I7Z0JBQ2hCLCtCQUF1QjtnQkFDdkIsNkJBQXNCO2dCQUN0Qix3QkFBYztnQkFDZCw2Q0FBaUIsQ0FBQyxPQUFPLENBQUM7b0JBQy9CLElBQUksRUFBRSwyQkFBMkI7b0JBQ3hCLEtBQUssRUFBRSx1QkFBdUI7b0JBQzlCLEtBQUssRUFBRSwyQkFBMkI7aUJBQzNDLENBQUM7YUFDQztZQUNELFlBQVksRUFBRTtnQkFDViw0QkFBWTtnQkFDWiw4QkFBYTtnQkFDYixpREFBc0I7Z0JBQ3RCLDBDQUFtQjtnQkFDbkIsb0NBQWdCO2dCQUNoQixtQ0FBZTtnQkFDZixvREFBdUI7Z0JBQ3ZCLG9EQUF1QjtnQkFDdkIsaURBQXNCO2dCQUN0QixpREFBc0I7Z0JBQ3RCLDJEQUEyQjtnQkFDM0IsMkRBQTJCO2dCQUMzQixzRUFBZ0M7Z0JBQ2hDLGtFQUE4QjtnQkFDOUIsa0VBQThCO2dCQUM5QixxQ0FBZ0I7Z0JBQ2hCLHFDQUFnQjthQUNuQjtZQUNELFNBQVMsRUFBRTtnQkFDUCw4QkFBYTtnQkFDYiw0QkFBWTtnQkFDWiwwQkFBVztnQkFDWCxvQ0FBZ0I7Z0JBQ2hCLDBCQUFXO2dCQUNYLDBCQUFXO2dCQUNYLDBCQUFXO2dCQUNYLDZDQUFvQjtnQkFDcEIscURBQXdCO2dCQUN4Qiw4QkFBYTthQUNoQjtZQUNELE9BQU8sRUFBRTtnQkFDTCx1QkFBZ0I7YUFDbkI7U0FDSixDQUFDO1FBQ0Y7O1VBRUU7T0FDVyxTQUFTLENBQUk7SUFBRCxnQkFBQztDQUFBLEFBQTFCLElBQTBCO0FBQWIsOEJBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbmF0aXZlc2NyaXB0Lm1vZHVsZVwiO1xuaW1wb3J0IHsgQXBwUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL2FwcC5yb3V0aW5nXCI7XG5pbXBvcnQgeyBBcHBDb21wb25lbnQgfSBmcm9tIFwiLi9hcHAuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBIb21lQ29tcG9uZW50IH0gZnJvbSBcIi4vaG9tZS9ob21lLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cFwiO1xuaW1wb3J0IHsgTG9naW5TZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZXMvbG9naW4vbG9naW4uc2VydmljZVwiO1xuaW1wb3J0IHsgR2xvYmFsU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2VzL2dsb2JhbC9nbG9iYWwuc2VydmljZVwiXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBSZWZ1ZWxpbmdTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZXMvcmVmdWVsaW5nL3JlZnVlbGluZy5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBEcm9wRG93bk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtZHJvcC1kb3duL2FuZ3VsYXJcIjtcbmltcG9ydCB7IFJlZnVlbGluZ0VkaXRDb21wb25lbnQgfSBmcm9tIFwiLi9yZWZ1ZWxpbmctZWRpdC9yZWZ1ZWxpbmctZWRpdC5jb21wb25lbnRcIlxuaW1wb3J0IHsgRGF0ZVNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy9kYXRlL2RhdGUuc2VydmljZVwiO1xuaW1wb3J0IHsgTGlzdFNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy9saXN0L2xpc3Quc2VydmljZVwiO1xuaW1wb3J0IHsgTmF2aWdhdGlvbkNvbXBvbmVudCB9IGZyb20gXCIuL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnRcIlxuaW1wb3J0IHsgTmF2cGFnZUNvbXBvbmVudCB9IGZyb20gXCIuL25hdnBhZ2UvbmF2cGFnZS5jb21wb25lbnRcIlxuaW1wb3J0IHsgQ2FyQWRkQ29tcG9uZW50IH0gZnJvbSBcIi4vY2FyLWFkZC9jYXItYWRkLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgSnNvblNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy9qc29uL2pzb24uc2VydmljZVwiO1xuaW1wb3J0IHsgVGVjaG5pY2FsQ2FyZFNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy90ZWNobmljYWwtY2FyZC90ZWNobmljYWwtY2FyZC5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBUZWNobmljYWxDYXJkMUNvbXBvbmVudCB9IGZyb20gXCIuL3RlY2huaWNhbC1jYXJkLTEvdGVjaG5pY2FsLWNhcmQtMS5jb21wb25lbnRcIjtcbmltcG9ydCB7IFRlY2huaWNhbENhcmQyQ29tcG9uZW50IH0gZnJvbSBcIi4vdGVjaG5pY2FsLWNhcmQtMi90ZWNobmljYWwtY2FyZC0yLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUmVmdWVsbGluZ0FkZENvbXBvbmVudCB9IGZyb20gXCIuL3JlZnVlbGxpbmctYWRkL3JlZnVlbGxpbmctYWRkLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUmVmdWVsaW5nTGlzdENvbXBvbmVudCB9IGZyb20gXCIuL3JlZnVlbGluZy1saXN0L3JlZnVlbGluZy1saXN0LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWRkaXRpb25hbFBheW1lbnQxQ29tcG9uZW50IH0gZnJvbSBcIi4vYWRkaXRpb25hbC1wYXltZW50MS9hZGRpdGlvbmFsLXBheW1lbnQxLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWRkaXRpb25hbFBheW1lbnQyQ29tcG9uZW50IH0gZnJvbSBcIi4vYWRkaXRpb25hbC1wYXltZW50Mi9hZGRpdGlvbmFsLXBheW1lbnQyLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZXMvYWRkaXRpb25hbC1wYXltZW50L2FkZGl0aW9uYWwtcGF5bWVudC5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBBZGRpaW9uYWxQYXltZW50RGV0YWlsc0NvbXBvbmVudCB9IGZyb20gXCIuL2FkZGlpb25hbC1wYXltZW50LWRldGFpbHMvYWRkaWlvbmFsLXBheW1lbnQtZGV0YWlscy5jb21wb25lbnRcIjtcbmltcG9ydCB7IEFkZGl0aW9ubFBheW1lbnRzTGlzdENvbXBvbmVudCB9IGZyb20gXCIuL2FkZGl0aW9ubC1wYXltZW50cy1saXN0L2FkZGl0aW9ubC1wYXltZW50cy1saXN0LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWRkaXRpb25hbFBheW1lbnRFZGl0Q29tcG9uZW50IH0gZnJvbSBcIi4vYWRkaXRpb25hbC1wYXltZW50LWVkaXQvYWRkaXRpb25hbC1wYXltZW50LWVkaXQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBSZXBhaXIxQ29tcG9uZW50IH0gZnJvbSBcIi4vcmVwYWlyLTEvcmVwYWlyLTEuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBSZXBhaXJTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZXMvcmVwYWlyL3JlcGFpci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBSZXBhaXIyQ29tcG9uZW50IH0gZnJvbSBcIi4vcmVwYWlyLTIvcmVwYWlyLTIuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBUTlNGb250SWNvbk1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1uZ3gtZm9udGljb24nO1xuXG5cblxuQE5nTW9kdWxlKHtcbiAgICBib290c3RyYXA6IFtcbiAgICAgICAgQXBwQ29tcG9uZW50XG4gICAgXSxcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcbiAgICAgICAgQXBwUm91dGluZ01vZHVsZSxcbiAgICAgICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGUsXG4gICAgICAgIERyb3BEb3duTW9kdWxlLFxuICAgICAgICBUTlNGb250SWNvbk1vZHVsZS5mb3JSb290KHtcblx0XHRcdCdmYSc6ICcuL2Fzc2V0cy9mb250LWF3ZXNvbWUuY3NzJyxcbiAgICAgICAgICAgICdpb24nOiAnLi9hc3NldHMvaW9uaWNvbnMuY3NzJyxcbiAgICAgICAgICAgICdtZGknOiAnbWF0ZXJpYWwtZGVzaWduLWljb25zLmNzcydcblx0XHR9KVxuICAgIF0sXG4gICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgIEFwcENvbXBvbmVudCxcbiAgICAgICAgSG9tZUNvbXBvbmVudCxcbiAgICAgICAgUmVmdWVsaW5nRWRpdENvbXBvbmVudCxcbiAgICAgICAgTmF2aWdhdGlvbkNvbXBvbmVudCxcbiAgICAgICAgTmF2cGFnZUNvbXBvbmVudCxcbiAgICAgICAgQ2FyQWRkQ29tcG9uZW50LFxuICAgICAgICBUZWNobmljYWxDYXJkMUNvbXBvbmVudCxcbiAgICAgICAgVGVjaG5pY2FsQ2FyZDJDb21wb25lbnQsXG4gICAgICAgIFJlZnVlbGxpbmdBZGRDb21wb25lbnQsXG4gICAgICAgIFJlZnVlbGluZ0xpc3RDb21wb25lbnQsXG4gICAgICAgIEFkZGl0aW9uYWxQYXltZW50MUNvbXBvbmVudCxcbiAgICAgICAgQWRkaXRpb25hbFBheW1lbnQyQ29tcG9uZW50LFxuICAgICAgICBBZGRpaW9uYWxQYXltZW50RGV0YWlsc0NvbXBvbmVudCxcbiAgICAgICAgQWRkaXRpb25sUGF5bWVudHNMaXN0Q29tcG9uZW50LFxuICAgICAgICBBZGRpdGlvbmFsUGF5bWVudEVkaXRDb21wb25lbnQsXG4gICAgICAgIFJlcGFpcjFDb21wb25lbnQsXG4gICAgICAgIFJlcGFpcjJDb21wb25lbnRcbiAgICBdLFxuICAgIHByb3ZpZGVyczogW1xuICAgICAgICBHbG9iYWxTZXJ2aWNlLFxuICAgICAgICBMb2dpblNlcnZpY2UsXG4gICAgICAgIFVzZXJTZXJ2aWNlLFxuICAgICAgICBSZWZ1ZWxpbmdTZXJ2aWNlLFxuICAgICAgICBEYXRlU2VydmljZSxcbiAgICAgICAgTGlzdFNlcnZpY2UsXG4gICAgICAgIEpzb25TZXJ2aWNlLFxuICAgICAgICBUZWNobmljYWxDYXJkU2VydmljZSxcbiAgICAgICAgQWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLFxuICAgICAgICBSZXBhaXJTZXJ2aWNlXG4gICAgXSxcbiAgICBzY2hlbWFzOiBbXG4gICAgICAgIE5PX0VSUk9SU19TQ0hFTUFcbiAgICBdXG59KVxuLypcblBhc3MgeW91ciBhcHBsaWNhdGlvbiBtb2R1bGUgdG8gdGhlIGJvb3RzdHJhcE1vZHVsZSBmdW5jdGlvbiBsb2NhdGVkIGluIG1haW4udHMgdG8gc3RhcnQgeW91ciBhcHBcbiovXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfVxuIl19