import { Component, OnInit } from '@angular/core';
import { TechnicalCardService } from "../services/technical-card/technical-card.service";
import { Page } from "ui/page";
import { ScrollEventData } from "ui/scroll-view";


@Component({
	moduleId: module.id,
	selector: 'app-technical-card-2',
	templateUrl: './technical-card-2.component.html',
	styleUrls: ['./technical-card-2.component.scss']
})
export class TechnicalCard2Component implements OnInit {

	constructor(private technicalCardService:TechnicalCardService, private page: Page ) { }

	ngOnInit() {
		// this.page.actionBarHidden = true;
		this.technicalCardService.purity = "";
		this.technicalCardService.lighting = "";
		this.technicalCardService.suspension = "";
		this.technicalCardService.engineAndAccessories = "";
		this.technicalCardService.carBody = "";
		this.technicalCardService.composeOptions  = null;
		this.technicalCardService.imagesPaths = [];
		this.technicalCardService.arrayOfAttachments = [];
	 }

}
