import { Component, OnInit } from '@angular/core';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ListPicker } from "ui/list-picker";
import { LoginService } from "../services/login/login.service";
import { GlobalService } from "../services/global/global.service";
import { UserService } from "../services/user/user.service";
import { RouterExtensions } from "nativescript-angular/router";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { Page } from "ui/page";
import {
    getString,
    setString, 
} from "application-settings";
import { TNSFontIconService } from 'nativescript-ngx-fonticon';


@Component({
	moduleId: module.id,
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public pin;
	public body;
	public isDriverPicked: boolean = false;

	constructor(private loginService: LoginService, private globalService: GlobalService, private userService: UserService,
		private routerExtensions: RouterExtensions, private page: Page, private fonticon: TNSFontIconService) {
	}

	ngOnInit(): void {
		console.log(getString("username"));
		this.globalService.activityIndicator = false;
		this.page.actionBarHidden = true;
		this.globalService.loginFormVisibility = true;
		this.automaticLogin();
		
	}

	public selectedIndexChanged(args) {
		let picker = <ListPicker>args.object;
		this.userService.driverName = this.globalService.driversList[picker.selectedIndex];
		this.isDriverPicked = true;
	}

	public login() {
		
		this.globalService.loginFormVisibility = false;
		this.globalService.activityIndicator = true;

		setString("username", this.userService.driverName);
		setString("pin", this.pin);
	
		this.body = this.body = `username=` + this.userService.driverName + '&password=' + this.pin + '&client_id='
			+ this.globalService.client_id + '&client_secret=' + this.globalService.client_secret + '&grant_type=' + this.globalService.grant_type;

		this.loginService.login(this.body).subscribe(
			(res) => { this.userService.userApiAccessData = res, this.userService.isUserLogged = true, this.routerExtensions.navigate(["/navpage"],{ clearHistory: true }),  setString("username", this.userService.driverName), setString("pin", this.pin), this.pin = ""},
			(err) => {
				alert({
					title: "Błąd",
					message: "Wprowadź poprawny numer PIN do wybranego kierowcy",
					okButtonText: "OK"
				}),
				console.log(err);
				this.userService.userApiAccessData = null;
				this.userService.isUserLogged = false;
				this.globalService.activityIndicator = false;
				this.globalService.loginFormVisibility = true;
			}
		)
	}

	public automaticLogin(){
		if(getString("username") !== undefined && getString("pin") !== undefined){
			this.userService.driverName = getString("username");
			this.pin = getString("pin");
			console.log(this.userService.driverName);
			console.log(this.pin);
			this.login();
		}
	}



}
