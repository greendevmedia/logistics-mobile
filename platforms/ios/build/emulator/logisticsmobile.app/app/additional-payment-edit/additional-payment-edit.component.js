"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var date_service_1 = require("../services/date/date.service");
var additional_payment_service_1 = require("../services/additional-payment/additional-payment.service");
var global_service_1 = require("../services/global/global.service");
var list_service_1 = require("../services/list/list.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var AdditionalPaymentEditComponent = (function () {
    function AdditionalPaymentEditComponent(page, listService, dateService, additionalPaymentService, globalService, userService, routerExtensions) {
        this.page = page;
        this.listService = listService;
        this.dateService = dateService;
        this.additionalPaymentService = additionalPaymentService;
        this.globalService = globalService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
    }
    AdditionalPaymentEditComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.listService.getCurrencies();
        this.listService.getCars();
        this.listService.getAdditionalPaymentsNames();
        this.currency = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.currency.currencyCode;
        this.paymentType = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.paymentType;
        this.additionaPaymentId = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.id;
        this.totalPrice = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.totalPrice;
        console.log(this.totalPrice);
        this.paymentName = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.paymentName.paymentName;
        this.carName = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.car.name;
    };
    AdditionalPaymentEditComponent.prototype.selectedIndexChangedOnPaymentTypes = function (args) {
        var picker = args.object;
        this.paymentType = this.globalService.paymentTypesList[picker.selectedIndex];
    };
    AdditionalPaymentEditComponent.prototype.selectedIndexChangedOnCurrencies = function (args) {
        var picker = args.object;
        this.currencyId = picker.selectedIndex + 1;
    };
    AdditionalPaymentEditComponent.prototype.selectedIndexChangedOnCars = function (args) {
        var picker = args.object;
        this.carId = picker.selectedIndex + 1;
    };
    AdditionalPaymentEditComponent.prototype.updateAdditionalPayment = function () {
        var _this = this;
        this.additionalPaymentService.additionalPaymentJSONforPOST = {
            id: this.additionaPaymentId,
            paymentDay: this.dateService.dateFormatForPost(),
            paymentName: {
                id: this.additionalPaymentService.pickedAdditionalpaymentNameId
            },
            totalPrice: this.totalPrice,
            paymentType: this.paymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.carId
            },
            currency: {
                id: this.currencyId
            }
        };
        this.additionalPaymentService.updateAdditionalPayment(this.additionalPaymentService.additionalPaymentJSONforPOST).subscribe(function (res) {
            alert({
                title: "Potwierdzenie",
                message: "Opłata zmieniona poprawnie",
                okButtonText: "OK"
            }), _this.routerExtensions.navigate(["/additional-payment-details"]);
        }, function (err) {
            console.log(err);
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    AdditionalPaymentEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-additional-payment-edit',
            templateUrl: './additional-payment-edit.component.html',
            styleUrls: ['./additional-payment-edit.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, list_service_1.ListService, date_service_1.DateService, additional_payment_service_1.AdditionalPaymentService, global_service_1.GlobalService, user_service_1.UserService, router_1.RouterExtensions])
    ], AdditionalPaymentEditComponent);
    return AdditionalPaymentEditComponent;
}());
exports.AdditionalPaymentEditComponent = AdditionalPaymentEditComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkaXRpb25hbC1wYXltZW50LWVkaXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYWRkaXRpb25hbC1wYXltZW50LWVkaXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELGdDQUErQjtBQUMvQiw4REFBNEQ7QUFDNUQsd0dBQXFHO0FBQ3JHLG9FQUFrRTtBQUNsRSw4REFBNEQ7QUFFNUQsOERBQTREO0FBQzVELHNEQUErRDtBQVEvRDtJQWNDLHdDQUFvQixJQUFVLEVBQVUsV0FBd0IsRUFBVSxXQUF3QixFQUFVLHdCQUFrRCxFQUFVLGFBQTRCLEVBQVUsV0FBd0IsRUFBVSxnQkFBa0M7UUFBOVAsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO1FBQVUsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFBSyxDQUFDO0lBRXhSLGlEQUFRLEdBQVI7UUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztRQUM5QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDO1FBQ3JHLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGdDQUFnQyxDQUFDLFdBQVcsQ0FBQztRQUM5RixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGdDQUFnQyxDQUFDLEVBQUUsQ0FBQztRQUM1RixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxVQUFVLENBQUM7UUFDNUYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0NBQWdDLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQztRQUMxRyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDO0lBQ3hGLENBQUM7SUFFTSwyRUFBa0MsR0FBekMsVUFBMEMsSUFBSTtRQUN2QyxJQUFJLE1BQU0sR0FBZSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVNLHlFQUFnQyxHQUF2QyxVQUF3QyxJQUFJO1FBQ3hDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRU0sbUVBQTBCLEdBQWpDLFVBQWtDLElBQUk7UUFDbEMsSUFBSSxNQUFNLEdBQWUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFTSxnRUFBdUIsR0FBOUI7UUFBQSxpQkFtQ0M7UUFsQ0EsSUFBSSxDQUFDLHdCQUF3QixDQUFDLDRCQUE0QixHQUFHO1lBQzVELEVBQUUsRUFBQyxJQUFJLENBQUMsa0JBQWtCO1lBQzFCLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixFQUFFO1lBQ2hELFdBQVcsRUFBRTtnQkFDWixFQUFFLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLDZCQUE2QjthQUMvRDtZQUNELFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTtZQUMzQixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7WUFDN0IsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtZQUN2QyxHQUFHLEVBQUU7Z0JBQ0osRUFBRSxFQUFFLElBQUksQ0FBQyxLQUFLO2FBQ2Q7WUFDRCxRQUFRLEVBQUU7Z0JBQ1QsRUFBRSxFQUFFLElBQUksQ0FBQyxVQUFVO2FBQ25CO1NBRUQsQ0FBQTtRQUVELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxTQUFTLENBQzFILFVBQUMsR0FBRztZQUNILEtBQUssQ0FBQztnQkFDTCxLQUFLLEVBQUUsZUFBZTtnQkFDdEIsT0FBTyxFQUFFLDRCQUE0QjtnQkFDckMsWUFBWSxFQUFFLElBQUk7YUFDbEIsQ0FBQyxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUE7UUFDcEUsQ0FBQyxFQUNELFVBQUMsR0FBRztZQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDekIsS0FBSyxDQUFDO2dCQUNMLEtBQUssRUFBRSxNQUFNO2dCQUNiLE9BQU8sRUFBRSxnQ0FBZ0M7Z0JBQ3pDLFlBQVksRUFBRSxJQUFJO2FBQ2xCLENBQUMsQ0FBQTtRQUNILENBQUMsQ0FDRCxDQUFDO0lBQ0gsQ0FBQztJQWhGVyw4QkFBOEI7UUFOMUMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsNkJBQTZCO1lBQ3ZDLFdBQVcsRUFBRSwwQ0FBMEM7WUFDdkQsU0FBUyxFQUFFLENBQUMsMENBQTBDLENBQUM7U0FDdkQsQ0FBQzt5Q0FleUIsV0FBSSxFQUF1QiwwQkFBVyxFQUF1QiwwQkFBVyxFQUFvQyxxREFBd0IsRUFBeUIsOEJBQWEsRUFBdUIsMEJBQVcsRUFBNEIseUJBQWdCO09BZHRRLDhCQUE4QixDQWtGMUM7SUFBRCxxQ0FBQztDQUFBLEFBbEZELElBa0ZDO0FBbEZZLHdFQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCB7IERhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZGF0ZS9kYXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWRkaXRpb25hbC1wYXltZW50L2FkZGl0aW9uYWwtcGF5bWVudC5zZXJ2aWNlJztcbmltcG9ydCB7IEdsb2JhbFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9nbG9iYWwvZ2xvYmFsLnNlcnZpY2UnO1xuaW1wb3J0IHsgTGlzdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9saXN0L2xpc3Quc2VydmljZSc7XG5pbXBvcnQgeyBMaXN0UGlja2VyIH0gZnJvbSBcInVpL2xpc3QtcGlja2VyXCI7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5cbkBDb21wb25lbnQoe1xuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHRzZWxlY3RvcjogJ2FwcC1hZGRpdGlvbmFsLXBheW1lbnQtZWRpdCcsXG5cdHRlbXBsYXRlVXJsOiAnLi9hZGRpdGlvbmFsLXBheW1lbnQtZWRpdC5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL2FkZGl0aW9uYWwtcGF5bWVudC1lZGl0LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQWRkaXRpb25hbFBheW1lbnRFZGl0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuXHRwdWJsaWMgYWRkaXRpb25hUGF5bWVudElkO1xuXHRwdWJsaWMgdG90YWxQcmljZTtcblx0cHVibGljIHBheW1lbnROYW1lO1xuXHRwdWJsaWMgcGF5bWVudE5hbWVJZDtcblx0cHVibGljIHBheW1lbnRUeXBlO1xuXHRwdWJsaWMgY2FyTmFtZTtcblx0cHVibGljIGNhcklkO1xuXHRwdWJsaWMgY3VycmVuY3k7XG5cdHB1YmxpYyBjdXJyZW5jeUlkO1xuXG5cblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgbGlzdFNlcnZpY2U6IExpc3RTZXJ2aWNlLCBwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSwgcHJpdmF0ZSBhZGRpdGlvbmFsUGF5bWVudFNlcnZpY2U6IEFkZGl0aW9uYWxQYXltZW50U2VydmljZSwgcHJpdmF0ZSBnbG9iYWxTZXJ2aWNlOiBHbG9iYWxTZXJ2aWNlLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCkgeyB9XG5cblx0bmdPbkluaXQoKSB7XG5cdFx0dGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG5cdFx0dGhpcy5saXN0U2VydmljZS5nZXRDdXJyZW5jaWVzKCk7XG5cdFx0dGhpcy5saXN0U2VydmljZS5nZXRDYXJzKCk7XG5cdFx0dGhpcy5saXN0U2VydmljZS5nZXRBZGRpdGlvbmFsUGF5bWVudHNOYW1lcygpO1xuXHRcdHRoaXMuY3VycmVuY3kgPSB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5hZGRpdGlvbmFsUGF5bWVudEpTT05mcm9tR0VUYnlJZC5jdXJyZW5jeS5jdXJyZW5jeUNvZGU7XG5cdFx0dGhpcy5wYXltZW50VHlwZSA9IHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLmFkZGl0aW9uYWxQYXltZW50SlNPTmZyb21HRVRieUlkLnBheW1lbnRUeXBlO1xuXHRcdHRoaXMuYWRkaXRpb25hUGF5bWVudElkID0gdGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UuYWRkaXRpb25hbFBheW1lbnRKU09OZnJvbUdFVGJ5SWQuaWQ7XG5cdFx0dGhpcy50b3RhbFByaWNlID0gdGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UuYWRkaXRpb25hbFBheW1lbnRKU09OZnJvbUdFVGJ5SWQudG90YWxQcmljZTtcblx0XHRjb25zb2xlLmxvZyh0aGlzLnRvdGFsUHJpY2UpO1xuXHRcdHRoaXMucGF5bWVudE5hbWUgPSB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5hZGRpdGlvbmFsUGF5bWVudEpTT05mcm9tR0VUYnlJZC5wYXltZW50TmFtZS5wYXltZW50TmFtZTtcblx0XHR0aGlzLmNhck5hbWUgPSB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5hZGRpdGlvbmFsUGF5bWVudEpTT05mcm9tR0VUYnlJZC5jYXIubmFtZTtcblx0fVxuXG5cdHB1YmxpYyBzZWxlY3RlZEluZGV4Q2hhbmdlZE9uUGF5bWVudFR5cGVzKGFyZ3MpIHtcbiAgICAgICAgbGV0IHBpY2tlciA9IDxMaXN0UGlja2VyPmFyZ3Mub2JqZWN0O1xuICAgICAgICB0aGlzLnBheW1lbnRUeXBlID0gdGhpcy5nbG9iYWxTZXJ2aWNlLnBheW1lbnRUeXBlc0xpc3RbcGlja2VyLnNlbGVjdGVkSW5kZXhdO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RlZEluZGV4Q2hhbmdlZE9uQ3VycmVuY2llcyhhcmdzKSB7XG4gICAgICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcbiAgICAgICAgdGhpcy5jdXJyZW5jeUlkID0gcGlja2VyLnNlbGVjdGVkSW5kZXggKyAxO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RlZEluZGV4Q2hhbmdlZE9uQ2FycyhhcmdzKSB7XG4gICAgICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcbiAgICAgICAgdGhpcy5jYXJJZCA9IHBpY2tlci5zZWxlY3RlZEluZGV4ICsgMTtcblx0fVxuXHRcblx0cHVibGljIHVwZGF0ZUFkZGl0aW9uYWxQYXltZW50KCkge1xuXHRcdHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLmFkZGl0aW9uYWxQYXltZW50SlNPTmZvclBPU1QgPSB7XG5cdFx0XHRpZDp0aGlzLmFkZGl0aW9uYVBheW1lbnRJZCxcblx0XHRcdHBheW1lbnREYXk6IHRoaXMuZGF0ZVNlcnZpY2UuZGF0ZUZvcm1hdEZvclBvc3QoKSxcblx0XHRcdHBheW1lbnROYW1lOiB7IFxuXHRcdFx0XHRpZDogdGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UucGlja2VkQWRkaXRpb25hbHBheW1lbnROYW1lSWQgXG5cdFx0XHR9LFxuXHRcdFx0dG90YWxQcmljZTogdGhpcy50b3RhbFByaWNlLFxuXHRcdFx0cGF5bWVudFR5cGU6IHRoaXMucGF5bWVudFR5cGUsXG5cdFx0XHRkcml2ZXJOYW1lOiB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUsXG5cdFx0XHRjYXI6IHtcblx0XHRcdFx0aWQ6IHRoaXMuY2FySWRcblx0XHRcdH0sXG5cdFx0XHRjdXJyZW5jeToge1xuXHRcdFx0XHRpZDogdGhpcy5jdXJyZW5jeUlkXG5cdFx0XHR9XG5cblx0XHR9XG5cblx0XHR0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS51cGRhdGVBZGRpdGlvbmFsUGF5bWVudCh0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5hZGRpdGlvbmFsUGF5bWVudEpTT05mb3JQT1NUKS5zdWJzY3JpYmUoXG5cdFx0XHQocmVzKSA9PiB7XG5cdFx0XHRcdGFsZXJ0KHtcblx0XHRcdFx0XHR0aXRsZTogXCJQb3R3aWVyZHplbmllXCIsXG5cdFx0XHRcdFx0bWVzc2FnZTogXCJPcMWCYXRhIHptaWVuaW9uYSBwb3ByYXduaWVcIixcblx0XHRcdFx0XHRva0J1dHRvblRleHQ6IFwiT0tcIlxuXHRcdFx0XHR9KSwgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9hZGRpdGlvbmFsLXBheW1lbnQtZGV0YWlsc1wiXSlcblx0XHRcdH0sXG5cdFx0XHQoZXJyKSA9PiB7Y29uc29sZS5sb2coZXJyKVxuXHRcdFx0XHRhbGVydCh7XG5cdFx0XHRcdFx0dGl0bGU6IFwiQsWCxIVkXCIsXG5cdFx0XHRcdFx0bWVzc2FnZTogXCJQb3ByYXcgZGFuZSBpIHNwcsOzYnVqIHBvbm93bmllXCIsXG5cdFx0XHRcdFx0b2tCdXR0b25UZXh0OiBcIk9LXCJcblx0XHRcdFx0fSlcblx0XHRcdH0sXG5cdFx0KTtcblx0fVxuXG59XG4iXX0=