"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var refueling_service_1 = require("../services/refueling/refueling.service");
var list_service_1 = require("../services/list/list.service");
var NavigationComponent = (function () {
    function NavigationComponent(page, refuelingService, listService) {
        this.page = page;
        this.refuelingService = refuelingService;
        this.listService = listService;
    }
    NavigationComponent.prototype.ngOnInit = function () {
        this.data = this.refuelingService.refuelingJSONforPOST;
        this.page.actionBarHidden = true;
    };
    NavigationComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-navigation',
            templateUrl: './navigation.component.html',
            styleUrls: ['./navigation.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, refueling_service_1.RefuelingService, list_service_1.ListService])
    ], NavigationComponent);
    return NavigationComponent;
}());
exports.NavigationComponent = NavigationComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuYXZpZ2F0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxnQ0FBK0I7QUFDL0IsNkVBQTJFO0FBQzNFLDhEQUE0RDtBQVM1RDtJQUdDLDZCQUFxQixJQUFVLEVBQVUsZ0JBQWtDLEVBQVUsV0FBd0I7UUFBeEYsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUFLLENBQUM7SUFFbkgsc0NBQVEsR0FBUjtRQUNDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLG9CQUFvQixDQUFDO1FBQ3ZELElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztJQUdqQyxDQUFDO0lBVlUsbUJBQW1CO1FBTi9CLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixXQUFXLEVBQUUsNkJBQTZCO1lBQzFDLFNBQVMsRUFBRSxDQUFDLDZCQUE2QixDQUFDO1NBQzFDLENBQUM7eUNBSTBCLFdBQUksRUFBNEIsb0NBQWdCLEVBQXVCLDBCQUFXO09BSGpHLG1CQUFtQixDQWMvQjtJQUFELDBCQUFDO0NBQUEsQUFkRCxJQWNDO0FBZFksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0IHsgUmVmdWVsaW5nU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy9yZWZ1ZWxpbmcvcmVmdWVsaW5nLnNlcnZpY2VcIjtcbmltcG9ydCB7IExpc3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbGlzdC9saXN0LnNlcnZpY2UnO1xuXG5cbkBDb21wb25lbnQoe1xuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHRzZWxlY3RvcjogJ2FwcC1uYXZpZ2F0aW9uJyxcblx0dGVtcGxhdGVVcmw6ICcuL25hdmlnYXRpb24uY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTmF2aWdhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cblx0cHVibGljIGRhdGE7XG5cdGNvbnN0cnVjdG9yKCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgcmVmdWVsaW5nU2VydmljZTogUmVmdWVsaW5nU2VydmljZSwgcHJpdmF0ZSBsaXN0U2VydmljZTogTGlzdFNlcnZpY2UgKSB7IH1cblxuXHRuZ09uSW5pdCgpIHtcblx0XHR0aGlzLmRhdGEgPSB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZvclBPU1Q7XG5cdFx0dGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG5cdFxuXHRcdFxuXHQgfVxuXG5cblxufVxuIl19