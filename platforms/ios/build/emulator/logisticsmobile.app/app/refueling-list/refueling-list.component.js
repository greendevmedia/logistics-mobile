"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var refueling_service_1 = require("../services/refueling/refueling.service");
var router_1 = require("nativescript-angular/router");
var user_service_1 = require("../services/user/user.service");
var page_1 = require("ui/page");
var RefuelingListComponent = (function () {
    function RefuelingListComponent(refuelingService, routerExtensions, userService, page) {
        this.refuelingService = refuelingService;
        this.routerExtensions = routerExtensions;
        this.userService = userService;
        this.page = page;
    }
    RefuelingListComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.getRefuelings();
    };
    RefuelingListComponent.prototype.getRefuelings = function () {
        var _this = this;
        this.refuelingService.getRefuelings().subscribe(function (res) { _this.refuelingsJSONfromGET = res.json().content, _this.refuelingsJSONfromGET = _this.refuelingsJSONfromGET.reverse().filter(function (refueling) { return refueling.driverName === _this.userService.driverName; }); }, function (err) { alert("Błąd przy załadowaniu listy tankowań"); });
    };
    RefuelingListComponent.prototype.getRefuelingsById = function (id) {
        var _this = this;
        this.refuelingService.getRefuelingsById(id).subscribe(function (res) { _this.refuelingService.refuelingJSONfromGETbyID = res.json(), _this.routerExtensions.navigate(["/refueling-edit"]); }, function (err) { alert("Błąd przy załadowaniu tankowania do edycji"); });
    };
    RefuelingListComponent.prototype.showRefuelingDetails = function (refueling) {
        var note;
        if (refueling.warningNote == null) {
            note = "Brak";
        }
        else {
            note = refueling.warningNote;
        }
        alert({
            title: "Szczegóły tankowania",
            message: "Imię kierowcy: " + refueling.driverName + "\nStan licznika w km: " + refueling.counter + "\nIlość paliwa w litrach: " + refueling.quantity + "\nCena za litr paliwa: " + refueling.price + "\nTyp płatności: " + refueling.paymentType + "\nWaluta: " + refueling.currency.currencyCode + "\nAuto: " + refueling.car.name +
                "\nCena za litr adBlue: " + refueling.adBluePrice + "\nIlość adBlue w litrach: " + refueling.adBlueQuantity +
                "\nDodatkowe uwagi: " + note,
            okButtonText: "OK"
        });
    };
    RefuelingListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-refueling-list',
            templateUrl: './refueling-list.component.html',
            styleUrls: ['./refueling-list.component.scss']
        }),
        __metadata("design:paramtypes", [refueling_service_1.RefuelingService, router_1.RouterExtensions, user_service_1.UserService, page_1.Page])
    ], RefuelingListComponent);
    return RefuelingListComponent;
}());
exports.RefuelingListComponent = RefuelingListComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmdWVsaW5nLWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVmdWVsaW5nLWxpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELDZFQUEyRTtBQUMzRSxzREFBK0Q7QUFDL0QsOERBQTREO0FBQzVELGdDQUErQjtBQVEvQjtJQUlDLGdDQUFvQixnQkFBa0MsRUFBVSxnQkFBa0MsRUFBVSxXQUF3QixFQUFVLElBQVU7UUFBcEkscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07SUFBSSxDQUFDO0lBRTdKLHlDQUFRLEdBQVI7UUFDVyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRyw4Q0FBYSxHQUFwQjtRQUFBLGlCQUtJO1FBSkcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxDQUFDLFNBQVMsQ0FDM0MsVUFBQyxHQUFHLElBQU8sS0FBSSxDQUFDLHFCQUFxQixHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUMsVUFBQSxTQUFTLElBQUksT0FBQSxTQUFTLENBQUMsVUFBVSxLQUFLLEtBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFwRCxDQUFvRCxDQUFDLENBQUEsQ0FBQyxDQUFDLEVBQ3pNLFVBQUMsR0FBRyxJQUFPLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUM3RCxDQUFBO0lBQ0wsQ0FBQztJQUdNLGtEQUFpQixHQUF4QixVQUF5QixFQUFFO1FBQTNCLGlCQUtDO1FBSkcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FDakQsVUFBQyxHQUFHLElBQU8sS0FBSSxDQUFDLGdCQUFnQixDQUFDLHdCQUF3QixHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBRyxDQUFBLENBQUMsQ0FBQyxFQUMvSCxVQUFDLEdBQUcsSUFBTyxLQUFLLENBQUMsNENBQTRDLENBQUMsQ0FBQSxDQUFDLENBQUMsQ0FDbkUsQ0FBQTtJQUNMLENBQUM7SUFFTSxxREFBb0IsR0FBM0IsVUFBNEIsU0FBUztRQUNqQyxJQUFJLElBQUksQ0FBQztRQUNULEVBQUUsQ0FBQSxDQUFDLFNBQVMsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztZQUM5QixJQUFJLEdBQUcsTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFBQSxJQUFJLENBQUEsQ0FBQztZQUNGLElBQUksR0FBRyxTQUFTLENBQUMsV0FBVyxDQUFBO1FBQ2hDLENBQUM7UUFDRCxLQUFLLENBQUM7WUFDRixLQUFLLEVBQUUsc0JBQXNCO1lBQzdCLE9BQU8sRUFBRSxpQkFBaUIsR0FBRyxTQUFTLENBQUMsVUFBVSxHQUFHLHdCQUF3QixHQUFHLFNBQVMsQ0FBQyxPQUFPLEdBQUcsNEJBQTRCLEdBQUcsU0FBUyxDQUFDLFFBQVEsR0FBRyx5QkFBeUIsR0FBRyxTQUFTLENBQUMsS0FBSyxHQUFHLG1CQUFtQixHQUFHLFNBQVMsQ0FBQyxXQUFXLEdBQUcsWUFBWSxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMsWUFBWSxHQUFHLFVBQVUsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUk7Z0JBQ25VLHlCQUF5QixHQUFHLFNBQVMsQ0FBQyxXQUFXLEdBQUcsNEJBQTRCLEdBQUcsU0FBUyxDQUFDLGNBQWM7Z0JBQzNHLHFCQUFxQixHQUFHLElBQUk7WUFDNUIsWUFBWSxFQUFFLElBQUk7U0FDckIsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQXhDUSxzQkFBc0I7UUFObEMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFdBQVcsRUFBRSxpQ0FBaUM7WUFDOUMsU0FBUyxFQUFFLENBQUMsaUNBQWlDLENBQUM7U0FDOUMsQ0FBQzt5Q0FLcUMsb0NBQWdCLEVBQTRCLHlCQUFnQixFQUF1QiwwQkFBVyxFQUFnQixXQUFJO09BSjVJLHNCQUFzQixDQTBDbEM7SUFBRCw2QkFBQztDQUFBLEFBMUNELElBMENDO0FBMUNZLHdEQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSZWZ1ZWxpbmdTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3JlZnVlbGluZy9yZWZ1ZWxpbmcuc2VydmljZVwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdXNlci91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XG5cbkBDb21wb25lbnQoe1xuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHRzZWxlY3RvcjogJ2FwcC1yZWZ1ZWxpbmctbGlzdCcsXG5cdHRlbXBsYXRlVXJsOiAnLi9yZWZ1ZWxpbmctbGlzdC5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL3JlZnVlbGluZy1saXN0LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUmVmdWVsaW5nTGlzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cblx0cHJpdmF0ZSByZWZ1ZWxpbmdzSlNPTmZyb21HRVQ7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSByZWZ1ZWxpbmdTZXJ2aWNlOiBSZWZ1ZWxpbmdTZXJ2aWNlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBwcml2YXRlIHBhZ2U6IFBhZ2UpIHsgfVxuXG5cdG5nT25Jbml0KCkgeyBcbiAgICAgICAgICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5nZXRSZWZ1ZWxpbmdzKCk7XG4gICAgfVxuXG5cdHB1YmxpYyBnZXRSZWZ1ZWxpbmdzKCkge1xuICAgICAgICB0aGlzLnJlZnVlbGluZ1NlcnZpY2UuZ2V0UmVmdWVsaW5ncygpLnN1YnNjcmliZShcbiAgICAgICAgICAgIChyZXMpID0+IHsgdGhpcy5yZWZ1ZWxpbmdzSlNPTmZyb21HRVQgPSByZXMuanNvbigpLmNvbnRlbnQsIHRoaXMucmVmdWVsaW5nc0pTT05mcm9tR0VUID0gdGhpcy5yZWZ1ZWxpbmdzSlNPTmZyb21HRVQucmV2ZXJzZSgpLmZpbHRlcihyZWZ1ZWxpbmcgPT4gcmVmdWVsaW5nLmRyaXZlck5hbWUgPT09IHRoaXMudXNlclNlcnZpY2UuZHJpdmVyTmFtZSkgfSxcbiAgICAgICAgICAgIChlcnIpID0+IHsgYWxlcnQoXCJCxYLEhWQgcHJ6eSB6YcWCYWRvd2FuaXUgbGlzdHkgdGFua293YcWEXCIpIH1cbiAgICAgICAgKVxuICAgIH1cblxuXG4gICAgcHVibGljIGdldFJlZnVlbGluZ3NCeUlkKGlkKSB7XG4gICAgICAgIHRoaXMucmVmdWVsaW5nU2VydmljZS5nZXRSZWZ1ZWxpbmdzQnlJZChpZCkuc3Vic2NyaWJlKFxuICAgICAgICAgICAgKHJlcykgPT4geyB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZyb21HRVRieUlEID0gcmVzLmpzb24oKSwgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9yZWZ1ZWxpbmctZWRpdFwiXSwgKSB9LFxuICAgICAgICAgICAgKGVycikgPT4geyBhbGVydChcIkLFgsSFZCBwcnp5IHphxYJhZG93YW5pdSB0YW5rb3dhbmlhIGRvIGVkeWNqaVwiKSB9XG4gICAgICAgIClcbiAgICB9XG5cbiAgICBwdWJsaWMgc2hvd1JlZnVlbGluZ0RldGFpbHMocmVmdWVsaW5nKSB7XG4gICAgICAgIHZhciBub3RlO1xuICAgICAgICBpZihyZWZ1ZWxpbmcud2FybmluZ05vdGUgPT0gbnVsbCl7XG4gICAgICAgICAgICBub3RlID0gXCJCcmFrXCI7XG4gICAgICAgIH1lbHNle1xuICAgICAgICAgICAgbm90ZSA9IHJlZnVlbGluZy53YXJuaW5nTm90ZVxuICAgICAgICB9XG4gICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICAgIHRpdGxlOiBcIlN6Y3plZ8OzxYJ5IHRhbmtvd2FuaWFcIixcbiAgICAgICAgICAgIG1lc3NhZ2U6IFwiSW1pxJkga2llcm93Y3k6IFwiICsgcmVmdWVsaW5nLmRyaXZlck5hbWUgKyBcIlxcblN0YW4gbGljem5pa2EgdyBrbTogXCIgKyByZWZ1ZWxpbmcuY291bnRlciArIFwiXFxuSWxvxZvEhyBwYWxpd2EgdyBsaXRyYWNoOiBcIiArIHJlZnVlbGluZy5xdWFudGl0eSArIFwiXFxuQ2VuYSB6YSBsaXRyIHBhbGl3YTogXCIgKyByZWZ1ZWxpbmcucHJpY2UgKyBcIlxcblR5cCBwxYJhdG5vxZtjaTogXCIgKyByZWZ1ZWxpbmcucGF5bWVudFR5cGUgKyBcIlxcbldhbHV0YTogXCIgKyByZWZ1ZWxpbmcuY3VycmVuY3kuY3VycmVuY3lDb2RlICsgXCJcXG5BdXRvOiBcIiArIHJlZnVlbGluZy5jYXIubmFtZSArXG4gICAgICAgICAgICBcIlxcbkNlbmEgemEgbGl0ciBhZEJsdWU6IFwiICsgcmVmdWVsaW5nLmFkQmx1ZVByaWNlICsgXCJcXG5JbG/Fm8SHIGFkQmx1ZSB3IGxpdHJhY2g6IFwiICsgcmVmdWVsaW5nLmFkQmx1ZVF1YW50aXR5ICtcbiAgICAgICAgICAgIFwiXFxuRG9kYXRrb3dlIHV3YWdpOiBcIiArIG5vdGUsXG4gICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICB9KVxuICAgIH1cblxufVxuIl19