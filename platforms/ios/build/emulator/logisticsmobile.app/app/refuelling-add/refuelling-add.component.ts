import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { DateService } from "../services/date/date.service";
import { ListService } from '../services/list/list.service';
import { UserService } from "../services/user/user.service";
import { JsonService } from "../services/json/json.service";
import { GlobalService } from "../services/global/global.service";
import { RefuelingService } from "../services/refueling/refueling.service";
import { RouterExtensions } from "nativescript-angular/router";



@Component({
	moduleId: module.id,
	selector: 'app-refuelling-add',
	templateUrl: './refuelling-add.component.html',
	styleUrls: ['./refuelling-add.component.scss']
})
export class RefuellingAddComponent implements OnInit {

	constructor(private page: Page, private listService: ListService, private dateService: DateService, private userService: UserService, private jsonService: JsonService, private globalService: GlobalService, private refuelingService: RefuelingService, private routerExtensions: RouterExtensions) { }

	ngOnInit() { 
		this.jsonService.counter = null;
		this.jsonService.quantity = null;
		this.jsonService.price = null;
		this.jsonService.pickedPaymentType = null;
        this.jsonService.pickedCurrencyId = null;
        this.jsonService.adBluePrice = null;
        this.jsonService.adBlueQuantity = null;

	}

	public refuelingAdded() {
        this.refuelingService.refuelingJSONforPOST = {
            refuelingDate: this.dateService.dateFormatForPost(),
            counter: this.jsonService.counter,
            quantity: this.jsonService.quantity,
            price: this.jsonService.price,
            paymentType: this.jsonService.pickedPaymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.jsonService.pickedCarId
            },
            currency: {
                id: this.jsonService.pickedCurrencyId
            },
            warningNote: this.jsonService.warningNote,
            adBlueQuantity: this.jsonService.adBlueQuantity,
            adBluePrice: this.jsonService.adBluePrice
        }

        this.refuelingService.addRefueling(this.refuelingService.refuelingJSONforPOST).subscribe(
            (res) => {
                alert({
                    title: "Potwierdzenie",
                    message: "Tankowanie dodane poprawnie",
                    okButtonText: "OK"
                }), this.routerExtensions.navigate(["/navigation"])
            },
            (err) => {
                alert({
                    title: "Błąd",
                    message: "Popraw dane i spróbuj ponownie",
                    okButtonText: "OK"
                })
            },
        );
    }

}
