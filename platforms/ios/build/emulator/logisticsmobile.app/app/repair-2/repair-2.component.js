"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var repair_service_1 = require("../services/repair/repair.service");
var date_service_1 = require("../services/date/date.service");
var user_service_1 = require("../services/user/user.service");
var json_service_1 = require("../services/json/json.service");
var router_1 = require("nativescript-angular/router");
var Repair2Component = (function () {
    function Repair2Component(page, repairService, dateService, userService, jsonService, routerExtensions) {
        this.page = page;
        this.repairService = repairService;
        this.dateService = dateService;
        this.userService = userService;
        this.jsonService = jsonService;
        this.routerExtensions = routerExtensions;
    }
    Repair2Component.prototype.ngOnInit = function () {
    };
    Repair2Component.prototype.addService = function () {
        var _this = this;
        this.repairService.repairJSONforPOST = {
            repairDate: this.dateService.dateFormatForPost(),
            counter: this.repairService.counter,
            price: this.repairService.price,
            repairType: this.repairService.repairType,
            driverName: this.userService.driverName,
            carShopAddress: this.repairService.carShopAddress,
            activityType: this.repairService.activityType,
            vehicalCheck: this.repairService.vehicalCheck
        };
        if (this.jsonService.pickedCarId > 0) {
            this.repairService.repairJSONforPOST.car = { id: this.jsonService.pickedCarId };
        }
        if (this.jsonService.pickedTrailerId > 0) {
            this.repairService.repairJSONforPOST.trailer = { id: this.jsonService.pickedTrailerId };
        }
        this.repairService.addService(this.repairService.repairJSONforPOST).subscribe(function (res) {
            alert({
                title: "Potwierdzenie",
                message: "Serwis dodany poprawnie",
                okButtonText: "OK"
            }), _this.repairService.sendService();
        }, function (err) {
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    Repair2Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-repair-2',
            templateUrl: './repair-2.component.html',
            styleUrls: ['./repair-2.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, repair_service_1.RepairService, date_service_1.DateService, user_service_1.UserService, json_service_1.JsonService, router_1.RouterExtensions])
    ], Repair2Component);
    return Repair2Component;
}());
exports.Repair2Component = Repair2Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVwYWlyLTIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVwYWlyLTIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHNEQUFxRDtBQUNyRCxvRUFBa0U7QUFDbEUsOERBQTREO0FBQzVELDhEQUE0RDtBQUM1RCw4REFBNEQ7QUFDNUQsc0RBQStEO0FBUS9EO0lBRUUsMEJBQW9CLElBQVUsRUFBVSxhQUE0QixFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSxXQUF3QixFQUFVLGdCQUFrQztRQUFsTSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO0lBQUksQ0FBQztJQUUzTixtQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUVNLHFDQUFVLEdBQWpCO1FBQUEsaUJBb0NDO1FBbkNDLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLEdBQUc7WUFDckMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEVBQUU7WUFDaEQsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTztZQUNuQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLO1lBQy9CLFVBQVUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVU7WUFDekMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtZQUN2QyxjQUFjLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjO1lBQ2pELFlBQVksRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVk7WUFDN0MsWUFBWSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWTtTQUM5QyxDQUFBO1FBRUQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsR0FBRyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFBO1FBQ2pGLENBQUM7UUFFRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUE7UUFDekYsQ0FBQztRQUVELElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxTQUFTLENBQ3pFLFVBQUMsR0FBRztZQUNBLEtBQUssQ0FBQztnQkFDRixLQUFLLEVBQUUsZUFBZTtnQkFDdEIsT0FBTyxFQUFFLHlCQUF5QjtnQkFDbEMsWUFBWSxFQUFFLElBQUk7YUFDckIsQ0FBQyxFQUFFLEtBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUE7UUFDeEMsQ0FBQyxFQUNELFVBQUMsR0FBRztZQUNBLEtBQUssQ0FBQztnQkFDRixLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsZ0NBQWdDO2dCQUN6QyxZQUFZLEVBQUUsSUFBSTthQUNyQixDQUFDLENBQUE7UUFDTixDQUFDLENBQ0osQ0FBQztJQUNKLENBQUM7SUEzQ1UsZ0JBQWdCO1FBTjVCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGNBQWM7WUFDeEIsV0FBVyxFQUFFLDJCQUEyQjtZQUN4QyxTQUFTLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztTQUN6QyxDQUFDO3lDQUcwQixXQUFJLEVBQXlCLDhCQUFhLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQTRCLHlCQUFnQjtPQUYzTSxnQkFBZ0IsQ0E2QzVCO0lBQUQsdUJBQUM7Q0FBQSxBQTdDRCxJQTZDQztBQTdDWSw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlJztcbmltcG9ydCB7IFJlcGFpclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9yZXBhaXIvcmVwYWlyLnNlcnZpY2UnO1xuaW1wb3J0IHsgRGF0ZVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9kYXRlL2RhdGUuc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IEpzb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvanNvbi9qc29uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuQENvbXBvbmVudCh7XG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gIHNlbGVjdG9yOiAnYXBwLXJlcGFpci0yJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3JlcGFpci0yLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcmVwYWlyLTIuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBSZXBhaXIyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgcmVwYWlyU2VydmljZTogUmVwYWlyU2VydmljZSwgcHJpdmF0ZSBkYXRlU2VydmljZTogRGF0ZVNlcnZpY2UsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBwcml2YXRlIGpzb25TZXJ2aWNlOiBKc29uU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIHB1YmxpYyBhZGRTZXJ2aWNlKCkge1xuICAgIHRoaXMucmVwYWlyU2VydmljZS5yZXBhaXJKU09OZm9yUE9TVCA9IHtcbiAgICAgIHJlcGFpckRhdGU6IHRoaXMuZGF0ZVNlcnZpY2UuZGF0ZUZvcm1hdEZvclBvc3QoKSxcbiAgICAgIGNvdW50ZXI6IHRoaXMucmVwYWlyU2VydmljZS5jb3VudGVyLFxuICAgICAgcHJpY2U6IHRoaXMucmVwYWlyU2VydmljZS5wcmljZSxcbiAgICAgIHJlcGFpclR5cGU6IHRoaXMucmVwYWlyU2VydmljZS5yZXBhaXJUeXBlLFxuICAgICAgZHJpdmVyTmFtZTogdGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lLFxuICAgICAgY2FyU2hvcEFkZHJlc3M6IHRoaXMucmVwYWlyU2VydmljZS5jYXJTaG9wQWRkcmVzcyxcbiAgICAgIGFjdGl2aXR5VHlwZTogdGhpcy5yZXBhaXJTZXJ2aWNlLmFjdGl2aXR5VHlwZSxcbiAgICAgIHZlaGljYWxDaGVjazogdGhpcy5yZXBhaXJTZXJ2aWNlLnZlaGljYWxDaGVja1xuICAgIH1cblxuICAgIGlmKHRoaXMuanNvblNlcnZpY2UucGlja2VkQ2FySWQgPiAwKXtcbiAgICAgIHRoaXMucmVwYWlyU2VydmljZS5yZXBhaXJKU09OZm9yUE9TVC5jYXIgPSB7IGlkOiB0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZENhcklkIH1cbiAgICB9XG5cbiAgICBpZih0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZFRyYWlsZXJJZCA+IDApe1xuICAgICAgdGhpcy5yZXBhaXJTZXJ2aWNlLnJlcGFpckpTT05mb3JQT1NULnRyYWlsZXIgPSB7IGlkOiB0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZFRyYWlsZXJJZCB9XG4gICAgfVxuXG4gICAgdGhpcy5yZXBhaXJTZXJ2aWNlLmFkZFNlcnZpY2UodGhpcy5yZXBhaXJTZXJ2aWNlLnJlcGFpckpTT05mb3JQT1NUKS5zdWJzY3JpYmUoXG4gICAgICAgIChyZXMpID0+IHtcbiAgICAgICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICAgICAgICB0aXRsZTogXCJQb3R3aWVyZHplbmllXCIsXG4gICAgICAgICAgICAgICAgbWVzc2FnZTogXCJTZXJ3aXMgZG9kYW55IHBvcHJhd25pZVwiLFxuICAgICAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJPS1wiXG4gICAgICAgICAgICB9KSwgdGhpcy5yZXBhaXJTZXJ2aWNlLnNlbmRTZXJ2aWNlKCkgXG4gICAgICAgIH0sXG4gICAgICAgIChlcnIpID0+IHtcbiAgICAgICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICAgICAgICB0aXRsZTogXCJCxYLEhWRcIixcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiBcIlBvcHJhdyBkYW5lIGkgc3Byw7NidWogcG9ub3duaWVcIixcbiAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSxcbiAgICApO1xuICB9XG5cbn0iXX0=