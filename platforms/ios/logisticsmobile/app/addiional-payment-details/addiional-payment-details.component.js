"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var date_service_1 = require("../services/date/date.service");
var additional_payment_service_1 = require("../services/additional-payment/additional-payment.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var list_service_1 = require("../services/list/list.service");
var AddiionalPaymentDetailsComponent = (function () {
    function AddiionalPaymentDetailsComponent(page, dateService, additionalPaymentService, userService, routerExtensions, listService) {
        this.page = page;
        this.dateService = dateService;
        this.additionalPaymentService = additionalPaymentService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.listService = listService;
    }
    AddiionalPaymentDetailsComponent.prototype.ngOnInit = function () {
        this.data = this.additionalPaymentService.additionalPaymentJSONforPOST;
        this.page.actionBarHidden = true;
    };
    AddiionalPaymentDetailsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-addiional-payment-details',
            templateUrl: './addiional-payment-details.component.html',
            styleUrls: ['./addiional-payment-details.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, date_service_1.DateService, additional_payment_service_1.AdditionalPaymentService, user_service_1.UserService, router_1.RouterExtensions, list_service_1.ListService])
    ], AddiionalPaymentDetailsComponent);
    return AddiionalPaymentDetailsComponent;
}());
exports.AddiionalPaymentDetailsComponent = AddiionalPaymentDetailsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkaWlvbmFsLXBheW1lbnQtZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhZGRpaW9uYWwtcGF5bWVudC1kZXRhaWxzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxnQ0FBK0I7QUFDL0IsOERBQTREO0FBQzVELHdHQUFxRztBQUVyRyw4REFBNEQ7QUFDNUQsc0RBQStEO0FBQy9ELDhEQUE0RDtBQVE1RDtJQUlDLDBDQUFvQixJQUFVLEVBQVUsV0FBd0IsRUFBVSx3QkFBa0QsRUFBUyxXQUF3QixFQUFVLGdCQUFrQyxFQUFVLFdBQXdCO1FBQXZOLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFBUyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUN2TyxDQUFDO0lBRUwsbURBQVEsR0FBUjtRQUNDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLDRCQUE0QixDQUFDO1FBQ3ZFLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztJQUNsQyxDQUFDO0lBVlcsZ0NBQWdDO1FBTjVDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLCtCQUErQjtZQUN6QyxXQUFXLEVBQUUsNENBQTRDO1lBQ3pELFNBQVMsRUFBRSxDQUFDLDRDQUE0QyxDQUFDO1NBQ3pELENBQUM7eUNBS3lCLFdBQUksRUFBdUIsMEJBQVcsRUFBb0MscURBQXdCLEVBQXNCLDBCQUFXLEVBQTRCLHlCQUFnQixFQUF1QiwwQkFBVztPQUovTixnQ0FBZ0MsQ0FZNUM7SUFBRCx1Q0FBQztDQUFBLEFBWkQsSUFZQztBQVpZLDRFQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCB7IERhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZGF0ZS9kYXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWRkaXRpb25hbC1wYXltZW50L2FkZGl0aW9uYWwtcGF5bWVudC5zZXJ2aWNlJztcbmltcG9ydCB7IEpzb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvanNvbi9qc29uLnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy91c2VyL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgTGlzdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9saXN0L2xpc3Quc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHRzZWxlY3RvcjogJ2FwcC1hZGRpaW9uYWwtcGF5bWVudC1kZXRhaWxzJyxcblx0dGVtcGxhdGVVcmw6ICcuL2FkZGlpb25hbC1wYXltZW50LWRldGFpbHMuY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9hZGRpaW9uYWwtcGF5bWVudC1kZXRhaWxzLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQWRkaWlvbmFsUGF5bWVudERldGFpbHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXHRcblx0cHVibGljIGRhdGE7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSwgcHJpdmF0ZSBhZGRpdGlvbmFsUGF5bWVudFNlcnZpY2U6IEFkZGl0aW9uYWxQYXltZW50U2VydmljZSxwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIGxpc3RTZXJ2aWNlOiBMaXN0U2VydmljZVxuXHQpIHsgfVxuXG5cdG5nT25Jbml0KCkgeyBcblx0XHR0aGlzLmRhdGEgPSB0aGlzLmFkZGl0aW9uYWxQYXltZW50U2VydmljZS5hZGRpdGlvbmFsUGF5bWVudEpTT05mb3JQT1NUO1xuXHRcdHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xuXHR9XG5cbn1cbiJdfQ==