import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { DateService } from '../services/date/date.service';
import { AdditionalPaymentService } from '../services/additional-payment/additional-payment.service';
import { JsonService } from '../services/json/json.service';
import { UserService } from '../services/user/user.service';
import { RouterExtensions } from "nativescript-angular/router";
import { ListService } from '../services/list/list.service';

@Component({
	moduleId: module.id,
	selector: 'app-addiional-payment-details',
	templateUrl: './addiional-payment-details.component.html',
	styleUrls: ['./addiional-payment-details.component.scss']
})
export class AddiionalPaymentDetailsComponent implements OnInit {
	
	public data;

	constructor(private page: Page, private dateService: DateService, private additionalPaymentService: AdditionalPaymentService,private userService: UserService, private routerExtensions: RouterExtensions, private listService: ListService
	) { }

	ngOnInit() { 
		this.data = this.additionalPaymentService.additionalPaymentJSONforPOST;
		this.page.actionBarHidden = true;
	}

}
