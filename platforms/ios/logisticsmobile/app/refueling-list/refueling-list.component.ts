import { Component, OnInit } from '@angular/core';
import { RefuelingService } from "../services/refueling/refueling.service";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from '../services/user/user.service';
import { Page } from "ui/page";

@Component({
	moduleId: module.id,
	selector: 'app-refueling-list',
	templateUrl: './refueling-list.component.html',
	styleUrls: ['./refueling-list.component.scss']
})
export class RefuelingListComponent implements OnInit {

	private refuelingsJSONfromGET;

	constructor(private refuelingService: RefuelingService, private routerExtensions: RouterExtensions, private userService: UserService, private page: Page) { }

	ngOnInit() { 
            this.page.actionBarHidden = true;
            this.getRefuelings();
    }

	public getRefuelings() {
        this.refuelingService.getRefuelings().subscribe(
            // (res) => { this.refuelingsJSONfromGET = res.json().content, this.refuelingsJSONfromGET = this.refuelingsJSONfromGET.reverse().filter(refueling => refueling.driverName === this.userService.driverName) },
            (res) => { this.refuelingsJSONfromGET = res.json().content, this.refuelingsJSONfromGET = this.refuelingsJSONfromGET.filter(refueling => refueling.driverName === this.userService.driverName) },
            (err) => { alert("Błąd przy załadowaniu listy tankowań") }
        )
    }


    public getRefuelingsById(id) {
        this.refuelingService.getRefuelingsById(id).subscribe(
            (res) => { this.refuelingService.refuelingJSONfromGETbyID = res.json(), this.routerExtensions.navigate(["/refueling-edit"], ) },
            (err) => { alert("Błąd przy załadowaniu tankowania do edycji") }
        )
    }

    public showRefuelingDetails(refueling) {
        var note;
        if(refueling.warningNote == null){
            note = "Brak";
        }else{
            note = refueling.warningNote
        }
        alert({
            title: "Szczegóły tankowania",
            message: "Imię kierowcy: " + refueling.driverName + "\nStan licznika w km: " + refueling.counter + "\nIlość paliwa w litrach: " + refueling.quantity + "\nCena za litr paliwa: " + refueling.price + "\nTyp płatności: " + refueling.paymentType + "\nWaluta: " + refueling.currency.currencyCode + "\nAuto: " + refueling.car.name +
            "\nCena za litr adBlue: " + refueling.adBluePrice + "\nIlość adBlue w litrach: " + refueling.adBlueQuantity +
            "\nDodatkowe uwagi: " + note,
            okButtonText: "OK"
        })
    }

}
