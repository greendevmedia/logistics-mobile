"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_routing_1 = require("./app.routing");
var app_component_1 = require("./app.component");
var home_component_1 = require("./home/home.component");
var forms_1 = require("nativescript-angular/forms");
var http_1 = require("nativescript-angular/http");
var login_service_1 = require("./services/login/login.service");
var global_service_1 = require("./services/global/global.service");
var user_service_1 = require("./services/user/user.service");
var refueling_service_1 = require("./services/refueling/refueling.service");
var angular_1 = require("nativescript-drop-down/angular");
var refueling_edit_component_1 = require("./refueling-edit/refueling-edit.component");
var date_service_1 = require("./services/date/date.service");
var list_service_1 = require("./services/list/list.service");
var navigation_component_1 = require("./navigation/navigation.component");
var navpage_component_1 = require("./navpage/navpage.component");
var car_add_component_1 = require("./car-add/car-add.component");
var json_service_1 = require("./services/json/json.service");
var technical_card_service_1 = require("./services/technical-card/technical-card.service");
var technical_card_1_component_1 = require("./technical-card-1/technical-card-1.component");
var technical_card_2_component_1 = require("./technical-card-2/technical-card-2.component");
var refuelling_add_component_1 = require("./refuelling-add/refuelling-add.component");
var refueling_list_component_1 = require("./refueling-list/refueling-list.component");
var additional_payment1_component_1 = require("./additional-payment1/additional-payment1.component");
var additional_payment2_component_1 = require("./additional-payment2/additional-payment2.component");
var additional_payment_service_1 = require("./services/additional-payment/additional-payment.service");
var addiional_payment_details_component_1 = require("./addiional-payment-details/addiional-payment-details.component");
var additionl_payments_list_component_1 = require("./additionl-payments-list/additionl-payments-list.component");
var additional_payment_edit_component_1 = require("./additional-payment-edit/additional-payment-edit.component");
var repair_1_component_1 = require("./repair-1/repair-1.component");
var repair_service_1 = require("./services/repair/repair.service");
var repair_2_component_1 = require("./repair-2/repair-2.component");
var nativescript_ngx_fonticon_1 = require("nativescript-ngx-fonticon");
var url_service_1 = require("./services/urlService/url.service");
var shipment_service_1 = require("./services/shipment/shipment.service");
var shipment_component_1 = require("./shipment/shipment.component");
var AppModule = (function () {
    /*
    Pass your application module to the bootstrapModule function located in main.ts to start your app
    */
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                app_component_1.AppComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_1.AppRoutingModule,
                forms_1.NativeScriptFormsModule,
                http_1.NativeScriptHttpModule,
                angular_1.DropDownModule,
                nativescript_ngx_fonticon_1.TNSFontIconModule.forRoot({
                    'fa': './assets/font-awesome.css',
                    'ion': './assets/ionicons.css',
                    'mdi': 'material-design-icons.css'
                })
            ],
            declarations: [
                app_component_1.AppComponent,
                home_component_1.HomeComponent,
                refueling_edit_component_1.RefuelingEditComponent,
                navigation_component_1.NavigationComponent,
                navpage_component_1.NavpageComponent,
                car_add_component_1.CarAddComponent,
                technical_card_1_component_1.TechnicalCard1Component,
                technical_card_2_component_1.TechnicalCard2Component,
                refuelling_add_component_1.RefuellingAddComponent,
                refueling_list_component_1.RefuelingListComponent,
                additional_payment1_component_1.AdditionalPayment1Component,
                additional_payment2_component_1.AdditionalPayment2Component,
                addiional_payment_details_component_1.AddiionalPaymentDetailsComponent,
                additionl_payments_list_component_1.AdditionlPaymentsListComponent,
                additional_payment_edit_component_1.AdditionalPaymentEditComponent,
                repair_1_component_1.Repair1Component,
                repair_2_component_1.Repair2Component,
                shipment_component_1.ShipmentComponent
            ],
            providers: [
                global_service_1.GlobalService,
                login_service_1.LoginService,
                user_service_1.UserService,
                refueling_service_1.RefuelingService,
                date_service_1.DateService,
                list_service_1.ListService,
                json_service_1.JsonService,
                technical_card_service_1.TechnicalCardService,
                additional_payment_service_1.AdditionalPaymentService,
                repair_service_1.RepairService,
                url_service_1.UrlService,
                shipment_service_1.ShipmentService
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
        /*
        Pass your application module to the bootstrapModule function located in main.ts to start your app
        */
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBQzlFLDZDQUFpRDtBQUNqRCxpREFBK0M7QUFDL0Msd0RBQXNEO0FBQ3RELG9EQUFxRTtBQUNyRSxrREFBbUU7QUFDbkUsZ0VBQThEO0FBQzlELG1FQUFnRTtBQUNoRSw2REFBMkQ7QUFFM0QsNEVBQTBFO0FBQzFFLDBEQUFnRTtBQUNoRSxzRkFBa0Y7QUFDbEYsNkRBQTJEO0FBQzNELDZEQUEyRDtBQUMzRCwwRUFBdUU7QUFDdkUsaUVBQThEO0FBQzlELGlFQUE4RDtBQUM5RCw2REFBMkQ7QUFDM0QsMkZBQXdGO0FBQ3hGLDRGQUF3RjtBQUN4Riw0RkFBd0Y7QUFDeEYsc0ZBQW1GO0FBQ25GLHNGQUFtRjtBQUNuRixxR0FBa0c7QUFDbEcscUdBQWtHO0FBQ2xHLHVHQUFvRztBQUNwRyx1SEFBbUg7QUFDbkgsaUhBQTZHO0FBQzdHLGlIQUE2RztBQUM3RyxvRUFBaUU7QUFDakUsbUVBQWlFO0FBQ2pFLG9FQUFpRTtBQUNqRSx1RUFBOEQ7QUFDOUQsaUVBQStEO0FBQy9ELHlFQUF1RTtBQUN2RSxvRUFBa0U7QUE2RGxFO0lBSEE7O01BRUU7SUFDRjtJQUF5QixDQUFDO0lBQWIsU0FBUztRQXpEckIsZUFBUSxDQUFDO1lBQ04sU0FBUyxFQUFFO2dCQUNQLDRCQUFZO2FBQ2Y7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsd0NBQWtCO2dCQUNsQiw4QkFBZ0I7Z0JBQ2hCLCtCQUF1QjtnQkFDdkIsNkJBQXNCO2dCQUN0Qix3QkFBYztnQkFDZCw2Q0FBaUIsQ0FBQyxPQUFPLENBQUM7b0JBQy9CLElBQUksRUFBRSwyQkFBMkI7b0JBQ3hCLEtBQUssRUFBRSx1QkFBdUI7b0JBQzlCLEtBQUssRUFBRSwyQkFBMkI7aUJBQzNDLENBQUM7YUFDQztZQUNELFlBQVksRUFBRTtnQkFDViw0QkFBWTtnQkFDWiw4QkFBYTtnQkFDYixpREFBc0I7Z0JBQ3RCLDBDQUFtQjtnQkFDbkIsb0NBQWdCO2dCQUNoQixtQ0FBZTtnQkFDZixvREFBdUI7Z0JBQ3ZCLG9EQUF1QjtnQkFDdkIsaURBQXNCO2dCQUN0QixpREFBc0I7Z0JBQ3RCLDJEQUEyQjtnQkFDM0IsMkRBQTJCO2dCQUMzQixzRUFBZ0M7Z0JBQ2hDLGtFQUE4QjtnQkFDOUIsa0VBQThCO2dCQUM5QixxQ0FBZ0I7Z0JBQ2hCLHFDQUFnQjtnQkFDaEIsc0NBQWlCO2FBQ3BCO1lBQ0QsU0FBUyxFQUFFO2dCQUNQLDhCQUFhO2dCQUNiLDRCQUFZO2dCQUNaLDBCQUFXO2dCQUNYLG9DQUFnQjtnQkFDaEIsMEJBQVc7Z0JBQ1gsMEJBQVc7Z0JBQ1gsMEJBQVc7Z0JBQ1gsNkNBQW9CO2dCQUNwQixxREFBd0I7Z0JBQ3hCLDhCQUFhO2dCQUNiLHdCQUFVO2dCQUNWLGtDQUFlO2FBQ2xCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHVCQUFnQjthQUNuQjtTQUNKLENBQUM7UUFDRjs7VUFFRTtPQUNXLFNBQVMsQ0FBSTtJQUFELGdCQUFDO0NBQUEsQUFBMUIsSUFBMEI7QUFBYiw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlXCI7XG5pbXBvcnQgeyBBcHBSb3V0aW5nTW9kdWxlIH0gZnJvbSBcIi4vYXBwLnJvdXRpbmdcIjtcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gXCIuL2FwcC5jb21wb25lbnRcIjtcbmltcG9ydCB7IEhvbWVDb21wb25lbnQgfSBmcm9tIFwiLi9ob21lL2hvbWUuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwXCI7XG5pbXBvcnQgeyBMb2dpblNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy9sb2dpbi9sb2dpbi5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBHbG9iYWxTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZXMvZ2xvYmFsL2dsb2JhbC5zZXJ2aWNlXCJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZXMvdXNlci91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFJlZnVlbGluZ1NlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy9yZWZ1ZWxpbmcvcmVmdWVsaW5nLnNlcnZpY2VcIjtcbmltcG9ydCB7IERyb3BEb3duTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1kcm9wLWRvd24vYW5ndWxhclwiO1xuaW1wb3J0IHsgUmVmdWVsaW5nRWRpdENvbXBvbmVudCB9IGZyb20gXCIuL3JlZnVlbGluZy1lZGl0L3JlZnVlbGluZy1lZGl0LmNvbXBvbmVudFwiXG5pbXBvcnQgeyBEYXRlU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2VzL2RhdGUvZGF0ZS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBMaXN0U2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2VzL2xpc3QvbGlzdC5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBOYXZpZ2F0aW9uQ29tcG9uZW50IH0gZnJvbSBcIi4vbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLmNvbXBvbmVudFwiXG5pbXBvcnQgeyBOYXZwYWdlQ29tcG9uZW50IH0gZnJvbSBcIi4vbmF2cGFnZS9uYXZwYWdlLmNvbXBvbmVudFwiXG5pbXBvcnQgeyBDYXJBZGRDb21wb25lbnQgfSBmcm9tIFwiLi9jYXItYWRkL2Nhci1hZGQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBKc29uU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2VzL2pzb24vanNvbi5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBUZWNobmljYWxDYXJkU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2VzL3RlY2huaWNhbC1jYXJkL3RlY2huaWNhbC1jYXJkLnNlcnZpY2VcIjtcbmltcG9ydCB7IFRlY2huaWNhbENhcmQxQ29tcG9uZW50IH0gZnJvbSBcIi4vdGVjaG5pY2FsLWNhcmQtMS90ZWNobmljYWwtY2FyZC0xLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgVGVjaG5pY2FsQ2FyZDJDb21wb25lbnQgfSBmcm9tIFwiLi90ZWNobmljYWwtY2FyZC0yL3RlY2huaWNhbC1jYXJkLTIuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBSZWZ1ZWxsaW5nQWRkQ29tcG9uZW50IH0gZnJvbSBcIi4vcmVmdWVsbGluZy1hZGQvcmVmdWVsbGluZy1hZGQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBSZWZ1ZWxpbmdMaXN0Q29tcG9uZW50IH0gZnJvbSBcIi4vcmVmdWVsaW5nLWxpc3QvcmVmdWVsaW5nLWxpc3QuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBZGRpdGlvbmFsUGF5bWVudDFDb21wb25lbnQgfSBmcm9tIFwiLi9hZGRpdGlvbmFsLXBheW1lbnQxL2FkZGl0aW9uYWwtcGF5bWVudDEuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBZGRpdGlvbmFsUGF5bWVudDJDb21wb25lbnQgfSBmcm9tIFwiLi9hZGRpdGlvbmFsLXBheW1lbnQyL2FkZGl0aW9uYWwtcGF5bWVudDIuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy9hZGRpdGlvbmFsLXBheW1lbnQvYWRkaXRpb25hbC1wYXltZW50LnNlcnZpY2VcIjtcbmltcG9ydCB7IEFkZGlpb25hbFBheW1lbnREZXRhaWxzQ29tcG9uZW50IH0gZnJvbSBcIi4vYWRkaWlvbmFsLXBheW1lbnQtZGV0YWlscy9hZGRpaW9uYWwtcGF5bWVudC1kZXRhaWxzLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWRkaXRpb25sUGF5bWVudHNMaXN0Q29tcG9uZW50IH0gZnJvbSBcIi4vYWRkaXRpb25sLXBheW1lbnRzLWxpc3QvYWRkaXRpb25sLXBheW1lbnRzLWxpc3QuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBZGRpdGlvbmFsUGF5bWVudEVkaXRDb21wb25lbnQgfSBmcm9tIFwiLi9hZGRpdGlvbmFsLXBheW1lbnQtZWRpdC9hZGRpdGlvbmFsLXBheW1lbnQtZWRpdC5jb21wb25lbnRcIjtcbmltcG9ydCB7IFJlcGFpcjFDb21wb25lbnQgfSBmcm9tIFwiLi9yZXBhaXItMS9yZXBhaXItMS5jb21wb25lbnRcIjtcbmltcG9ydCB7IFJlcGFpclNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlcy9yZXBhaXIvcmVwYWlyLnNlcnZpY2VcIjtcbmltcG9ydCB7IFJlcGFpcjJDb21wb25lbnQgfSBmcm9tIFwiLi9yZXBhaXItMi9yZXBhaXItMi5jb21wb25lbnRcIjtcbmltcG9ydCB7IFROU0ZvbnRJY29uTW9kdWxlIH0gZnJvbSAnbmF0aXZlc2NyaXB0LW5neC1mb250aWNvbic7XG5pbXBvcnQgeyBVcmxTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZXMvdXJsU2VydmljZS91cmwuc2VydmljZVwiO1xuaW1wb3J0IHsgU2hpcG1lbnRTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZXMvc2hpcG1lbnQvc2hpcG1lbnQuc2VydmljZVwiO1xuaW1wb3J0IHsgU2hpcG1lbnRDb21wb25lbnQgfSBmcm9tIFwiLi9zaGlwbWVudC9zaGlwbWVudC5jb21wb25lbnRcIjtcblxuXG5cbkBOZ01vZHVsZSh7XG4gICAgYm9vdHN0cmFwOiBbXG4gICAgICAgIEFwcENvbXBvbmVudFxuICAgIF0sXG4gICAgaW1wb3J0czogW1xuICAgICAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXG4gICAgICAgIEFwcFJvdXRpbmdNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRIdHRwTW9kdWxlLFxuICAgICAgICBEcm9wRG93bk1vZHVsZSxcbiAgICAgICAgVE5TRm9udEljb25Nb2R1bGUuZm9yUm9vdCh7XG5cdFx0XHQnZmEnOiAnLi9hc3NldHMvZm9udC1hd2Vzb21lLmNzcycsXG4gICAgICAgICAgICAnaW9uJzogJy4vYXNzZXRzL2lvbmljb25zLmNzcycsXG4gICAgICAgICAgICAnbWRpJzogJ21hdGVyaWFsLWRlc2lnbi1pY29ucy5jc3MnXG5cdFx0fSlcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICBBcHBDb21wb25lbnQsXG4gICAgICAgIEhvbWVDb21wb25lbnQsXG4gICAgICAgIFJlZnVlbGluZ0VkaXRDb21wb25lbnQsXG4gICAgICAgIE5hdmlnYXRpb25Db21wb25lbnQsXG4gICAgICAgIE5hdnBhZ2VDb21wb25lbnQsXG4gICAgICAgIENhckFkZENvbXBvbmVudCxcbiAgICAgICAgVGVjaG5pY2FsQ2FyZDFDb21wb25lbnQsXG4gICAgICAgIFRlY2huaWNhbENhcmQyQ29tcG9uZW50LFxuICAgICAgICBSZWZ1ZWxsaW5nQWRkQ29tcG9uZW50LFxuICAgICAgICBSZWZ1ZWxpbmdMaXN0Q29tcG9uZW50LFxuICAgICAgICBBZGRpdGlvbmFsUGF5bWVudDFDb21wb25lbnQsXG4gICAgICAgIEFkZGl0aW9uYWxQYXltZW50MkNvbXBvbmVudCxcbiAgICAgICAgQWRkaWlvbmFsUGF5bWVudERldGFpbHNDb21wb25lbnQsXG4gICAgICAgIEFkZGl0aW9ubFBheW1lbnRzTGlzdENvbXBvbmVudCxcbiAgICAgICAgQWRkaXRpb25hbFBheW1lbnRFZGl0Q29tcG9uZW50LFxuICAgICAgICBSZXBhaXIxQ29tcG9uZW50LFxuICAgICAgICBSZXBhaXIyQ29tcG9uZW50LFxuICAgICAgICBTaGlwbWVudENvbXBvbmVudFxuICAgIF0sXG4gICAgcHJvdmlkZXJzOiBbXG4gICAgICAgIEdsb2JhbFNlcnZpY2UsXG4gICAgICAgIExvZ2luU2VydmljZSxcbiAgICAgICAgVXNlclNlcnZpY2UsXG4gICAgICAgIFJlZnVlbGluZ1NlcnZpY2UsXG4gICAgICAgIERhdGVTZXJ2aWNlLFxuICAgICAgICBMaXN0U2VydmljZSxcbiAgICAgICAgSnNvblNlcnZpY2UsXG4gICAgICAgIFRlY2huaWNhbENhcmRTZXJ2aWNlLFxuICAgICAgICBBZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UsXG4gICAgICAgIFJlcGFpclNlcnZpY2UsXG4gICAgICAgIFVybFNlcnZpY2UsXG4gICAgICAgIFNoaXBtZW50U2VydmljZVxuICAgIF0sXG4gICAgc2NoZW1hczogW1xuICAgICAgICBOT19FUlJPUlNfU0NIRU1BXG4gICAgXVxufSlcbi8qXG5QYXNzIHlvdXIgYXBwbGljYXRpb24gbW9kdWxlIHRvIHRoZSBib290c3RyYXBNb2R1bGUgZnVuY3Rpb24gbG9jYXRlZCBpbiBtYWluLnRzIHRvIHN0YXJ0IHlvdXIgYXBwXG4qL1xuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7IH1cbiJdfQ==