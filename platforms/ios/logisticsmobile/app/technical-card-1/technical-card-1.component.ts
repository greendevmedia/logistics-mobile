import { Component, OnInit } from '@angular/core';
import { DateService } from "../services/date/date.service";
import { TechnicalCardService } from "../services/technical-card/technical-card.service";
import { Page } from "ui/page";

@Component({
	moduleId: module.id,
	selector: 'app-technical-card-1',
	templateUrl: './technical-card-1.component.html',
	styleUrls: ['./technical-card-1.component.scss']
})
export class TechnicalCard1Component implements OnInit {

	constructor(private dateService: DateService, private page: Page, private technicalCardService:TechnicalCardService) { }

	ngOnInit() {
		this.technicalCardService.counterStart = null;
		this.technicalCardService.counterStop = null;
	 }

}
