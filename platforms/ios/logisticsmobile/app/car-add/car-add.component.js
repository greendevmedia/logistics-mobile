"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var list_service_1 = require("../services/list/list.service");
var page_1 = require("ui/page");
var json_service_1 = require("../services/json/json.service");
var router_1 = require("nativescript-angular/router");
var global_service_1 = require("../services/global/global.service");
var CarAddComponent = (function () {
    function CarAddComponent(listService, page, jsonService, globalService, routerExtensions) {
        this.listService = listService;
        this.page = page;
        this.jsonService = jsonService;
        this.globalService = globalService;
        this.routerExtensions = routerExtensions;
    }
    CarAddComponent.prototype.ngOnInit = function () {
        this.jsonService.pickedCarId = null;
        this.jsonService.carName = null;
        this.jsonService.trailerName = null;
        this.jsonService.pickedTrailerId = null;
        this.listService.getCars();
        // this.page.actionBarHidden = true;
        this.listService.getCurrencies();
        this.listService.getTrailers();
    };
    CarAddComponent.prototype.isCarIdPicked = function () {
        if (this.jsonService.pickedCarId > 0 || this.jsonService.pickedTrailerId > 0) {
            return true;
        }
    };
    CarAddComponent.prototype.setLinkForNextView = function () {
        this.routerExtensions.navigate(this.globalService.carAddLink);
    };
    CarAddComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-car-add',
            templateUrl: './car-add.component.html',
            styleUrls: ['./car-add.component.scss']
        }),
        __metadata("design:paramtypes", [list_service_1.ListService, page_1.Page, json_service_1.JsonService, global_service_1.GlobalService, router_1.RouterExtensions])
    ], CarAddComponent);
    return CarAddComponent;
}());
exports.CarAddComponent = CarAddComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyLWFkZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjYXItYWRkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUVsRCw4REFBNEQ7QUFDNUQsZ0NBQStCO0FBQy9CLDhEQUE0RDtBQUM1RCxzREFBK0Q7QUFDL0Qsb0VBQWtFO0FBUWxFO0lBSUMseUJBQW9CLFdBQXdCLEVBQVcsSUFBVSxFQUFVLFdBQXdCLEVBQVUsYUFBNEIsRUFBVSxnQkFBa0M7UUFBakssZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVyxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFBSyxDQUFDO0lBRTNMLGtDQUFRLEdBQVI7UUFDQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMzQixvQ0FBb0M7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFTSx1Q0FBYSxHQUFwQjtRQUNDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlFLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDUCxDQUFDO0lBQ1IsQ0FBQztJQUVNLDRDQUFrQixHQUF6QjtRQUNDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBekJXLGVBQWU7UUFOM0IsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsYUFBYTtZQUN2QixXQUFXLEVBQUUsMEJBQTBCO1lBQ3ZDLFNBQVMsRUFBRSxDQUFDLDBCQUEwQixDQUFDO1NBQ3ZDLENBQUM7eUNBS2dDLDBCQUFXLEVBQWlCLFdBQUksRUFBdUIsMEJBQVcsRUFBeUIsOEJBQWEsRUFBNEIseUJBQWdCO09BSnpLLGVBQWUsQ0E2QjNCO0lBQUQsc0JBQUM7Q0FBQSxBQTdCRCxJQTZCQztBQTdCWSwwQ0FBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBMaXN0UGlja2VyIH0gZnJvbSBcInVpL2xpc3QtcGlja2VyXCI7XG5pbXBvcnQgeyBMaXN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2xpc3QvbGlzdC5zZXJ2aWNlJztcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0IHsgSnNvblNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvanNvbi9qc29uLnNlcnZpY2VcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBHbG9iYWxTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL2dsb2JhbC9nbG9iYWwuc2VydmljZVwiO1xuXG5AQ29tcG9uZW50KHtcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0c2VsZWN0b3I6ICdhcHAtY2FyLWFkZCcsXG5cdHRlbXBsYXRlVXJsOiAnLi9jYXItYWRkLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vY2FyLWFkZC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENhckFkZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cblx0XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSBsaXN0U2VydmljZTogTGlzdFNlcnZpY2UsICBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUganNvblNlcnZpY2U6IEpzb25TZXJ2aWNlLCBwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyApIHsgfVxuXG5cdG5nT25Jbml0KCkgeyBcblx0XHR0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZENhcklkID0gbnVsbDtcblx0XHR0aGlzLmpzb25TZXJ2aWNlLmNhck5hbWUgPSBudWxsO1xuXHRcdHRoaXMuanNvblNlcnZpY2UudHJhaWxlck5hbWUgPSBudWxsO1xuXHRcdHRoaXMuanNvblNlcnZpY2UucGlja2VkVHJhaWxlcklkID0gbnVsbDtcblx0XHR0aGlzLmxpc3RTZXJ2aWNlLmdldENhcnMoKTtcblx0XHQvLyB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcblx0XHR0aGlzLmxpc3RTZXJ2aWNlLmdldEN1cnJlbmNpZXMoKTtcblx0XHR0aGlzLmxpc3RTZXJ2aWNlLmdldFRyYWlsZXJzKCk7XG5cdH1cblx0XG5cdHB1YmxpYyBpc0NhcklkUGlja2VkKCl7XG5cdFx0aWYgKHRoaXMuanNvblNlcnZpY2UucGlja2VkQ2FySWQgPiAwIHx8IHRoaXMuanNvblNlcnZpY2UucGlja2VkVHJhaWxlcklkID4gMCkge1xuXHRcdFx0cmV0dXJuIHRydWU7XG4gICAgICAgIH1cblx0fVxuXG5cdHB1YmxpYyBzZXRMaW5rRm9yTmV4dFZpZXcoKXtcblx0XHR0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUodGhpcy5nbG9iYWxTZXJ2aWNlLmNhckFkZExpbmspO1xuXHR9XG5cblxuXG59XG4iXX0=