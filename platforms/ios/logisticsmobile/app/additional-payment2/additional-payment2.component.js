"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var date_service_1 = require("../services/date/date.service");
var additional_payment_service_1 = require("../services/additional-payment/additional-payment.service");
var global_service_1 = require("../services/global/global.service");
var list_service_1 = require("../services/list/list.service");
var json_service_1 = require("../services/json/json.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var AdditionalPayment2Component = (function () {
    function AdditionalPayment2Component(page, dateService, additionalPaymentService, globalService, listService, jsonService, userService, routerExtensions) {
        this.page = page;
        this.dateService = dateService;
        this.additionalPaymentService = additionalPaymentService;
        this.globalService = globalService;
        this.listService = listService;
        this.jsonService = jsonService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
    }
    AdditionalPayment2Component.prototype.ngOnInit = function () {
        this.additionalPaymentService.totalPrice = null;
    };
    AdditionalPayment2Component.prototype.addAdditionalPayment = function () {
        var _this = this;
        this.additionalPaymentService.additionalPaymentJSONforPOST = {
            paymentDay: this.dateService.dateFormatForPost(),
            paymentName: {
                id: this.additionalPaymentService.pickedAdditionalpaymentNameId
            },
            totalPrice: this.additionalPaymentService.totalPrice,
            paymentType: this.jsonService.pickedPaymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.jsonService.pickedCarId
            },
            currency: {
                id: this.jsonService.pickedCurrencyId
            }
        };
        this.additionalPaymentService.addAdditionalPayment(this.additionalPaymentService.additionalPaymentJSONforPOST).subscribe(function (res) {
            console.log(_this.additionalPaymentService.additionalPaymentJSONforPOST.totalPrice);
            alert({
                title: "Potwierdzenie",
                message: "Opłata dodana poprawnie",
                okButtonText: "OK"
            }), _this.routerExtensions.navigate(["/additional-payment-details"]);
        }, function (err) {
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    AdditionalPayment2Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-additional-payment2',
            templateUrl: './additional-payment2.component.html',
            styleUrls: ['./additional-payment2.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, date_service_1.DateService, additional_payment_service_1.AdditionalPaymentService, global_service_1.GlobalService, list_service_1.ListService, json_service_1.JsonService, user_service_1.UserService, router_1.RouterExtensions])
    ], AdditionalPayment2Component);
    return AdditionalPayment2Component;
}());
exports.AdditionalPayment2Component = AdditionalPayment2Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkaXRpb25hbC1wYXltZW50Mi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhZGRpdGlvbmFsLXBheW1lbnQyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxnQ0FBK0I7QUFDL0IsOERBQTREO0FBQzVELHdHQUFxRztBQUNyRyxvRUFBa0U7QUFDbEUsOERBQTREO0FBQzVELDhEQUE0RDtBQUM1RCw4REFBNEQ7QUFDNUQsc0RBQStEO0FBUS9EO0lBRUMscUNBQW9CLElBQVUsRUFBVSxXQUF3QixFQUFVLHdCQUFrRCxFQUFVLGFBQTRCLEVBQVUsV0FBd0IsRUFBVSxXQUF3QixFQUFVLFdBQXdCLEVBQVUsZ0JBQWtDO1FBQWhTLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFBVSxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFDaFQsQ0FBQztJQUVMLDhDQUFRLEdBQVI7UUFDQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztJQUNqRCxDQUFDO0lBR00sMERBQW9CLEdBQTNCO1FBQUEsaUJBa0NDO1FBakNBLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyw0QkFBNEIsR0FBRztZQUM1RCxVQUFVLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRTtZQUNoRCxXQUFXLEVBQUU7Z0JBQ1osRUFBRSxFQUFFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyw2QkFBNkI7YUFDL0Q7WUFDRCxVQUFVLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFVBQVU7WUFDcEQsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCO1lBQy9DLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVU7WUFDdkMsR0FBRyxFQUFFO2dCQUNKLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVc7YUFDaEM7WUFDRCxRQUFRLEVBQUU7Z0JBQ1QsRUFBRSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCO2FBQ3JDO1NBRUQsQ0FBQTtRQUVELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxTQUFTLENBQ3ZILFVBQUMsR0FBRztZQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLHdCQUF3QixDQUFDLDRCQUE0QixDQUFDLFVBQVUsQ0FBQyxDQUFBO1lBQzNGLEtBQUssQ0FBQztnQkFDTCxLQUFLLEVBQUUsZUFBZTtnQkFDdEIsT0FBTyxFQUFFLHlCQUF5QjtnQkFDbEMsWUFBWSxFQUFFLElBQUk7YUFDbEIsQ0FBQyxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUE7UUFDcEUsQ0FBQyxFQUNELFVBQUMsR0FBRztZQUNILEtBQUssQ0FBQztnQkFDTCxLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsZ0NBQWdDO2dCQUN6QyxZQUFZLEVBQUUsSUFBSTthQUNsQixDQUFDLENBQUE7UUFDSCxDQUFDLENBQ0QsQ0FBQztJQUNILENBQUM7SUE1Q1csMkJBQTJCO1FBTnZDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxXQUFXLEVBQUUsc0NBQXNDO1lBQ25ELFNBQVMsRUFBRSxDQUFDLHNDQUFzQyxDQUFDO1NBQ25ELENBQUM7eUNBR3lCLFdBQUksRUFBdUIsMEJBQVcsRUFBb0MscURBQXdCLEVBQXlCLDhCQUFhLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQTRCLHlCQUFnQjtPQUZ4UywyQkFBMkIsQ0E4Q3ZDO0lBQUQsa0NBQUM7Q0FBQSxBQTlDRCxJQThDQztBQTlDWSxrRUFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XG5pbXBvcnQgeyBEYXRlU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2RhdGUvZGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IEFkZGl0aW9uYWxQYXltZW50U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FkZGl0aW9uYWwtcGF5bWVudC9hZGRpdGlvbmFsLXBheW1lbnQuc2VydmljZSc7XG5pbXBvcnQgeyBHbG9iYWxTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZ2xvYmFsL2dsb2JhbC5zZXJ2aWNlJztcbmltcG9ydCB7IExpc3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbGlzdC9saXN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgSnNvblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9qc29uL2pzb24uc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5cbkBDb21wb25lbnQoe1xuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHRzZWxlY3RvcjogJ2FwcC1hZGRpdGlvbmFsLXBheW1lbnQyJyxcblx0dGVtcGxhdGVVcmw6ICcuL2FkZGl0aW9uYWwtcGF5bWVudDIuY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9hZGRpdGlvbmFsLXBheW1lbnQyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQWRkaXRpb25hbFBheW1lbnQyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgZGF0ZVNlcnZpY2U6IERhdGVTZXJ2aWNlLCBwcml2YXRlIGFkZGl0aW9uYWxQYXltZW50U2VydmljZTogQWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLCBwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgbGlzdFNlcnZpY2U6IExpc3RTZXJ2aWNlLCBwcml2YXRlIGpzb25TZXJ2aWNlOiBKc29uU2VydmljZSwgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9uc1xuXHQpIHsgfVxuXG5cdG5nT25Jbml0KCkge1xuXHRcdHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLnRvdGFsUHJpY2UgPSBudWxsO1xuXHR9XG5cblxuXHRwdWJsaWMgYWRkQWRkaXRpb25hbFBheW1lbnQoKSB7XG5cdFx0dGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UuYWRkaXRpb25hbFBheW1lbnRKU09OZm9yUE9TVCA9IHtcblx0XHRcdHBheW1lbnREYXk6IHRoaXMuZGF0ZVNlcnZpY2UuZGF0ZUZvcm1hdEZvclBvc3QoKSxcblx0XHRcdHBheW1lbnROYW1lOiB7IFxuXHRcdFx0XHRpZDogdGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UucGlja2VkQWRkaXRpb25hbHBheW1lbnROYW1lSWQgXG5cdFx0XHR9LFxuXHRcdFx0dG90YWxQcmljZTogdGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UudG90YWxQcmljZSxcblx0XHRcdHBheW1lbnRUeXBlOiB0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZFBheW1lbnRUeXBlLFxuXHRcdFx0ZHJpdmVyTmFtZTogdGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lLFxuXHRcdFx0Y2FyOiB7XG5cdFx0XHRcdGlkOiB0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZENhcklkXG5cdFx0XHR9LFxuXHRcdFx0Y3VycmVuY3k6IHtcblx0XHRcdFx0aWQ6IHRoaXMuanNvblNlcnZpY2UucGlja2VkQ3VycmVuY3lJZFxuXHRcdFx0fVxuXG5cdFx0fVxuXG5cdFx0dGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UuYWRkQWRkaXRpb25hbFBheW1lbnQodGhpcy5hZGRpdGlvbmFsUGF5bWVudFNlcnZpY2UuYWRkaXRpb25hbFBheW1lbnRKU09OZm9yUE9TVCkuc3Vic2NyaWJlKFxuXHRcdFx0KHJlcykgPT4ge2NvbnNvbGUubG9nKHRoaXMuYWRkaXRpb25hbFBheW1lbnRTZXJ2aWNlLmFkZGl0aW9uYWxQYXltZW50SlNPTmZvclBPU1QudG90YWxQcmljZSlcblx0XHRcdFx0YWxlcnQoe1xuXHRcdFx0XHRcdHRpdGxlOiBcIlBvdHdpZXJkemVuaWVcIixcblx0XHRcdFx0XHRtZXNzYWdlOiBcIk9wxYJhdGEgZG9kYW5hIHBvcHJhd25pZVwiLFxuXHRcdFx0XHRcdG9rQnV0dG9uVGV4dDogXCJPS1wiXG5cdFx0XHRcdH0pLCB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2FkZGl0aW9uYWwtcGF5bWVudC1kZXRhaWxzXCJdKVxuXHRcdFx0fSxcblx0XHRcdChlcnIpID0+IHtcblx0XHRcdFx0YWxlcnQoe1xuXHRcdFx0XHRcdHRpdGxlOiBcIkLFgsSFZFwiLFxuXHRcdFx0XHRcdG1lc3NhZ2U6IFwiUG9wcmF3IGRhbmUgaSBzcHLDs2J1aiBwb25vd25pZVwiLFxuXHRcdFx0XHRcdG9rQnV0dG9uVGV4dDogXCJPS1wiXG5cdFx0XHRcdH0pXG5cdFx0XHR9LFxuXHRcdCk7XG5cdH1cblxufVxuIl19