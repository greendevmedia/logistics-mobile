"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var user_service_1 = require("../user/user.service");
var url_service_1 = require("../urlService/url.service");
var ShipmentService = (function () {
    function ShipmentService(http, userService, urlService) {
        this.http = http;
        this.userService = userService;
        this.urlService = urlService;
    }
    ShipmentService.prototype.getShipmentForDriverByDrivernameAndNumber = function (number) {
        var headers = new http_1.Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
        var options = new http_1.RequestOptions({ headers: headers });
        var url = this.urlService.apiUrl + "/driver/api/v1/shipments/ship/" + encodeURI(this.userService.driverName.toLowerCase()) + "/" + number;
        return this.http.get(url, options);
    };
    ShipmentService.prototype.getShipmentForDriverByNumber = function (number) {
        var headers = new http_1.Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token });
        var options = new http_1.RequestOptions({ headers: headers });
        var url = this.urlService.apiUrl + "/driver/api/v1/shipments/ship/" + number;
        console.log(url);
        return this.http.get(url, options);
    };
    ShipmentService.prototype.updateUnloadingDate = function (shipmentWithNewDate) {
        var headers = new http_1.Headers({ "Authorization": "bearer " + this.userService.userApiAccessData.json().access_token, "Content-Type": "application/json" });
        var options = new http_1.RequestOptions({ headers: headers });
        var url = this.urlService.apiUrl + "/driver/api/v1/shipments/";
        return this.http.put(url, shipmentWithNewDate, options);
    };
    ShipmentService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, user_service_1.UserService, url_service_1.UrlService])
    ], ShipmentService);
    return ShipmentService;
}());
exports.ShipmentService = ShipmentService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hpcG1lbnQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNoaXBtZW50LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0Msc0NBQXdFO0FBQ3hFLHFEQUFtRDtBQUNuRCx5REFBdUQ7QUFHdkQ7SUFFRSx5QkFBb0IsSUFBVSxFQUFVLFdBQXdCLEVBQVUsVUFBc0I7UUFBNUUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUFJLENBQUM7SUFFOUYsbUVBQXlDLEdBQWhELFVBQWlELE1BQU07UUFDckQsSUFBSSxPQUFPLEdBQUcsSUFBSSxjQUFPLENBQUMsRUFBRSxlQUFlLEVBQUUsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztRQUNuSCxJQUFJLE9BQU8sR0FBRyxJQUFJLHFCQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUN2RCxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxnQ0FBZ0MsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxHQUFHLEdBQUcsTUFBTSxDQUFDO1FBQzFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVNLHNEQUE0QixHQUFuQyxVQUFvQyxNQUFNO1FBQ3hDLElBQUksT0FBTyxHQUFHLElBQUksY0FBTyxDQUFDLEVBQUUsZUFBZSxFQUFFLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7UUFDbkgsSUFBSSxPQUFPLEdBQUcsSUFBSSxxQkFBYyxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDdkQsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsZ0NBQWdDLEdBQUcsTUFBTSxDQUFDO1FBQzdFLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRU0sNkNBQW1CLEdBQTFCLFVBQTJCLG1CQUFtQjtRQUM1QyxJQUFJLE9BQU8sR0FBRyxJQUFJLGNBQU8sQ0FBQyxFQUFFLGVBQWUsRUFBRSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxZQUFZLEVBQUcsY0FBYyxFQUFFLGtCQUFrQixFQUFFLENBQUMsQ0FBQztRQUN4SixJQUFJLE9BQU8sR0FBRyxJQUFJLHFCQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUN2RCxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRywyQkFBMkIsQ0FBQztRQUMvRCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLG1CQUFtQixFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQzFELENBQUM7SUF4QlUsZUFBZTtRQUQzQixpQkFBVSxFQUFFO3lDQUdlLFdBQUksRUFBdUIsMEJBQVcsRUFBc0Isd0JBQVU7T0FGckYsZUFBZSxDQTBCM0I7SUFBRCxzQkFBQztDQUFBLEFBMUJELElBMEJDO0FBMUJZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cCwgSGVhZGVycywgUmVzcG9uc2UsIFJlcXVlc3RPcHRpb25zIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFVybFNlcnZpY2UgfSBmcm9tICcuLi91cmxTZXJ2aWNlL3VybC5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFNoaXBtZW50U2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJpdmF0ZSB1cmxTZXJ2aWNlOiBVcmxTZXJ2aWNlKSB7IH1cblxuICBwdWJsaWMgZ2V0U2hpcG1lbnRGb3JEcml2ZXJCeURyaXZlcm5hbWVBbmROdW1iZXIobnVtYmVyKSB7XG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSGVhZGVycyh7IFwiQXV0aG9yaXphdGlvblwiOiBcImJlYXJlciBcIiArIHRoaXMudXNlclNlcnZpY2UudXNlckFwaUFjY2Vzc0RhdGEuanNvbigpLmFjY2Vzc190b2tlbiB9KTtcbiAgICBsZXQgb3B0aW9ucyA9IG5ldyBSZXF1ZXN0T3B0aW9ucyh7IGhlYWRlcnM6IGhlYWRlcnMgfSk7XG4gICAgbGV0IHVybCA9IHRoaXMudXJsU2VydmljZS5hcGlVcmwgKyBcIi9kcml2ZXIvYXBpL3YxL3NoaXBtZW50cy9zaGlwL1wiICsgZW5jb2RlVVJJKHRoaXMudXNlclNlcnZpY2UuZHJpdmVyTmFtZS50b0xvd2VyQ2FzZSgpKSArIFwiL1wiICsgbnVtYmVyO1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KHVybCwgb3B0aW9ucyk7XG4gIH1cblxuICBwdWJsaWMgZ2V0U2hpcG1lbnRGb3JEcml2ZXJCeU51bWJlcihudW1iZXIpIHtcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIZWFkZXJzKHsgXCJBdXRob3JpemF0aW9uXCI6IFwiYmVhcmVyIFwiICsgdGhpcy51c2VyU2VydmljZS51c2VyQXBpQWNjZXNzRGF0YS5qc29uKCkuYWNjZXNzX3Rva2VuIH0pO1xuICAgIGxldCBvcHRpb25zID0gbmV3IFJlcXVlc3RPcHRpb25zKHsgaGVhZGVyczogaGVhZGVycyB9KTtcbiAgICBsZXQgdXJsID0gdGhpcy51cmxTZXJ2aWNlLmFwaVVybCArIFwiL2RyaXZlci9hcGkvdjEvc2hpcG1lbnRzL3NoaXAvXCIgKyBudW1iZXI7XG4gICAgY29uc29sZS5sb2codXJsKTtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldCh1cmwsIG9wdGlvbnMpO1xuICB9XG5cbiAgcHVibGljIHVwZGF0ZVVubG9hZGluZ0RhdGUoc2hpcG1lbnRXaXRoTmV3RGF0ZSl7XG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSGVhZGVycyh7IFwiQXV0aG9yaXphdGlvblwiOiBcImJlYXJlciBcIiArIHRoaXMudXNlclNlcnZpY2UudXNlckFwaUFjY2Vzc0RhdGEuanNvbigpLmFjY2Vzc190b2tlbiwgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiIH0pO1xuICAgIGxldCBvcHRpb25zID0gbmV3IFJlcXVlc3RPcHRpb25zKHsgaGVhZGVyczogaGVhZGVycyB9KTtcbiAgICBsZXQgdXJsID0gdGhpcy51cmxTZXJ2aWNlLmFwaVVybCArIFwiL2RyaXZlci9hcGkvdjEvc2hpcG1lbnRzL1wiO1xuICAgIHJldHVybiB0aGlzLmh0dHAucHV0KHVybCwgc2hpcG1lbnRXaXRoTmV3RGF0ZSwgb3B0aW9ucyk7XG4gIH1cblxufVxuIl19