"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var global_service_1 = require("../global/global.service");
var LoginService = (function () {
    function LoginService(http, globalService) {
        this.http = http;
        this.globalService = globalService;
        this.loginUrl = this.globalService.keycloakUrl + "/auth/realms/Logistics/protocol/openid-connect/token";
        this.logoutUrl = this.globalService.keycloakUrl + "/auth/realms/Logistics/protocol/openid-connect/logout";
        this.headers = new http_1.Headers({ "Content-Type": "application/x-www-form-urlencoded" });
        this.options = new http_1.RequestOptions({ headers: this.headers });
    }
    LoginService.prototype.login = function (body) {
        return this.http.post(this.loginUrl, encodeURI(body), this.options);
    };
    LoginService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, global_service_1.GlobalService])
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvZ2luLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0Msc0NBQXdFO0FBQ3hFLDJEQUF5RDtBQUt6RDtJQU9DLHNCQUFvQixJQUFVLEVBQVUsYUFBNEI7UUFBaEQsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBTDdELGFBQVEsR0FBVyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxzREFBc0QsQ0FBQztRQUMzRyxjQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsdURBQXVELENBQUM7UUFDckcsWUFBTyxHQUFHLElBQUksY0FBTyxDQUFDLEVBQUUsY0FBYyxFQUFFLG1DQUFtQyxFQUFFLENBQUMsQ0FBQztRQUMvRSxZQUFPLEdBQUcsSUFBSSxxQkFBYyxDQUFDLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBRVMsQ0FBQztJQUVsRSw0QkFBSyxHQUFaLFVBQWEsSUFBSTtRQUNoQixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFYVyxZQUFZO1FBRHhCLGlCQUFVLEVBQUU7eUNBUWMsV0FBSSxFQUF5Qiw4QkFBYTtPQVB4RCxZQUFZLENBYXhCO0lBQUQsbUJBQUM7Q0FBQSxBQWJELElBYUM7QUFiWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHAsIEhlYWRlcnMsIFJlc3BvbnNlLCBSZXF1ZXN0T3B0aW9ucyB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XG5pbXBvcnQgeyBHbG9iYWxTZXJ2aWNlIH0gZnJvbSBcIi4uL2dsb2JhbC9nbG9iYWwuc2VydmljZVwiO1xuaW1wb3J0ICogYXMgIGJhc2U2NCBmcm9tIFwiYmFzZS02NFwiO1xuaW1wb3J0ICogYXMgdXRmOCBmcm9tIFwidXRmOFwiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9naW5TZXJ2aWNlIHtcblxuXHRwdWJsaWMgbG9naW5Vcmw6IHN0cmluZyA9IHRoaXMuZ2xvYmFsU2VydmljZS5rZXljbG9ha1VybCArIFwiL2F1dGgvcmVhbG1zL0xvZ2lzdGljcy9wcm90b2NvbC9vcGVuaWQtY29ubmVjdC90b2tlblwiO1xuXHRwdWJsaWMgbG9nb3V0VXJsID0gdGhpcy5nbG9iYWxTZXJ2aWNlLmtleWNsb2FrVXJsICsgXCIvYXV0aC9yZWFsbXMvTG9naXN0aWNzL3Byb3RvY29sL29wZW5pZC1jb25uZWN0L2xvZ291dFwiO1xuXHRwdWJsaWMgaGVhZGVycyA9IG5ldyBIZWFkZXJzKHsgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWRcIiB9KTtcblx0cHVibGljIG9wdGlvbnMgPSBuZXcgUmVxdWVzdE9wdGlvbnMoeyBoZWFkZXJzOiB0aGlzLmhlYWRlcnMgfSk7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwLCBwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UpIHsgfVxuXG5cdHB1YmxpYyBsb2dpbihib2R5KSB7XG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wb3N0KHRoaXMubG9naW5VcmwsIGVuY29kZVVSSShib2R5KSwgdGhpcy5vcHRpb25zKTtcblx0fVxuXG59XG4iXX0=