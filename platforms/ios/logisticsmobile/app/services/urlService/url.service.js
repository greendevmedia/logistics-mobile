"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var UrlService = (function () {
    function UrlService() {
        // public apiUrl: string = "https://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT";
        this.apiUrl = "https://api.plewinski-logistics.com/logistics-0.0.1-SNAPSHOT";
    }
    UrlService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], UrlService);
    return UrlService;
}());
exports.UrlService = UrlService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ1cmwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUczQztJQUtFO1FBSEEsdUZBQXVGO1FBQ2hGLFdBQU0sR0FBVyw4REFBOEQsQ0FBQztJQUV2RSxDQUFDO0lBTE4sVUFBVTtRQUR0QixpQkFBVSxFQUFFOztPQUNBLFVBQVUsQ0FPdEI7SUFBRCxpQkFBQztDQUFBLEFBUEQsSUFPQztBQVBZLGdDQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVXJsU2VydmljZSB7XG5cbiAgLy8gcHVibGljIGFwaVVybDogc3RyaW5nID0gXCJodHRwczovL2FwaWxvZ2lzdGljcy5ncmVlbmRldi5pbi9sb2dpc3RpY3MtMC4wLjEtU05BUFNIT1RcIjtcbiAgcHVibGljIGFwaVVybDogc3RyaW5nID0gXCJodHRwczovL2FwaS5wbGV3aW5za2ktbG9naXN0aWNzLmNvbS9sb2dpc3RpY3MtMC4wLjEtU05BUFNIT1RcIjtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG59XG4iXX0=