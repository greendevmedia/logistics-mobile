import { Injectable } from '@angular/core';
import { DatePicker } from "ui/date-picker";
import { RefuelingService } from "../refueling/refueling.service";
import { EventData } from "data/observable";
import { AdditionalPaymentService } from '../additional-payment/additional-payment.service';

@Injectable()
export class DateService {

	public refuelingDate: any = {
        year: "",
        month: "",
        day: ""
    }

	constructor(private refuelingService: RefuelingService, private additionalPaymentService: AdditionalPaymentService) { }

	onPickerLoadedCurrentDate(args) {
        let datePicker = <DatePicker>args.object;
        datePicker.date = new Date();
        this.refuelingDate.year = datePicker.year;
        this.refuelingDate.month = datePicker.month;
        this.refuelingDate.day = datePicker.day;
	}
	
	onPickerLoadedDateFromGET(args) {
        let datePicker = <DatePicker>args.object;
        datePicker.day =this.refuelingService.refuelingJSONfromGETbyID.refuelingDate[2];
        datePicker.month = this.refuelingService.refuelingJSONfromGETbyID.refuelingDate[1];
		datePicker.year = this.refuelingService.refuelingJSONfromGETbyID.refuelingDate[0];
    }
    
    onPickerLoadedDateFromGETPayment(args) {
        let datePicker = <DatePicker>args.object;
        datePicker.day =this.additionalPaymentService.additionalPaymentJSONfromGETbyId.paymentDay[2];
        datePicker.month = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.paymentDay[1];
		datePicker.year = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.paymentDay[0];
	}

    onDateChanged(args) {
        let datePicker = <DatePicker>args.object;
        this.refuelingDate.year = datePicker.year;
        this.refuelingDate.month = datePicker.month;
        this.refuelingDate.day = datePicker.day;
        console.log(this.refuelingDate.year + " " + this.refuelingDate.month + " " + this.refuelingDate.day)
	}
	
	public datechecker(dateValue) {
        if (dateValue < 10) {
            dateValue = "0" + dateValue;
        }
        return dateValue;
	}
	
	public dateFormatForPost(){
		return this.refuelingDate.year + "-" + this.datechecker(this.refuelingDate.month) + "-" + this.datechecker(this.refuelingDate.day);
    }
    
    public dateFormatForShowing(date){
		return this.datechecker(date[2]) + "-" + this.datechecker(date[1]) + "-" + date[0];
	}


}
