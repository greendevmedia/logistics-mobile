"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GlobalService = (function () {
    function GlobalService() {
        this.grant_type = "password";
        this.client_id = "Logistics";
        this.keycloakUrl = "https://www.keycloak.plewinski-logistics.com:5513";
        this.client_secret = "92a5e14c-0588-43ca-96ef-921003eb2bdd";
        this.driversList = ["Tomasz Czekała", "Włodzimierz Fertyk", "Krzysiek Chojnacki", "Adam Ciechański",
            "Adam Wilczyński", "Leszek Tecław", "Paweł Plewiński", "Mirosław Tecław"];
        this.paymentTypesList = ["CASH", "DKV"];
        this.repairActivityList = ["DELIVERY", "RECEPTION"];
        this.loginFormVisibility = true;
        this.isTrailersListVisible = false;
    }
    GlobalService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], GlobalService);
    return GlobalService;
}());
exports.GlobalService = GlobalService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2xvYmFsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJnbG9iYWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUczQztJQWlCQztRQWRPLGVBQVUsR0FBVSxVQUFVLENBQUM7UUFDL0IsY0FBUyxHQUFVLFdBQVcsQ0FBQztRQUMvQixnQkFBVyxHQUFVLG1EQUFtRCxDQUFDO1FBQ3pFLGtCQUFhLEdBQVcsc0NBQXNDLENBQUM7UUFDL0QsZ0JBQVcsR0FBRyxDQUFDLGdCQUFnQixFQUFFLG9CQUFvQixFQUFFLG9CQUFvQixFQUFFLGlCQUFpQjtZQUNyRyxpQkFBaUIsRUFBRSxlQUFlLEVBQUUsaUJBQWlCLEVBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNsRSxxQkFBZ0IsR0FBRyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNuQyx1QkFBa0IsR0FBRyxDQUFDLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQTtRQUU5Qyx3QkFBbUIsR0FBWSxJQUFJLENBQUM7UUFHcEMsMEJBQXFCLEdBQUcsS0FBSyxDQUFDO0lBRXJCLENBQUM7SUFqQkwsYUFBYTtRQUR6QixpQkFBVSxFQUFFOztPQUNBLGFBQWEsQ0FtQnpCO0lBQUQsb0JBQUM7Q0FBQSxBQW5CRCxJQW1CQztBQW5CWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEdsb2JhbFNlcnZpY2Uge1xuXG5cblx0cHVibGljIGdyYW50X3R5cGU6IHN0cmluZz0gXCJwYXNzd29yZFwiO1xuXHRwdWJsaWMgY2xpZW50X2lkOiBzdHJpbmc9IFwiTG9naXN0aWNzXCI7XG5cdHB1YmxpYyBrZXljbG9ha1VybDogc3RyaW5nPSBcImh0dHBzOi8vd3d3LmtleWNsb2FrLnBsZXdpbnNraS1sb2dpc3RpY3MuY29tOjU1MTNcIjtcblx0cHVibGljIGNsaWVudF9zZWNyZXQ6IHN0cmluZyA9IFwiOTJhNWUxNGMtMDU4OC00M2NhLTk2ZWYtOTIxMDAzZWIyYmRkXCI7XG5cdHB1YmxpYyBkcml2ZXJzTGlzdCA9IFtcIlRvbWFzeiBDemVrYcWCYVwiLCBcIlfFgm9kemltaWVyeiBGZXJ0eWtcIiwgXCJLcnp5c2llayBDaG9qbmFja2lcIiwgXCJBZGFtIENpZWNoYcWEc2tpXCIsXG5cdFwiQWRhbSBXaWxjennFhHNraVwiLCBcIkxlc3playBUZWPFgmF3XCIsIFwiUGF3ZcWCIFBsZXdpxYRza2lcIixcIk1pcm9zxYJhdyBUZWPFgmF3XCJdO1xuXHRwdWJsaWMgcGF5bWVudFR5cGVzTGlzdCA9IFtcIkNBU0hcIiwgXCJES1ZcIl07XG5cdHB1YmxpYyByZXBhaXJBY3Rpdml0eUxpc3QgPSBbXCJERUxJVkVSWVwiLCBcIlJFQ0VQVElPTlwiXVxuXHRwdWJsaWMgYWN0aXZpdHlJbmRpY2F0b3I6IGJvb2xlYW47XG5cdHB1YmxpYyBsb2dpbkZvcm1WaXNpYmlsaXR5OiBib29sZWFuID0gdHJ1ZTtcblx0cHVibGljIGNhckFkZExpbms7XG5cdHB1YmxpYyBjYXJBZGRUaXRsZTtcblx0cHVibGljIGlzVHJhaWxlcnNMaXN0VmlzaWJsZSA9IGZhbHNlO1xuXG5cdGNvbnN0cnVjdG9yKCkgeyB9XG5cbn1cbiJdfQ==