"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var global_service_1 = require("../global/global.service");
var list_service_1 = require("../list/list.service");
var JsonService = (function () {
    function JsonService(globalService, listService) {
        this.globalService = globalService;
        this.listService = listService;
        this.isWarningNoteCollapsed = false;
        this.switchState = "Dodatkowe uwagi - NIE";
        this.isAdBlueCollapsed = false;
        this.switchStateAdBlue = "AdBlue - NIE";
    }
    JsonService.prototype.selectedIndexChangedOnPaymentTypes = function (args) {
        var picker = args.object;
        this.pickedPaymentType = this.globalService.paymentTypesList[picker.selectedIndex];
        this.isListItemsPicked();
    };
    JsonService.prototype.selectedIndexChangedOnCurrencies = function (args) {
        var picker = args.object;
        this.pickedCurrencyId = picker.selectedIndex + 1;
        this.isListItemsPicked();
    };
    JsonService.prototype.selectedIndexChangedOnCars = function (args) {
        var picker = args.object;
        this.pickedCarId = picker.selectedIndex + 1;
        this.carName = this.listService.carsList[picker.selectedIndex];
    };
    JsonService.prototype.selectedIndexChangedOnTrailers = function (args) {
        var picker = args.object;
        this.pickedTrailerId = picker.selectedIndex + 1;
        this.trailerName = this.listService.trailersList[picker.selectedIndex];
    };
    JsonService.prototype.isListItemsPicked = function () {
        if (this.pickedCurrencyId > 0 && this.pickedPaymentType !== null) {
            return true;
        }
    };
    JsonService.prototype.onFirstChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.switchState = "Dodatkowe uwagi - TAK";
            this.isWarningNoteCollapsed = true;
        }
        else {
            this.switchState = "Dodatkowe uwagi - NIE";
            this.isWarningNoteCollapsed = false;
        }
    };
    JsonService.prototype.onSecondChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.switchStateAdBlue = "AdBlue - TAK";
            this.isAdBlueCollapsed = true;
        }
        else {
            this.switchStateAdBlue = "AdBlue - NIE";
            this.isAdBlueCollapsed = false;
        }
    };
    JsonService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [global_service_1.GlobalService, list_service_1.ListService])
    ], JsonService);
    return JsonService;
}());
exports.JsonService = JsonService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsianNvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBRTNDLDJEQUF5RDtBQUV6RCxxREFBbUQ7QUFHbkQ7SUFxQkMscUJBQW9CLGFBQTRCLEVBQVUsV0FBd0I7UUFBOUQsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVp4RSwyQkFBc0IsR0FBWSxLQUFLLENBQUM7UUFDeEMsZ0JBQVcsR0FBVyx1QkFBdUIsQ0FBQztRQU9qRCxzQkFBaUIsR0FBWSxLQUFLLENBQUM7UUFDbkMsc0JBQWlCLEdBQVcsY0FBYyxDQUFDO0lBR29DLENBQUM7SUFFaEYsd0RBQWtDLEdBQXpDLFVBQTBDLElBQUk7UUFDdkMsSUFBSSxNQUFNLEdBQWUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbkYsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVTLHNEQUFnQyxHQUF2QyxVQUF3QyxJQUFJO1FBQ3hDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFUyxnREFBMEIsR0FBakMsVUFBa0MsSUFBSTtRQUNsQyxJQUFJLE1BQU0sR0FBZSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVNLG9EQUE4QixHQUFyQyxVQUFzQyxJQUFJO1FBQ3RDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDM0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBR00sdUNBQWlCLEdBQXhCO1FBQ08sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEtBQUssSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNoRSxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7SUFDUixDQUFDO0lBRU0sb0NBQWMsR0FBckIsVUFBc0IsSUFBSTtRQUNuQixJQUFJLFdBQVcsR0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3RDLEVBQUUsQ0FBQSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxXQUFXLEdBQUcsdUJBQXVCLENBQUM7WUFDM0MsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztRQUV2QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsV0FBVyxHQUFHLHVCQUF1QixDQUFDO1lBQzNDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFFeEMsQ0FBQztJQUNMLENBQUM7SUFFTSxxQ0FBZSxHQUF0QixVQUF1QixJQUFJO1FBQzdCLElBQUksV0FBVyxHQUFXLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdEMsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGNBQWMsQ0FBQztZQUN4QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQy9CLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNQLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxjQUFjLENBQUM7WUFDeEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztRQUNoQyxDQUFDO0lBQ0YsQ0FBQztJQTVFVyxXQUFXO1FBRHZCLGlCQUFVLEVBQUU7eUNBc0J1Qiw4QkFBYSxFQUF1QiwwQkFBVztPQXJCdEUsV0FBVyxDQThFdkI7SUFBRCxrQkFBQztDQUFBLEFBOUVELElBOEVDO0FBOUVZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTGlzdFBpY2tlciB9IGZyb20gXCJ1aS9saXN0LXBpY2tlclwiO1xuaW1wb3J0IHsgR2xvYmFsU2VydmljZSB9IGZyb20gXCIuLi9nbG9iYWwvZ2xvYmFsLnNlcnZpY2VcIjtcbmltcG9ydCB7IFN3aXRjaCB9IGZyb20gXCJ1aS9zd2l0Y2hcIjtcbmltcG9ydCB7IExpc3RTZXJ2aWNlIH0gZnJvbSAnLi4vbGlzdC9saXN0LnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgSnNvblNlcnZpY2Uge1xuXG5cdHB1YmxpYyBjYXJOYW1lO1xuXHRwdWJsaWMgY291bnRlcjogbnVtYmVyO1xuICAgIHB1YmxpYyBxdWFudGl0eTogbnVtYmVyO1xuXHRwdWJsaWMgcHJpY2U6IG51bWJlcjtcblx0cHVibGljIHBpY2tlZFBheW1lbnRUeXBlOiBzdHJpbmc7XG5cdHB1YmxpYyBwaWNrZWRDdXJyZW5jeUlkOiBudW1iZXI7XG5cdHB1YmxpYyBwaWNrZWRDYXJJZDogbnVtYmVyO1xuICAgIHB1YmxpYyBpc1dhcm5pbmdOb3RlQ29sbGFwc2VkOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHVibGljIHN3aXRjaFN0YXRlOiBzdHJpbmcgPSBcIkRvZGF0a293ZSB1d2FnaSAtIE5JRVwiO1xuICAgIHB1YmxpYyB3YXJuaW5nTm90ZTtcbiAgICBwdWJsaWMgYWRCbHVlUXVhbnRpdHk7XG4gICAgcHVibGljIGFkQmx1ZVByaWNlO1xuICAgIHB1YmxpYyB0cmFpbGVyTmFtZTtcbiAgICBwdWJsaWMgcGlja2VkVHJhaWxlcklkO1xuXG5cdHB1YmxpYyBpc0FkQmx1ZUNvbGxhcHNlZDogYm9vbGVhbiA9IGZhbHNlO1xuXHRwdWJsaWMgc3dpdGNoU3RhdGVBZEJsdWU6IHN0cmluZyA9IFwiQWRCbHVlIC0gTklFXCI7XG4gICAgXG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSBnbG9iYWxTZXJ2aWNlOiBHbG9iYWxTZXJ2aWNlLCBwcml2YXRlIGxpc3RTZXJ2aWNlOiBMaXN0U2VydmljZSkgeyB9XG5cblx0cHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkT25QYXltZW50VHlwZXMoYXJncykge1xuICAgICAgICBsZXQgcGlja2VyID0gPExpc3RQaWNrZXI+YXJncy5vYmplY3Q7XG4gICAgICAgIHRoaXMucGlja2VkUGF5bWVudFR5cGUgPSB0aGlzLmdsb2JhbFNlcnZpY2UucGF5bWVudFR5cGVzTGlzdFtwaWNrZXIuc2VsZWN0ZWRJbmRleF07XG4gICAgICAgIHRoaXMuaXNMaXN0SXRlbXNQaWNrZWQoKTtcblx0fVxuXG4gICAgcHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkT25DdXJyZW5jaWVzKGFyZ3MpIHtcbiAgICAgICAgbGV0IHBpY2tlciA9IDxMaXN0UGlja2VyPmFyZ3Mub2JqZWN0O1xuICAgICAgICB0aGlzLnBpY2tlZEN1cnJlbmN5SWQgPSBwaWNrZXIuc2VsZWN0ZWRJbmRleCArIDE7IFxuICAgICAgICB0aGlzLmlzTGlzdEl0ZW1zUGlja2VkKCk7XG5cdH1cbiAgICBcbiAgICBwdWJsaWMgc2VsZWN0ZWRJbmRleENoYW5nZWRPbkNhcnMoYXJncykge1xuICAgICAgICBsZXQgcGlja2VyID0gPExpc3RQaWNrZXI+YXJncy5vYmplY3Q7XG5cdFx0dGhpcy5waWNrZWRDYXJJZCA9IHBpY2tlci5zZWxlY3RlZEluZGV4ICsgMTtcblx0XHR0aGlzLmNhck5hbWUgPSB0aGlzLmxpc3RTZXJ2aWNlLmNhcnNMaXN0W3BpY2tlci5zZWxlY3RlZEluZGV4XTtcbiAgICB9XG4gICAgXG4gICAgcHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkT25UcmFpbGVycyhhcmdzKSB7XG4gICAgICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcblx0XHR0aGlzLnBpY2tlZFRyYWlsZXJJZCA9IHBpY2tlci5zZWxlY3RlZEluZGV4ICsgMTtcblx0XHR0aGlzLnRyYWlsZXJOYW1lID0gdGhpcy5saXN0U2VydmljZS50cmFpbGVyc0xpc3RbcGlja2VyLnNlbGVjdGVkSW5kZXhdO1xuXHR9XG5cblx0XG5cdHB1YmxpYyBpc0xpc3RJdGVtc1BpY2tlZCgpIHtcbiAgICAgICAgaWYgKHRoaXMucGlja2VkQ3VycmVuY3lJZCA+IDAgJiYgdGhpcy5waWNrZWRQYXltZW50VHlwZSAhPT0gbnVsbCApIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG5cdH1cblx0XG5cdHB1YmxpYyBvbkZpcnN0Q2hlY2tlZChhcmdzKSB7XG4gICAgICAgIGxldCBmaXJzdFN3aXRjaCA9IDxTd2l0Y2g+YXJncy5vYmplY3Q7XG4gICAgICAgIGlmKGZpcnN0U3dpdGNoLmNoZWNrZWQpIHtcbiAgICAgICAgICAgIHRoaXMuc3dpdGNoU3RhdGUgPSBcIkRvZGF0a293ZSB1d2FnaSAtIFRBS1wiO1xuICAgICAgICAgICAgdGhpcy5pc1dhcm5pbmdOb3RlQ29sbGFwc2VkID0gdHJ1ZTtcblxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gTklFXCI7XG4gICAgICAgICAgICB0aGlzLmlzV2FybmluZ05vdGVDb2xsYXBzZWQgPSBmYWxzZTtcbiAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIG9uU2Vjb25kQ2hlY2tlZChhcmdzKSB7XG5cdFx0bGV0IGZpcnN0U3dpdGNoID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcblx0XHRpZiAoZmlyc3RTd2l0Y2guY2hlY2tlZCkge1xuXHRcdFx0dGhpcy5zd2l0Y2hTdGF0ZUFkQmx1ZSA9IFwiQWRCbHVlIC0gVEFLXCI7XG5cdFx0XHR0aGlzLmlzQWRCbHVlQ29sbGFwc2VkID0gdHJ1ZTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy5zd2l0Y2hTdGF0ZUFkQmx1ZSA9IFwiQWRCbHVlIC0gTklFXCI7XG5cdFx0XHR0aGlzLmlzQWRCbHVlQ29sbGFwc2VkID0gZmFsc2U7XG5cdFx0fVxuXHR9XG5cbn1cbiJdfQ==