import { Injectable } from '@angular/core';
import { ListPicker } from "ui/list-picker";
import { GlobalService } from "../global/global.service";
import { Switch } from "ui/switch";
import { ListService } from '../list/list.service';

@Injectable()
export class JsonService {

	public carName;
	public counter: number;
    public quantity: number;
	public price: number;
	public pickedPaymentType: string;
	public pickedCurrencyId: number;
	public pickedCarId: number;
    public isWarningNoteCollapsed: boolean = false;
    public switchState: string = "Dodatkowe uwagi - NIE";
    public warningNote;
    public adBlueQuantity;
    public adBluePrice;
    public trailerName;
    public pickedTrailerId;

	public isAdBlueCollapsed: boolean = false;
	public switchStateAdBlue: string = "AdBlue - NIE";
    

	constructor(private globalService: GlobalService, private listService: ListService) { }

	public selectedIndexChangedOnPaymentTypes(args) {
        let picker = <ListPicker>args.object;
        this.pickedPaymentType = this.globalService.paymentTypesList[picker.selectedIndex];
        this.isListItemsPicked();
	}

    public selectedIndexChangedOnCurrencies(args) {
        let picker = <ListPicker>args.object;
        this.pickedCurrencyId = picker.selectedIndex + 1; 
        this.isListItemsPicked();
	}
    
    public selectedIndexChangedOnCars(args) {
        let picker = <ListPicker>args.object;
		this.pickedCarId = picker.selectedIndex + 1;
		this.carName = this.listService.carsList[picker.selectedIndex];
    }
    
    public selectedIndexChangedOnTrailers(args) {
        let picker = <ListPicker>args.object;
		this.pickedTrailerId = picker.selectedIndex + 1;
		this.trailerName = this.listService.trailersList[picker.selectedIndex];
	}

	
	public isListItemsPicked() {
        if (this.pickedCurrencyId > 0 && this.pickedPaymentType !== null ) {
            return true;
        }
	}
	
	public onFirstChecked(args) {
        let firstSwitch = <Switch>args.object;
        if(firstSwitch.checked) {
            this.switchState = "Dodatkowe uwagi - TAK";
            this.isWarningNoteCollapsed = true;

        } else {
            this.switchState = "Dodatkowe uwagi - NIE";
            this.isWarningNoteCollapsed = false;
            
        }
    }

    public onSecondChecked(args) {
		let firstSwitch = <Switch>args.object;
		if (firstSwitch.checked) {
			this.switchStateAdBlue = "AdBlue - TAK";
			this.isAdBlueCollapsed = true;
		} else {
			this.switchStateAdBlue = "AdBlue - NIE";
			this.isAdBlueCollapsed = false;
		}
	}

}
