"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var email = require("nativescript-email");
var json_service_1 = require("../json/json.service");
var date_service_1 = require("../date/date.service");
var user_service_1 = require("../user/user.service");
var router_1 = require("nativescript-angular/router");
var imagepicker = require("nativescript-imagepicker");
// var fs = require("file-system");
var TechnicalCardService = (function () {
    function TechnicalCardService(jsonService, dateService, userService, routerExtensions) {
        this.jsonService = jsonService;
        this.dateService = dateService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.oilLevel = "";
        this.purity = "";
        this.lighting = "";
        this.suspension = "";
        this.engineAndAccessories = "";
        this.carBody = "";
        this.imagesPaths = [];
        this.arrayOfAttachments = [];
    }
    TechnicalCardService.prototype.getPicture = function () {
        var that = this;
        var context = imagepicker.create({
            mode: "multiple" // use "multiple" for multiple selection
        });
        context
            .authorize()
            .then(function () {
            return context.present();
        })
            .then(function (selection) {
            selection.forEach(function (selected) {
                that.imagesPaths.push(selected.fileUri);
                console.log(selected.fileUri);
                console.log(that.imagesPaths.length);
            });
        }).catch(function (e) {
            // process error
        });
    };
    TechnicalCardService.prototype.sendTechnicalCard = function () {
        var _this = this;
        if (this.imagesPaths.length > 0) {
            this.createMultipleAttachemnts(this.imagesPaths, this.arrayOfAttachments);
        }
        if (this.jsonService.trailerName == null) {
            this.trailerName = "";
        }
        else {
            this.trailerName = "Przyczepa: " + this.jsonService.trailerName;
        }
        if (this.jsonService.carName == null) {
            this.carName = "";
        }
        else {
            this.carName = "Pojazd: " + this.jsonService.carName;
        }
        this.composeOptions = {
            to: ['pawel@plewinski-logistics.com', 'adam.b.ciechanski@wp.pl', 'tomasz.czekala1@gmail.com'],
            subject: 'Karta techniczna - ' + this.carName + " " + this.trailerName,
            body: "Kierowca: " + this.userService.driverName + "\n" + this.carName + " " + this.trailerName + "\nData: " + this.dateService.refuelingDate.day + "." + this.dateService.refuelingDate.month + "." + this.dateService.refuelingDate.year + "\nPoczątkowy stan licznika: " + this.counterStart + "\nKońcowy stan licznika: " + this.counterStop + "\nStan oleju: " + this.oilLevel + "\nCzystość: " + this.purity + "\nOświetlenie: " + this.lighting + "\nZawieszenie: " + this.suspension + "\nSilnik oraz osprzęt: " + this.engineAndAccessories + "\nKaroseria: " + this.carBody
        };
        if (this.imagesPaths.length > 0) {
            this.composeOptions.attachments = this.arrayOfAttachments;
        }
        email.available().then(function (available) {
            if (available) {
                email.compose(_this.composeOptions).then(function (result) {
                    _this.routerExtensions.navigate(["/navpage"]);
                });
            }
        }).catch(function (error) { return console.error(error); });
    };
    TechnicalCardService.prototype.createMultipleAttachemnts = function (arrayOfLinks, arrayOfAttachments) {
        var num = 0;
        arrayOfLinks.forEach(function (element) {
            num = num + 1;
            arrayOfAttachments.push({
                fileName: num + 'kt.png',
                path: element,
                mimeType: 'image/png'
            });
        });
    };
    TechnicalCardService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [json_service_1.JsonService, date_service_1.DateService, user_service_1.UserService, router_1.RouterExtensions])
    ], TechnicalCardService);
    return TechnicalCardService;
}());
exports.TechnicalCardService = TechnicalCardService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVjaG5pY2FsLWNhcmQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInRlY2huaWNhbC1jYXJkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsMENBQTRDO0FBQzVDLHFEQUFtRDtBQUNuRCxxREFBbUQ7QUFDbkQscURBQW1EO0FBQ25ELHNEQUErRDtBQUMvRCxzREFBd0Q7QUFFeEQsbUNBQW1DO0FBSW5DO0lBa0JDLDhCQUFvQixXQUF3QixFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSxnQkFBa0M7UUFBeEksZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQWRySixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2QsV0FBTSxHQUFHLEVBQUUsQ0FBQztRQUNaLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFDZCxlQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLHlCQUFvQixHQUFHLEVBQUUsQ0FBQztRQUMxQixZQUFPLEdBQUcsRUFBRSxDQUFDO1FBRWIsZ0JBQVcsR0FBRyxFQUFFLENBQUM7UUFDakIsdUJBQWtCLEdBQUcsRUFBRSxDQUFDO0lBTWlJLENBQUM7SUFJMUoseUNBQVUsR0FBakI7UUFDQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxPQUFPLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUNoQyxJQUFJLEVBQUUsVUFBVSxDQUFDLHdDQUF3QztTQUN6RCxDQUFDLENBQUM7UUFDSCxPQUFPO2FBQ0wsU0FBUyxFQUFFO2FBQ1gsSUFBSSxDQUFDO1lBQ0wsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMxQixDQUFDLENBQUM7YUFDRCxJQUFJLENBQUMsVUFBVSxTQUFTO1lBRXhCLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxRQUFRO2dCQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFdEMsQ0FBQyxDQUFDLENBQUM7UUFFSixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBQ25CLGdCQUFnQjtRQUNqQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxnREFBaUIsR0FBeEI7UUFBQSxpQkFrQ0M7UUFqQ0EsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUMzRSxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQTtRQUN0QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDUCxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQztRQUNqRSxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQTtRQUNsQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDUCxJQUFJLENBQUMsT0FBTyxHQUFHLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQztRQUN0RCxDQUFDO1FBRUQsSUFBSSxDQUFDLGNBQWMsR0FBRztZQUNyQixFQUFFLEVBQUUsQ0FBQywrQkFBK0IsRUFBRSx5QkFBeUIsRUFBRSwyQkFBMkIsQ0FBQztZQUM3RixPQUFPLEVBQUUscUJBQXFCLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVc7WUFDdEUsSUFBSSxFQUFFLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksR0FBRyw4QkFBOEIsR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLDJCQUEyQixHQUFHLElBQUksQ0FBQyxXQUFXLEdBQUcsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxjQUFjLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLGlCQUFpQixHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcseUJBQXlCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixHQUFHLGVBQWUsR0FBRyxJQUFJLENBQUMsT0FBTztTQUNyakIsQ0FBQTtRQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFBO1FBQzFELENBQUM7UUFHRCxLQUFLLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsU0FBUztZQUMvQixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNmLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU07b0JBQzdDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFBO2dCQUM3QyxDQUFDLENBQUMsQ0FBQTtZQUNILENBQUM7UUFDRixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFwQixDQUFvQixDQUFDLENBQUE7SUFDeEMsQ0FBQztJQUVNLHdEQUF5QixHQUFoQyxVQUFpQyxZQUFZLEVBQUUsa0JBQWtCO1FBQ2hFLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztRQUNaLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO1lBQzNCLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ2Qsa0JBQWtCLENBQUMsSUFBSSxDQUN0QjtnQkFDQyxRQUFRLEVBQUUsR0FBRyxHQUFHLFFBQVE7Z0JBQ3hCLElBQUksRUFBRSxPQUFPO2dCQUNiLFFBQVEsRUFBRSxXQUFXO2FBQ3JCLENBQUMsQ0FBQTtRQUVKLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQTlGVyxvQkFBb0I7UUFEaEMsaUJBQVUsRUFBRTt5Q0FtQnFCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQTRCLHlCQUFnQjtPQWxCaEosb0JBQW9CLENBa0doQztJQUFELDJCQUFDO0NBQUEsQUFsR0QsSUFrR0M7QUFsR1ksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0ICogYXMgZW1haWwgZnJvbSBcIm5hdGl2ZXNjcmlwdC1lbWFpbFwiO1xuaW1wb3J0IHsgSnNvblNlcnZpY2UgfSBmcm9tIFwiLi4vanNvbi9qc29uLnNlcnZpY2VcIjtcbmltcG9ydCB7IERhdGVTZXJ2aWNlIH0gZnJvbSBcIi4uL2RhdGUvZGF0ZS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgKiBhcyBpbWFnZXBpY2tlciBmcm9tIFwibmF0aXZlc2NyaXB0LWltYWdlcGlja2VyXCI7XG5pbXBvcnQgKiBhcyBpbWFnZXNvdXJjZU1vZHVsZSBmcm9tIFwiaW1hZ2Utc291cmNlXCI7XG4vLyB2YXIgZnMgPSByZXF1aXJlKFwiZmlsZS1zeXN0ZW1cIik7XG5cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFRlY2huaWNhbENhcmRTZXJ2aWNlIHtcblxuXHRwdWJsaWMgY291bnRlclN0YXJ0O1xuXHRwdWJsaWMgY291bnRlclN0b3A7XG5cdHB1YmxpYyBvaWxMZXZlbCA9IFwiXCI7XG5cdHB1YmxpYyBwdXJpdHkgPSBcIlwiO1xuXHRwdWJsaWMgbGlnaHRpbmcgPSBcIlwiO1xuXHRwdWJsaWMgc3VzcGVuc2lvbiA9IFwiXCI7XG5cdHB1YmxpYyBlbmdpbmVBbmRBY2Nlc3NvcmllcyA9IFwiXCI7XG5cdHB1YmxpYyBjYXJCb2R5ID0gXCJcIjtcblx0cHVibGljIGNvbXBvc2VPcHRpb25zOiBlbWFpbC5Db21wb3NlT3B0aW9ucztcblx0cHVibGljIGltYWdlc1BhdGhzID0gW107XG5cdHB1YmxpYyBhcnJheU9mQXR0YWNobWVudHMgPSBbXTtcblx0cHVibGljIHRyYWlsZXJOYW1lO1xuXHRwdWJsaWMgY2FyTmFtZTtcblxuXG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSBqc29uU2VydmljZTogSnNvblNlcnZpY2UsIHByaXZhdGUgZGF0ZVNlcnZpY2U6IERhdGVTZXJ2aWNlLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zKSB7IH1cblxuXG5cblx0cHVibGljIGdldFBpY3R1cmUoKSB7XG5cdFx0dmFyIHRoYXQgPSB0aGlzO1xuXHRcdGxldCBjb250ZXh0ID0gaW1hZ2VwaWNrZXIuY3JlYXRlKHtcblx0XHRcdG1vZGU6IFwibXVsdGlwbGVcIiAvLyB1c2UgXCJtdWx0aXBsZVwiIGZvciBtdWx0aXBsZSBzZWxlY3Rpb25cblx0XHR9KTtcblx0XHRjb250ZXh0XG5cdFx0XHQuYXV0aG9yaXplKClcblx0XHRcdC50aGVuKGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0cmV0dXJuIGNvbnRleHQucHJlc2VudCgpO1xuXHRcdFx0fSlcblx0XHRcdC50aGVuKGZ1bmN0aW9uIChzZWxlY3Rpb24pIHtcblxuXHRcdFx0XHRzZWxlY3Rpb24uZm9yRWFjaChmdW5jdGlvbiAoc2VsZWN0ZWQpIHtcblx0XHRcdFx0XHR0aGF0LmltYWdlc1BhdGhzLnB1c2goc2VsZWN0ZWQuZmlsZVVyaSk7XG5cdFx0XHRcdFx0Y29uc29sZS5sb2coc2VsZWN0ZWQuZmlsZVVyaSk7XG5cdFx0XHRcdFx0Y29uc29sZS5sb2codGhhdC5pbWFnZXNQYXRocy5sZW5ndGgpO1xuXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHR9KS5jYXRjaChmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHQvLyBwcm9jZXNzIGVycm9yXG5cdFx0XHR9KTtcblx0fVxuXG5cdHB1YmxpYyBzZW5kVGVjaG5pY2FsQ2FyZCgpIHtcblx0XHRpZiAodGhpcy5pbWFnZXNQYXRocy5sZW5ndGggPiAwKSB7XG5cdFx0XHR0aGlzLmNyZWF0ZU11bHRpcGxlQXR0YWNoZW1udHModGhpcy5pbWFnZXNQYXRocywgdGhpcy5hcnJheU9mQXR0YWNobWVudHMpO1xuXHRcdH1cblxuXHRcdGlmICh0aGlzLmpzb25TZXJ2aWNlLnRyYWlsZXJOYW1lID09IG51bGwpIHtcblx0XHRcdHRoaXMudHJhaWxlck5hbWUgPSBcIlwiXG5cdFx0fSBlbHNlIHtcblx0XHRcdHRoaXMudHJhaWxlck5hbWUgPSBcIlByenljemVwYTogXCIgKyB0aGlzLmpzb25TZXJ2aWNlLnRyYWlsZXJOYW1lO1xuXHRcdH1cblxuXHRcdGlmICh0aGlzLmpzb25TZXJ2aWNlLmNhck5hbWUgPT0gbnVsbCkge1xuXHRcdFx0dGhpcy5jYXJOYW1lID0gXCJcIlxuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLmNhck5hbWUgPSBcIlBvamF6ZDogXCIgKyB0aGlzLmpzb25TZXJ2aWNlLmNhck5hbWU7XG5cdFx0fVxuXG5cdFx0dGhpcy5jb21wb3NlT3B0aW9ucyA9IHtcblx0XHRcdHRvOiBbJ3Bhd2VsQHBsZXdpbnNraS1sb2dpc3RpY3MuY29tJywgJ2FkYW0uYi5jaWVjaGFuc2tpQHdwLnBsJywgJ3RvbWFzei5jemVrYWxhMUBnbWFpbC5jb20nXSxcblx0XHRcdHN1YmplY3Q6ICdLYXJ0YSB0ZWNobmljem5hIC0gJyArIHRoaXMuY2FyTmFtZSArIFwiIFwiICsgdGhpcy50cmFpbGVyTmFtZSxcblx0XHRcdGJvZHk6IFwiS2llcm93Y2E6IFwiICsgdGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lICsgXCJcXG5cIiArIHRoaXMuY2FyTmFtZSArIFwiIFwiICsgdGhpcy50cmFpbGVyTmFtZSArIFwiXFxuRGF0YTogXCIgKyB0aGlzLmRhdGVTZXJ2aWNlLnJlZnVlbGluZ0RhdGUuZGF5ICsgXCIuXCIgKyB0aGlzLmRhdGVTZXJ2aWNlLnJlZnVlbGluZ0RhdGUubW9udGggKyBcIi5cIiArIHRoaXMuZGF0ZVNlcnZpY2UucmVmdWVsaW5nRGF0ZS55ZWFyICsgXCJcXG5Qb2N6xIV0a293eSBzdGFuIGxpY3puaWthOiBcIiArIHRoaXMuY291bnRlclN0YXJ0ICsgXCJcXG5Lb8WEY293eSBzdGFuIGxpY3puaWthOiBcIiArIHRoaXMuY291bnRlclN0b3AgKyBcIlxcblN0YW4gb2xlanU6IFwiICsgdGhpcy5vaWxMZXZlbCArIFwiXFxuQ3p5c3RvxZvEhzogXCIgKyB0aGlzLnB1cml0eSArIFwiXFxuT8Wbd2lldGxlbmllOiBcIiArIHRoaXMubGlnaHRpbmcgKyBcIlxcblphd2llc3plbmllOiBcIiArIHRoaXMuc3VzcGVuc2lvbiArIFwiXFxuU2lsbmlrIG9yYXogb3NwcnrEmXQ6IFwiICsgdGhpcy5lbmdpbmVBbmRBY2Nlc3NvcmllcyArIFwiXFxuS2Fyb3NlcmlhOiBcIiArIHRoaXMuY2FyQm9keVxuXHRcdH1cblx0XHRpZiAodGhpcy5pbWFnZXNQYXRocy5sZW5ndGggPiAwKSB7XG5cdFx0XHR0aGlzLmNvbXBvc2VPcHRpb25zLmF0dGFjaG1lbnRzID0gdGhpcy5hcnJheU9mQXR0YWNobWVudHNcblx0XHR9XG5cblxuXHRcdGVtYWlsLmF2YWlsYWJsZSgpLnRoZW4oYXZhaWxhYmxlID0+IHtcblx0XHRcdGlmIChhdmFpbGFibGUpIHtcblx0XHRcdFx0ZW1haWwuY29tcG9zZSh0aGlzLmNvbXBvc2VPcHRpb25zKS50aGVuKHJlc3VsdCA9PiB7XG5cdFx0XHRcdFx0dGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9uYXZwYWdlXCJdKVxuXHRcdFx0XHR9KVxuXHRcdFx0fVxuXHRcdH0pLmNhdGNoKGVycm9yID0+IGNvbnNvbGUuZXJyb3IoZXJyb3IpKVxuXHR9XG5cblx0cHVibGljIGNyZWF0ZU11bHRpcGxlQXR0YWNoZW1udHMoYXJyYXlPZkxpbmtzLCBhcnJheU9mQXR0YWNobWVudHMpIHtcblx0XHRsZXQgbnVtID0gMDtcblx0XHRhcnJheU9mTGlua3MuZm9yRWFjaChlbGVtZW50ID0+IHtcblx0XHRcdG51bSA9IG51bSArIDE7XG5cdFx0XHRhcnJheU9mQXR0YWNobWVudHMucHVzaChcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGZpbGVOYW1lOiBudW0gKyAna3QucG5nJyxcblx0XHRcdFx0XHRwYXRoOiBlbGVtZW50LFxuXHRcdFx0XHRcdG1pbWVUeXBlOiAnaW1hZ2UvcG5nJ1xuXHRcdFx0XHR9KVxuXG5cdFx0fSk7XG5cdH1cblxuXG5cbn1cbiJdfQ==