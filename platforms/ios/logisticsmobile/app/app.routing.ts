import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { RefuelingEditComponent } from "./refueling-edit/refueling-edit.component";
import { NavigationComponent } from "./navigation/navigation.component";
import { NavpageComponent } from "./navpage/navpage.component";
import { CarAddComponent } from "./car-add/car-add.component";
import { TechnicalCard1Component } from "./technical-card-1/technical-card-1.component";
import { TechnicalCard2Component } from "./technical-card-2/technical-card-2.component";
import { RefuellingAddComponent } from "./refuelling-add/refuelling-add.component";
import { RefuelingListComponent } from "./refueling-list/refueling-list.component";
import { AdditionalPayment1Component } from "./additional-payment1/additional-payment1.component";
import { AdditionalPayment2Component } from "./additional-payment2/additional-payment2.component";
import { AddiionalPaymentDetailsComponent } from "./addiional-payment-details/addiional-payment-details.component";
import { AdditionlPaymentsListComponent } from "./additionl-payments-list/additionl-payments-list.component";
import { AdditionalPaymentEditComponent } from "./additional-payment-edit/additional-payment-edit.component";
import { Repair1Component } from "./repair-1/repair-1.component";
import { Repair2Component } from "./repair-2/repair-2.component";



const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: HomeComponent },
    { path: "refueling-edit", component: RefuelingEditComponent },
    { path: "navigation", component: NavigationComponent },
    { path: "navpage", component: NavpageComponent },
    { path: "car-add", component: CarAddComponent },
    { path: "technical-card-1", component: TechnicalCard1Component },
    { path: "technical-card-2", component: TechnicalCard2Component },
    { path: "refuelling-add", component: RefuellingAddComponent },
    { path: "refueling-list", component: RefuelingListComponent },
    { path: "additional-payment1", component: AdditionalPayment1Component },
    { path: "additional-payment2", component: AdditionalPayment2Component },
    { path: "additional-payment-details", component: AddiionalPaymentDetailsComponent },
    { path: "addittional-payments-list", component: AdditionlPaymentsListComponent},
    { path: "additional -payments-edit", component: AdditionalPaymentEditComponent},
    { path: "repair-1", component: Repair1Component},
    { path: "repair-2", component: Repair2Component}


];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }