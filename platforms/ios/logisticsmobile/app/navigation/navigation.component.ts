import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { RefuelingService } from "../services/refueling/refueling.service";
import { ListService } from '../services/list/list.service';


@Component({
	moduleId: module.id,
	selector: 'app-navigation',
	templateUrl: './navigation.component.html',
	styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

	public data;
	constructor( private page: Page, private refuelingService: RefuelingService, private listService: ListService ) { }

	ngOnInit() {
		this.data = this.refuelingService.refuelingJSONforPOST;
		this.page.actionBarHidden = true;
	
		
	 }



}
