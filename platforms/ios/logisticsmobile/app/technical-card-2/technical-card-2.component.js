"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var technical_card_service_1 = require("../services/technical-card/technical-card.service");
var page_1 = require("ui/page");
var TechnicalCard2Component = (function () {
    function TechnicalCard2Component(technicalCardService, page) {
        this.technicalCardService = technicalCardService;
        this.page = page;
    }
    TechnicalCard2Component.prototype.ngOnInit = function () {
        // this.page.actionBarHidden = true;
        this.technicalCardService.purity = "";
        this.technicalCardService.lighting = "";
        this.technicalCardService.suspension = "";
        this.technicalCardService.engineAndAccessories = "";
        this.technicalCardService.carBody = "";
        this.technicalCardService.composeOptions = null;
        this.technicalCardService.imagesPaths = [];
        this.technicalCardService.arrayOfAttachments = [];
    };
    TechnicalCard2Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-technical-card-2',
            templateUrl: './technical-card-2.component.html',
            styleUrls: ['./technical-card-2.component.scss']
        }),
        __metadata("design:paramtypes", [technical_card_service_1.TechnicalCardService, page_1.Page])
    ], TechnicalCard2Component);
    return TechnicalCard2Component;
}());
exports.TechnicalCard2Component = TechnicalCard2Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVjaG5pY2FsLWNhcmQtMi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0ZWNobmljYWwtY2FyZC0yLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCw0RkFBeUY7QUFDekYsZ0NBQStCO0FBVS9CO0lBRUMsaUNBQW9CLG9CQUF5QyxFQUFVLElBQVU7UUFBN0QseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFxQjtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07SUFBSyxDQUFDO0lBRXZGLDBDQUFRLEdBQVI7UUFDQyxvQ0FBb0M7UUFDcEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDdEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDeEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDMUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztRQUNwRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsY0FBYyxHQUFJLElBQUksQ0FBQztRQUNqRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUMzQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxDQUFDO0lBQ2xELENBQUM7SUFkVSx1QkFBdUI7UUFObkMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLFdBQVcsRUFBRSxtQ0FBbUM7WUFDaEQsU0FBUyxFQUFFLENBQUMsbUNBQW1DLENBQUM7U0FDaEQsQ0FBQzt5Q0FHd0MsNkNBQW9CLEVBQWdCLFdBQUk7T0FGckUsdUJBQXVCLENBZ0JuQztJQUFELDhCQUFDO0NBQUEsQUFoQkQsSUFnQkM7QUFoQlksMERBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFRlY2huaWNhbENhcmRTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3RlY2huaWNhbC1jYXJkL3RlY2huaWNhbC1jYXJkLnNlcnZpY2VcIjtcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0IHsgU2Nyb2xsRXZlbnREYXRhIH0gZnJvbSBcInVpL3Njcm9sbC12aWV3XCI7XG5cblxuQENvbXBvbmVudCh7XG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXG5cdHNlbGVjdG9yOiAnYXBwLXRlY2huaWNhbC1jYXJkLTInLFxuXHR0ZW1wbGF0ZVVybDogJy4vdGVjaG5pY2FsLWNhcmQtMi5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL3RlY2huaWNhbC1jYXJkLTIuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBUZWNobmljYWxDYXJkMkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSB0ZWNobmljYWxDYXJkU2VydmljZTpUZWNobmljYWxDYXJkU2VydmljZSwgcHJpdmF0ZSBwYWdlOiBQYWdlICkgeyB9XG5cblx0bmdPbkluaXQoKSB7XG5cdFx0Ly8gdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG5cdFx0dGhpcy50ZWNobmljYWxDYXJkU2VydmljZS5wdXJpdHkgPSBcIlwiO1xuXHRcdHRoaXMudGVjaG5pY2FsQ2FyZFNlcnZpY2UubGlnaHRpbmcgPSBcIlwiO1xuXHRcdHRoaXMudGVjaG5pY2FsQ2FyZFNlcnZpY2Uuc3VzcGVuc2lvbiA9IFwiXCI7XG5cdFx0dGhpcy50ZWNobmljYWxDYXJkU2VydmljZS5lbmdpbmVBbmRBY2Nlc3NvcmllcyA9IFwiXCI7XG5cdFx0dGhpcy50ZWNobmljYWxDYXJkU2VydmljZS5jYXJCb2R5ID0gXCJcIjtcblx0XHR0aGlzLnRlY2huaWNhbENhcmRTZXJ2aWNlLmNvbXBvc2VPcHRpb25zICA9IG51bGw7XG5cdFx0dGhpcy50ZWNobmljYWxDYXJkU2VydmljZS5pbWFnZXNQYXRocyA9IFtdO1xuXHRcdHRoaXMudGVjaG5pY2FsQ2FyZFNlcnZpY2UuYXJyYXlPZkF0dGFjaG1lbnRzID0gW107XG5cdCB9XG5cbn1cbiJdfQ==