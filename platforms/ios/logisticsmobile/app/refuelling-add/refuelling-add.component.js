"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var date_service_1 = require("../services/date/date.service");
var list_service_1 = require("../services/list/list.service");
var user_service_1 = require("../services/user/user.service");
var json_service_1 = require("../services/json/json.service");
var global_service_1 = require("../services/global/global.service");
var refueling_service_1 = require("../services/refueling/refueling.service");
var router_1 = require("nativescript-angular/router");
var RefuellingAddComponent = (function () {
    function RefuellingAddComponent(page, listService, dateService, userService, jsonService, globalService, refuelingService, routerExtensions) {
        this.page = page;
        this.listService = listService;
        this.dateService = dateService;
        this.userService = userService;
        this.jsonService = jsonService;
        this.globalService = globalService;
        this.refuelingService = refuelingService;
        this.routerExtensions = routerExtensions;
    }
    RefuellingAddComponent.prototype.ngOnInit = function () {
        this.jsonService.counter = null;
        this.jsonService.quantity = null;
        this.jsonService.price = null;
        this.jsonService.pickedPaymentType = null;
        this.jsonService.pickedCurrencyId = null;
        this.jsonService.adBluePrice = null;
        this.jsonService.adBlueQuantity = null;
    };
    RefuellingAddComponent.prototype.refuelingAdded = function () {
        var _this = this;
        this.refuelingService.refuelingJSONforPOST = {
            refuelingDate: this.dateService.dateFormatForPost(),
            counter: this.jsonService.counter,
            quantity: this.jsonService.quantity,
            price: this.jsonService.price,
            paymentType: this.jsonService.pickedPaymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.jsonService.pickedCarId
            },
            currency: {
                id: this.jsonService.pickedCurrencyId
            },
            warningNote: this.jsonService.warningNote,
            adBlueQuantity: this.jsonService.adBlueQuantity,
            adBluePrice: this.jsonService.adBluePrice
        };
        this.refuelingService.addRefueling(this.refuelingService.refuelingJSONforPOST).subscribe(function (res) {
            alert({
                title: "Potwierdzenie",
                message: "Tankowanie dodane poprawnie",
                okButtonText: "OK"
            }), _this.routerExtensions.navigate(["/navigation"]);
        }, function (err) {
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    RefuellingAddComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-refuelling-add',
            templateUrl: './refuelling-add.component.html',
            styleUrls: ['./refuelling-add.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, list_service_1.ListService, date_service_1.DateService, user_service_1.UserService, json_service_1.JsonService, global_service_1.GlobalService, refueling_service_1.RefuelingService, router_1.RouterExtensions])
    ], RefuellingAddComponent);
    return RefuellingAddComponent;
}());
exports.RefuellingAddComponent = RefuellingAddComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmdWVsbGluZy1hZGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVmdWVsbGluZy1hZGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELGdDQUErQjtBQUMvQiw4REFBNEQ7QUFDNUQsOERBQTREO0FBQzVELDhEQUE0RDtBQUM1RCw4REFBNEQ7QUFDNUQsb0VBQWtFO0FBQ2xFLDZFQUEyRTtBQUMzRSxzREFBK0Q7QUFVL0Q7SUFFQyxnQ0FBb0IsSUFBVSxFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSxXQUF3QixFQUFVLFdBQXdCLEVBQVUsYUFBNEIsRUFBVSxnQkFBa0MsRUFBVSxnQkFBa0M7UUFBaFIsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtJQUFJLENBQUM7SUFFelMseUNBQVEsR0FBUjtRQUNDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNoQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7SUFFOUMsQ0FBQztJQUVNLCtDQUFjLEdBQXJCO1FBQUEsaUJBbUNJO1FBbENHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsR0FBRztZQUN6QyxhQUFhLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRTtZQUNuRCxPQUFPLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPO1lBQ2pDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVE7WUFDbkMsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSztZQUM3QixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUI7WUFDL0MsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtZQUN2QyxHQUFHLEVBQUU7Z0JBQ0QsRUFBRSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVzthQUNuQztZQUNELFFBQVEsRUFBRTtnQkFDTixFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0I7YUFDeEM7WUFDRCxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXO1lBQ3pDLGNBQWMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWM7WUFDL0MsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVztTQUM1QyxDQUFBO1FBRUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxTQUFTLENBQ3BGLFVBQUMsR0FBRztZQUNBLEtBQUssQ0FBQztnQkFDRixLQUFLLEVBQUUsZUFBZTtnQkFDdEIsT0FBTyxFQUFFLDZCQUE2QjtnQkFDdEMsWUFBWSxFQUFFLElBQUk7YUFDckIsQ0FBQyxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFBO1FBQ3ZELENBQUMsRUFDRCxVQUFDLEdBQUc7WUFDQSxLQUFLLENBQUM7Z0JBQ0YsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLGdDQUFnQztnQkFDekMsWUFBWSxFQUFFLElBQUk7YUFDckIsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUNKLENBQUM7SUFDTixDQUFDO0lBbERRLHNCQUFzQjtRQU5sQyxnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxpQ0FBaUMsQ0FBQztTQUM5QyxDQUFDO3lDQUd5QixXQUFJLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQXlCLDhCQUFhLEVBQTRCLG9DQUFnQixFQUE0Qix5QkFBZ0I7T0FGeFIsc0JBQXNCLENBb0RsQztJQUFELDZCQUFDO0NBQUEsQUFwREQsSUFvREM7QUFwRFksd0RBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0IHsgRGF0ZVNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvZGF0ZS9kYXRlLnNlcnZpY2VcIjtcbmltcG9ydCB7IExpc3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbGlzdC9saXN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvdXNlci91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCB7IEpzb25TZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL2pzb24vanNvbi5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBHbG9iYWxTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL2dsb2JhbC9nbG9iYWwuc2VydmljZVwiO1xuaW1wb3J0IHsgUmVmdWVsaW5nU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy9yZWZ1ZWxpbmcvcmVmdWVsaW5nLnNlcnZpY2VcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5cblxuXG5AQ29tcG9uZW50KHtcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0c2VsZWN0b3I6ICdhcHAtcmVmdWVsbGluZy1hZGQnLFxuXHR0ZW1wbGF0ZVVybDogJy4vcmVmdWVsbGluZy1hZGQuY29tcG9uZW50Lmh0bWwnLFxuXHRzdHlsZVVybHM6IFsnLi9yZWZ1ZWxsaW5nLWFkZC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFJlZnVlbGxpbmdBZGRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSBsaXN0U2VydmljZTogTGlzdFNlcnZpY2UsIHByaXZhdGUgZGF0ZVNlcnZpY2U6IERhdGVTZXJ2aWNlLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJpdmF0ZSBqc29uU2VydmljZTogSnNvblNlcnZpY2UsIHByaXZhdGUgZ2xvYmFsU2VydmljZTogR2xvYmFsU2VydmljZSwgcHJpdmF0ZSByZWZ1ZWxpbmdTZXJ2aWNlOiBSZWZ1ZWxpbmdTZXJ2aWNlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpIHsgfVxuXG5cdG5nT25Jbml0KCkgeyBcblx0XHR0aGlzLmpzb25TZXJ2aWNlLmNvdW50ZXIgPSBudWxsO1xuXHRcdHRoaXMuanNvblNlcnZpY2UucXVhbnRpdHkgPSBudWxsO1xuXHRcdHRoaXMuanNvblNlcnZpY2UucHJpY2UgPSBudWxsO1xuXHRcdHRoaXMuanNvblNlcnZpY2UucGlja2VkUGF5bWVudFR5cGUgPSBudWxsO1xuICAgICAgICB0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZEN1cnJlbmN5SWQgPSBudWxsO1xuICAgICAgICB0aGlzLmpzb25TZXJ2aWNlLmFkQmx1ZVByaWNlID0gbnVsbDtcbiAgICAgICAgdGhpcy5qc29uU2VydmljZS5hZEJsdWVRdWFudGl0eSA9IG51bGw7XG5cblx0fVxuXG5cdHB1YmxpYyByZWZ1ZWxpbmdBZGRlZCgpIHtcbiAgICAgICAgdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mb3JQT1NUID0ge1xuICAgICAgICAgICAgcmVmdWVsaW5nRGF0ZTogdGhpcy5kYXRlU2VydmljZS5kYXRlRm9ybWF0Rm9yUG9zdCgpLFxuICAgICAgICAgICAgY291bnRlcjogdGhpcy5qc29uU2VydmljZS5jb3VudGVyLFxuICAgICAgICAgICAgcXVhbnRpdHk6IHRoaXMuanNvblNlcnZpY2UucXVhbnRpdHksXG4gICAgICAgICAgICBwcmljZTogdGhpcy5qc29uU2VydmljZS5wcmljZSxcbiAgICAgICAgICAgIHBheW1lbnRUeXBlOiB0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZFBheW1lbnRUeXBlLFxuICAgICAgICAgICAgZHJpdmVyTmFtZTogdGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lLFxuICAgICAgICAgICAgY2FyOiB7XG4gICAgICAgICAgICAgICAgaWQ6IHRoaXMuanNvblNlcnZpY2UucGlja2VkQ2FySWRcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjdXJyZW5jeToge1xuICAgICAgICAgICAgICAgIGlkOiB0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZEN1cnJlbmN5SWRcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB3YXJuaW5nTm90ZTogdGhpcy5qc29uU2VydmljZS53YXJuaW5nTm90ZSxcbiAgICAgICAgICAgIGFkQmx1ZVF1YW50aXR5OiB0aGlzLmpzb25TZXJ2aWNlLmFkQmx1ZVF1YW50aXR5LFxuICAgICAgICAgICAgYWRCbHVlUHJpY2U6IHRoaXMuanNvblNlcnZpY2UuYWRCbHVlUHJpY2VcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMucmVmdWVsaW5nU2VydmljZS5hZGRSZWZ1ZWxpbmcodGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mb3JQT1NUKS5zdWJzY3JpYmUoXG4gICAgICAgICAgICAocmVzKSA9PiB7XG4gICAgICAgICAgICAgICAgYWxlcnQoe1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJQb3R3aWVyZHplbmllXCIsXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IFwiVGFua293YW5pZSBkb2RhbmUgcG9wcmF3bmllXCIsXG4gICAgICAgICAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJPS1wiXG4gICAgICAgICAgICAgICAgfSksIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvbmF2aWdhdGlvblwiXSlcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgYWxlcnQoe1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJCxYLEhWRcIixcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogXCJQb3ByYXcgZGFuZSBpIHNwcsOzYnVqIHBvbm93bmllXCIsXG4gICAgICAgICAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJPS1wiXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0sXG4gICAgICAgICk7XG4gICAgfVxuXG59XG4iXX0=