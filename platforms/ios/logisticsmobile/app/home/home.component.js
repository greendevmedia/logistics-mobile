"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var login_service_1 = require("../services/login/login.service");
var global_service_1 = require("../services/global/global.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var application_settings_1 = require("application-settings");
var nativescript_ngx_fonticon_1 = require("nativescript-ngx-fonticon");
var HomeComponent = (function () {
    function HomeComponent(loginService, globalService, userService, routerExtensions, page, fonticon) {
        this.loginService = loginService;
        this.globalService = globalService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.fonticon = fonticon;
        this.isDriverPicked = false;
    }
    HomeComponent.prototype.ngOnInit = function () {
        console.log(application_settings_1.getString("username"));
        this.globalService.activityIndicator = false;
        this.page.actionBarHidden = true;
        this.globalService.loginFormVisibility = true;
        this.automaticLogin();
    };
    HomeComponent.prototype.selectedIndexChanged = function (args) {
        var picker = args.object;
        this.userService.driverName = this.globalService.driversList[picker.selectedIndex];
        this.isDriverPicked = true;
    };
    HomeComponent.prototype.login = function () {
        var _this = this;
        this.globalService.loginFormVisibility = false;
        this.globalService.activityIndicator = true;
        application_settings_1.setString("username", this.userService.driverName);
        application_settings_1.setString("pin", this.pin);
        this.body = this.body = "username=" + this.userService.driverName + '&password=' + this.pin + '&client_id='
            + this.globalService.client_id + '&client_secret=' + this.globalService.client_secret + '&grant_type=' + this.globalService.grant_type;
        this.loginService.login(this.body).subscribe(function (res) { _this.userService.userApiAccessData = res, _this.userService.isUserLogged = true, _this.routerExtensions.navigate(["/navpage"], { clearHistory: true }), application_settings_1.setString("username", _this.userService.driverName), application_settings_1.setString("pin", _this.pin), _this.pin = ""; }, function (err) {
            alert({
                title: "Błąd",
                message: "Wprowadź poprawny numer PIN do wybranego kierowcy",
                okButtonText: "OK"
            }),
                console.log(err);
            _this.userService.userApiAccessData = null;
            _this.userService.isUserLogged = false;
            _this.globalService.activityIndicator = false;
            _this.globalService.loginFormVisibility = true;
        });
    };
    HomeComponent.prototype.automaticLogin = function () {
        if (application_settings_1.getString("username") !== undefined && application_settings_1.getString("pin") !== undefined) {
            this.userService.driverName = application_settings_1.getString("username");
            this.pin = application_settings_1.getString("pin");
            console.log(this.userService.driverName);
            console.log(this.pin);
            this.login();
        }
    };
    HomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-home',
            templateUrl: './home.component.html',
            styleUrls: ['./home.component.scss']
        }),
        __metadata("design:paramtypes", [login_service_1.LoginService, global_service_1.GlobalService, user_service_1.UserService,
            router_1.RouterExtensions, page_1.Page, nativescript_ngx_fonticon_1.TNSFontIconService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUdsRCxpRUFBK0Q7QUFDL0Qsb0VBQWtFO0FBQ2xFLDhEQUE0RDtBQUM1RCxzREFBK0Q7QUFFL0QsZ0NBQStCO0FBQy9CLDZEQUc4QjtBQUM5Qix1RUFBK0Q7QUFTL0Q7SUFNQyx1QkFBb0IsWUFBMEIsRUFBVSxhQUE0QixFQUFVLFdBQXdCLEVBQzdHLGdCQUFrQyxFQUFVLElBQVUsRUFBVSxRQUE0QjtRQURqRixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDN0cscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFvQjtRQUg5RixtQkFBYyxHQUFZLEtBQUssQ0FBQztJQUl2QyxDQUFDO0lBRUQsZ0NBQVEsR0FBUjtRQUNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzdDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUM5QyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7SUFFdkIsQ0FBQztJQUVNLDRDQUFvQixHQUEzQixVQUE0QixJQUFJO1FBQy9CLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ25GLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFTSw2QkFBSyxHQUFaO1FBQUEsaUJBMEJDO1FBeEJBLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1FBQy9DLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBRTVDLGdDQUFTLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDbkQsZ0NBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTNCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsWUFBWSxHQUFHLElBQUksQ0FBQyxHQUFHLEdBQUcsYUFBYTtjQUN4RyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsR0FBRyxjQUFjLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUM7UUFFeEksSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FDM0MsVUFBQyxHQUFHLElBQU8sS0FBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsR0FBRyxHQUFHLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEdBQUcsSUFBSSxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsRUFBQyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFHLGdDQUFTLENBQUMsVUFBVSxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEVBQUUsZ0NBQVMsQ0FBQyxLQUFLLEVBQUUsS0FBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFBLENBQUEsQ0FBQyxFQUMvUCxVQUFDLEdBQUc7WUFDSCxLQUFLLENBQUM7Z0JBQ0wsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLG1EQUFtRDtnQkFDNUQsWUFBWSxFQUFFLElBQUk7YUFDbEIsQ0FBQztnQkFDRixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2pCLEtBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzFDLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUN0QyxLQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztZQUM3QyxLQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUMvQyxDQUFDLENBQ0QsQ0FBQTtJQUNGLENBQUM7SUFFTSxzQ0FBYyxHQUFyQjtRQUNDLEVBQUUsQ0FBQSxDQUFDLGdDQUFTLENBQUMsVUFBVSxDQUFDLEtBQUssU0FBUyxJQUFJLGdDQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUEsQ0FBQztZQUN6RSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxnQ0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BELElBQUksQ0FBQyxHQUFHLEdBQUcsZ0NBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2QsQ0FBQztJQUNGLENBQUM7SUE3RFcsYUFBYTtRQU56QixnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFdBQVcsRUFBRSx1QkFBdUI7WUFDcEMsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7U0FDcEMsQ0FBQzt5Q0FPaUMsNEJBQVksRUFBeUIsOEJBQWEsRUFBdUIsMEJBQVc7WUFDM0YseUJBQWdCLEVBQWdCLFdBQUksRUFBb0IsOENBQWtCO09BUHpGLGFBQWEsQ0FpRXpCO0lBQUQsb0JBQUM7Q0FBQSxBQWpFRCxJQWlFQztBQWpFWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgTGlzdFBpY2tlciB9IGZyb20gXCJ1aS9saXN0LXBpY2tlclwiO1xuaW1wb3J0IHsgTG9naW5TZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL2xvZ2luL2xvZ2luLnNlcnZpY2VcIjtcbmltcG9ydCB7IEdsb2JhbFNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvZ2xvYmFsL2dsb2JhbC5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy91c2VyL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1kcm9wLWRvd25cIjtcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0IHtcbiAgICBnZXRTdHJpbmcsXG4gICAgc2V0U3RyaW5nLCBcbn0gZnJvbSBcImFwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5pbXBvcnQgeyBUTlNGb250SWNvblNlcnZpY2UgfSBmcm9tICduYXRpdmVzY3JpcHQtbmd4LWZvbnRpY29uJztcblxuXG5AQ29tcG9uZW50KHtcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0c2VsZWN0b3I6ICdhcHAtaG9tZScsXG5cdHRlbXBsYXRlVXJsOiAnLi9ob21lLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vaG9tZS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG5cdHB1YmxpYyBwaW47XG5cdHB1YmxpYyBib2R5O1xuXHRwdWJsaWMgaXNEcml2ZXJQaWNrZWQ6IGJvb2xlYW4gPSBmYWxzZTtcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGxvZ2luU2VydmljZTogTG9naW5TZXJ2aWNlLCBwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLFxuXHRcdHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucywgcHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIGZvbnRpY29uOiBUTlNGb250SWNvblNlcnZpY2UpIHtcblx0fVxuXG5cdG5nT25Jbml0KCk6IHZvaWQge1xuXHRcdGNvbnNvbGUubG9nKGdldFN0cmluZyhcInVzZXJuYW1lXCIpKTtcblx0XHR0aGlzLmdsb2JhbFNlcnZpY2UuYWN0aXZpdHlJbmRpY2F0b3IgPSBmYWxzZTtcblx0XHR0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcblx0XHR0aGlzLmdsb2JhbFNlcnZpY2UubG9naW5Gb3JtVmlzaWJpbGl0eSA9IHRydWU7XG5cdFx0dGhpcy5hdXRvbWF0aWNMb2dpbigpO1xuXHRcdFxuXHR9XG5cblx0cHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkKGFyZ3MpIHtcblx0XHRsZXQgcGlja2VyID0gPExpc3RQaWNrZXI+YXJncy5vYmplY3Q7XG5cdFx0dGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lID0gdGhpcy5nbG9iYWxTZXJ2aWNlLmRyaXZlcnNMaXN0W3BpY2tlci5zZWxlY3RlZEluZGV4XTtcblx0XHR0aGlzLmlzRHJpdmVyUGlja2VkID0gdHJ1ZTtcblx0fVxuXG5cdHB1YmxpYyBsb2dpbigpIHtcblx0XHRcblx0XHR0aGlzLmdsb2JhbFNlcnZpY2UubG9naW5Gb3JtVmlzaWJpbGl0eSA9IGZhbHNlO1xuXHRcdHRoaXMuZ2xvYmFsU2VydmljZS5hY3Rpdml0eUluZGljYXRvciA9IHRydWU7XG5cblx0XHRzZXRTdHJpbmcoXCJ1c2VybmFtZVwiLCB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUpO1xuXHRcdHNldFN0cmluZyhcInBpblwiLCB0aGlzLnBpbik7XG5cdFxuXHRcdHRoaXMuYm9keSA9IHRoaXMuYm9keSA9IGB1c2VybmFtZT1gICsgdGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lICsgJyZwYXNzd29yZD0nICsgdGhpcy5waW4gKyAnJmNsaWVudF9pZD0nXG5cdFx0XHQrIHRoaXMuZ2xvYmFsU2VydmljZS5jbGllbnRfaWQgKyAnJmNsaWVudF9zZWNyZXQ9JyArIHRoaXMuZ2xvYmFsU2VydmljZS5jbGllbnRfc2VjcmV0ICsgJyZncmFudF90eXBlPScgKyB0aGlzLmdsb2JhbFNlcnZpY2UuZ3JhbnRfdHlwZTtcblxuXHRcdHRoaXMubG9naW5TZXJ2aWNlLmxvZ2luKHRoaXMuYm9keSkuc3Vic2NyaWJlKFxuXHRcdFx0KHJlcykgPT4geyB0aGlzLnVzZXJTZXJ2aWNlLnVzZXJBcGlBY2Nlc3NEYXRhID0gcmVzLCB0aGlzLnVzZXJTZXJ2aWNlLmlzVXNlckxvZ2dlZCA9IHRydWUsIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvbmF2cGFnZVwiXSx7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KSwgIHNldFN0cmluZyhcInVzZXJuYW1lXCIsIHRoaXMudXNlclNlcnZpY2UuZHJpdmVyTmFtZSksIHNldFN0cmluZyhcInBpblwiLCB0aGlzLnBpbiksIHRoaXMucGluID0gXCJcIn0sXG5cdFx0XHQoZXJyKSA9PiB7XG5cdFx0XHRcdGFsZXJ0KHtcblx0XHRcdFx0XHR0aXRsZTogXCJCxYLEhWRcIixcblx0XHRcdFx0XHRtZXNzYWdlOiBcIldwcm93YWTFuiBwb3ByYXdueSBudW1lciBQSU4gZG8gd3licmFuZWdvIGtpZXJvd2N5XCIsXG5cdFx0XHRcdFx0b2tCdXR0b25UZXh0OiBcIk9LXCJcblx0XHRcdFx0fSksXG5cdFx0XHRcdGNvbnNvbGUubG9nKGVycik7XG5cdFx0XHRcdHRoaXMudXNlclNlcnZpY2UudXNlckFwaUFjY2Vzc0RhdGEgPSBudWxsO1xuXHRcdFx0XHR0aGlzLnVzZXJTZXJ2aWNlLmlzVXNlckxvZ2dlZCA9IGZhbHNlO1xuXHRcdFx0XHR0aGlzLmdsb2JhbFNlcnZpY2UuYWN0aXZpdHlJbmRpY2F0b3IgPSBmYWxzZTtcblx0XHRcdFx0dGhpcy5nbG9iYWxTZXJ2aWNlLmxvZ2luRm9ybVZpc2liaWxpdHkgPSB0cnVlO1xuXHRcdFx0fVxuXHRcdClcblx0fVxuXG5cdHB1YmxpYyBhdXRvbWF0aWNMb2dpbigpe1xuXHRcdGlmKGdldFN0cmluZyhcInVzZXJuYW1lXCIpICE9PSB1bmRlZmluZWQgJiYgZ2V0U3RyaW5nKFwicGluXCIpICE9PSB1bmRlZmluZWQpe1xuXHRcdFx0dGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lID0gZ2V0U3RyaW5nKFwidXNlcm5hbWVcIik7XG5cdFx0XHR0aGlzLnBpbiA9IGdldFN0cmluZyhcInBpblwiKTtcblx0XHRcdGNvbnNvbGUubG9nKHRoaXMudXNlclNlcnZpY2UuZHJpdmVyTmFtZSk7XG5cdFx0XHRjb25zb2xlLmxvZyh0aGlzLnBpbik7XG5cdFx0XHR0aGlzLmxvZ2luKCk7XG5cdFx0fVxuXHR9XG5cblxuXG59XG4iXX0=