"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var shipment_service_1 = require("../services/shipment/shipment.service");
var date_service_1 = require("../services/date/date.service");
var ShipmentComponent = (function () {
    function ShipmentComponent(shipmentService, dateService) {
        this.shipmentService = shipmentService;
        this.dateService = dateService;
        this.shipment = [];
        this.isUnload = null;
        this.isDeliverd = false;
        this.firstSwitchState = "Potwierdzam doręczenie przesyłki";
    }
    ShipmentComponent.prototype.ngOnInit = function () {
    };
    ShipmentComponent.prototype.getShipmentForDriverByNumber = function (number) {
        var _this = this;
        this.shipmentService.getShipmentForDriverByNumber(number).subscribe(function (res) {
            _this.shipment = res.json(), console.log(_this.shipment[0].loadingDate), _this.isUnload = _this.shipmentUnloadingDateChecker(_this.shipment[0]);
        }, function (err) {
            _this.isUnload = null;
            _this.infoAboutShipment = null;
            alert({
                title: "Brak przesyłki o podanym numerze",
                message: "Popraw numer i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    ShipmentComponent.prototype.shipmentUnloadingDateChecker = function (shipment) {
        var isUnload;
        if (shipment.unloadingDate !== null && shipment.loadingDate !== null) {
            isUnload = true;
        }
        else if (shipment.loadingDate != null && shipment.unloadingDate == null) {
            isUnload = false;
        }
        else {
            this.infoAboutShipment = "Przesyłka czeka na załadunek";
            isUnload = null;
        }
        return isUnload;
    };
    ShipmentComponent.prototype.onFirstChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.isDeliverd = true;
            this.firstSwitchState = "POTWIERDZAM doręczenie/rozładunek przesyłki dnia";
        }
        else {
            this.isDeliverd = false;
            this.firstSwitchState = "NIE POTWIERDZAM doręczenie/rozładunku przesyłki dnia";
        }
    };
    ShipmentComponent.prototype.updateUnloadingDate = function (shipmentWithUnloadingDate) {
        var _this = this;
        shipmentWithUnloadingDate.unloadingDate = this.dateService.dateFormatForPost();
        console.log(shipmentWithUnloadingDate);
        this.shipmentService.updateUnloadingDate(shipmentWithUnloadingDate).subscribe(function (res) {
            console.log(res), _this.isUnload = null;
            _this.infoAboutShipment = null;
            alert({
                title: "Potwierdzenie dla paczki o numerze " + shipmentWithUnloadingDate.shipmentNumber,
                message: "Paczka doręczona dnia " + shipmentWithUnloadingDate.unloadingDate,
                okButtonText: "OK"
            });
        }, function (err) {
            console.log(err);
        });
    };
    ShipmentComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-shipment',
            templateUrl: './shipment.component.html',
            styleUrls: ['./shipment.component.scss']
        }),
        __metadata("design:paramtypes", [shipment_service_1.ShipmentService, date_service_1.DateService])
    ], ShipmentComponent);
    return ShipmentComponent;
}());
exports.ShipmentComponent = ShipmentComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hpcG1lbnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2hpcG1lbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELDBFQUF3RTtBQUN4RSw4REFBNEQ7QUFVNUQ7SUFVRSwyQkFBb0IsZUFBZ0MsRUFBVSxXQUF3QjtRQUFsRSxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVA5RSxhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2QsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLHFCQUFnQixHQUFHLGtDQUFrQyxDQUFDO0lBSTRCLENBQUM7SUFFM0Ysb0NBQVEsR0FBUjtJQUNBLENBQUM7SUFFTSx3REFBNEIsR0FBbkMsVUFBb0MsTUFBTTtRQUExQyxpQkFhQztRQVpDLElBQUksQ0FBQyxlQUFlLENBQUMsNEJBQTRCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUNqRSxVQUFDLEdBQUc7WUFDRixLQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEVBQUUsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsNEJBQTRCLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdJLENBQUMsRUFDRCxVQUFDLEdBQUc7WUFDRixLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUFDLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDcEQsS0FBSyxDQUFDO2dCQUNKLEtBQUssRUFBRSxrQ0FBa0M7Z0JBQ3pDLE9BQU8sRUFBRSxpQ0FBaUM7Z0JBQzFDLFlBQVksRUFBRSxJQUFJO2FBQ25CLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVNLHdEQUE0QixHQUFuQyxVQUFvQyxRQUFRO1FBQzFDLElBQUksUUFBUSxDQUFDO1FBQ2IsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLGFBQWEsS0FBSyxJQUFJLElBQUksUUFBUSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDbEIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxJQUFJLElBQUksSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDekUsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUNuQixDQUFDO1FBQUEsSUFBSSxDQUFBLENBQUM7WUFDSixJQUFJLENBQUMsaUJBQWlCLEdBQUcsOEJBQThCLENBQUM7WUFDeEQsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNsQixDQUFDO1FBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQztJQUNsQixDQUFDO0lBRU0sMENBQWMsR0FBckIsVUFBc0IsSUFBSTtRQUN4QixJQUFJLFdBQVcsR0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3RDLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxrREFBa0QsQ0FBQztRQUM3RSxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUN4QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsc0RBQXNELENBQUM7UUFDakYsQ0FBQztJQUNILENBQUM7SUFHTSwrQ0FBbUIsR0FBMUIsVUFBMkIseUJBQXlCO1FBQXBELGlCQWdCQztRQWZDLHlCQUF5QixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDL0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMseUJBQXlCLENBQUMsQ0FBQyxTQUFTLENBQzNFLFVBQUMsR0FBRztZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFBQyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQ3RFLEtBQUssQ0FBQztnQkFDSixLQUFLLEVBQUUscUNBQXFDLEdBQUcseUJBQXlCLENBQUMsY0FBYztnQkFDdkYsT0FBTyxFQUFFLHdCQUF3QixHQUFHLHlCQUF5QixDQUFDLGFBQWE7Z0JBQzNFLFlBQVksRUFBRSxJQUFJO2FBQ25CLENBQUMsQ0FBQTtRQUNKLENBQUMsRUFDRCxVQUFDLEdBQUc7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ2xCLENBQUMsQ0FDRixDQUFBO0lBQ0gsQ0FBQztJQXZFVSxpQkFBaUI7UUFON0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsY0FBYztZQUN4QixXQUFXLEVBQUUsMkJBQTJCO1lBQ3hDLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO1NBQ3pDLENBQUM7eUNBV3FDLGtDQUFlLEVBQXVCLDBCQUFXO09BVjNFLGlCQUFpQixDQTBFN0I7SUFBRCx3QkFBQztDQUFBLEFBMUVELElBMEVDO0FBMUVZLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTaGlwbWVudFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9zaGlwbWVudC9zaGlwbWVudC5zZXJ2aWNlJztcbmltcG9ydCB7IERhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZGF0ZS9kYXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgU3dpdGNoIH0gZnJvbSBcInVpL3N3aXRjaFwiO1xuXG5cbkBDb21wb25lbnQoe1xuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICBzZWxlY3RvcjogJ2FwcC1zaGlwbWVudCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9zaGlwbWVudC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3NoaXBtZW50LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgU2hpcG1lbnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHByaXZhdGUgbnVtYmVyO1xuICBwcml2YXRlIHNoaXBtZW50ID0gW107XG4gIHByaXZhdGUgaXNVbmxvYWQgPSBudWxsO1xuICBwcml2YXRlIGlzRGVsaXZlcmQgPSBmYWxzZTtcbiAgcHJpdmF0ZSBmaXJzdFN3aXRjaFN0YXRlID0gXCJQb3R3aWVyZHphbSBkb3LEmWN6ZW5pZSBwcnplc3nFgmtpXCI7XG4gIHByaXZhdGUgaW5mb0Fib3V0U2hpcG1lbnQ7XG5cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNoaXBtZW50U2VydmljZTogU2hpcG1lbnRTZXJ2aWNlLCBwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBwdWJsaWMgZ2V0U2hpcG1lbnRGb3JEcml2ZXJCeU51bWJlcihudW1iZXIpIHtcbiAgICB0aGlzLnNoaXBtZW50U2VydmljZS5nZXRTaGlwbWVudEZvckRyaXZlckJ5TnVtYmVyKG51bWJlcikuc3Vic2NyaWJlKFxuICAgICAgKHJlcykgPT4ge1xuICAgICAgICB0aGlzLnNoaXBtZW50ID0gcmVzLmpzb24oKSwgY29uc29sZS5sb2codGhpcy5zaGlwbWVudFswXS5sb2FkaW5nRGF0ZSksIHRoaXMuaXNVbmxvYWQgPSB0aGlzLnNoaXBtZW50VW5sb2FkaW5nRGF0ZUNoZWNrZXIodGhpcy5zaGlwbWVudFswXSk7XG4gICAgICB9LFxuICAgICAgKGVycikgPT4ge1xuICAgICAgICB0aGlzLmlzVW5sb2FkID0gbnVsbDsgdGhpcy5pbmZvQWJvdXRTaGlwbWVudCA9IG51bGw7XG4gICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICB0aXRsZTogXCJCcmFrIHByemVzecWCa2kgbyBwb2RhbnltIG51bWVyemVcIixcbiAgICAgICAgICBtZXNzYWdlOiBcIlBvcHJhdyBudW1lciBpIHNwcsOzYnVqIHBvbm93bmllXCIsXG4gICAgICAgICAgb2tCdXR0b25UZXh0OiBcIk9LXCJcbiAgICAgICAgfSlcbiAgICAgIH0pXG4gIH1cblxuICBwdWJsaWMgc2hpcG1lbnRVbmxvYWRpbmdEYXRlQ2hlY2tlcihzaGlwbWVudCkge1xuICAgIGxldCBpc1VubG9hZDtcbiAgICBpZiAoc2hpcG1lbnQudW5sb2FkaW5nRGF0ZSAhPT0gbnVsbCAmJiBzaGlwbWVudC5sb2FkaW5nRGF0ZSAhPT0gbnVsbCkge1xuICAgICAgaXNVbmxvYWQgPSB0cnVlO1xuICAgIH0gZWxzZSBpZiAoc2hpcG1lbnQubG9hZGluZ0RhdGUgIT0gbnVsbCAmJiBzaGlwbWVudC51bmxvYWRpbmdEYXRlID09IG51bGwpe1xuICAgICAgaXNVbmxvYWQgPSBmYWxzZTtcbiAgICB9ZWxzZXtcbiAgICAgIHRoaXMuaW5mb0Fib3V0U2hpcG1lbnQgPSBcIlByemVzecWCa2EgY3pla2EgbmEgemHFgmFkdW5la1wiO1xuICAgICAgaXNVbmxvYWQgPSBudWxsO1xuICAgIH1cbiAgICByZXR1cm4gaXNVbmxvYWQ7XG4gIH1cblxuICBwdWJsaWMgb25GaXJzdENoZWNrZWQoYXJncykge1xuICAgIGxldCBmaXJzdFN3aXRjaCA9IDxTd2l0Y2g+YXJncy5vYmplY3Q7XG4gICAgaWYgKGZpcnN0U3dpdGNoLmNoZWNrZWQpIHtcbiAgICAgIHRoaXMuaXNEZWxpdmVyZCA9IHRydWU7XG4gICAgICB0aGlzLmZpcnN0U3dpdGNoU3RhdGUgPSBcIlBPVFdJRVJEWkFNIGRvcsSZY3plbmllL3JvesWCYWR1bmVrIHByemVzecWCa2kgZG5pYVwiO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmlzRGVsaXZlcmQgPSBmYWxzZTtcbiAgICAgIHRoaXMuZmlyc3RTd2l0Y2hTdGF0ZSA9IFwiTklFIFBPVFdJRVJEWkFNIGRvcsSZY3plbmllL3JvesWCYWR1bmt1IHByemVzecWCa2kgZG5pYVwiO1xuICAgIH1cbiAgfVxuXG5cbiAgcHVibGljIHVwZGF0ZVVubG9hZGluZ0RhdGUoc2hpcG1lbnRXaXRoVW5sb2FkaW5nRGF0ZSkge1xuICAgIHNoaXBtZW50V2l0aFVubG9hZGluZ0RhdGUudW5sb2FkaW5nRGF0ZSA9IHRoaXMuZGF0ZVNlcnZpY2UuZGF0ZUZvcm1hdEZvclBvc3QoKTtcbiAgICBjb25zb2xlLmxvZyhzaGlwbWVudFdpdGhVbmxvYWRpbmdEYXRlKTtcbiAgICB0aGlzLnNoaXBtZW50U2VydmljZS51cGRhdGVVbmxvYWRpbmdEYXRlKHNoaXBtZW50V2l0aFVubG9hZGluZ0RhdGUpLnN1YnNjcmliZShcbiAgICAgIChyZXMpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2cocmVzKSwgdGhpcy5pc1VubG9hZCA9IG51bGw7IHRoaXMuaW5mb0Fib3V0U2hpcG1lbnQgPSBudWxsO1xuICAgICAgICBhbGVydCh7XG4gICAgICAgICAgdGl0bGU6IFwiUG90d2llcmR6ZW5pZSBkbGEgcGFjemtpIG8gbnVtZXJ6ZSBcIiArIHNoaXBtZW50V2l0aFVubG9hZGluZ0RhdGUuc2hpcG1lbnROdW1iZXIsXG4gICAgICAgICAgbWVzc2FnZTogXCJQYWN6a2EgZG9yxJljem9uYSBkbmlhIFwiICsgc2hpcG1lbnRXaXRoVW5sb2FkaW5nRGF0ZS51bmxvYWRpbmdEYXRlLFxuICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJPS1wiXG4gICAgICAgIH0pXG4gICAgICB9LFxuICAgICAgKGVycikgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhlcnIpXG4gICAgICB9XG4gICAgKVxuICB9XG5cblxufVxuIl19