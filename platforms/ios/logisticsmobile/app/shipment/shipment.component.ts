import { Component, OnInit } from '@angular/core';
import { ShipmentService } from '../services/shipment/shipment.service';
import { DateService } from '../services/date/date.service';
import { Switch } from "ui/switch";


@Component({
  moduleId: module.id,
  selector: 'app-shipment',
  templateUrl: './shipment.component.html',
  styleUrls: ['./shipment.component.scss']
})
export class ShipmentComponent implements OnInit {

  private number;
  private shipment = [];
  private isUnload = null;
  private isDeliverd = false;
  private firstSwitchState = "Potwierdzam doręczenie przesyłki";
  private infoAboutShipment;


  constructor(private shipmentService: ShipmentService, private dateService: DateService) { }

  ngOnInit() {
  }

  public getShipmentForDriverByNumber(number) {
    this.shipmentService.getShipmentForDriverByNumber(number).subscribe(
      (res) => {
        this.shipment = res.json(), console.log(this.shipment[0].loadingDate), this.isUnload = this.shipmentUnloadingDateChecker(this.shipment[0]);
      },
      (err) => {
        this.isUnload = null; this.infoAboutShipment = null;
        alert({
          title: "Brak przesyłki o podanym numerze",
          message: "Popraw numer i spróbuj ponownie",
          okButtonText: "OK"
        })
      })
  }

  public shipmentUnloadingDateChecker(shipment) {
    let isUnload;
    if (shipment.unloadingDate !== null && shipment.loadingDate !== null) {
      isUnload = true;
    } else if (shipment.loadingDate != null && shipment.unloadingDate == null){
      isUnload = false;
    }else{
      this.infoAboutShipment = "Przesyłka czeka na załadunek";
      isUnload = null;
    }
    return isUnload;
  }

  public onFirstChecked(args) {
    let firstSwitch = <Switch>args.object;
    if (firstSwitch.checked) {
      this.isDeliverd = true;
      this.firstSwitchState = "POTWIERDZAM doręczenie/rozładunek przesyłki dnia";
    } else {
      this.isDeliverd = false;
      this.firstSwitchState = "NIE POTWIERDZAM doręczenie/rozładunku przesyłki dnia";
    }
  }


  public updateUnloadingDate(shipmentWithUnloadingDate) {
    shipmentWithUnloadingDate.unloadingDate = this.dateService.dateFormatForPost();
    console.log(shipmentWithUnloadingDate);
    this.shipmentService.updateUnloadingDate(shipmentWithUnloadingDate).subscribe(
      (res) => {
        console.log(res), this.isUnload = null; this.infoAboutShipment = null;
        alert({
          title: "Potwierdzenie dla paczki o numerze " + shipmentWithUnloadingDate.shipmentNumber,
          message: "Paczka doręczona dnia " + shipmentWithUnloadingDate.unloadingDate,
          okButtonText: "OK"
        })
      },
      (err) => {
        console.log(err)
      }
    )
  }


}
