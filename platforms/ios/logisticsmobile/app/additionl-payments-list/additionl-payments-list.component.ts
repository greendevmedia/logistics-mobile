import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from '../services/user/user.service';
import { AdditionalPaymentService } from '../services/additional-payment/additional-payment.service';

@Component({
    moduleId: module.id,
    selector: 'app-additionl-payments-list',
    templateUrl: './additionl-payments-list.component.html',
    styleUrls: ['./additionl-payments-list.component.scss']
})
export class AdditionlPaymentsListComponent implements OnInit {

    constructor(private additionalPaymentService: AdditionalPaymentService, private routerExtensions: RouterExtensions, private userService: UserService, private page: Page) { }


    ngOnInit() {
        this.page.actionBarHidden = true;
        this.getAdditionalPayments();

    }

    public getAdditionalPayments() {
        this.additionalPaymentService.getAdditionalPayment().subscribe(
            // (res) => { this.additionalPaymentService.additionalPaymentJSONfromGET = res.json().content, this.additionalPaymentService.additionalPaymentJSONfromGET = this.additionalPaymentService.additionalPaymentJSONfromGET.reverse().filter(additionalPayment => additionalPayment.driverName === this.userService.driverName) },
            (res) => { this.additionalPaymentService.additionalPaymentJSONfromGET = res.json().content, this.additionalPaymentService.additionalPaymentJSONfromGET = this.additionalPaymentService.additionalPaymentJSONfromGET.filter(additionalPayment => additionalPayment.driverName === this.userService.driverName) },
            (err) => { alert("Błąd przy załadowaniu listy tankowań") }
        )
    }


    public getAdditionalPaymentById(id) {
        this.additionalPaymentService.getAdditionalPaymentById(id).subscribe(
            (res) => { this.additionalPaymentService.additionalPaymentJSONfromGETbyId = res.json(), this.routerExtensions.navigate(["/additional -payments-edit"], ) },
            (err) => { alert("Błąd przy załadowaniu tankowania do edycji") }
        )
    }

    public showAdditionalPaymentsDetailsDetails(additionalPayments) {
        alert({
            title: "Szczegóły tankowania",
            message: "Imię kierowcy: " + additionalPayments.driverName + "\nKwota łączna: " + additionalPayments.totalPrice + "\nRodzaj dodatkowej opłaty: " + additionalPayments.paymentName.paymentName + "\nTyp płatności: " + additionalPayments.paymentType + "\nWaluta: " + additionalPayments.currency.currencyCode + "\nAuto: " + additionalPayments.car.name,
            okButtonText: "OK"
        })
    }

}
