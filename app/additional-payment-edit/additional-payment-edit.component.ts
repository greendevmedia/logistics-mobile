import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { DateService } from '../services/date/date.service';
import { AdditionalPaymentService } from '../services/additional-payment/additional-payment.service';
import { GlobalService } from '../services/global/global.service';
import { ListService } from '../services/list/list.service';
import { ListPicker } from "ui/list-picker";
import { UserService } from '../services/user/user.service';
import { RouterExtensions } from "nativescript-angular/router";

@Component({
	moduleId: module.id,
	selector: 'app-additional-payment-edit',
	templateUrl: './additional-payment-edit.component.html',
	styleUrls: ['./additional-payment-edit.component.scss']
})
export class AdditionalPaymentEditComponent implements OnInit {

	public additionaPaymentId;
	public totalPrice;
	public paymentName;
	public paymentNameId;
	public paymentType;
	public carName;
	public carId;
	public correctPickedCarId;
	public currency;
	public currencyId;
	private isBusy;



	constructor(private page: Page, private listService: ListService, private dateService: DateService, private additionalPaymentService: AdditionalPaymentService, private globalService: GlobalService, private userService: UserService, private routerExtensions: RouterExtensions, ) { }

	ngOnInit() {
		this.isBusy = false;
		this.page.actionBarHidden = true;
		this.listService.getCurrencies();
		this.listService.getCars();
		this.listService.getAdditionalPaymentsNames();
		this.currency = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.currency.currencyCode;
		this.paymentType = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.paymentType;
		this.additionaPaymentId = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.id;
		this.totalPrice = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.totalPrice;
		console.log(this.totalPrice);
		this.paymentName = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.paymentName.paymentName;
		this.carName = this.additionalPaymentService.additionalPaymentJSONfromGETbyId.car.name;
	}

	public selectedIndexChangedOnPaymentTypes(args) {
		let picker = <ListPicker>args.object;
		this.paymentType = this.globalService.paymentTypesList[picker.selectedIndex];
	}

	public selectedIndexChangedOnCurrencies(args) {
		let picker = <ListPicker>args.object;
		this.currencyId = picker.selectedIndex + 1;
	}

	public selectedIndexChangedOnCars(args) {
		let picker = <ListPicker>args.object;
		this.carId = picker.selectedIndex + 1;
		this.correctPickedCarId = this.listService.carsListIndexes[this.carId - 1];
	}

	public updateAdditionalPayment() {
		this.isBusy = true;
		this.additionalPaymentService.additionalPaymentJSONforPOST = {
			id: this.additionaPaymentId,
			paymentDay: this.dateService.dateFormatForPost(),
			paymentName: {
				id: this.additionalPaymentService.pickedAdditionalpaymentNameId
			},
			totalPrice: this.totalPrice,
			paymentType: this.paymentType,
			driverName: this.userService.driverName,
			car: {
				id: this.correctPickedCarId
			},
			currency: {
				id: this.currencyId
			}

		}

		this.additionalPaymentService.updateAdditionalPayment(this.additionalPaymentService.additionalPaymentJSONforPOST).subscribe(
			(res) => {
			this.isBusy = false;
				alert({
					title: "Potwierdzenie",
					message: "Opłata zmieniona poprawnie",
					okButtonText: "OK"
				}), this.routerExtensions.navigate(["/additional-payment-details"])
			},
			(err) => {
			this.isBusy = false;
				alert({
					title: "Błąd",
					message: "Popraw dane i spróbuj ponownie",
					okButtonText: "OK"
				})
			},
		);
	}

}
