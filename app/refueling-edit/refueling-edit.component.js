"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var refueling_service_1 = require("../services/refueling/refueling.service");
var date_service_1 = require("../services/date/date.service");
var global_service_1 = require("../services/global/global.service");
var list_service_1 = require("../services/list/list.service");
var user_service_1 = require("../services/user/user.service");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var RefuelingEditComponent = (function () {
    function RefuelingEditComponent(refuelingService, dateService, globalService, listService, userService, routerExtensions, page) {
        this.refuelingService = refuelingService;
        this.dateService = dateService;
        this.globalService = globalService;
        this.listService = listService;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.isWarningNoteCollapsed = false;
        this.firstChecked = false;
        this.switchState = "Dodatkowe uwagi - NIE";
        this.isAllPicked = true;
        this.adBlueSwitchState = "AdBlue - NIE";
    }
    RefuelingEditComponent.prototype.ngOnInit = function () {
        this.isBusy = false;
        this.page.actionBarHidden = true;
        this.listService.getCars();
        this.listService.getCurrencies();
        this.refuelingID = this.refuelingService.refuelingJSONfromGETbyID.id;
        this.paymentType = this.refuelingService.refuelingJSONfromGETbyID.paymentType;
        this.counter = this.refuelingService.refuelingJSONfromGETbyID.counter;
        this.quantity = this.refuelingService.refuelingJSONfromGETbyID.quantity;
        this.price = this.refuelingService.refuelingJSONfromGETbyID.price;
        this.warningNote = this.refuelingService.refuelingJSONfromGETbyID.warningNote;
        this.currency = this.refuelingService.refuelingJSONfromGETbyID.currency.currencyCode;
        this.car = this.refuelingService.refuelingJSONfromGETbyID.car.name;
        this.firstChecked = this.firstSwitchCheck(this.switchState, this.warningNote);
        this.adBluePrice = this.refuelingService.refuelingJSONfromGETbyID.adBluePrice;
        this.adBlueQuantity = this.refuelingService.refuelingJSONfromGETbyID.adBlueQuantity;
        this.adBlueFirstCheck = this.firstSwitchCheckAdBlue(this.adBlueSwitchState, this.adBluePrice, this.adBlueQuantity);
    };
    RefuelingEditComponent.prototype.firstSwitchCheck = function (switchState, warningNote) {
        if (warningNote !== null) {
            switchState = "Dodatkowe uwagi - TAK";
            console.log(true);
            return true;
        }
        else {
            switchState = "Dodatkowe Uwagi - NIE";
            console.log(false);
            return false;
        }
    };
    RefuelingEditComponent.prototype.firstSwitchCheckAdBlue = function (switchState, adBluePrice, adBlueQuantity) {
        if (adBluePrice != 0 && adBlueQuantity != 0) {
            switchState = "AdBlue - TAK";
            console.log(true);
            return true;
        }
        else {
            switchState = "AdBlue - NIE";
            console.log(false);
            return false;
        }
    };
    RefuelingEditComponent.prototype.onFirstChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.switchState = "Dodatkowe uwagi - TAK";
            this.isWarningNoteCollapsed = true;
        }
        else {
            this.switchState = "Dodatkowe uwagi - NIE";
            this.isWarningNoteCollapsed = false;
        }
    };
    RefuelingEditComponent.prototype.onFirstCheckedAdBlue = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.adBlueSwitchState = "AdBlue - TAK";
            this.isAdBlueCollapsed = true;
        }
        else {
            this.adBlueSwitchState = "AdBlue - NIE";
            this.isAdBlueCollapsed = false;
        }
    };
    RefuelingEditComponent.prototype.selectedIndexChangedOnPaymentTypes = function (args) {
        var picker = args.object;
        this.paymentType = this.globalService.paymentTypesList[picker.selectedIndex];
        this.pickedPaymentId = picker.selectedIndex + 1;
        console.log(this.paymentType + " " + this.pickedPaymentId);
    };
    RefuelingEditComponent.prototype.selectedIndexChangedOnCurrencies = function (args) {
        var picker = args.object;
        this.currency = this.listService.currenciesList[picker.selectedIndex];
        this.pickedCurrencyId = picker.selectedIndex + 1;
        console.log(this.currency + " " + this.pickedCurrencyId);
    };
    RefuelingEditComponent.prototype.selectedIndexChangedOnCars = function (args) {
        var picker = args.object;
        this.car = this.listService.carsList[picker.selectedIndex];
        this.pickedCarId = picker.selectedIndex + 1;
        this.correctPickedCarId = this.listService.carsListIndexes[this.pickedCarId - 1];
        console.log(this.car + " " + this.pickedCarId);
    };
    RefuelingEditComponent.prototype.updateRefueling = function () {
        var _this = this;
        this.isBusy = true;
        this.refuelingService.refuelingJSONforPOST = {
            id: this.refuelingID,
            refuelingDate: this.dateService.dateFormatForPost(),
            counter: this.counter,
            quantity: this.quantity,
            price: this.price,
            paymentType: this.paymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.correctPickedCarId
            },
            currency: {
                id: this.pickedCurrencyId
            },
            warningNote: this.warningNote,
            adBlueQuantity: this.adBlueQuantity,
            adBluePrice: this.adBluePrice
        };
        this.refuelingService.updateRefueling(this.refuelingService.refuelingJSONforPOST).subscribe(function (res) {
            _this.isBusy = false;
            alert({
                title: "Potwierdzenie",
                message: "Tankowanie zmienione poprawnie",
                okButtonText: "OK"
            }), _this.routerExtensions.navigate(["/navigation"], { clearHistory: true });
        }, function (err) {
            _this.isBusy = false;
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    RefuelingEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-refueling-edit',
            templateUrl: './refueling-edit.component.html',
            styleUrls: ['./refueling-edit.component.scss']
        }),
        __metadata("design:paramtypes", [refueling_service_1.RefuelingService, date_service_1.DateService, global_service_1.GlobalService, list_service_1.ListService, user_service_1.UserService, router_1.RouterExtensions, page_1.Page])
    ], RefuelingEditComponent);
    return RefuelingEditComponent;
}());
exports.RefuelingEditComponent = RefuelingEditComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmdWVsaW5nLWVkaXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVmdWVsaW5nLWVkaXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELDZFQUEyRTtBQUUzRSw4REFBNEQ7QUFDNUQsb0VBQWtFO0FBQ2xFLDhEQUE0RDtBQUc1RCw4REFBNEQ7QUFDNUQsc0RBQStEO0FBQy9ELGdDQUErQjtBQVMvQjtJQTRCSSxnQ0FBb0IsZ0JBQWtDLEVBQVUsV0FBd0IsRUFBVSxhQUE0QixFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSxnQkFBa0MsRUFBVSxJQUFVO1FBQTlPLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBWjNQLDJCQUFzQixHQUFZLEtBQUssQ0FBQztRQUN4QyxpQkFBWSxHQUFZLEtBQUssQ0FBQztRQUM5QixnQkFBVyxHQUFHLHVCQUF1QixDQUFDO1FBQ3RDLGdCQUFXLEdBQVksSUFBSSxDQUFDO1FBSTVCLHNCQUFpQixHQUFHLGNBQWMsQ0FBQztJQUs0TixDQUFDO0lBRXZRLHlDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHdCQUF3QixDQUFDLEVBQUUsQ0FBQztRQUNyRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLENBQUM7UUFDOUUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHdCQUF3QixDQUFDLFFBQVEsQ0FBQztRQUN4RSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUM7UUFDbEUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDO1FBQzlFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7UUFDckYsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztRQUNuRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5RSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLENBQUM7UUFDOUUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUMsY0FBYyxDQUFDO1FBQ3BGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO0lBQ3RILENBQUM7SUFHTSxpREFBZ0IsR0FBdkIsVUFBd0IsV0FBVyxFQUFFLFdBQVc7UUFDNUMsRUFBRSxDQUFDLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdkIsV0FBVyxHQUFHLHVCQUF1QixDQUFDO1lBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNmLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLFdBQVcsR0FBRyx1QkFBdUIsQ0FBQztZQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25CLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakIsQ0FBQztJQUNMLENBQUM7SUFFTSx1REFBc0IsR0FBN0IsVUFBOEIsV0FBVyxFQUFFLFdBQVcsRUFBRSxjQUFjO1FBQ2xFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsSUFBRyxDQUFDLElBQUksY0FBYyxJQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEMsV0FBVyxHQUFHLGNBQWMsQ0FBQztZQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xCLE1BQU0sQ0FBQyxJQUFJLENBQUE7UUFDZixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixXQUFXLEdBQUcsY0FBYyxDQUFDO1lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkIsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO0lBQ0wsQ0FBQztJQUVNLCtDQUFjLEdBQXJCLFVBQXNCLElBQUk7UUFDdEIsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLHVCQUF1QixDQUFDO1lBQzNDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7UUFDdkMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLFdBQVcsR0FBRyx1QkFBdUIsQ0FBQztZQUMzQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1FBQ3hDLENBQUM7SUFDTCxDQUFDO0lBRU0scURBQW9CLEdBQTNCLFVBQTRCLElBQUk7UUFDNUIsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsY0FBYyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDbEMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLGlCQUFpQixHQUFHLGNBQWMsQ0FBQztZQUN4QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQ25DLENBQUM7SUFDTCxDQUFDO0lBRU0sbUVBQWtDLEdBQXpDLFVBQTBDLElBQUk7UUFDMUMsSUFBSSxNQUFNLEdBQWUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzdFLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7SUFDOUQsQ0FBQztJQUVNLGlFQUFnQyxHQUF2QyxVQUF3QyxJQUFJO1FBQ3hDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVNLDJEQUEwQixHQUFqQyxVQUFrQyxJQUFJO1FBQ2xDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtJQUNsRCxDQUFDO0lBRU0sZ0RBQWUsR0FBdEI7UUFBQSxpQkFxQ0M7UUFwQ0csSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLG9CQUFvQixHQUFHO1lBQ3pDLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVztZQUNwQixhQUFhLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRTtZQUNuRCxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7WUFDN0IsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtZQUN2QyxHQUFHLEVBQUU7Z0JBQ0QsRUFBRSxFQUFFLElBQUksQ0FBQyxrQkFBa0I7YUFDOUI7WUFDRCxRQUFRLEVBQUU7Z0JBQ04sRUFBRSxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7YUFDNUI7WUFDRCxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7WUFDN0IsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjO1lBQ25DLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVztTQUNoQyxDQUFBO1FBRUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxTQUFTLENBQ3ZGLFVBQUMsR0FBRztZQUFNLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQzFCLEtBQUssQ0FBQztnQkFDRixLQUFLLEVBQUUsZUFBZTtnQkFDdEIsT0FBTyxFQUFFLGdDQUFnQztnQkFDekMsWUFBWSxFQUFFLElBQUk7YUFDckIsQ0FBQyxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFBO1FBQy9FLENBQUMsRUFDRCxVQUFDLEdBQUc7WUFBTSxLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUMxQixLQUFLLENBQUM7Z0JBQ0YsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUFFLGdDQUFnQztnQkFDekMsWUFBWSxFQUFFLElBQUk7YUFDckIsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUNKLENBQUM7SUFDTixDQUFDO0lBM0pRLHNCQUFzQjtRQU5sQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxpQ0FBaUMsQ0FBQztTQUNqRCxDQUFDO3lDQTZCd0Msb0NBQWdCLEVBQXVCLDBCQUFXLEVBQXlCLDhCQUFhLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXLEVBQTRCLHlCQUFnQixFQUFnQixXQUFJO09BNUJ6UCxzQkFBc0IsQ0E2SmxDO0lBQUQsNkJBQUM7Q0FBQSxBQTdKRCxJQTZKQztBQTdKWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUmVmdWVsaW5nU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy9yZWZ1ZWxpbmcvcmVmdWVsaW5nLnNlcnZpY2VcIjtcbmltcG9ydCB7IERhdGVQaWNrZXIgfSBmcm9tIFwidWkvZGF0ZS1waWNrZXJcIjtcbmltcG9ydCB7IERhdGVTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL2RhdGUvZGF0ZS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBHbG9iYWxTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL2dsb2JhbC9nbG9iYWwuc2VydmljZVwiO1xuaW1wb3J0IHsgTGlzdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9saXN0L2xpc3Quc2VydmljZSc7XG5pbXBvcnQgeyBTd2l0Y2ggfSBmcm9tIFwidWkvc3dpdGNoXCI7XG5pbXBvcnQgeyBMaXN0UGlja2VyIH0gZnJvbSBcInVpL2xpc3QtcGlja2VyXCI7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3VzZXIvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcblxuXG5AQ29tcG9uZW50KHtcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHNlbGVjdG9yOiAnYXBwLXJlZnVlbGluZy1lZGl0JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vcmVmdWVsaW5nLWVkaXQuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL3JlZnVlbGluZy1lZGl0LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUmVmdWVsaW5nRWRpdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBwdWJsaWMgcmVmdWVsaW5nSlNPTmZvclVwZGF0ZTtcbiAgICBwdWJsaWMgcmVmdWVsaW5nSUQ7XG4gICAgcHVibGljIGNvdW50ZXI6IG51bWJlcjtcbiAgICBwdWJsaWMgcXVhbnRpdHk6IG51bWJlcjtcbiAgICBwdWJsaWMgcHJpY2U6IG51bWJlcjtcbiAgICBwdWJsaWMgcGF5bWVudFR5cGU6IHN0cmluZztcbiAgICBwdWJsaWMgcGlja2VkUGF5bWVudElkO1xuICAgIHB1YmxpYyBjYXI6IHN0cmluZztcbiAgICBwdWJsaWMgcGlja2VkQ2FySWQ7XG4gICAgcHVibGljIGNvcnJlY3RQaWNrZWRDYXJJZFxuICAgIHB1YmxpYyBjdXJyZW5jeTogc3RyaW5nO1xuICAgIHB1YmxpYyBwaWNrZWRDdXJyZW5jeUlkO1xuICAgIHB1YmxpYyByZWZ1ZWxpbmdKU09OZm9yVVBEQVRFO1xuICAgIHB1YmxpYyB3YXJuaW5nTm90ZTogc3RyaW5nO1xuICAgIHB1YmxpYyBpc1dhcm5pbmdOb3RlQ29sbGFwc2VkOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHVibGljIGZpcnN0Q2hlY2tlZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHB1YmxpYyBzd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gTklFXCI7XG4gICAgcHVibGljIGlzQWxsUGlja2VkOiBib29sZWFuID0gdHJ1ZTtcbiAgICBwdWJsaWMgYWRCbHVlUXVhbnRpdHk7XG4gICAgcHVibGljIGFkQmx1ZVByaWNlO1xuICAgIHB1YmxpYyBhZEJsdWVGaXJzdENoZWNrO1xuICAgIHB1YmxpYyBhZEJsdWVTd2l0Y2hTdGF0ZSA9IFwiQWRCbHVlIC0gTklFXCI7XG4gICAgcHVibGljIGlzQWRCbHVlQ29sbGFwc2VkO1xuICAgIHByaXZhdGUgaXNCdXN5O1xuXG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJlZnVlbGluZ1NlcnZpY2U6IFJlZnVlbGluZ1NlcnZpY2UsIHByaXZhdGUgZGF0ZVNlcnZpY2U6IERhdGVTZXJ2aWNlLCBwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgbGlzdFNlcnZpY2U6IExpc3RTZXJ2aWNlLCBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHBhZ2U6IFBhZ2UpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuaXNCdXN5ID0gZmFsc2U7XG4gICAgICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xuICAgICAgICB0aGlzLmxpc3RTZXJ2aWNlLmdldENhcnMoKTtcbiAgICAgICAgdGhpcy5saXN0U2VydmljZS5nZXRDdXJyZW5jaWVzKCk7XG4gICAgICAgIHRoaXMucmVmdWVsaW5nSUQgPSB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZyb21HRVRieUlELmlkO1xuICAgICAgICB0aGlzLnBheW1lbnRUeXBlID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5wYXltZW50VHlwZTtcbiAgICAgICAgdGhpcy5jb3VudGVyID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5jb3VudGVyO1xuICAgICAgICB0aGlzLnF1YW50aXR5ID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5xdWFudGl0eTtcbiAgICAgICAgdGhpcy5wcmljZSA9IHRoaXMucmVmdWVsaW5nU2VydmljZS5yZWZ1ZWxpbmdKU09OZnJvbUdFVGJ5SUQucHJpY2U7XG4gICAgICAgIHRoaXMud2FybmluZ05vdGUgPSB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZyb21HRVRieUlELndhcm5pbmdOb3RlO1xuICAgICAgICB0aGlzLmN1cnJlbmN5ID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5jdXJyZW5jeS5jdXJyZW5jeUNvZGU7XG4gICAgICAgIHRoaXMuY2FyID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5jYXIubmFtZTtcbiAgICAgICAgdGhpcy5maXJzdENoZWNrZWQgPSB0aGlzLmZpcnN0U3dpdGNoQ2hlY2sodGhpcy5zd2l0Y2hTdGF0ZSwgdGhpcy53YXJuaW5nTm90ZSk7XG4gICAgICAgIHRoaXMuYWRCbHVlUHJpY2UgPSB0aGlzLnJlZnVlbGluZ1NlcnZpY2UucmVmdWVsaW5nSlNPTmZyb21HRVRieUlELmFkQmx1ZVByaWNlO1xuICAgICAgICB0aGlzLmFkQmx1ZVF1YW50aXR5ID0gdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mcm9tR0VUYnlJRC5hZEJsdWVRdWFudGl0eTtcbiAgICAgICAgdGhpcy5hZEJsdWVGaXJzdENoZWNrID0gdGhpcy5maXJzdFN3aXRjaENoZWNrQWRCbHVlKHRoaXMuYWRCbHVlU3dpdGNoU3RhdGUsIHRoaXMuYWRCbHVlUHJpY2UsIHRoaXMuYWRCbHVlUXVhbnRpdHkpXG4gICAgfVxuXG5cbiAgICBwdWJsaWMgZmlyc3RTd2l0Y2hDaGVjayhzd2l0Y2hTdGF0ZSwgd2FybmluZ05vdGUpIHtcbiAgICAgICAgaWYgKHdhcm5pbmdOb3RlICE9PSBudWxsKSB7XG4gICAgICAgICAgICBzd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gVEFLXCI7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0cnVlKTtcbiAgICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBzd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIFV3YWdpIC0gTklFXCI7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhmYWxzZSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgZmlyc3RTd2l0Y2hDaGVja0FkQmx1ZShzd2l0Y2hTdGF0ZSwgYWRCbHVlUHJpY2UsIGFkQmx1ZVF1YW50aXR5KSB7XG4gICAgICAgIGlmIChhZEJsdWVQcmljZSAhPTAgJiYgYWRCbHVlUXVhbnRpdHkgIT0wKSB7XG4gICAgICAgICAgICBzd2l0Y2hTdGF0ZSA9IFwiQWRCbHVlIC0gVEFLXCI7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0cnVlKTtcbiAgICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBzd2l0Y2hTdGF0ZSA9IFwiQWRCbHVlIC0gTklFXCI7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhmYWxzZSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgb25GaXJzdENoZWNrZWQoYXJncykge1xuICAgICAgICBsZXQgZmlyc3RTd2l0Y2ggPSA8U3dpdGNoPmFyZ3Mub2JqZWN0O1xuICAgICAgICBpZiAoZmlyc3RTd2l0Y2guY2hlY2tlZCkge1xuICAgICAgICAgICAgdGhpcy5zd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gVEFLXCI7XG4gICAgICAgICAgICB0aGlzLmlzV2FybmluZ05vdGVDb2xsYXBzZWQgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zd2l0Y2hTdGF0ZSA9IFwiRG9kYXRrb3dlIHV3YWdpIC0gTklFXCI7XG4gICAgICAgICAgICB0aGlzLmlzV2FybmluZ05vdGVDb2xsYXBzZWQgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBvbkZpcnN0Q2hlY2tlZEFkQmx1ZShhcmdzKSB7XG4gICAgICAgIGxldCBmaXJzdFN3aXRjaCA9IDxTd2l0Y2g+YXJncy5vYmplY3Q7XG4gICAgICAgIGlmIChmaXJzdFN3aXRjaC5jaGVja2VkKSB7XG4gICAgICAgICAgICB0aGlzLmFkQmx1ZVN3aXRjaFN0YXRlID0gXCJBZEJsdWUgLSBUQUtcIjtcbiAgICAgICAgICAgIHRoaXMuaXNBZEJsdWVDb2xsYXBzZWQgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5hZEJsdWVTd2l0Y2hTdGF0ZSA9IFwiQWRCbHVlIC0gTklFXCI7XG4gICAgICAgICAgICB0aGlzLmlzQWRCbHVlQ29sbGFwc2VkID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc2VsZWN0ZWRJbmRleENoYW5nZWRPblBheW1lbnRUeXBlcyhhcmdzKSB7XG4gICAgICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcbiAgICAgICAgdGhpcy5wYXltZW50VHlwZSA9IHRoaXMuZ2xvYmFsU2VydmljZS5wYXltZW50VHlwZXNMaXN0W3BpY2tlci5zZWxlY3RlZEluZGV4XTtcbiAgICAgICAgdGhpcy5waWNrZWRQYXltZW50SWQgPSBwaWNrZXIuc2VsZWN0ZWRJbmRleCArIDE7XG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMucGF5bWVudFR5cGUgKyBcIiBcIiArIHRoaXMucGlja2VkUGF5bWVudElkKVxuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RlZEluZGV4Q2hhbmdlZE9uQ3VycmVuY2llcyhhcmdzKSB7XG4gICAgICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcbiAgICAgICAgdGhpcy5jdXJyZW5jeSA9IHRoaXMubGlzdFNlcnZpY2UuY3VycmVuY2llc0xpc3RbcGlja2VyLnNlbGVjdGVkSW5kZXhdO1xuICAgICAgICB0aGlzLnBpY2tlZEN1cnJlbmN5SWQgPSBwaWNrZXIuc2VsZWN0ZWRJbmRleCArIDE7XG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuY3VycmVuY3kgKyBcIiBcIiArIHRoaXMucGlja2VkQ3VycmVuY3lJZCk7XG4gICAgfVxuXG4gICAgcHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkT25DYXJzKGFyZ3MpIHtcbiAgICAgICAgbGV0IHBpY2tlciA9IDxMaXN0UGlja2VyPmFyZ3Mub2JqZWN0O1xuICAgICAgICB0aGlzLmNhciA9IHRoaXMubGlzdFNlcnZpY2UuY2Fyc0xpc3RbcGlja2VyLnNlbGVjdGVkSW5kZXhdO1xuICAgICAgICB0aGlzLnBpY2tlZENhcklkID0gcGlja2VyLnNlbGVjdGVkSW5kZXggKyAxO1xuICAgICAgICB0aGlzLmNvcnJlY3RQaWNrZWRDYXJJZCA9IHRoaXMubGlzdFNlcnZpY2UuY2Fyc0xpc3RJbmRleGVzW3RoaXMucGlja2VkQ2FySWQtMV07XG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuY2FyICsgXCIgXCIgKyB0aGlzLnBpY2tlZENhcklkKVxuICAgIH1cblxuICAgIHB1YmxpYyB1cGRhdGVSZWZ1ZWxpbmcoKSB7XG4gICAgICAgIHRoaXMuaXNCdXN5ID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mb3JQT1NUID0ge1xuICAgICAgICAgICAgaWQ6IHRoaXMucmVmdWVsaW5nSUQsXG4gICAgICAgICAgICByZWZ1ZWxpbmdEYXRlOiB0aGlzLmRhdGVTZXJ2aWNlLmRhdGVGb3JtYXRGb3JQb3N0KCksXG4gICAgICAgICAgICBjb3VudGVyOiB0aGlzLmNvdW50ZXIsXG4gICAgICAgICAgICBxdWFudGl0eTogdGhpcy5xdWFudGl0eSxcbiAgICAgICAgICAgIHByaWNlOiB0aGlzLnByaWNlLFxuICAgICAgICAgICAgcGF5bWVudFR5cGU6IHRoaXMucGF5bWVudFR5cGUsXG4gICAgICAgICAgICBkcml2ZXJOYW1lOiB0aGlzLnVzZXJTZXJ2aWNlLmRyaXZlck5hbWUsXG4gICAgICAgICAgICBjYXI6IHtcbiAgICAgICAgICAgICAgICBpZDogdGhpcy5jb3JyZWN0UGlja2VkQ2FySWRcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjdXJyZW5jeToge1xuICAgICAgICAgICAgICAgIGlkOiB0aGlzLnBpY2tlZEN1cnJlbmN5SWRcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB3YXJuaW5nTm90ZTogdGhpcy53YXJuaW5nTm90ZSxcbiAgICAgICAgICAgIGFkQmx1ZVF1YW50aXR5OiB0aGlzLmFkQmx1ZVF1YW50aXR5LFxuICAgICAgICAgICAgYWRCbHVlUHJpY2U6IHRoaXMuYWRCbHVlUHJpY2VcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMucmVmdWVsaW5nU2VydmljZS51cGRhdGVSZWZ1ZWxpbmcodGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mb3JQT1NUKS5zdWJzY3JpYmUoXG4gICAgICAgICAgICAocmVzKSA9PiB7dGhpcy5pc0J1c3kgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBhbGVydCh7XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIlBvdHdpZXJkemVuaWVcIixcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogXCJUYW5rb3dhbmllIHptaWVuaW9uZSBwb3ByYXduaWVcIixcbiAgICAgICAgICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIk9LXCJcbiAgICAgICAgICAgICAgICB9KSwgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9uYXZpZ2F0aW9uXCJdLCB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIChlcnIpID0+IHt0aGlzLmlzQnVzeSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGFsZXJ0KHsgXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIkLFgsSFZFwiLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBcIlBvcHJhdyBkYW5lIGkgc3Byw7NidWogcG9ub3duaWVcIixcbiAgICAgICAgICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIk9LXCJcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSxcbiAgICAgICAgKTtcbiAgICB9XG5cbn1cbiJdfQ==