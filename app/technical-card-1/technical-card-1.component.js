"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var date_service_1 = require("../services/date/date.service");
var technical_card_service_1 = require("../services/technical-card/technical-card.service");
var page_1 = require("ui/page");
var TechnicalCard1Component = (function () {
    function TechnicalCard1Component(dateService, page, technicalCardService) {
        this.dateService = dateService;
        this.page = page;
        this.technicalCardService = technicalCardService;
    }
    TechnicalCard1Component.prototype.ngOnInit = function () {
        this.technicalCardService.counterStart = null;
        this.technicalCardService.counterStop = null;
    };
    TechnicalCard1Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-technical-card-1',
            templateUrl: './technical-card-1.component.html',
            styleUrls: ['./technical-card-1.component.scss']
        }),
        __metadata("design:paramtypes", [date_service_1.DateService, page_1.Page, technical_card_service_1.TechnicalCardService])
    ], TechnicalCard1Component);
    return TechnicalCard1Component;
}());
exports.TechnicalCard1Component = TechnicalCard1Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVjaG5pY2FsLWNhcmQtMS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0ZWNobmljYWwtY2FyZC0xLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCw4REFBNEQ7QUFDNUQsNEZBQXlGO0FBQ3pGLGdDQUErQjtBQVEvQjtJQUVDLGlDQUFvQixXQUF3QixFQUFVLElBQVUsRUFBVSxvQkFBeUM7UUFBL0YsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFxQjtJQUFJLENBQUM7SUFFeEgsMENBQVEsR0FBUjtRQUNDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQzlDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzdDLENBQUM7SUFQVSx1QkFBdUI7UUFObkMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLFdBQVcsRUFBRSxtQ0FBbUM7WUFDaEQsU0FBUyxFQUFFLENBQUMsbUNBQW1DLENBQUM7U0FDaEQsQ0FBQzt5Q0FHZ0MsMEJBQVcsRUFBZ0IsV0FBSSxFQUErQiw2Q0FBb0I7T0FGdkcsdUJBQXVCLENBU25DO0lBQUQsOEJBQUM7Q0FBQSxBQVRELElBU0M7QUFUWSwwREFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGF0ZVNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvZGF0ZS9kYXRlLnNlcnZpY2VcIjtcbmltcG9ydCB7IFRlY2huaWNhbENhcmRTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3RlY2huaWNhbC1jYXJkL3RlY2huaWNhbC1jYXJkLnNlcnZpY2VcIjtcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuXG5AQ29tcG9uZW50KHtcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0c2VsZWN0b3I6ICdhcHAtdGVjaG5pY2FsLWNhcmQtMScsXG5cdHRlbXBsYXRlVXJsOiAnLi90ZWNobmljYWwtY2FyZC0xLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vdGVjaG5pY2FsLWNhcmQtMS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFRlY2huaWNhbENhcmQxQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSwgcHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIHRlY2huaWNhbENhcmRTZXJ2aWNlOlRlY2huaWNhbENhcmRTZXJ2aWNlKSB7IH1cblxuXHRuZ09uSW5pdCgpIHtcblx0XHR0aGlzLnRlY2huaWNhbENhcmRTZXJ2aWNlLmNvdW50ZXJTdGFydCA9IG51bGw7XG5cdFx0dGhpcy50ZWNobmljYWxDYXJkU2VydmljZS5jb3VudGVyU3RvcCA9IG51bGw7XG5cdCB9XG5cbn1cbiJdfQ==