"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var shipment_service_1 = require("../services/shipment/shipment.service");
var date_service_1 = require("../services/date/date.service");
var user_service_1 = require("../services/user/user.service");
var application_settings_1 = require("tns-core-modules/application-settings/application-settings");
var ShipmentComponent = (function () {
    function ShipmentComponent(shipmentService, dateService, userService) {
        this.shipmentService = shipmentService;
        this.dateService = dateService;
        this.userService = userService;
        this.shipment = [];
        this.isUnload = null;
        this.isDeliverd = false;
        this.isPacked = null;
        this.firstSwitchState = "NIE POTWIERDZAM";
    }
    ShipmentComponent.prototype.ngOnInit = function () {
        this.isBusySendUnloading = false;
        this.isBusyCheck = false;
        this.isBusySend = false;
    };
    ShipmentComponent.prototype.getShipmentForDriverByNumber = function (number) {
        var _this = this;
        this.isBusyCheck = true;
        this.shipmentService.getShipmentForDriverByNumber(number).subscribe(function (res) {
            _this.isBusyCheck = false;
            _this.shipment = res.json(), _this.shipmentUnloadingDateChecker(_this.shipment[0]);
        }, function (err) {
            _this.isBusyCheck = false;
            _this.shipment = [], _this.isUnload = null;
            _this.infoAboutShipment = null;
            _this.isPacked = null;
            alert({
                title: "Brak przesyłki o podanym numerze",
                message: "Popraw numer i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    ShipmentComponent.prototype.shipmentUnloadingDateChecker = function (shipment) {
        if (shipment.unloadingDate !== null && shipment.loadingDate !== null) {
            this.isUnload = true;
        }
        else if (shipment.loadingDate != null && shipment.unloadingDate == null) {
            this.isUnload = false;
        }
        else {
            this.infoAboutShipment = "Przesyłka czeka na załadunek";
            this.isPacked = false;
            this.isUnload = null;
        }
    };
    ShipmentComponent.prototype.onFirstCheckedUnloading = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.isDeliverd = true;
            this.firstSwitchState = "POTWIERDZAM doręczenie/rozładunek przesyłki dnia";
        }
        else {
            this.isDeliverd = false;
            this.firstSwitchState = "NIE POTWIERDZAM doręczenie/rozładunku przesyłki dnia";
        }
    };
    ShipmentComponent.prototype.onFirstCheckedPacking = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            this.isDeliverd = true;
            this.firstSwitchState = "POTWIERDZAM załadunek przesyłki dnia";
        }
        else {
            this.isDeliverd = false;
            this.firstSwitchState = "NIE POTWIERDZAM załadunku przesyłki dnia";
        }
    };
    ShipmentComponent.prototype.updateUnloadingDate = function (shipmentWithUnloadingDate) {
        var _this = this;
        this.isBusySendUnloading = true;
        shipmentWithUnloadingDate.unloadingDate = this.dateService.dateFormatForPost();
        console.log(shipmentWithUnloadingDate);
        this.shipmentService.updateDate(shipmentWithUnloadingDate).subscribe(function (res) {
            _this.isBusySendUnloading = false;
            _this.isUnload = null;
            _this.infoAboutShipment = null;
            _this.isDeliverd = false;
            _this.firstSwitchState = "NIE POTWIERDZAM";
            alert({
                title: "Potwierdzenie dla paczki o numerze " + shipmentWithUnloadingDate.shipmentNumber,
                message: "Paczka doręczona/rozładowana dnia " + shipmentWithUnloadingDate.unloadingDate,
                okButtonText: "OK"
            });
        }, function (err) {
            _this.isBusySendUnloading = false;
            console.log(err);
        });
    };
    ShipmentComponent.prototype.updatePackingDate = function (shipmentWithpackingDate) {
        var _this = this;
        this.isBusySend = true;
        shipmentWithpackingDate.loadingDate = this.dateService.dateFormatForPost();
        if (this.driverChecker(shipmentWithpackingDate) === true) {
            shipmentWithpackingDate.drivers.push({ id: application_settings_1.getNumber("driverId") });
        }
        else {
            shipmentWithpackingDate.drivers = [{ id: application_settings_1.getNumber("driverId") }];
        }
        this.shipmentService.updateDate(shipmentWithpackingDate).subscribe(function (res) {
            _this.isBusySend = false;
            _this.isUnload = null;
            _this.infoAboutShipment = null;
            _this.isPacked = null;
            _this.isDeliverd = false;
            _this.firstSwitchState = "NIE POTWIERDZAM";
            alert({
                title: "Potwierdzenie dla paczki o numerze " + shipmentWithpackingDate.shipmentNumber,
                message: "Paczka załadowana dnia " + shipmentWithpackingDate.loadingDate,
                okButtonText: "OK"
            });
        }, function (err) {
            _this.isBusySend = false;
            console.log(err);
        });
    };
    ShipmentComponent.prototype.driverChecker = function (shipment) {
        if (shipment.drivers === undefined || shipment.drivers.length < 1) {
            return false;
        }
        else
            return true;
    };
    ShipmentComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-shipment',
            templateUrl: './shipment.component.html',
            styleUrls: ['./shipment.component.scss']
        }),
        __metadata("design:paramtypes", [shipment_service_1.ShipmentService, date_service_1.DateService, user_service_1.UserService])
    ], ShipmentComponent);
    return ShipmentComponent;
}());
exports.ShipmentComponent = ShipmentComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hpcG1lbnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2hpcG1lbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELDBFQUF3RTtBQUN4RSw4REFBNEQ7QUFFNUQsOERBQTREO0FBQzVELG1HQUFrRztBQVNsRztJQWdCRSwyQkFBb0IsZUFBZ0MsRUFBVSxXQUF3QixFQUFVLFdBQXdCO1FBQXBHLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFiaEgsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFDaEIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixhQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLHFCQUFnQixHQUFHLGlCQUFpQixDQUFDO0lBUytFLENBQUM7SUFFN0gsb0NBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQztJQUVNLHdEQUE0QixHQUFuQyxVQUFvQyxNQUFNO1FBQTFDLGlCQWNDO1FBYkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyw0QkFBNEIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQ2pFLFVBQUMsR0FBRztZQUFNLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1lBQ2pDLEtBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLElBQUksRUFBRSxFQUFFLEtBQUksQ0FBQyw0QkFBNEIsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEYsQ0FBQyxFQUNELFVBQUMsR0FBRztZQUFNLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1lBQ2pDLEtBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxFQUFFLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQUMsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztZQUFDLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQzlGLEtBQUssQ0FBQztnQkFDSixLQUFLLEVBQUUsa0NBQWtDO2dCQUN6QyxPQUFPLEVBQUUsaUNBQWlDO2dCQUMxQyxZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDLENBQUE7UUFDSixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFTSx3REFBNEIsR0FBbkMsVUFBb0MsUUFBUTtRQUUxQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsYUFBYSxLQUFLLElBQUksSUFBSSxRQUFRLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDdkIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxJQUFJLElBQUksSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDMUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDeEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLGlCQUFpQixHQUFHLDhCQUE4QixDQUFDO1lBQ3hELElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLENBQUM7SUFDSCxDQUFDO0lBRU0sbURBQXVCLEdBQTlCLFVBQStCLElBQUk7UUFDakMsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsa0RBQWtELENBQUM7UUFDN0UsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLHNEQUFzRCxDQUFDO1FBQ2pGLENBQUM7SUFDSCxDQUFDO0lBRU0saURBQXFCLEdBQTVCLFVBQTZCLElBQUk7UUFDL0IsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsc0NBQXNDLENBQUM7UUFDakUsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLDBDQUEwQyxDQUFDO1FBQ3JFLENBQUM7SUFDSCxDQUFDO0lBR00sK0NBQW1CLEdBQTFCLFVBQTJCLHlCQUF5QjtRQUFwRCxpQkFpQkM7UUFoQkMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUNoQyx5QkFBeUIsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQy9FLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUN2QyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLFNBQVMsQ0FDbEUsVUFBQyxHQUFHO1lBQU8sS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztZQUMxQyxLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUFDLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFBQyxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUFDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxpQkFBaUIsQ0FBQztZQUN4SCxLQUFLLENBQUM7Z0JBQ0osS0FBSyxFQUFFLHFDQUFxQyxHQUFHLHlCQUF5QixDQUFDLGNBQWM7Z0JBQ3ZGLE9BQU8sRUFBRSxvQ0FBb0MsR0FBRyx5QkFBeUIsQ0FBQyxhQUFhO2dCQUN2RixZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDLENBQUE7UUFDSixDQUFDLEVBQ0QsVUFBQyxHQUFHO1lBQU8sS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztZQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ2xCLENBQUMsQ0FDRixDQUFBO0lBQ0gsQ0FBQztJQUVNLDZDQUFpQixHQUF4QixVQUF5Qix1QkFBdUI7UUFBaEQsaUJBcUJDO1FBcEJDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLHVCQUF1QixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDM0UsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBQyxLQUFHLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDckQsdUJBQXVCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxnQ0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUFDLENBQUMsQ0FBQztRQUNyRSxDQUFDO1FBQUEsSUFBSSxDQUFBLENBQUM7WUFDSix1QkFBdUIsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxFQUFDLEVBQUUsRUFBRSxnQ0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUFDLENBQUMsQ0FBQztRQUNsRSxDQUFDO1FBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxTQUFTLENBQ2hFLFVBQUMsR0FBRztZQUFNLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQUMsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztZQUFDLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQUMsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFBQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsaUJBQWlCLENBQUM7WUFDOUksS0FBSyxDQUFDO2dCQUNKLEtBQUssRUFBRSxxQ0FBcUMsR0FBRyx1QkFBdUIsQ0FBQyxjQUFjO2dCQUNyRixPQUFPLEVBQUUseUJBQXlCLEdBQUcsdUJBQXVCLENBQUMsV0FBVztnQkFDeEUsWUFBWSxFQUFFLElBQUk7YUFDbkIsQ0FBQyxDQUFBO1FBQ0osQ0FBQyxFQUNELFVBQUMsR0FBRztZQUFNLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDbEIsQ0FBQyxDQUNGLENBQUE7SUFDSCxDQUFDO0lBRU0seUNBQWEsR0FBcEIsVUFBcUIsUUFBUTtRQUMzQixFQUFFLENBQUEsQ0FBQyxRQUFRLENBQUMsT0FBTyxLQUFHLFNBQVMsSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQzVELE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDZixDQUFDO1FBQUEsSUFBSTtZQUNMLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBM0hVLGlCQUFpQjtRQU43QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxjQUFjO1lBQ3hCLFdBQVcsRUFBRSwyQkFBMkI7WUFDeEMsU0FBUyxFQUFFLENBQUMsMkJBQTJCLENBQUM7U0FDekMsQ0FBQzt5Q0FpQnFDLGtDQUFlLEVBQXVCLDBCQUFXLEVBQXVCLDBCQUFXO09BaEI3RyxpQkFBaUIsQ0E4SDdCO0lBQUQsd0JBQUM7Q0FBQSxBQTlIRCxJQThIQztBQTlIWSw4Q0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU2hpcG1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvc2hpcG1lbnQvc2hpcG1lbnQuc2VydmljZSc7XG5pbXBvcnQgeyBEYXRlU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2RhdGUvZGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IFN3aXRjaCB9IGZyb20gXCJ1aS9zd2l0Y2hcIjtcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdXNlci91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgZ2V0U3RyaW5nLCBnZXROdW1iZXIgfSBmcm9tICd0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzL2FwcGxpY2F0aW9uLXNldHRpbmdzJztcblxuXG5AQ29tcG9uZW50KHtcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgc2VsZWN0b3I6ICdhcHAtc2hpcG1lbnQnLFxuICB0ZW1wbGF0ZVVybDogJy4vc2hpcG1lbnQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zaGlwbWVudC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFNoaXBtZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcml2YXRlIG51bWJlcjtcbiAgcHJpdmF0ZSBzaGlwbWVudCA9IFtdO1xuICBwcml2YXRlIGlzVW5sb2FkID0gbnVsbDtcbiAgcHJpdmF0ZSBpc0RlbGl2ZXJkID0gZmFsc2U7XG4gIHByaXZhdGUgaXNQYWNrZWQgPSBudWxsO1xuICBwcml2YXRlIGZpcnN0U3dpdGNoU3RhdGUgPSBcIk5JRSBQT1RXSUVSRFpBTVwiO1xuICBwcml2YXRlIGluZm9BYm91dFNoaXBtZW50O1xuICBwcml2YXRlIGlzQnVzeUNoZWNrO1xuICBwcml2YXRlIGlzQnVzeVNlbmQ7XG4gIHByaXZhdGUgaXNCdXN5U2VuZFVubG9hZGluZztcblxuXG5cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNoaXBtZW50U2VydmljZTogU2hpcG1lbnRTZXJ2aWNlLCBwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSwgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuaXNCdXN5U2VuZFVubG9hZGluZyA9IGZhbHNlO1xuICAgIHRoaXMuaXNCdXN5Q2hlY2sgPSBmYWxzZTtcbiAgICB0aGlzLmlzQnVzeVNlbmQgPSBmYWxzZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRTaGlwbWVudEZvckRyaXZlckJ5TnVtYmVyKG51bWJlcikge1xuICAgIHRoaXMuaXNCdXN5Q2hlY2sgPSB0cnVlO1xuICAgIHRoaXMuc2hpcG1lbnRTZXJ2aWNlLmdldFNoaXBtZW50Rm9yRHJpdmVyQnlOdW1iZXIobnVtYmVyKS5zdWJzY3JpYmUoXG4gICAgICAocmVzKSA9PiB7dGhpcy5pc0J1c3lDaGVjayA9IGZhbHNlO1xuICAgICAgICB0aGlzLnNoaXBtZW50ID0gcmVzLmpzb24oKSwgdGhpcy5zaGlwbWVudFVubG9hZGluZ0RhdGVDaGVja2VyKHRoaXMuc2hpcG1lbnRbMF0pO1xuICAgICAgfSxcbiAgICAgIChlcnIpID0+IHt0aGlzLmlzQnVzeUNoZWNrID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc2hpcG1lbnQgPSBbXSwgdGhpcy5pc1VubG9hZCA9IG51bGw7IHRoaXMuaW5mb0Fib3V0U2hpcG1lbnQgPSBudWxsOyB0aGlzLmlzUGFja2VkID0gbnVsbDtcbiAgICAgICAgYWxlcnQoe1xuICAgICAgICAgIHRpdGxlOiBcIkJyYWsgcHJ6ZXN5xYJraSBvIHBvZGFueW0gbnVtZXJ6ZVwiLFxuICAgICAgICAgIG1lc3NhZ2U6IFwiUG9wcmF3IG51bWVyIGkgc3Byw7NidWogcG9ub3duaWVcIixcbiAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICB9KVxuICAgICAgfSlcbiAgfVxuXG4gIHB1YmxpYyBzaGlwbWVudFVubG9hZGluZ0RhdGVDaGVja2VyKHNoaXBtZW50KSB7XG5cbiAgICBpZiAoc2hpcG1lbnQudW5sb2FkaW5nRGF0ZSAhPT0gbnVsbCAmJiBzaGlwbWVudC5sb2FkaW5nRGF0ZSAhPT0gbnVsbCkge1xuICAgICAgdGhpcy5pc1VubG9hZCA9IHRydWU7XG4gICAgfSBlbHNlIGlmIChzaGlwbWVudC5sb2FkaW5nRGF0ZSAhPSBudWxsICYmIHNoaXBtZW50LnVubG9hZGluZ0RhdGUgPT0gbnVsbCkge1xuICAgICAgdGhpcy5pc1VubG9hZCA9IGZhbHNlO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmluZm9BYm91dFNoaXBtZW50ID0gXCJQcnplc3nFgmthIGN6ZWthIG5hIHphxYJhZHVuZWtcIjtcbiAgICAgIHRoaXMuaXNQYWNrZWQgPSBmYWxzZTtcbiAgICAgIHRoaXMuaXNVbmxvYWQgPSBudWxsO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBvbkZpcnN0Q2hlY2tlZFVubG9hZGluZyhhcmdzKSB7XG4gICAgbGV0IGZpcnN0U3dpdGNoID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcbiAgICBpZiAoZmlyc3RTd2l0Y2guY2hlY2tlZCkge1xuICAgICAgdGhpcy5pc0RlbGl2ZXJkID0gdHJ1ZTtcbiAgICAgIHRoaXMuZmlyc3RTd2l0Y2hTdGF0ZSA9IFwiUE9UV0lFUkRaQU0gZG9yxJljemVuaWUvcm96xYJhZHVuZWsgcHJ6ZXN5xYJraSBkbmlhXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuaXNEZWxpdmVyZCA9IGZhbHNlO1xuICAgICAgdGhpcy5maXJzdFN3aXRjaFN0YXRlID0gXCJOSUUgUE9UV0lFUkRaQU0gZG9yxJljemVuaWUvcm96xYJhZHVua3UgcHJ6ZXN5xYJraSBkbmlhXCI7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIG9uRmlyc3RDaGVja2VkUGFja2luZyhhcmdzKSB7XG4gICAgbGV0IGZpcnN0U3dpdGNoID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcbiAgICBpZiAoZmlyc3RTd2l0Y2guY2hlY2tlZCkge1xuICAgICAgdGhpcy5pc0RlbGl2ZXJkID0gdHJ1ZTtcbiAgICAgIHRoaXMuZmlyc3RTd2l0Y2hTdGF0ZSA9IFwiUE9UV0lFUkRaQU0gemHFgmFkdW5layBwcnplc3nFgmtpIGRuaWFcIjtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5pc0RlbGl2ZXJkID0gZmFsc2U7XG4gICAgICB0aGlzLmZpcnN0U3dpdGNoU3RhdGUgPSBcIk5JRSBQT1RXSUVSRFpBTSB6YcWCYWR1bmt1IHByemVzecWCa2kgZG5pYVwiO1xuICAgIH1cbiAgfVxuXG5cbiAgcHVibGljIHVwZGF0ZVVubG9hZGluZ0RhdGUoc2hpcG1lbnRXaXRoVW5sb2FkaW5nRGF0ZSkge1xuICAgIHRoaXMuaXNCdXN5U2VuZFVubG9hZGluZyA9IHRydWU7XG4gICAgc2hpcG1lbnRXaXRoVW5sb2FkaW5nRGF0ZS51bmxvYWRpbmdEYXRlID0gdGhpcy5kYXRlU2VydmljZS5kYXRlRm9ybWF0Rm9yUG9zdCgpO1xuICAgIGNvbnNvbGUubG9nKHNoaXBtZW50V2l0aFVubG9hZGluZ0RhdGUpO1xuICAgIHRoaXMuc2hpcG1lbnRTZXJ2aWNlLnVwZGF0ZURhdGUoc2hpcG1lbnRXaXRoVW5sb2FkaW5nRGF0ZSkuc3Vic2NyaWJlKFxuICAgICAgKHJlcykgPT4geyB0aGlzLmlzQnVzeVNlbmRVbmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5pc1VubG9hZCA9IG51bGw7IHRoaXMuaW5mb0Fib3V0U2hpcG1lbnQgPSBudWxsOyB0aGlzLmlzRGVsaXZlcmQgPSBmYWxzZTsgdGhpcy5maXJzdFN3aXRjaFN0YXRlID0gXCJOSUUgUE9UV0lFUkRaQU1cIjtcbiAgICAgICAgYWxlcnQoe1xuICAgICAgICAgIHRpdGxlOiBcIlBvdHdpZXJkemVuaWUgZGxhIHBhY3praSBvIG51bWVyemUgXCIgKyBzaGlwbWVudFdpdGhVbmxvYWRpbmdEYXRlLnNoaXBtZW50TnVtYmVyLFxuICAgICAgICAgIG1lc3NhZ2U6IFwiUGFjemthIGRvcsSZY3pvbmEvcm96xYJhZG93YW5hIGRuaWEgXCIgKyBzaGlwbWVudFdpdGhVbmxvYWRpbmdEYXRlLnVubG9hZGluZ0RhdGUsXG4gICAgICAgICAgb2tCdXR0b25UZXh0OiBcIk9LXCJcbiAgICAgICAgfSlcbiAgICAgIH0sXG4gICAgICAoZXJyKSA9PiB7IHRoaXMuaXNCdXN5U2VuZFVubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICBjb25zb2xlLmxvZyhlcnIpXG4gICAgICB9XG4gICAgKVxuICB9XG5cbiAgcHVibGljIHVwZGF0ZVBhY2tpbmdEYXRlKHNoaXBtZW50V2l0aHBhY2tpbmdEYXRlKSB7XG4gICAgdGhpcy5pc0J1c3lTZW5kID0gdHJ1ZTtcbiAgICBzaGlwbWVudFdpdGhwYWNraW5nRGF0ZS5sb2FkaW5nRGF0ZSA9IHRoaXMuZGF0ZVNlcnZpY2UuZGF0ZUZvcm1hdEZvclBvc3QoKTtcbiAgICBpZih0aGlzLmRyaXZlckNoZWNrZXIoc2hpcG1lbnRXaXRocGFja2luZ0RhdGUpPT09dHJ1ZSl7XG4gICAgICBzaGlwbWVudFdpdGhwYWNraW5nRGF0ZS5kcml2ZXJzLnB1c2goeyBpZDogZ2V0TnVtYmVyKFwiZHJpdmVySWRcIil9KTtcbiAgICB9ZWxzZXtcbiAgICAgIHNoaXBtZW50V2l0aHBhY2tpbmdEYXRlLmRyaXZlcnMgPSBbe2lkOiBnZXROdW1iZXIoXCJkcml2ZXJJZFwiKX1dO1xuICAgIH1cbiAgICB0aGlzLnNoaXBtZW50U2VydmljZS51cGRhdGVEYXRlKHNoaXBtZW50V2l0aHBhY2tpbmdEYXRlKS5zdWJzY3JpYmUoXG4gICAgICAocmVzKSA9PiB7dGhpcy5pc0J1c3lTZW5kID0gZmFsc2U7XG4gICAgICAgIHRoaXMuaXNVbmxvYWQgPSBudWxsOyB0aGlzLmluZm9BYm91dFNoaXBtZW50ID0gbnVsbDsgdGhpcy5pc1BhY2tlZCA9IG51bGw7IHRoaXMuaXNEZWxpdmVyZCA9IGZhbHNlOyB0aGlzLmZpcnN0U3dpdGNoU3RhdGUgPSBcIk5JRSBQT1RXSUVSRFpBTVwiO1xuICAgICAgICBhbGVydCh7XG4gICAgICAgICAgdGl0bGU6IFwiUG90d2llcmR6ZW5pZSBkbGEgcGFjemtpIG8gbnVtZXJ6ZSBcIiArIHNoaXBtZW50V2l0aHBhY2tpbmdEYXRlLnNoaXBtZW50TnVtYmVyLFxuICAgICAgICAgIG1lc3NhZ2U6IFwiUGFjemthIHphxYJhZG93YW5hIGRuaWEgXCIgKyBzaGlwbWVudFdpdGhwYWNraW5nRGF0ZS5sb2FkaW5nRGF0ZSxcbiAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICB9KVxuICAgICAgfSxcbiAgICAgIChlcnIpID0+IHt0aGlzLmlzQnVzeVNlbmQgPSBmYWxzZTtcbiAgICAgICAgY29uc29sZS5sb2coZXJyKVxuICAgICAgfVxuICAgIClcbiAgfVxuXG4gIHB1YmxpYyBkcml2ZXJDaGVja2VyKHNoaXBtZW50KXtcbiAgICBpZihzaGlwbWVudC5kcml2ZXJzPT09dW5kZWZpbmVkIHx8IHNoaXBtZW50LmRyaXZlcnMubGVuZ3RoPDEpe1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1lbHNlXG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuXG59XG4iXX0=