import { Component, OnInit } from '@angular/core';
import { ShipmentService } from '../services/shipment/shipment.service';
import { DateService } from '../services/date/date.service';
import { Switch } from "ui/switch";
import { UserService } from '../services/user/user.service';
import { getString, getNumber } from 'tns-core-modules/application-settings/application-settings';


@Component({
  moduleId: module.id,
  selector: 'app-shipment',
  templateUrl: './shipment.component.html',
  styleUrls: ['./shipment.component.scss']
})
export class ShipmentComponent implements OnInit {

  private number;
  private shipment = [];
  private isUnload = null;
  private isDeliverd = false;
  private isPacked = null;
  private firstSwitchState = "NIE POTWIERDZAM";
  private infoAboutShipment;
  private isBusyCheck;
  private isBusySend;
  private isBusySendUnloading;




  constructor(private shipmentService: ShipmentService, private dateService: DateService, private userService: UserService) { }

  ngOnInit() {
    this.isBusySendUnloading = false;
    this.isBusyCheck = false;
    this.isBusySend = false;
  }

  public getShipmentForDriverByNumber(number) {
    this.isBusyCheck = true;
    this.shipmentService.getShipmentForDriverByNumber(number).subscribe(
      (res) => {this.isBusyCheck = false;
        this.shipment = res.json(), this.shipmentUnloadingDateChecker(this.shipment[0]);
      },
      (err) => {this.isBusyCheck = false;
        this.shipment = [], this.isUnload = null; this.infoAboutShipment = null; this.isPacked = null;
        alert({
          title: "Brak przesyłki o podanym numerze",
          message: "Popraw numer i spróbuj ponownie",
          okButtonText: "OK"
        })
      })
  }

  public shipmentUnloadingDateChecker(shipment) {

    if (shipment.unloadingDate !== null && shipment.loadingDate !== null) {
      this.isUnload = true;
    } else if (shipment.loadingDate != null && shipment.unloadingDate == null) {
      this.isUnload = false;
    } else {
      this.infoAboutShipment = "Przesyłka czeka na załadunek";
      this.isPacked = false;
      this.isUnload = null;
    }
  }

  public onFirstCheckedUnloading(args) {
    let firstSwitch = <Switch>args.object;
    if (firstSwitch.checked) {
      this.isDeliverd = true;
      this.firstSwitchState = "POTWIERDZAM doręczenie/rozładunek przesyłki dnia";
    } else {
      this.isDeliverd = false;
      this.firstSwitchState = "NIE POTWIERDZAM doręczenie/rozładunku przesyłki dnia";
    }
  }

  public onFirstCheckedPacking(args) {
    let firstSwitch = <Switch>args.object;
    if (firstSwitch.checked) {
      this.isDeliverd = true;
      this.firstSwitchState = "POTWIERDZAM załadunek przesyłki dnia";
    } else {
      this.isDeliverd = false;
      this.firstSwitchState = "NIE POTWIERDZAM załadunku przesyłki dnia";
    }
  }


  public updateUnloadingDate(shipmentWithUnloadingDate) {
    this.isBusySendUnloading = true;
    shipmentWithUnloadingDate.unloadingDate = this.dateService.dateFormatForPost();
    console.log(shipmentWithUnloadingDate);
    this.shipmentService.updateDate(shipmentWithUnloadingDate).subscribe(
      (res) => { this.isBusySendUnloading = false;
        this.isUnload = null; this.infoAboutShipment = null; this.isDeliverd = false; this.firstSwitchState = "NIE POTWIERDZAM";
        alert({
          title: "Potwierdzenie dla paczki o numerze " + shipmentWithUnloadingDate.shipmentNumber,
          message: "Paczka doręczona/rozładowana dnia " + shipmentWithUnloadingDate.unloadingDate,
          okButtonText: "OK"
        })
      },
      (err) => { this.isBusySendUnloading = false;
        console.log(err)
      }
    )
  }

  public updatePackingDate(shipmentWithpackingDate) {
    this.isBusySend = true;
    shipmentWithpackingDate.loadingDate = this.dateService.dateFormatForPost();
    if(this.driverChecker(shipmentWithpackingDate)===true){
      shipmentWithpackingDate.drivers.push({ id: getNumber("driverId")});
    }else{
      shipmentWithpackingDate.drivers = [{id: getNumber("driverId")}];
    }
    this.shipmentService.updateDate(shipmentWithpackingDate).subscribe(
      (res) => {this.isBusySend = false;
        this.isUnload = null; this.infoAboutShipment = null; this.isPacked = null; this.isDeliverd = false; this.firstSwitchState = "NIE POTWIERDZAM";
        alert({
          title: "Potwierdzenie dla paczki o numerze " + shipmentWithpackingDate.shipmentNumber,
          message: "Paczka załadowana dnia " + shipmentWithpackingDate.loadingDate,
          okButtonText: "OK"
        })
      },
      (err) => {this.isBusySend = false;
        console.log(err)
      }
    )
  }

  public driverChecker(shipment){
    if(shipment.drivers===undefined || shipment.drivers.length<1){
      return false;
    }else
    return true;
  }


}
