"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var date_service_1 = require("../services/date/date.service");
var list_service_1 = require("../services/list/list.service");
var user_service_1 = require("../services/user/user.service");
var json_service_1 = require("../services/json/json.service");
var global_service_1 = require("../services/global/global.service");
var refueling_service_1 = require("../services/refueling/refueling.service");
var router_1 = require("nativescript-angular/router");
var RefuellingAddComponent = (function () {
    function RefuellingAddComponent(page, listService, dateService, userService, jsonService, globalService, refuelingService, routerExtensions) {
        this.page = page;
        this.listService = listService;
        this.dateService = dateService;
        this.userService = userService;
        this.jsonService = jsonService;
        this.globalService = globalService;
        this.refuelingService = refuelingService;
        this.routerExtensions = routerExtensions;
    }
    RefuellingAddComponent.prototype.ngOnInit = function () {
        this.isBusy = false;
        this.jsonService.counter = null;
        this.jsonService.quantity = null;
        this.jsonService.price = null;
        this.jsonService.pickedPaymentType = null;
        this.jsonService.pickedCurrencyId = null;
        this.jsonService.adBluePrice = null;
        this.jsonService.adBlueQuantity = null;
    };
    RefuellingAddComponent.prototype.refuelingAdded = function () {
        var _this = this;
        this.isBusy = true;
        this.refuelingService.refuelingJSONforPOST = {
            refuelingDate: this.dateService.dateFormatForPost(),
            counter: this.jsonService.counter,
            quantity: this.jsonService.quantity,
            price: this.jsonService.price,
            paymentType: this.jsonService.pickedPaymentType,
            driverName: this.userService.driverName,
            car: {
                id: this.jsonService.correctPickedCarId
            },
            currency: {
                id: this.jsonService.pickedCurrencyId
            },
            warningNote: this.jsonService.warningNote,
            adBlueQuantity: this.jsonService.adBlueQuantity,
            adBluePrice: this.jsonService.adBluePrice
        };
        this.refuelingService.addRefueling(this.refuelingService.refuelingJSONforPOST).subscribe(function (res) {
            _this.isBusy = false;
            alert({
                title: "Potwierdzenie",
                message: "Tankowanie dodane poprawnie",
                okButtonText: "OK"
            }), _this.routerExtensions.navigate(["/navigation"]);
        }, function (err) {
            _this.isBusy = false;
            alert({
                title: "Błąd",
                message: "Popraw dane i spróbuj ponownie",
                okButtonText: "OK"
            });
        });
    };
    RefuellingAddComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-refuelling-add',
            templateUrl: './refuelling-add.component.html',
            styleUrls: ['./refuelling-add.component.scss']
        }),
        __metadata("design:paramtypes", [page_1.Page, list_service_1.ListService, date_service_1.DateService, user_service_1.UserService, json_service_1.JsonService, global_service_1.GlobalService, refueling_service_1.RefuelingService, router_1.RouterExtensions])
    ], RefuellingAddComponent);
    return RefuellingAddComponent;
}());
exports.RefuellingAddComponent = RefuellingAddComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmdWVsbGluZy1hZGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVmdWVsbGluZy1hZGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELGdDQUErQjtBQUMvQiw4REFBNEQ7QUFDNUQsOERBQTREO0FBQzVELDhEQUE0RDtBQUM1RCw4REFBNEQ7QUFDNUQsb0VBQWtFO0FBQ2xFLDZFQUEyRTtBQUMzRSxzREFBK0Q7QUFVL0Q7SUFJQyxnQ0FBb0IsSUFBVSxFQUFVLFdBQXdCLEVBQVUsV0FBd0IsRUFBVSxXQUF3QixFQUFVLFdBQXdCLEVBQVUsYUFBNEIsRUFBVSxnQkFBa0MsRUFBVSxnQkFBa0M7UUFBaFIsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVUsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtJQUFJLENBQUM7SUFFelMseUNBQVEsR0FBUjtRQUNPLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNoQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7SUFFOUMsQ0FBQztJQUVNLCtDQUFjLEdBQXJCO1FBQUEsaUJBb0NJO1FBbkNHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsR0FBRztZQUN6QyxhQUFhLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRTtZQUNuRCxPQUFPLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPO1lBQ2pDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVE7WUFDbkMsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSztZQUM3QixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUI7WUFDL0MsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtZQUN2QyxHQUFHLEVBQUU7Z0JBQ0QsRUFBRSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCO2FBQzFDO1lBQ0QsUUFBUSxFQUFFO2dCQUNOLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQjthQUN4QztZQUNELFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVc7WUFDekMsY0FBYyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYztZQUMvQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXO1NBQzVDLENBQUE7UUFFRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLFNBQVMsQ0FDcEYsVUFBQyxHQUFHO1lBQU8sS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDM0IsS0FBSyxDQUFDO2dCQUNGLEtBQUssRUFBRSxlQUFlO2dCQUN0QixPQUFPLEVBQUUsNkJBQTZCO2dCQUN0QyxZQUFZLEVBQUUsSUFBSTthQUNyQixDQUFDLEVBQUUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUE7UUFDdkQsQ0FBQyxFQUNELFVBQUMsR0FBRztZQUFNLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQzFCLEtBQUssQ0FBQztnQkFDRixLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsZ0NBQWdDO2dCQUN6QyxZQUFZLEVBQUUsSUFBSTthQUNyQixDQUFDLENBQUE7UUFDTixDQUFDLENBQ0osQ0FBQztJQUNOLENBQUM7SUF0RFEsc0JBQXNCO1FBTmxDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixXQUFXLEVBQUUsaUNBQWlDO1lBQzlDLFNBQVMsRUFBRSxDQUFDLGlDQUFpQyxDQUFDO1NBQzlDLENBQUM7eUNBS3lCLFdBQUksRUFBdUIsMEJBQVcsRUFBdUIsMEJBQVcsRUFBdUIsMEJBQVcsRUFBdUIsMEJBQVcsRUFBeUIsOEJBQWEsRUFBNEIsb0NBQWdCLEVBQTRCLHlCQUFnQjtPQUp4UixzQkFBc0IsQ0F3RGxDO0lBQUQsNkJBQUM7Q0FBQSxBQXhERCxJQXdEQztBQXhEWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XG5pbXBvcnQgeyBEYXRlU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy9kYXRlL2RhdGUuc2VydmljZVwiO1xuaW1wb3J0IHsgTGlzdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9saXN0L2xpc3Quc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlcy91c2VyL3VzZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgSnNvblNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvanNvbi9qc29uLnNlcnZpY2VcIjtcbmltcG9ydCB7IEdsb2JhbFNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZXMvZ2xvYmFsL2dsb2JhbC5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBSZWZ1ZWxpbmdTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2VzL3JlZnVlbGluZy9yZWZ1ZWxpbmcuc2VydmljZVwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuXG5cbkBDb21wb25lbnQoe1xuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHRzZWxlY3RvcjogJ2FwcC1yZWZ1ZWxsaW5nLWFkZCcsXG5cdHRlbXBsYXRlVXJsOiAnLi9yZWZ1ZWxsaW5nLWFkZC5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL3JlZnVlbGxpbmctYWRkLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUmVmdWVsbGluZ0FkZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBwcml2YXRlIGlzQnVzeTtcblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgbGlzdFNlcnZpY2U6IExpc3RTZXJ2aWNlLCBwcml2YXRlIGRhdGVTZXJ2aWNlOiBEYXRlU2VydmljZSwgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsIHByaXZhdGUganNvblNlcnZpY2U6IEpzb25TZXJ2aWNlLCBwcml2YXRlIGdsb2JhbFNlcnZpY2U6IEdsb2JhbFNlcnZpY2UsIHByaXZhdGUgcmVmdWVsaW5nU2VydmljZTogUmVmdWVsaW5nU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zKSB7IH1cblxuXHRuZ09uSW5pdCgpIHsgXG4gICAgICAgIHRoaXMuaXNCdXN5ID0gZmFsc2U7XG5cdFx0dGhpcy5qc29uU2VydmljZS5jb3VudGVyID0gbnVsbDtcblx0XHR0aGlzLmpzb25TZXJ2aWNlLnF1YW50aXR5ID0gbnVsbDtcblx0XHR0aGlzLmpzb25TZXJ2aWNlLnByaWNlID0gbnVsbDtcblx0XHR0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZFBheW1lbnRUeXBlID0gbnVsbDtcbiAgICAgICAgdGhpcy5qc29uU2VydmljZS5waWNrZWRDdXJyZW5jeUlkID0gbnVsbDtcbiAgICAgICAgdGhpcy5qc29uU2VydmljZS5hZEJsdWVQcmljZSA9IG51bGw7XG4gICAgICAgIHRoaXMuanNvblNlcnZpY2UuYWRCbHVlUXVhbnRpdHkgPSBudWxsO1xuXG5cdH1cblxuXHRwdWJsaWMgcmVmdWVsaW5nQWRkZWQoKSB7XG4gICAgICAgIHRoaXMuaXNCdXN5ID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5yZWZ1ZWxpbmdTZXJ2aWNlLnJlZnVlbGluZ0pTT05mb3JQT1NUID0ge1xuICAgICAgICAgICAgcmVmdWVsaW5nRGF0ZTogdGhpcy5kYXRlU2VydmljZS5kYXRlRm9ybWF0Rm9yUG9zdCgpLFxuICAgICAgICAgICAgY291bnRlcjogdGhpcy5qc29uU2VydmljZS5jb3VudGVyLFxuICAgICAgICAgICAgcXVhbnRpdHk6IHRoaXMuanNvblNlcnZpY2UucXVhbnRpdHksXG4gICAgICAgICAgICBwcmljZTogdGhpcy5qc29uU2VydmljZS5wcmljZSxcbiAgICAgICAgICAgIHBheW1lbnRUeXBlOiB0aGlzLmpzb25TZXJ2aWNlLnBpY2tlZFBheW1lbnRUeXBlLFxuICAgICAgICAgICAgZHJpdmVyTmFtZTogdGhpcy51c2VyU2VydmljZS5kcml2ZXJOYW1lLFxuICAgICAgICAgICAgY2FyOiB7XG4gICAgICAgICAgICAgICAgaWQ6IHRoaXMuanNvblNlcnZpY2UuY29ycmVjdFBpY2tlZENhcklkXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY3VycmVuY3k6IHtcbiAgICAgICAgICAgICAgICBpZDogdGhpcy5qc29uU2VydmljZS5waWNrZWRDdXJyZW5jeUlkXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgd2FybmluZ05vdGU6IHRoaXMuanNvblNlcnZpY2Uud2FybmluZ05vdGUsXG4gICAgICAgICAgICBhZEJsdWVRdWFudGl0eTogdGhpcy5qc29uU2VydmljZS5hZEJsdWVRdWFudGl0eSxcbiAgICAgICAgICAgIGFkQmx1ZVByaWNlOiB0aGlzLmpzb25TZXJ2aWNlLmFkQmx1ZVByaWNlXG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnJlZnVlbGluZ1NlcnZpY2UuYWRkUmVmdWVsaW5nKHRoaXMucmVmdWVsaW5nU2VydmljZS5yZWZ1ZWxpbmdKU09OZm9yUE9TVCkuc3Vic2NyaWJlKFxuICAgICAgICAgICAgKHJlcykgPT4geyB0aGlzLmlzQnVzeSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwiUG90d2llcmR6ZW5pZVwiLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBcIlRhbmtvd2FuaWUgZG9kYW5lIHBvcHJhd25pZVwiLFxuICAgICAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICAgICAgICAgIH0pLCB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL25hdmlnYXRpb25cIl0pXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgKGVycikgPT4ge3RoaXMuaXNCdXN5ID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgYWxlcnQoe1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJCxYLEhWRcIixcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogXCJQb3ByYXcgZGFuZSBpIHNwcsOzYnVqIHBvbm93bmllXCIsXG4gICAgICAgICAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJPS1wiXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0sXG4gICAgICAgICk7XG4gICAgfVxuXG59XG4iXX0=