"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var UrlService = (function () {
    // public apiUrl: string = "http://10.0.2.2:8080";
    function UrlService() {
        // public apiUrl: string = "https://apilogistics.greendev.in/logistics-0.0.1-SNAPSHOT";
        this.apiUrl = "https://api.plewinski-logistics.com/logistics-0.0.1-SNAPSHOT";
    }
    UrlService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], UrlService);
    return UrlService;
}());
exports.UrlService = UrlService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ1cmwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUczQztJQUlFLGtEQUFrRDtJQUVsRDtRQUpBLHVGQUF1RjtRQUNoRixXQUFNLEdBQVcsOERBQThELENBQUM7SUFHdkUsQ0FBQztJQU5OLFVBQVU7UUFEdEIsaUJBQVUsRUFBRTs7T0FDQSxVQUFVLENBUXRCO0lBQUQsaUJBQUM7Q0FBQSxBQVJELElBUUM7QUFSWSxnQ0FBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFVybFNlcnZpY2Uge1xuXG4gIC8vIHB1YmxpYyBhcGlVcmw6IHN0cmluZyA9IFwiaHR0cHM6Ly9hcGlsb2dpc3RpY3MuZ3JlZW5kZXYuaW4vbG9naXN0aWNzLTAuMC4xLVNOQVBTSE9UXCI7XG4gIHB1YmxpYyBhcGlVcmw6IHN0cmluZyA9IFwiaHR0cHM6Ly9hcGkucGxld2luc2tpLWxvZ2lzdGljcy5jb20vbG9naXN0aWNzLTAuMC4xLVNOQVBTSE9UXCI7XG4gIC8vIHB1YmxpYyBhcGlVcmw6IHN0cmluZyA9IFwiaHR0cDovLzEwLjAuMi4yOjgwODBcIjtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG59XG4iXX0=